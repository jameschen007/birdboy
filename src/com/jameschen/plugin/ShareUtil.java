package com.jameschen.plugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.actionbarsherlock.R.bool;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseBindActivity;
import com.jameschen.birdboy.base.BaseShareAcitivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.model.entity.QQ;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.model.entity.Weibo;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.utils.Utility;
import com.umeng.analytics.MobclickAgent;

public class ShareUtil {
	public static final String imgShareLink = "http://img.36tr.com/picture/20140409/5344b3e996f4d_picturebig.jpg";

	public static final String ShareLink = "http://www.wandoujia.com/apps/com.jameschen.birdboy";
	public static String shareBindContent="约运动这个APP还不错，可以约附近的人运动，可以找场馆，看天气预报。喜欢运动的朋友都来试试吧，Android下载:"+ShareLink;

	public static void doShare(
			BaseShareAcitivity shareAcitivity,
			ShareModel shareModel) {

		MobclickAgent.onEvent(shareAcitivity, "share_sns");
		if (LogInController.currentAccount==null) {
			return;
		}
		SNS sns =LogInController.currentAccount.getSnsObj();
		if (sns!=null) {
			QQ qq =sns.getQq();
			if (qq!=null) {
				shareAcitivity.sendQQTShareReq(shareModel, sns.getQq(), shareAcitivity);
					
			}
			
			Weibo weibo =sns.getWeibo();
			if (weibo!=null) {
				Oauth2AccessToken accessToken=new Oauth2AccessToken();
				//accessToken.setExpiresTime(mExpiresTime);
				accessToken.setToken(weibo.getToken());
				accessToken.setUid(weibo.getNick());
				shareAcitivity.sendWeiboShareMsg(shareAcitivity, shareModel, accessToken, weibo.getToken());		
					
			}
		
		}
	
	}
	
	
	 public static String getShareImgFilePath(Context context){
		return  getShareImgFilePath(context, false);
	 }

		static String logo ="mnt/sdcard/birdboy/share_icon.jpg";

		public static String shareTitle="约运动";
	 public static String getShareImgFilePath(Context context,boolean bindary){
		
			File file = new File(logo);
			if (!file.exists()) {
				Bitmap logoBitmap =BitmapFactory.decodeResource(context.getResources(), R.drawable.share_logo);
				int status = copyLogoToSdcard(logoBitmap,file,bindary);
				if (status!=0) {
					return null;
				}
			}

			String filePath =file.getAbsolutePath();
			return filePath;
	    }
	    
	 public static Bitmap getShareImgFileBitmap(Context context){
			return  BitmapFactory.decodeResource(context.getResources(), R.drawable.share_logo);
			
	    }
	    
	 
	    private static int copyLogoToSdcard(Bitmap logoBitmap, File logo,boolean bindary) {
	    		if (logoBitmap==null) {
					return -1;
				}
	    	  FileOutputStream fOut = null;
	    	    try {
	    	     fOut = new FileOutputStream(logo);
	    	    } catch (Exception e) {
	    	     e.printStackTrace();
	    	    }
	    	    logoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
	    	    try {
	    	     fOut.flush();
	    	   
	    	     return 0;
	    	    } catch (IOException e) {
	    	     e.printStackTrace();
	    	    }
	    	    try {
	    	     fOut.close();
	    	    } catch (IOException e) {
	    	     e.printStackTrace();
	    	    }
	    	 
	    	return -1;
		}


    /**
     * 创建文本消息对象。
     * 
     * @return 文本消息对象。
     */
    public static TextObject getTextObj(String shardText) {
        TextObject textObject = new TextObject();
        textObject.text = shardText;
        return textObject;
    }

    /**
     * 创建图片消息对象。
     * 
     * @return 图片消息对象。
     */
    public static ImageObject getImageObj(Bitmap bitmap) {
        ImageObject imageObject = new ImageObject();
        imageObject.setImageObject(bitmap);
        return imageObject;
    }

    /**
     * 创建多媒体（网页）消息对象。
     * 
     * @return 多媒体（网页）消息对象。
     */
    public static WebpageObject getWebpageObj(String title,String desc,String defaultText,String url,Bitmap thumbImg) {
        WebpageObject mediaObject = new WebpageObject();
        mediaObject.identify = Utility.generateGUID();
        mediaObject.title = title;
        mediaObject.description = defaultText;
        
        // 设置 Bitmap 类型的图片到视频对象里
        mediaObject.setThumbImage(thumbImg);
        mediaObject.actionUrl = url;
        mediaObject.defaultText = defaultText;
        return mediaObject;
    }

	
}
