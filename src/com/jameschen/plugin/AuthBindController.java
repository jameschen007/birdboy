package com.jameschen.plugin;

import java.lang.reflect.Type;
import java.util.Map;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.SNS;

public class AuthBindController {

	
	
public interface OnbindResultListener{
	void bindSucc(BindModel bindModel);
	void bindFailed(String reason);
}


//https://open.t.qq.com/cgi-bin/oauth2/access_token?client_id=APP_KEY&grant_type=refresh_token&refresh_token=REFRESH_TOKEN
/**
 * refresh token
 * */
/*client_id	 true	 申请应用时分配的app_key
grant_type	 true	 固定为“refresh_token”
refresh_token	 true	 上次授权或者刷新时获取的refresh_token*/
public static void bindAuth(final BaseActivity activity,final BindModel bindModel,OnbindResultListener resultListener) {
	bindAuth(activity, bindModel, resultListener, false);
}
public static void bindAuth(final BaseActivity activity,final BindModel bindModel,final OnbindResultListener resultListener,boolean backgournd) {
	
	if (!backgournd) {
		String platStr="腾讯微博";
		if (bindModel.fromtype==BindModel.QQ) {
			
		}else {
			platStr="新浪微博";
		}
		activity.showProgressDialog("绑定"+platStr, "绑定中...", null);
	}
	
	UIController bindController = new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			if (resultListener!=null) {
				resultListener.bindFailed("");
			}			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
				if (resultListener!=null) {
					resultListener.bindSucc(bindModel);
				}
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "bindPlugIn"+bindModel.fromtype;
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return  new TypeToken<WebBirdboyJsonResponseContent<SNS> >(){}.getType();
		}
	};
	Map<String, String> map=activity.getPublicParamRequstMap();
//	birdboy.cn/snsAPI  POST请求
//
//	字段               类型                描述                           备注            值条件
//
//	fromtype  int           来自哪个sns           1微博 2QQ       必填
//
//	token        string     
//
//	nick           string     微博，qq账号
	map.put("fromtype", bindModel.fromtype+"");
	map.put("token", bindModel.token);
	map.put("nick", bindModel.nick);
	
	if (bindModel.openid!=null) {
		map.put("openid", bindModel.openid);
	}
	
	if (bindModel.expires_in!=-1) {
		map.put("expires_in", ""+bindModel.expires_in);
	}
	//..
	
	activity.postDataLoader(map, HttpConfig.REQUST_SNS_URL, bindController, false, null, new HttpClientStack(LogInController.getHttpClientInstance()));
	
}	
	
}
