package com.jameschen.plugin.qq;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.util.Log;

import com.sina.weibo.sdk.openapi.models.Geo;
import com.sina.weibo.sdk.openapi.models.Status;
import com.sina.weibo.sdk.openapi.models.User;
import com.sina.weibo.sdk.openapi.models.Visible;

/**
 * "city_code": "",
                "country_code": "",
                "emotiontype": 0,
                "emotionurl": "",
                "from": "腾讯微博",
                "fromurl": "http:\/\/t.qq.com\/\u000a",
                "geo": "",
                "id": "233096025789440",
                "image": [
                    "http:\/\/app.qpic.cn\/mblogpic\/cf9a7c008e0372d10f6a"
                ],
                "latitude": "0",
                "location": "未知",
                "longitude": "0",
                "music": null,
                "origtext": "&quot;I am what I am, 我喜欢我,让蔷薇开出一种结果。&quot; 《我》-by张国荣",
                "province_code": "",
                "self": 1,
                "status": 0,
                "text": "&quot;I am what I am, 我喜欢我,让蔷薇开出一种结果。&quot; 《我》-by张国荣",
                "timestamp": 1396197605,
                "type": 1,
                "video": null
 * */
public class QQStatus {
   
	/**心情url*/
	public String emotionurl;
	public int emotiontype;
	/**微博内容*/
	private String text;
	/**时间戳*/
	private long timestamp;
    /** 微博配图地址。多图时返回多图链接。无配图返回"[]" */
    public List<String> image;
    /** 微博流内的推广微博ID */
    //public Ad ad;
    
    public static Status parse(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return QQStatus.parse(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    public static Status parse(JSONObject jsonObject) {
        if (null == jsonObject) {
            return null;
        }
        
        QQStatus status = new QQStatus();
        status.emotionurl       = jsonObject.optString("emotionurl");
        status.text             = jsonObject.optString("text");
        status.timestamp             = jsonObject.optLong("timestamp", -1);
        
        JSONArray picUrlsArray = jsonObject.optJSONArray("image");
        if (picUrlsArray != null && picUrlsArray.length() > 0) {

            int length = picUrlsArray.length();
            status.image = new ArrayList<String>(length);
            String tmpObject = null;
            for (int ix = 0; ix < length; ix++) {
                tmpObject = picUrlsArray.optString(ix);
                if (tmpObject != null) {
                    status.image.add(tmpObject+"/120");
                }
            }
        }
        
        //status.ad = jsonObject.optString("ad", "");
        Status mStatus = new Status();
        if (status.image!=null&&status.image.size()>0) {
        	 mStatus.thumbnail_pic = status.image.get(0);
		}
        
        mStatus.text =status.text;
        
//        if (status.timestamp>0) {
//        	 SimpleDateFormat dateformat2=new SimpleDateFormat("yy年MM月dd日 HH时mm分");   
//             String a2=dateformat2.format(new Date(status.timestamp));
//			mStatus.created_at=a2;
//		}else {
			 SimpleDateFormat dateformat2=new SimpleDateFormat("yy年MM月dd日 HH时mm分");   
             String a2=dateformat2.format(new Date(System.currentTimeMillis()));
			mStatus.created_at=a2+"前";
//		}
        
        
        
        return mStatus;
    }
}
