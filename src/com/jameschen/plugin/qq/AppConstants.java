package com.jameschen.plugin.qq;

public class AppConstants {
    // APP_ID 替换为你的应用从官方网站申请到的合法appId
    //public static final String APP_ID = "wxd930ea5d5a258f4f";

    public static String APP_ID="1101237826";
    //申请的开发appid
    public static final String WX_APP_ID = "wx8e8dc60535c9cd93";
    
    public static final String WX_ACTION = "action";
    
    public static final String WX_ACTION_INVITE = "invite";
    
    public static final String WX_RESULT_CODE = "ret";
    
    public static final String WX_RESULT_MSG = "msg";
    
    public static final String WX_RESULT = "result";
    
    public static class ShowMsgActivity {
        public static final String STitle = "showmsg_title";
        public static final String SMessage = "showmsg_message";
        public static final String BAThumbData = "showmsg_thumb_data";
        
    }
    
    //新浪：https://api.weibo.com/2/statuses/user_timeline.json?access_token=2.00eiJIFD0YUgDa42d2079e45_W7ZjB&uid=2824224164
    //
    
    //查看个人主页
    /**
     * 
     * https://graph.qq.com/user/get_info?access_token=D951C010B5EA60EB546D728AEA140254&oauth_consumer_key=1101237826&openid=9008DC7B7068DAA41CBE7268371F578E&format=json
     * 
     * */
    
}
