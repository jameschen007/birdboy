
package com.jameschen.plugin.qq;


import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.jameschen.birdboy.model.entity.QQ;
import com.jameschen.plugin.ShareModel;
import com.jameschen.plugin.ShareUtil;
import com.tencent.connect.auth.QQAuth;
import com.tencent.connect.share.QzoneShare;
import com.tencent.t.Weibo;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

/**
 * @author jameschen
 *
 */
public class TencentModel {

    //QZone分享， SHARE_TO_QQ_TYPE_DEFAULT 图文，SHARE_TO_QQ_TYPE_IMAGE 纯图
    private int shareType = QzoneShare.SHARE_TO_QZONE_TYPE_IMAGE_TEXT;
    
    private Tencent tencent;

	private QQAuth mQQAuth;

    public TencentModel(Context context) {
    
    	tencent = Tencent.createInstance(AppConstants.APP_ID, context);
    	mQQAuth = QQAuth.createInstance(AppConstants.APP_ID, context);
    }

    private static TencentModel tencentModel;
    
   public static synchronized TencentModel getTencentInstance(Context context) {
	   synchronized (TencentModel.class) {
		if (tencentModel==null) {
			tencentModel=new TencentModel(context);
		}
		return tencentModel;
	}
	   
}
    
   
   public String getOpenId(){
	   if (mQQAuth==null) {
		return null;
	}
	   return mQQAuth.getQQToken().getOpenId();
   }
   
   public String getToken(){
	   if (mQQAuth==null) {
		return null;
	}
	   return mQQAuth.getQQToken().getAccessToken();
   }
    
	public  boolean ready(Context context) {
		if (mQQAuth == null) {
			return false;
		}
		boolean ready = mQQAuth.isSessionValid()
				&& mQQAuth.getQQToken().getOpenId() != null;
		if (!ready){
			Toast.makeText(context, "login and get openId first, please!",
					Toast.LENGTH_SHORT).show();
		//show go to login first..
		}
			return ready;
	}

    
	public boolean isSessonValid() {
		// TODO Auto-generated method stub
		return mQQAuth.isSessionValid();
	}
	
    public void Login(Activity activity,IUiListener listener){
    	//if (!mQQAuth.isSessionValid()) {
			mQQAuth.login(activity, "all", listener);
			//tencent.loginWithOEM(activity, "all", listener,"10000144","10000144","xxxx");
		//}
    }
    
    public void Logout(Context context){
    	mQQAuth.logout(context);
//		updateUserInfo();
//		updateLoginButton();
    }
    
    
    public void getAccountInfo(){
    	
    }
    
    public void commitQQShare() {

	}
  
  
	 /**
     * @param context
     * @param uiListener
     */
    public void getTQQInfo(Context context,IUiListener uiListener){
    	if (ready(context)) {
    		Weibo mWeibo = new Weibo(context,mQQAuth.getQQToken());
			mWeibo.getWeiboInfo(uiListener);
			
		}
    	
    }
    
	
	/**
     * @param context
     * @param shareModel  less 140 char
     * @param uiListener
     */
    public boolean commitTQQShare(Activity context,ShareModel shareModel,IUiListener uiListener){
    	if (ready(context)) {
    		//图片
    		Weibo mWeibo = new Weibo(context,mQQAuth.getQQToken());
    		String content =shareModel.summary;
    		
			//mWeibo.sendText(content, uiListener);
	
			//Uri uri=Uri.parse("file//"+file.getAbsolutePath());
			
			String filePath = ShareUtil.getShareImgFilePath(context);//SystemUtils.getRealPathFromUri(context, uri);
			
			try {
				if (filePath==null) {
					mWeibo.sendText(content, uiListener);
				}else {
					mWeibo.sendPicText(content, filePath, uiListener);
				}
				
				
			} catch (NullPointerException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		return true;
		}
    	return false;
    }
    
   
	public void commitQZoneShare(QQ qq,Activity activity,IUiListener iUiListener,ShareModel share) {
    	  final Bundle params = new Bundle();
          params.putInt(QzoneShare.SHARE_TO_QZONE_KEY_TYPE, shareType);
          params.putString(QzoneShare.SHARE_TO_QQ_TITLE, share.title);
          params.putString(QzoneShare.SHARE_TO_QQ_SUMMARY, share.summary);
          params.putString(QzoneShare.SHARE_TO_QQ_TARGET_URL, share.url);
          // 支持传多个imageUrl
          ArrayList<String> imageUrls = new ArrayList<String>();
          imageUrls.add(share.imgUrl);
//          String imageUrl = "XXX";
 //        params.putString(QzoneShare.SHARE_TO_QQ_IMAGE_URL, share.imgUrl);
          params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL, imageUrls);
          doShareToQzone(qq,activity,params,iUiListener);
	}

    /**
     * 用异步方式启动分享
     * @param params
     */
    private void doShareToQzone(final QQ qq,final Activity activity,final Bundle params,final IUiListener shareListener) {
    	Tencent mTencent = Tencent.createInstance(AppConstants.APP_ID, activity);
    	
    	if (qq!=null) {
    		mTencent.setAccessToken(qq.getToken(), null);//ex time
        	mTencent.setOpenId(qq.getOpenid());
		}
    	if (!mTencent.isSessionValid()) 
        {
            mTencent.login(activity, "all", new IUiListener() {
     
                @Override
                public void onError(UiError arg0) {

                }
     
            
                @Override
                public void onCancel() {
                }

				@Override
				public void onComplete(Object arg0) {

                	new Thread(new Runnable() {
                        @Override
                        public void run() {
                        	Tencent mTencent = Tencent.createInstance(AppConstants.APP_ID, activity);
                        	//mTencent.setAccessToken(arg0, arg1);
                        	mTencent.shareToQzone(activity, params, shareListener);
                        }
                    }).start();
                					
				}
            });
        }else {
        	new Thread(new Runnable() {
                @Override
                public void run() {
                	Tencent mTencent = Tencent.createInstance(AppConstants.APP_ID, activity);
                	mTencent.shareToQzone(activity, params, shareListener);
                }
            }).start();
		}
    	
    	
    }
    
    Toast mToast = null;
    
}
