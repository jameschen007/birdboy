package com.jameschen.birdboy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.ui.CoachListFragment;

public class CoachActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
		getSupportActionBar().hide();
		initViews();

	}

	private void initViews() {
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment mFragment = fm.findFragmentByTag(ConstValues.COACH);
		if (mFragment==null) {
			mFragment = new CoachListFragment();
		}
			ft.add(R.id.fragment_content, mFragment, ConstValues.COACH);
			ft.commit();
		

	}

}
