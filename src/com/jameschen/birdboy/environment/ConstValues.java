package com.jameschen.birdboy.environment;

import android.R.integer;
import android.content.Intent;

public class ConstValues {
	
	public static final String COACH = "coach";
	public static final String STADIUM = "stadium";
	public static final String FAV_LIST = "fav_friend";
	public static final String BLOG = "blog";
	public static final String WEATHER = "weather";

	public static final String ACCOUNT = "account";
	public static final String SPORT_EVENT = "sport_event";
	public static final String AROUND_USER = "around_user";
	public static final String SPORT_EVENT_DETAIL = "sport_detail";
	public static final String MY_SPORT_LIST = "my_sport_list";
	
	
	
	public static final String SEARCH = "search";
	public static final String COMMENT = "comment";
	public static final String FEED_BACK = "feedback";
	public static final int MAP_VAL = 4;
	public static final int SPORT_EVENT_VAL = 2;
	public static final int MINE_VAL = 3;
	
	public static final int TYPE_USER_VAL=4,TYPE_COACH_VAL=2,TYPE_STADIUM_VAL=1,TYPE_EVENT_VAL=3;
	
	
	public static final int REQUST_CREATE_SPORT = 0X09;
	
	/**
	 * collect value =1. is check collect state, 1 means product has collected.
	 * */
	public static final int COLLECT = 1;
	/**
	 * collect value =0. is check collect state, 0 means product has not
	 * collected.
	 * */
	public static final int UNCOLLECT = 0;
	// for rotate animation
	public static final long ROTATE_TIME = 500;

	public static final String productTag = "product";
	public static final String DISTANCE = "distance";
	public static final String NOTIFICATION = "notification";
	public static final String Tag = "tag";


	public static final class IntentAction {
		public static final String FIND_LOCATION = "com.jameschen.location.FIND";
		public static final String LOCATION_CHANGED = "com.jameschen.location.CHANGED";
		
	}

	/**
	 * shop cart give a whole num for customer add product in cart. by default
	 * the value should be init, every time the app start .and sync from server.
	 * */
	public static class ShopCart {
		public static final String name = "shopcart";
		public static final int REQUST_ORDER_EDIT = 0;
		public static final int RESULT_ORDER_EDIT = 1;
		public static final String RICE = "rice";
		public static final String RICENUM = "ricenumber";
		public static float discount = 1f;
		private static float totalPrice = 0f;

		public static float getTotalPrice() {
			return totalPrice;
		}

		public static void setTotalPrice(float totalPrice) {
			ShopCart.totalPrice = totalPrice;
		}

		/** the product number for shop cart */
		private static int Num = 0;

		public static int getNum() {
			return Num;
		}

		/** set the current product num */
		public static void setNum(int num) {
			Num = num;
		}
	}

	
	public static final class Main{
		public static final String tag="main";
	}
	
	/**
	 * setting .
	 * */
	public static class Setting {

		public static final String SharedName = "set";
		public static final String receiveMsg = "receive_msg";
		public static final String selectFilter = "food_select_filter";
		public static final String selectTag = "select_tag";
		public static final String cityId = "cityId";
		public static final int REQUEST_SETTING = 0;
		public static final int REQUEST_SETTING_LOCATION = 4;
		public static final int RESULT_SETTING_FOOD_FILTER = 2;
		public static final int RESULT_SETTING_LOGOUT = 3;
		public static final int RESULT_SETTING_UPDATE_PASSWORD = 6;
		public static final int RESULT_SETTING_LOCATION = 5;
	}

	/**
	 * shop Info define category or other constvalues , thus values must has the
	 * same vaule with server.
	 * */
	public static class ShopInfoValue {

	}

	/**
	 * category info for define ids for diff category...
	 * */
	public static class CategoryInfo {
		public static String name = "category";

	public static class OrderForm {
		public static final int WAIT_START_STATUS = 30002;
		public static final int END_STATUS = 30004;
		public static final int START_STATUS = 30003;
		public static final int CANCEL_STATUS = 30005;
		public static final long TIME_DOWN = 15 * 60 * 1000;
		public static final String URGE = "urge";
		public static final String ORDERS = "orders";
	}


	/**
	 * register const values
	 * */
	public static class Register {
		/** requst login action */
		public static final int REQUST_REGISTER = 0X07;

		/** verfiycode success */
		public static final int VERIFIYCODE_SUCC = 0X05;
		/** verfiycode failed */
		public static final int VERIFIYCODE_FAIL = 0x09;
		/** register success */
		public static final int REGISTER_SUCC = 0X01;
		/** register failed */
		public static final int REGISTER_FAIL = 0x03;

		/** register error */
		public static final int REGISTER_ERROR = 0x04;
	}

	public static class User {

		public static final int FEED_BACK_SUCC = 0x02;
		public static final int FEED_BACK_FAIL = 0x03;

		public static final int PASSWORD_MODIFY_SUCC = 0x0;
		public static final int PASSWORD_MODIFY_FAIL = 0x01;

		public static final String SharedName = "user";
		public static final String account = "account";
		public static final String area = "area";
		public static final String addr = "addr";
		public static final String location = "location";
		public static final String phone = "phone";
		public static final String user_order_name = "user_order_name";
		public static final String password = "password";
		public static final String userid = "userid";
		public static final String logon = "loginstate";
		public static final String randomcode = "randomcode";
		public static final String tag = "personinfo";
	}

	public static class SoftwareInfo {

		public static int SOFT_VERSION_CODE = -1;

		public static final String SharedName = "software";
		public static final String guide = "guide";
		public static final String version = "version";
		public static final String pruducts_category = "category";//
		public static final String update_detail = "detail";
		public static final String download_url = "url";
		public static final String force_update = "force_update";
		public static final String donnloadid = "download_id";
		public static final String donnload_cache_path = "download_cache";
		public static final int REQUSET_UPDATE = 0x01;
	}

	public static class DeviceInfo {

		public static final String SharedName = "device";
	}

	public static class Intents {

		public static final String ACTION_SHOPCART = "com.jameschen.birdboy.ACTION.SHOPCART";

		public static final String ACTION_ORDER_LIST = "com.jameschen.birdboy.ACTION.ORDERLIST";
		public static final String ACTION_NOTIFICATION_FEEDBACK_SERVICE = "com.jameschen.birdboy.ACTION.NOTIFICATION.FEEDBACK";
		public static final String ACTION_LOCATION_FROM_MAIN_UI = "com.jameschen.birdboy.ACTION.LOCATION.MAIN";

		public static final String NETWORK_CONNECT_ERROR = "com.jameschen.birdboy.ACTION.LOCATION.ERROR";

		public static final String ACTION_CREATE_ENEVT_OK = "com.jameschen.birdboy.ACTION.EVENT.CREATE_OK";
	}

	/**
	 * login const values
	 * */
	public static class Login {

		/** requst login action */
		public static final int REQUST_LOGIN = 0X01;
		/** Login success */
		public static final int LOGIN_SUCC = 0X0;
		/** Login failed */
		public static final int LOGIN_FAIL = 0x03;
		/** Login error */
		public static final int LOGIN_ERROR = 0x04;

		public static final int ACCOUNT_ERROR = -1;

		public static final int PASSWORD_ERROR = -2;

		/** requst password forget action */
		public static final int REQUST_PASSWORD_FORGET = 0X05;

		/** result password reset action */
		public static final int RESULT_PASSWORD_RESET = 0X06;

	}

	public static class Cache {
		public static final String SharedName = "cache";
		public static final String IMAGE_CACHE_DIR = "imageCache";
		public static final String EXTRA_IMAGE = "extra_image";
		public static final String BANNER_IMAGE = "banner_image";
		public static final String path = "/mnt/sdcard/shijing/Cache";

		public static final int LOAD_CACHE = 1;
	}
}
}
