package com.jameschen.birdboy.environment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.utils.FliterDialog;

import android.R.string;

public class Config {
	public static class HttpConfig {
		public static String randomcode = null;
		/** define the http server root url & port */
		public static final String HTTP_BASE_URL = "http://birdboy.cn";

		/** define the coach detail info url */
		public static final String REQUST_COACH_DETAIL_ACTION_URL =HttpConfig.HTTP_BASE_URL+"/coachAPI?id=";
		public static final String REQUST_STADIUM_DETAIL_ACTION_URL =HttpConfig.HTTP_BASE_URL+"/buildingAPI?id=";
		public static final String REQUST_LOGIN_URL =HttpConfig.HTTP_BASE_URL+"/loginAPI";
		public static final String REQUST_REG_GET_SMS_VERIFY_CODE_URL =HttpConfig.HTTP_BASE_URL+"/getCodeAPI";
		public static final String REQUST_CHECK_SMS_VERIFY_CODE_URL =HttpConfig.HTTP_BASE_URL+"/checkCodeAPI";
		
		public static final String REQUST_LOGOUT_URL =HttpConfig.HTTP_BASE_URL+"/floginoutAPI";
		public static final String REQUST_REGISTER_URL =HttpConfig.HTTP_BASE_URL+"/regAPI";
		public static final String REQUST_USER_DETIAL_INFO_URL =HttpConfig.HTTP_BASE_URL+"/tousercenterAPI";
		public static final String REQUST_CHILD_AREA_URL =HttpConfig.HTTP_BASE_URL+"/getChildAreaAPI?areaid=";
		public static final String REQUST_COACH_SEARCH_URL =HttpConfig.HTTP_BASE_URL+"/solr/collection1/select";
		public static final String REQUST_STADIUM_SEARCH_URL =HttpConfig.HTTP_BASE_URL+"/solr/building/select";
		public static final String REQUST_FEEDBACK_URL =HttpConfig.HTTP_BASE_URL+"/addfeedbackAPI";
		public static final String REQUST_INBOX_MESSAGE_URL =HttpConfig.HTTP_BASE_URL+"/chatList";
		public static final String REQUST_INBOX_MESSAGE_DETAIL_URL =HttpConfig.HTTP_BASE_URL+"/chatDetail";
		public static final String REQUST_COACH_PHOTO_URL =HttpConfig.HTTP_BASE_URL+"/coachphotoAPI?id=";
		public static final String REQUST_COMMENT_URL =HttpConfig.HTTP_BASE_URL+"/addcommentAPI";
		public static final String REQUST_ADD_MESSAGE_URL =HttpConfig.HTTP_BASE_URL+"/addChat";
		public static final String REQUST_ADD_FAV_URL =HttpConfig.HTTP_BASE_URL+"/addFriendAPI";
		public static final String REQUST_CANCEL_FAV_URL =HttpConfig.HTTP_BASE_URL+"/cancelFriendAPI";
		public static final String REQUST_ACTION_COUNT_URL =HttpConfig.HTTP_BASE_URL+"/ActionCountAPI";
		public static final String REQUST_ACTION_VERSION_URL=HttpConfig.HTTP_BASE_URL+"/VersionCountAPI?version=";
		public static final String REQUST_BUILDING_PHOTO_URL =HttpConfig.HTTP_BASE_URL+"/fphotoAPI?bid=";
		public static final String REQUST_SPORTEVENT_URL =HttpConfig.HTTP_BASE_URL+"/SearchAAPI";
		public static final String REQUST_UNKEEP_EVENT_URL =HttpConfig.HTTP_BASE_URL+"/unkeepUactionAPI?aid=";
		public static final String REQUST_UNJOIN_EVENT_URL =HttpConfig.HTTP_BASE_URL+"/unJoinUactionAPI?aid=";
		public static final String REQUST_JOIN_EVENT_URL =HttpConfig.HTTP_BASE_URL+"/joinActionV3API?uid=";
		public static final String REQUST_SERACH_AREA_URL =HttpConfig.HTTP_BASE_URL+"/SearchArea";
		public static final String REQUST_COMMENT_LIST_URL =HttpConfig.HTTP_BASE_URL+"/commentListAPI";
		
		public static final String REQUST_INBOX_UNREAD_NUM_URL =HttpConfig.HTTP_BASE_URL+"/chatListNoRead";
		public static final String REQUST_SNS_URL =HttpConfig.HTTP_BASE_URL+"/snsAPI";
		
		public static final String REQUST_AROUND_SPORT_USER_SEARCH_LIST_URL =HttpConfig.HTTP_BASE_URL+"/SearchUAPI";
		public static final String REQUST_FAV_FRIEND_LIST_URL = HTTP_BASE_URL+"/friendsAPI";
		public static final String REQUST_POSITION_URL = HTTP_BASE_URL+"/positionAPI";
		
		public static final String REQUST_SEE_USER_HOME_PAGE_URL = HTTP_BASE_URL+"/userViewAPI";
		
		
		

	
	}

	public static class HttpConnect {
		public static final String HTTP_ERROR = "HTTP_ERROR";
		public static final String HTTP_CONNECT_TIMEOUT = "HTTP_ERROR";
		public static final String HTTP_UNKOWN_HOST_ERROR = "HTTP_ERROR";
		public static final String HTTP_OTHER_ERROR = "HTTP_ERROR";
		public static final int ERROR_FROM_SERVER = 0x05;
		public static final int ERROR_FROM_HTTP = 0x06;
		public static final int ERROR_FROM_UNKOWN = 0x07;

		public static final int SUCC = 0x17;// never use 0 value.
	}

	public static final boolean DEVELOPER_MODE = false;
	
	//NT '场馆子分类 羽毛球1 足球 2 网球 4 羽毛球 8 乒乓球 16 保龄球 64 游泳', 99健身
	public static final class SportType{
		
		private static HashMap<Integer, HashMap<String, Integer>> categorySportMap=new HashMap<Integer, HashMap<String,Integer>>();
		public static final int  TENNIS=4;
		public static final int  FOOTBALL=2;
		public static final int  SWIMMING=64;
		public static final int  BADMINTON=1;
		public static final int  TABLETENIS=8;
		public static final int  FITNESS=99;
		public static final int  BASKET_BALL=32;
		private static HashMap<String, Integer> createHashTable(String key,Integer val) {
			
			HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
			hashMap.put(key, val);
			return hashMap;
		}
		
	static{
			categorySportMap.put(BASKET_BALL, createHashTable("篮球", R.drawable.category_basketball));
			categorySportMap.put(SWIMMING, createHashTable("游泳", R.drawable.category_swimming));
			categorySportMap.put(FOOTBALL, createHashTable("足球", R.drawable.category_football));
			categorySportMap.put(TENNIS, createHashTable("网球", R.drawable.category_tennis));
			categorySportMap.put(BADMINTON, createHashTable("羽毛球", R.drawable.category_badminton));
			categorySportMap.put(TABLETENIS, createHashTable("乒乓球", R.drawable.category_tabletennis));
			//categorySportMap.put(16, "保龄球");
			categorySportMap.put(SWIMMING, createHashTable("游泳", R.drawable.category_swimming));
			categorySportMap.put(FITNESS, createHashTable("健身", R.drawable.fit_ness));
		}
	private static List<Category> sports,sorts;
	
	public static Category getSportCatogryById(int sportType) {
		List<Category> sportList =FliterDialog.getLikedSportCategories();
		for (Category category : sportList) {
			if (category.getType() ==sportType ) {
				return category;
			}
		}
		
		return sportList.get(0);
	}
	
	public static List<Category> initSportList() {
		if (sports!=null) {
			return sports;
		}
		sports = new ArrayList<Category>();

		Category categorySport =new Category();
		categorySport.setIconResource(R.drawable.menu_pressed);
		//createHashTable("所有运动", R.drawable.menu_pressed)
		categorySport.setType(-1);
		categorySport.setName("所有运动");
		sports.add(categorySport); 
		for(Iterator it=categorySportMap.entrySet().iterator();it.hasNext();){
			   Entry entry= (Entry)it.next();
			   HashMap<String, Integer> item = (HashMap<String, Integer>)entry.getValue(); //  StatItem 是我自己定义的一个简单的JAVABEAN 对象类型
			     categorySport = new Category();
			    Entry<String, Integer> content = item.entrySet().iterator().next();
			   categorySport.setName(content.getKey());
			   categorySport.setIconResource(content.getValue());
			   categorySport.setType((Integer)entry.getKey());
			   sports.add(categorySport);
			  }  

		return sports;
	}




	}
	
}
