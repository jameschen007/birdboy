package com.jameschen.birdboy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.RegisterBaseActivity;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Register;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.CheckValid;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.FliterDialog;
import com.jameschen.birdboy.utils.FliterDialog.onFilerSelectedListener;
import com.jameschen.birdboy.utils.MD5Encode;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.utils.UtilsUI;



public class Register2Activity extends RegisterBaseActivity {

	protected static final int UPLOAD_LOGO = 0x18;
	public static CharSequence accountStr;
	public static CharSequence passwordStr;
	private EditText password;
	private EditText confirmPassword;
	private EditText nickname;
	private EditText sign;
	private TextView sex;
	private EditText age;
	private TextView mylike;
	private String logoUrl;
	private ImageView uploadView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register2);
		Config.SportType.initSportList();//init r
		Category item0= new Category();
		item0.setName("女");
		item0.setType(2);
		categories.add(item0);
		Category item1= new Category();
		item1.setName("男");
		item1.setType(1);
		categories.add(item1);
			initViews();	
			// default is female
			sex.setTag(item0);
			setTitle("完善资料 (3/4)");
			Intent intent = getIntent();
			final String mobile = intent.getStringExtra("mobile");
			final String code = intent.getStringExtra("code");
			setTopBarLeftBtnVisiable(View.GONE);
			setTopBarRightBtnListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					Category category =(Category) sex.getTag();
				register(mobile, nickname.getText(), password.getText(), confirmPassword.getText(),code,category.getType());
				}
			});
			initDataLoaderFromNativeCache(null, LoadMode.NONE, uiFreshController);		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode==KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
	private void initViews() {
		// TODO Auto-generated method stub
		nickname = (EditText) findViewById(R.id.edtNickname);
		uploadView = (ImageView) findViewById(R.id.upload_logo);
		password = (EditText) findViewById(R.id.edtPasswrod);
		sign =(EditText) findViewById(R.id.sign);
		confirmPassword = (EditText) findViewById(R.id.edtconfirmPasswrod);
		sex = (TextView) findViewById(R.id.edtSex);
		age = (EditText) findViewById(R.id.age);
		mylike =(TextView) findViewById(R.id.mylike);
		mylike.setText("跑步");
		mylikeId=65;
		
		uploadView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				startActivityForResult(new Intent(Register2Activity.this,RegisterUploadPhotoActivity.class), UPLOAD_LOGO);
			}
		});
		
		mylike.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FliterDialog fliterDialog = new FliterDialog(false, false);
				fliterDialog.showFilterDialog(Register2Activity.this, new onFilerSelectedListener() {
					
					@Override
					public void onItemSelect(Category sex, Category sport) {
						if (sport!=null) {
							mylikeId=sport.getType();
							mylike.setText(sport.getName());
						}
					}
				},mylikeId);
			}
		});
		sex.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				showSex(categories);
				
			}
		});
		
		
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case UPLOAD_LOGO:
		{
			if (resultCode==RESULT_OK&&data!=null) {
				logoUrl = data.getStringExtra("url");
				String logo = data.getStringExtra("img_bitmap");
				if (logo!=null) {
					uploadView.setImageBitmap(BitmapFactory.decodeFile(logo));
				}
				
			}
		}
			break;

		default:
			break;
		}
	}
	
	
	private boolean checkAge(CharSequence s) {
		if (!TextUtils.isEmpty(s)) {
			try {
				int ageNum =Integer.parseInt(s.toString());
				if (ageNum<0||ageNum>110) {
					showToast("输入年龄非法");
					age.setText(18+"");
				}else {
					return true;
				}
			} catch (NumberFormatException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		
		}
		return false;
	}
	
	List<Category> categories = new ArrayList<Category>();
	private int mylikeId=-1;
	private void showSex(final List<Category> category) {

		
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		int width =UtilsUI.getWidth(getApplication())-Util.getPixByDPI(getBaseContext(), 20);
		customSelectPopupWindow.showActionWindow((View) (sex.getParent()),this, category);
		customSelectPopupWindow.getPopupWindow().update(width, LayoutParams.WRAP_CONTENT);
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			
			@Override
			public void onItemClick(int index) {
				Category mItem = category.get(index);
				sex.setText(mItem.getName());
				sex.setTag(mItem);
			}
		});
	//	sex.setSelected(true);
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				sex.setSelected(false);
			}
		});
	
		
	}
	
	
	private void register(CharSequence phone,CharSequence nickname, CharSequence password,CharSequence confirmPassword,
			CharSequence verifyCode,int sex) {
		if (TextUtils.isEmpty(logoUrl)) {
			showToast("请先上传头像");
			return;
		}
		
		boolean nextCheckStep =true;
		if (nextCheckStep) {
			nextCheckStep =  CheckValid.checkNickNameValid(this,nickname, 2, 6);
		}
		
		if (nextCheckStep) {
			nextCheckStep =  CheckValid.passwordCheckValid(this,password, 6, 15);
		}
		

		if (nextCheckStep) {
			if (TextUtils.isEmpty(confirmPassword)) {
				nextCheckStep=false;
				showToast("请再次输入密码!");
				return ;
			}
			if (!password.toString().equals(confirmPassword.toString())) {
				nextCheckStep=false;
				showToast("两次密码输入不一致!");
			}else {
				nextCheckStep=true;
			}
		}
			
		if (nextCheckStep) {
			if (TextUtils.isEmpty(age.getText())) {
				showToast("请输入年龄");
				nextCheckStep=false;
				return;
			}
			nextCheckStep = checkAge(age.getText());
			
		}
		
		if (nextCheckStep) {
				if (mylikeId==-1) {
					showToast("请选择体育爱好");
					nextCheckStep=false;
					return;
				}
		}
		
		if (nextCheckStep) {
			closeInputMethod();			
			showProgressDialog("注册", "注册中，请稍候", null);
			Map<String, String> map = getPublicParamRequstMap();
			map.put("nickname", nickname.toString());
			map.put("mobile", phone.toString());
			map.put("sex", ""+sex);
			map.put("age", ""+age.getText().toString());
			map.put("mylike", ""+mylikeId);
			map.put("password", MD5Encode.encode(password.toString()));
			map.put("gcode", verifyCode.toString());
			map.put("logo", logoUrl);
			
			if (!TextUtils.isEmpty(sign.getText())) {
				map.put("sign", sign.getText().toString());
			}
			postDataLoader(map,HttpConfig.REQUST_REGISTER_URL, uiFreshController,null);
			

		}
	}
	

	private UIController uiFreshController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode,Page page) {
			// TODO Auto-generated method stub
		}
		
		@Override
		public <E> void refreshUI(E content,Page page) {
			// TODO Auto-generated method stub
			Register0Activity.saveUserToPreference(Register2Activity.this, nickname.getText().toString(), password.getText().toString(), false);
			Account account = (Account) content;
			Intent intent = new Intent(Register2Activity.this,RegisterFinishActivity.class);
			sendBroadcast(new Intent(LoginFragment.ACCOUNT_CHANGED));
			accountStr=nickname.getText();
		    passwordStr =password.getText();
			if (account!=null) {
				intent.putExtra("id", account.getId());
			}
			
			startActivityForResult(intent, Register.REQUST_REGISTER);
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			return  new  TypeToken<WebBirdboyJsonResponseContent<Account>>(){}.getType();
		}
	};
	

}
