package com.jameschen.birdboy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;

import org.apache.http.Header;
import org.json.JSONObject;

import android.R.integer;
import android.accounts.Account;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import com.jameschen.birdboy.utils.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.internal.Utils;
import com.jameschen.birdboy.MessageReceiveActivity.MyClickAbleSpan;
import com.jameschen.birdboy.adapter.ChildAreaAdapter;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.CoachDetailAdapter;
import com.jameschen.birdboy.adapter.FeedbackExpandableAdapter;
import com.jameschen.birdboy.adapter.SportEventCreateExpandableAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.BaseShareAcitivity;
import com.jameschen.birdboy.base.BaseUploadPhotoActivity;
import com.jameschen.birdboy.base.BaseActivity.LoadMode;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Intents;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebCoachDetail;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.FavoriteFriend;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.model.entity.QQ;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.model.entity.SportUser;
import com.jameschen.birdboy.model.entity.Weibo;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.utils.AreaSelectPopupWindow;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.ImageTools;
import com.jameschen.birdboy.utils.MediaFile;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;
import com.jameschen.plugin.ShareModel;
import com.jameschen.plugin.ShareUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.umeng.analytics.MobclickAgent;


public class SportEventCreate2Activity extends BaseUploadPhotoActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportevent_create2);
		setTopBarRightBtnVisiable(View.GONE);
		 setTopBarLeftBtnVisiable(View.GONE);
		setTitle("");
		id =getIntent().getIntExtra("id", -1);
		Log.i(TAG, "id=="+id);
		crop=false;
		initviews();
		resultOk = true;
		
		title = getIntent().getStringExtra("title");
		
		String name ="";
		
		if (LogInController.currentAccount!=null) {//low memory
			name =LogInController.currentAccount.getNick();
			
		}else {
			finish();
			return;
			
		}
		final ShareModel shareModel=new ShareModel();
		shareModel.summary="我在“约运动”发起了“"+title+"”的活动，快来一起参与吧。“约运动”Android下载："+ShareUtil.ShareLink;
		shareModel.title="约运动";
		shareModel.imgUrl=ShareUtil.imgShareLink;
		shareModel.url=ShareUtil.ShareLink;
		
		if (LogInController.currentAccount.hasQq()) {
			btnQzoneShare.setVisibility(View.VISIBLE);
			btnQzoneShare.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					shareModel.summary="我在“约运动”发起了“"+title+"”的活动，快来一起参与吧。“约运动”Android下载";
					MobclickAgent.onEvent(SportEventCreate2Activity.this, "share_sns");
					sendQzoneShareReq(shareModel, LogInController.currentAccount.getSnsObj().getQq(), SportEventCreate2Activity.this);				
				}
			});
		}else {
			btnQzoneShare.setVisibility(View.GONE);
		}
		
		List<SportUser>sportUsers=new ArrayList<SportUser>();
		Bundle data = getIntent().getBundleExtra("data");
		if (data!=null) {
			if (data.getInt("flag")==1) {//single
				FavoriteFriend friendInfo=data.getParcelable("invite_one");
				if (friendInfo!=null) {
					SportUser sportUser = new SportUser();
					sportUser.setId(friendInfo.getUserid());
					sportUser.setName(friendInfo.getUsername());
					sportUsers.add(sportUser);
				}
				
			}else if (data.getInt("flag")==2) {
			List<InvitedUser>memeberInvitedUsers = data.getParcelableArrayList("invite_members");
				for (InvitedUser invitedUser : memeberInvitedUsers) {
					SportUser sportUser = new SportUser();
					sportUser.setId(invitedUser.getId());
					sportUser.setName(invitedUser.getNick());
					sportUsers.add(sportUser);
				}
			}
			}
			
		setInviteInfos(sportUsers);
		
		ShareUtil.doShare(SportEventCreate2Activity.this,shareModel);
	}
	private void setInviteInfos(final List<SportUser> sportUsers) {
		inviteInfo.setVisibility(View.GONE);
		if (sportUsers.size()==0) {
			return;
		}
		inviteInfo.setMovementMethod(LinkMovementMethod.getInstance());
		
		inviteInfo.setText("成功给");
		int len =sportUsers.size();
		for (int i = 0; i < len; i++) {
			final int position =i;
			String nameStr=sportUsers.get(position).getName();
			//event
			if (len>1&&i<len-1) {//center add 、
				nameStr+="、";
			}
			
			SpannableStringBuilder nameInfoBuilder =new SpannableStringBuilder(nameStr);
			nameInfoBuilder.setSpan(new UnderlineSpan(), 0, nameInfoBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			nameInfoBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue)), 
					0, nameInfoBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			nameInfoBuilder.setSpan(new MyClickAbleSpan(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Util.go2SeeUserHomePage(SportEventCreate2Activity.this, sportUsers.get(position).getId());		
				}
			}), 0, nameInfoBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
			inviteInfo.append(nameInfoBuilder);
		
		}
		inviteInfo.append("发送了活动邀请，一旦他们报名，会消息通知你");
		inviteInfo.setVisibility(View.VISIBLE);
	}
	
	public class MyClickAbleSpan extends ClickableSpan implements OnClickListener{
		private OnClickListener mClickListener;
		public MyClickAbleSpan(OnClickListener clickListener){
			mClickListener =clickListener;
		}
		@Override
		public void onClick(View widget) {
			// TODO Auto-generated method stub
			if (mClickListener!=null) {
				mClickListener.onClick(widget);
			}
		}
	}
	
	String title;
	boolean isStart=false;
	private Button upload,btnQzoneShare;
	TextView photoText;
	protected int id;

	
	@Override
	public void onBackPressed() {
		Intent intent=new Intent(SportEventCreate2Activity.this,MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (Util.hasHoneycomb()) {
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		}
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.putExtra("tag", ConstValues.SPORT_EVENT);
		startActivity(intent);
		setResult(Activity.RESULT_OK);
		super.onBackPressed();
	
	}
	private boolean isSucc=false;
	private TextView inviteInfo;
	// float money;
	private void initviews() {

		upload = (Button) findViewById(R.id.btn_upload);
		btnQzoneShare = (Button) findViewById(R.id.btn_qzone_share);
		photo = (ImageView) findViewById(R.id.image_choose);
		photoText = (TextView) findViewById(R.id.photo_text);
		inviteInfo = (TextView) findViewById(R.id.invite_info);
		
		upload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				if (isStart) {
//					showToast("正在上传，请稍候...");
//					return;
//				}
				if (photo.getTag()==null) {
					//showToast("请选择一张照片");
					sendCreateOKIntent();
					return;
				}
				File file  = new File((String)photo.getTag());
				if (!file.exists()) {//file deleted?
					showToast("图片不存在");
					return;
				}
				
				
				 RequestParams params = new RequestParams();  
				 
				 params.put("method", "action");
				 params.put("uid", id+"");
				 params.put("ssize", 144);
				 params.put("size", 500);
				 try {
					params.put("image_file", file);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			// Upload a File  
				 AsyncHttpClient client =new AsyncHttpClient();
				 
				 if (LogInController.getHttpClientInstance()!=null) {
						client.setCookieStore(LogInController.getHttpClientInstance().getCookieStore());
				}
			
				// Map<String, File> fileMap = new HashMap()<String, File>();
//				 fileMap.put("image_file", file);
//				 HttpUtil.get("http://birdboy.cn/upload", params, fileMap);
				 
				client.post("http://birdboy.cn/upload", params, new AsyncHttpResponseHandler(){
					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0, arg1, arg2);
						isSucc  = false;
						isStart=false;
						showToast("上传图片成功");
						debugHeaders(TAG, arg1);
						//setResult(RESULT_OK);
						sendCreateOKIntent();
						//finish();
					}
					 @Override
					public void onProgress(int bytesWritten, int totalSize) {
						// TODO Auto-generated method stub
						super.onProgress(bytesWritten, totalSize);
						int percent = bytesWritten*100/totalSize;
						//photoText.setText("已上传"+percent+"%");
						if (percent==100) {//ok.
							isSucc=true;
						}
						showProgressDialog("上传图片", "已上传"+percent+"%", null);
					}
					 @Override
					public void onStart() {
						super.onStart();
						isStart=true;
						showProgressDialog("上传图片", "已上传"+0+"%", new OnCancelListener() {
							
							@Override
							public void onCancel(DialogInterface arg0) {
								// TODO Auto-generated method stub
								isStart =false;
							}
						});
						//photoText.setText("开始上传。。。");
					}
					
					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						super.onFailure(arg0, arg1, arg2, arg3);
						if (isSucc) {
							isSucc=false;
							showToast("上传图片成功");
							debugHeaders(TAG, arg1);
							sendCreateOKIntent();
						}else {
							showToast("上传图片失败!");
						}
						
						
					}
					
				
					 @Override
					public void onFinish() {
						super.onFinish();

						cancelProgressDialog();
						isStart=false;
						//isSucc = false;
						if (isSucc) {
							showToast("上传图片成功");
							sendCreateOKIntent();
							isSucc=false;
						}
					}
				 
				});  
				 
				 
				 
			}
		});

		photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			showAlert();
			}
		});
		
	}

	private void sendCreateOKIntent() {
		
		onBackPressed();
		
//		Intent intent = new Intent(Intents.ACTION_CREATE_ENEVT_OK);
//		sendBroadcast(intent );
	}
	

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}

}
