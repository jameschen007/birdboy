package com.jameschen.birdboy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.utils.ImageTools;
import com.jameschen.birdboy.widget.image.GestureImageView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import com.jameschen.birdboy.utils.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.LinearLayout.LayoutParams;

public class ScanImageActivity extends BaseActivity {

	public static final String IMAGE_SCAN_RESIZE = "com.birdboy.image.scan.resize";
	private GestureImageView view;
	private Bitmap imgBitmap;
	public static String imageCachePath="mnt/sdcard/birdboy/eventPhoto/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_scan);
		setTitle("图片预览");
		view = (GestureImageView) findViewById(R.id.image);
		File file = new File(imageCachePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		final String imageFile = getIntent().getStringExtra("filePath");
		Log.i("ScanImageActivity", "image==" + imageFile);
		view.setImageBitmap(imgBitmap = getBitmap(this, imageFile, false));
		String action = getIntent().getAction();
		if (action != null && action.equals(IMAGE_SCAN_RESIZE)) {
			final String name = getIntent().getStringExtra("fileName");
			setTopBarRightBtnListener("确定", new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					String path = imageCachePath + name;
					Log.i("ScanImageActivity", "store path==" + path);
					File file = new File(imageFile);
					if (file.exists()) {
						if (file.length() <= maxImgFileLen) {
							// no need compress.
							// no need compress.
							Bitmap bmp = BitmapFactory.decodeFile(imageFile);
							if (bmp==null) {
								return;
							}
							transImage(imageFile, path, bmp.getWidth(),
									bmp.getHeight(), 100,
									ScanImageActivity.this, bmp, false);
							Intent data = new Intent();
							data.putExtra("imagePath", path);
							setResult(RESULT_OK, data);
							finish();
							return;
						}
					}
					boolean imagetransStatus = transImage(imageFile, path,
							1024, 720, 60, ScanImageActivity.this, imgBitmap,
							false);
					if (imagetransStatus) {
						Intent data = new Intent();
						data.putExtra("imagePath", path);
						setResult(RESULT_OK, data);
						finish();
					} else {
						Toast.makeText(ScanImageActivity.this,"出现错误",
								Toast.LENGTH_SHORT).show();
					}

									
				}
			});
			// show send button
		} else {
		}
	}

	private static Bitmap getBitmap(Activity context, String imageFile,
			boolean calcuRotation) {
		// in case of vm out of boundry.
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(imageFile, opts);
		// set pic h & w
		WindowManager m = context.getWindowManager();
		Display d = m.getDefaultDisplay();
		// LayoutParams p = getWindow().getAttributes();
		DisplayMetrics dm = new DisplayMetrics();
		d.getMetrics(dm);
		int height = dm.heightPixels;
		int width = dm.widthPixels;
		height = (int) (height * 0.8);
		width = (int) (width * 0.95);
		// opts.inSampleSize = ImageTools.computeSampleSize(opts, -1, 512 *
		// 512);
		opts.inSampleSize = ImageTools.calculateInSampleSize(opts, width,
				height);
		opts.inJustDecodeBounds = false;

		try {
			Bitmap bmp = BitmapFactory.decodeFile(imageFile, opts);
			// bmp = imsg.getImage();
			if (true) {
				/**
				 * 获取图片的旋转角度，有些系统把拍照的图片旋转了，有的没有旋转
				 */
				int degree = readPictureDegree(imageFile);
				/**
				 * 把图片旋转为正的方向
				 * 
				 */
				Bitmap newbitmap = rotaingImageView(degree, bmp);
				return newbitmap;
			}
			// return bmp;
		} catch (OutOfMemoryError err) {
			err.printStackTrace();
		}
		// String title = "Picture view";
		// // show primitive image
		// AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		// builder.setTitle(title);
		// builder.setView(viewImage);
		//
		// Dialog dialog = builder.create();
		// dialog.getWindow().setLayout(opts.outWidth, opts.outHeight);
		// dialog.show();
		return bitmap;
	}

	/**
	 * 读取图片属性：旋转的角度
	 * 
	 * @param path
	 *            图片绝对路径
	 * @return degree旋转的角度
	 */
	public static int readPictureDegree(String path) {
		int degree = 0;
		try {
			ExifInterface exifInterface = new ExifInterface(path);

			int orientation = exifInterface.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return degree;
	}

	/*
	 * 旋转图片
	 * 
	 * @param angle
	 * 
	 * @param bitmap
	 * 
	 * @return Bitmap
	 */
	public static Bitmap rotaingImageView(int angle, Bitmap bitmap) {

		System.out.println("angle2=" + angle);
		if (angle == 0) {
			return bitmap;
		}
		// 旋转图片 动作
		Matrix matrix = new Matrix();
		;
		matrix.postRotate(angle);
		// 创建新的图片
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
				bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		return resizedBitmap;
	}

	static long maxImgFileLen = 200 * 1024;

	/**
	 * @param take
	 *            picPath
	 */
	public static void compressImage(Activity activity, String picPath) {
		// TODO Auto-generated method stub
		File file = new File(picPath);
		if (file.exists()) {

			if (file.length() <= maxImgFileLen) {
				// no need compress.
				Bitmap bmp = BitmapFactory.decodeFile(picPath);
				transImage(picPath, picPath, bmp.getWidth(), bmp.getHeight(),
						100, activity, bmp, false, true);
				return;
			}

			transImage(picPath, picPath, 500, 500, 60, activity, null, true,
					true);
		}
	}

	
	public static void compressImage(Activity activity, String picPath,int size) {
		// TODO Auto-generated method stub
		File file = new File(picPath);
		if (file.exists()) {

			if (file.length() <= maxImgFileLen) {
				// no need compress.
				Bitmap bmp = BitmapFactory.decodeFile(picPath);
				transImage(picPath, picPath, bmp.getWidth(), bmp.getHeight(),
						100, activity, bmp, false, true);
				return;
			}

			transImage(picPath, picPath, 1024, 720, 60, activity, null, true,
					true);
		}
	}
	
	public static boolean transImage(String fromFile, String toFile, int width,
			int height, int quality, Activity context, Bitmap imgBitmap,
			boolean getBitmap, boolean reCalcuRotation) {
		try {
			Bitmap bitmap;
			if (getBitmap) {
				bitmap = getBitmap(context, fromFile, reCalcuRotation);
			} else {
				bitmap = imgBitmap;
			}
			if (bitmap == null) {// may cause out of memory issue
				if (imgBitmap == null) {
					return false;
				}
				bitmap = imgBitmap;
			}

			// int bitmapWidth = bitmap.getWidth();
			// int bitmapHeight = bitmap.getHeight();
			//
			// // 缩放图片的尺寸
			// float scaleWidth = (float) width / bitmapWidth;
			// float scaleHeight = (float) height / bitmapHeight;
			// Matrix matrix = new Matrix();
			// matrix.postScale(scaleWidth, scaleHeight);
			// // 产生缩放后的Bitmap对象
			// Bitmap resizeBitmap = Bitmap.createBitmap(bitmap, 0, 0,
			// bitmapWidth, bitmapHeight, matrix, false);
			Bitmap resizeBitmap = bitmap;
			// save file
			File myCaptureFile = new File(toFile);
			if (myCaptureFile.exists()) {
				myCaptureFile.delete();
			}
			myCaptureFile.createNewFile();
			Log.i("iamge file", toFile);

			FileOutputStream out = new FileOutputStream(myCaptureFile);
			if (resizeBitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)) {
				out.flush();
				out.close();
			}
			if (!bitmap.isRecycled()) {
				bitmap.recycle();// 记得释放资源，否则会内存溢出
			}
			if (!resizeBitmap.isRecycled()) {
				resizeBitmap.recycle();
			}
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static boolean transImage(String fromFile, String toFile, int width,
			int height, int quality, Activity context, Bitmap imgBitmap,
			boolean getBitmap) {
		return transImage(fromFile, toFile, width, height, quality, context,
				imgBitmap, getBitmap, false);
	}

	// 压缩保存图片
	@Override
	protected void onDestroy() {
		// if (imgBitmap!=null) {
		// if (!imgBitmap.isRecycled()) {
		// imgBitmap.recycle();
		// imgBitmap=null;
		// }
		// }
		super.onDestroy();
	}
}
