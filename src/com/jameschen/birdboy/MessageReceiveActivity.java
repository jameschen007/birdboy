package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.WebInbox;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.Message;
import com.jameschen.birdboy.model.entity.MessageJsonTag;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;
//
//http://birdboy.cn/addChat  Post请求
//
//字段               类型                描述                           备注            值条件
//

//
// 3、返回字段
//
//{
//    "success": true, 
//    "result": {
//    },
//
//    "info":""
//
//} 



public class MessageReceiveActivity extends BaseActivity{
	TextView contentText;
	Message message;
	private int inboxId=0;
	private TextView messgeRevName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_reveive_content_ui);
		setTitle("信件内容");
		Intent intent = getIntent();
		message = intent.getParcelableExtra("message");
		
		initViews();
		boolean notRead=false;
		if (message!=null) {
			notRead =message.getStatus()==0?true:false;
			inboxId =message.getId();
		}
		Map<String, String> paramMap = getPublicParamRequstMap();
		paramMap.put("id", inboxId+"");
		if (notRead) {
			paramMap.put("notread", notRead+"");
		}
		
		getDataLoader(HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_INBOX_MESSAGE_DETAIL_URL, paramMap), false, new Page(), msgDetailController,new HttpClientStack(LogInController.getHttpClientInstance()));
		showProgressDialog("获取内容", "请稍候...", null);
	}
	
	private UIController msgDetailController = new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			Message detailMsg = (Message) content;
			setMsgInfo(detailMsg);
		}
		
		@Override
		public String getTag() {
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			return new TypeToken<WebBirdboyJsonResponseContent<Message>>() {
			}.getType();
		}
	};
	
	public class MyClickAbleSpan extends ClickableSpan implements OnClickListener{
		private OnClickListener mClickListener;
		public MyClickAbleSpan(OnClickListener clickListener){
			mClickListener =clickListener;
		}
		@Override
		public void onClick(View widget) {
			// TODO Auto-generated method stub
			if (mClickListener!=null) {
				mClickListener.onClick(widget);
			}
		}
	}
	
	private void setMsgInfo(final Message message) {
		String name =message.getSname();
		SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(name+"对你说：");
		spannableStringBuilder.setSpan(new MyClickAbleSpan(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.go2SeeUserHomePage(MessageReceiveActivity.this, message.getSid());				
			}
		}), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		spannableStringBuilder.setSpan(new UnderlineSpan(), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		spannableStringBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue2)), 
				 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		messgeRevName.setMovementMethod(LinkMovementMethod.getInstance());
		messgeRevName.setText(spannableStringBuilder);
		contentText.setMovementMethod(LinkMovementMethod.getInstance());
		contentText.setText(Html.fromHtml(message.getContent()));
		if (!TextUtils.isEmpty(message.getJsoninfo())) {
			final MessageJsonTag tag =MessageJsonTag.parse(message.getJsoninfo());
			if (tag!=null) {
				if (tag.fromtype==4) {//event
					SpannableStringBuilder seemoreInfoBuilder =new SpannableStringBuilder("#点击查看详情#");
					seemoreInfoBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.darkgreen)), 
							0, seemoreInfoBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					seemoreInfoBuilder.setSpan(new MyClickAbleSpan(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							 Intent resultIntent = new Intent(MessageReceiveActivity.this, SportEventDetailActivity.class);
								resultIntent.putExtra("activity_id", tag.aid);
								startActivity(resultIntent);
													
						}
					}), 0, seemoreInfoBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					
					contentText.append(seemoreInfoBuilder);
				}
			}
		}
		findViewById(R.id.message_resend_btn).setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		Util.go2SendMessage(MessageReceiveActivity.this,message.getSname(),message.getSid()+"",0);

	}
});
	}
	
	
	private void initViews() {
		 contentText = (TextView) findViewById(R.id.message_content);
		 messgeRevName = (TextView) findViewById(R.id.message_tv);
	    findViewById(R.id.message_resend_btn).setVisibility(View.GONE);
	}

	protected UIController uiFreshPhotoController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
	if (errorCode>-1) {
				
			}
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			showToast("发送成功!");
			finish();
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}
	};
	
}
