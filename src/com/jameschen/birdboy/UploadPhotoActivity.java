package com.jameschen.birdboy;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;

import org.apache.http.Header;
import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.protocol.HttpContext;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import com.jameschen.birdboy.utils.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseUploadPhotoActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebSportEvents;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

public class UploadPhotoActivity extends BaseUploadPhotoActivity{
	private CookieStore cookies;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.upload_photo);
		setTopBarRightBtnVisiable(View.GONE);
		 setTopBarLeftBtnVisiable(View.GONE);
		setTitle("完成(3/3)");
		id =getIntent().getIntExtra("id", -1);
		Log.i(TAG, "id=="+id);
		
		initviews();
		resultOk = true;
	}
	boolean isStart=false;
	private Button upload;
	TextView photoText;
	protected int id;

	private boolean isSucc=false;
	// float money;
	private void initviews() {

		upload = (Button) findViewById(R.id.btn_upload);
		photo = (ImageView) findViewById(R.id.image_choose);
		photoText = (TextView) findViewById(R.id.photo_text);
		//
		
		upload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				if (isStart) {
//					showToast("正在上传，请稍候...");
//					return;
//				}
				if (photo.getTag()==null) {
					//showToast("请选择一张照片");
					setResult(RESULT_OK);
					finish();
					return;
				}
				File file  = new File((String)photo.getTag());
				if (!file.exists()) {//file deleted?
					showToast("图片不存在");
					return;
				}
				
				
				 RequestParams params = new RequestParams();  
				 
				 params.put("method", "logo");
				 params.put("id", 0+"");
				 params.put("ssize", 200);
				 params.put("size", 500);
				 try {
					params.put("image_file", file);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			// Upload a File  
				 PersistentCookieStore myCookieStore = new PersistentCookieStore(UploadPhotoActivity.this);
				 final AsyncHttpClient client =new AsyncHttpClient();
				 client.setCookieStore(myCookieStore);
				 if (LogInController.getHttpClientInstance()!=null) {
						client.setCookieStore(LogInController.getHttpClientInstance().getCookieStore());
				}
			
				// Map<String, File> fileMap = new HashMap()<String, File>();
//				 fileMap.put("image_file", file);
//				 HttpUtil.get("http://birdboy.cn/upload", params, fileMap);
				 
				client.post("http://birdboy.cn/upload", params, new AsyncHttpResponseHandler(){
					

					


					@Override
					@Deprecated
					public void onSuccess(String content) {
						// TODO Auto-generated method stub
						super.onSuccess(content);
						Log.i(TAG, "&&&&&"+content);
						readJsonData(content, uiController, null);
						  HttpContext httpContext = client.getHttpContext();
					        cookies = (CookieStore) httpContext.getAttribute(ClientContext.COOKIE_STORE);
					}
					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0, arg1, arg2);
						isSucc  = false;
						isStart=false;
						showToast("上传图片成功");
						debugHeaders(TAG, arg1);
						//setResult(RESULT_OK);
						sendCreateOKIntent();
						//finish();
					}
					 @Override
					public void onProgress(int bytesWritten, int totalSize) {
						// TODO Auto-generated method stub
						super.onProgress(bytesWritten, totalSize);
						int percent = bytesWritten*100/totalSize;
						//photoText.setText("已上传"+percent+"%");
						if (percent==100) {//ok.
							isSucc=true;
						}
						showProgressDialog("上传图片", "已上传"+percent+"%", null);
					}
					 @Override
					public void onStart() {
						super.onStart();
						isStart=true;
						showProgressDialog("上传图片", "已上传"+0+"%", new OnCancelListener() {
							
							@Override
							public void onCancel(DialogInterface arg0) {
								// TODO Auto-generated method stub
								isStart =false;
							}
						});
						//photoText.setText("开始上传。。。");
					}
					
					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						super.onFailure(arg0, arg1, arg2, arg3);
						if (isSucc) {
							isSucc=false;
							showToast("上传图片成功");
							debugHeaders(TAG, arg1);
							setResult(RESULT_OK);
							finish();
						}else {
							showToast("上传图片失败!");
						}
						
						
					}
					
				
					 @Override
					public void onFinish() {
						super.onFinish();

						cancelProgressDialog();
						isStart=false;
						//isSucc = false;
						if (isSucc) {
							showToast("上传图片成功");
							setResult(RESULT_OK);
							finish();
							isSucc=false;
						}
					}
				 
				});  
				 
				 
				 
			}
		});

		photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			showAlert();
			}
		});
		
	}

	private UIController uiController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			String logoUrl = (String) content;
			Log.i(TAG, "logoUrl="+logoUrl);
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "upload_logo";
		}
		
		@Override
		public Type getEntityType() {
			return new TypeToken<WebBirdboyJsonResponseContent<String>>() {
			}.getType();
		}
	};
	
	private void sendCreateOKIntent() {
		
		onBackPressed();
		
//		Intent intent = new Intent(Intents.ACTION_CREATE_ENEVT_OK);
//		sendBroadcast(intent );
	}
	

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}

}
