package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
//
//http://birdboy.cn/addChat  Post请求
//
//字段               类型                描述                           备注            值条件
//

//
// 3、返回字段
//
//{
//    "success": true, 
//    "result": {
//    },
//
//    "info":""
//
//} 



public class MessageSendActivity extends BaseActivity{
	EditText inputEditText;
	String id;
	int type;
	protected String name;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_send_ui);
		setTitle("发信箱");
		Intent intent = getIntent();
		 id = intent.getStringExtra("id");
		 type =intent.getIntExtra("type", -1);
		 name = intent.getStringExtra("name");
		 setTopBarRightBtnListener("发送", new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				closeInputMethod();

				if (TextUtils.isEmpty(inputEditText.getText())||inputEditText.getText().toString().trim().equals("")) {
					showToast("请输入内容");
					return;
				}else {
					if (!LogInController.IsLogOn()) {
						Util.go2LogInDialog(MessageSendActivity.this,new OnLogOnListener() {
							
							@Override
							public void logon() {
								MessageSend();
							}
						});
						return;
					}

					MessageSend();
				
				}
			
			}
		});
		initViews();
	}

	void MessageSend(){
		Map<String, String> map = getPublicParamRequstMap();
		showProgressDialog("信件", "正在发送,请稍候...", null);
		//int sid,                                发送者id
		//
		//String sname,                     发送者名字
		//
		//int revid,                             接受id
		//
		//String revname,                   接受者名字
		//
		//String content,                     接受者内容
		//
		//byte fromtype                      是玩家发，还是教练发
		map.put("sid", LogInController.currentAccount.getId()+"");
		map.put("sname", LogInController.currentAccount.getNick());
		map.put("revid", ""+id);
		map.put("revname", ""+name);
		map.put("content", inputEditText.getText().toString());
		map.put("fromtype", ""+type);
		postDataLoader(map,
				HttpConfig.REQUST_ADD_MESSAGE_URL, uiFreshPhotoController,new HttpClientStack(LogInController.getHttpClientInstance()));
	}
	private void initViews() {
		 inputEditText = (EditText) findViewById(R.id.message_Edit);
		TextView messgeRevName = (TextView) findViewById(R.id.message_tv);
//		if (name!=null&&!name.contains("教练")) {
//			name+="教练";
//		}
		messgeRevName.setText("发送至"+name);
		
	}

	protected UIController uiFreshPhotoController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
	if (errorCode>-1) {
				
			}
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			showToast("发送成功!");
			finish();
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}
	};
	
}
