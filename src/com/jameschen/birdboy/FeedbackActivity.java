package com.jameschen.birdboy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View.OnClickListener;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.ui.FeedbackFragment;

public class FeedbackActivity extends BaseActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
		setTitle(getString(R.string.more_advise));
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment mFragment = fm.findFragmentByTag(ConstValues.FEED_BACK);
		if (mFragment == null) {

			mFragment = FeedbackFragment.getFeedbackFragmentInstance(1);
	}
		ft.add(R.id.fragment_content, mFragment, ConstValues.FEED_BACK);
		ft.commit();
	}
}
