package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.R.integer;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.HeadAdapter;
import com.jameschen.birdboy.adapter.SportEventAdapter;
import com.jameschen.birdboy.base.BaseDetailActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebSportEventDetail;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Location;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.model.entity.SportEventDetail;
import com.jameschen.birdboy.model.entity.SportEventPhoto;
import com.jameschen.birdboy.model.entity.SportUser;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MGridView;
import com.jameschen.plugin.BindModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.umeng.analytics.MobclickAgent;


public class SportEventDetailActivity extends BaseDetailActivity {

	
//	1、功能说明
//	通过活动id查看活动详情
//
//	2、请求字段
//	http://birdboy.cn/uactionAPI?aid=10
//
//	get请求
//
//	 返回字段说明
//
//	字段               类型                描述                           备注                                   值条件
//
//
//	stime          int         活动开始时间                yyyy-MM-dd HH:mm    必填
//	etime          int           活动结束时间              yyyy-MM-dd HH:mm    必填
//
//	signstime      int         活动报名开始时间             yyyy-MM-dd HH:mm    必填
//	signetime      int          活动报名结束时间            yyyy-MM-dd HH:mm    必填
//	name           String      活动的名称								         必填
//	type           int            体育类别                    4网球            必填
//	cmobile        long        联系电话                    用户手机号           必填
//	place          String      活动地点                                       必填
//	rmb            float       活动报名费用                                    可选
//	position       String      活动的坐标位置                                  必填
//	buildid        int         活动所选的场馆                                  可选
//	area           int         活动的一级城市区域                               可选
//	childarea      int         活动的二级城市区域                               可选

	private SportEventDetail sportEventDetail;
	@Override
			protected void onCreate(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				super.onCreate(savedInstanceState);
				setContentView(R.layout.sportevent_detail);
				
				SlapshActivity.IsNeedCheckedGuidePage(SportEventDetailActivity.this, ConstValues.SPORT_EVENT_DETAIL);
 
				Intent intent =getIntent();
				SportEvent sportEvent =intent.getParcelableExtra(ConstValues.SPORT_EVENT);
				if (sportEvent==null) {
					id=intent.getIntExtra("activity_id", -1);
				}else{
					id=sportEvent.getId();
				
					dist = (float) sportEvent.getDist();
				}
				Log.i(TAG, "id==="+id);
				setTopBarRightBtnListener(R.drawable.share, new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (sportEventDetail!=null) {
							String content ="你的朋友使用了“鸟孩运动” 邀请你参加活动“"+sportEventDetail.getAction().getName()+"”，快来下载吧。你可以约周边的朋友一起运动。找到周边的场馆，教练。";
							v.setTag(content);
							share(v,ConstValues.TYPE_EVENT_VAL);
						}
						
					}
				});
				setTitle("活动详情");
			
				initViews();
				showProgressDialog("刷新", "请稍候...", null);
				setDetailInfoToUI(sportEvent);
				getDataLoader("http://birdboy.cn/uactionViewAPI?aid="+id, false,new Page(), uiFreshController);
				
	}
	
	long id;
	private ImageView photo,sex,createrPhoto;
	private TextView fav,join,comment,admin,jioners,commentNum;
	private TextView name;
	private TextView address;
	private TextView content;
	private TextView distance;
	private TextView time,extraInfo,join_Pass_tv;
	private View sexAgeView;
	private TextView createrAge;
	private HeadAdapter headAdapter;
	private ImageView weibo,qq;
	private void initViews() {
		name = (TextView)  findViewById(R.id.event_item_name);
		address = (TextView)findViewById(R.id.event_item_address);
		distance = (TextView) findViewById(R.id.event_item_distance);
		time = (TextView) findViewById(R.id.event_item_date_time);
		fav = (TextView) findViewById(R.id.event_fav_num);
		content =(TextView) findViewById(R.id.event_item_intro);
		comment = (TextView) findViewById(R.id.event_add_comment);
		commentNum = (TextView) findViewById(R.id.event_comment);
		fav = (TextView) findViewById(R.id.event_add_fav);
		join = (TextView) findViewById(R.id.event_add_join);
		jioners = (TextView) findViewById(R.id.event_joiner);
		extraInfo= (TextView) findViewById(R.id.event_item_price);
		admin =(TextView) findViewById(R.id.event_creater_name_text);
		sex =(ImageView) findViewById(R.id.sex_flag);
		sexAgeView = findViewById(R.id.sex_age_container);
		createrAge =(TextView) findViewById(R.id.age);
		weibo =(ImageView) findViewById(R.id.weibo);
		qq =(ImageView) findViewById(R.id.qq);
		memebersGridView=(MGridView)findViewById(R.id.event_joiners_girdview);
		memebersGridView.setAdapter(headAdapter=new HeadAdapter(SportEventDetailActivity.this));
		
		
		qq.setOnClickListener(new OnClickListener() {
			

			@Override
			public void onClick(View v) {
			Util.go2SeeSNS(SportEventDetailActivity.this, snsObj, BindModel.QQ);
			}
		});
		
		weibo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			Util.go2SeeSNS(SportEventDetailActivity.this, snsObj, BindModel.SINA);
			}
		});
		
		createrPhoto =(ImageView) findViewById(R.id.event_creater_photo);
		photo =(ImageView) findViewById(R.id.event_item_portrait);
	address.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (sportEventDetail!=null&&sportEventDetail.getAction()!=null) {
					Util.go2Map(getBaseContext(), Location.CreateLocation(sportEventDetail.getAction().getPlace(),sportEventDetail.getAction().getPosition()));
				}
			}
		});
		photo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				if (sportPhotos==null||sportPhotos.size() == 0) {
					return;
				}
				ArrayList<String> phoneList = new ArrayList<String>();
				for (int i = 0; i < sportPhotos.size(); i++) {
					phoneList.add(HttpConfig.HTTP_BASE_URL+sportPhotos.get(i).getUrl());
				}
				Util.go2SeeBigPics(SportEventDetailActivity.this, phoneList);
			
				
			}
		});
		
		roundOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.sport_logo).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.showImageForEmptyUri(R.drawable.sport_logo)
		.showImageOnFail(R.drawable.sport_logo).considerExifParams(true).cacheInMemory(true) 
		.cacheOnDisc(true).build();
		
		((View)comment.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				Util.go2SendComment(SportEventDetailActivity.this,id,ConstValues.TYPE_EVENT_VAL);
			}
		});
		
		((View)fav.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				if (LogInController.IsLogOn()) {
					showProgressDialog("关注活动", "提交，请稍候...", null);
					Log.i(TAG, "id==="+id);
					getDataLoader("http://birdboy.cn/keepUactionAPI?aid="+id, false, null, favController, new HttpClientStack(LogInController.getHttpClientInstance()));
				}else {
					Util.go2LogInDialog(SportEventDetailActivity.this,new OnLogOnListener() {
						
						@Override
						public void logon() {
							showProgressDialog("关注活动", "提交，请稍候...", null);
							Log.i(TAG, "id==="+id);
							getDataLoader("http://birdboy.cn/keepUactionAPI?aid="+id, false, null, favController, new HttpClientStack(LogInController.getHttpClientInstance()));
					
						}
					});
				}
		
			}
		});
		((View)join.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (sportEventDetail==null) {
					return ;
				}
				Util.CommitDialog(SportEventDetailActivity.this,"参与报名","请确认活动时间?\n"+
				""+sportEventDetail.getAction().getTimeStr() +" "+ 
				SportEventAdapter.getWeekDayStr(sportEventDetail.getAction().getSweek())		,new Dialog.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (LogInController.IsLogOn()) {
							showProgressDialog("参加活动", "提交，请稍候...", null);
							getDataLoader(HttpConfig.REQUST_JOIN_EVENT_URL+id, false, null, joinController, new HttpClientStack(LogInController.getHttpClientInstance()));
						}else {
							Util.go2LogInDialog(SportEventDetailActivity.this,new OnLogOnListener() {
								
								@Override
								public void logon() {
									showProgressDialog("参加活动", "提交，请稍候...", null);
									getDataLoader(HttpConfig.REQUST_JOIN_EVENT_URL+id, false, null, joinController, new HttpClientStack(LogInController.getHttpClientInstance()));
						
								}
							});
						}
				
					}
					
				});
				
			}
		});
		
		commentNum.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.go2CommentList(SportEventDetailActivity.this,ConstValues.TYPE_EVENT_VAL,id);
			}
		});
	}

	private DisplayImageOptions roundOptions;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case ConstValues.TYPE_EVENT_VAL:
		case ConstValues.TYPE_USER_VAL:
		{
			if (resultCode == RESULT_OK) {
				showProgressDialog("刷新", "请稍候...", null);
				doRefresh();
			}
		}
			break;

		default:
			break;
		}
	}
	private boolean showToast=true;
	
	void doRefresh(){
		showToast=false;
		getDataLoader("http://birdboy.cn/uactionViewAPI?aid="+id, false,new Page(), uiFreshController);

	}
	
	private String getCostInfo(float rmb) {
		return rmb==0?"免费":"￥"+rmb;
	}
	public String getDistanceStr(double  distance) {
		if (distance >= 1000) {// show in km
			DecimalFormat df = new DecimalFormat(".0");
			return df.format(distance / 1000) + "km";
		} else {
			return (int) distance + "m";
		}
	}
	
	private float dist=-1;
	
	private <T> void setDetailInfoToUI(T detail) {
		if (detail==null) {
			return;
		}
		if (detail instanceof SportEvent) {
			SportEvent sportEvent =(SportEvent) detail;
			List<SportUser> sportUsers =SportEventAdapter.getUsers(sportEvent.getUsers());
			int sexRes=R.drawable.fujin_boy;
			int sexAgeContaionRes=R.drawable.round_bg_blue;
			if (sportUsers!=null) {
				int sexId =sportUsers.get(0).getSex();
				sexRes=sexId==1?R.drawable.fujin_boy:R.drawable.fujin_girl;
				sexAgeContaionRes=sexId==1?R.drawable.round_bg_blue:R.drawable.round_bg_pink;
			}
			
			String businessTag =sportEvent.getAddint()>=2?"商家":"个人";
			if (sportEvent.getAddint()<2) {//personal 
				sexAgeView.setVisibility(View.VISIBLE);
				sexAgeView.setBackgroundResource(sexAgeContaionRes);
				sex.setImageResource(sexRes);
			}else {
				sexAgeView.setVisibility(View.GONE);
			}
			extraInfo.setText(CoachAdapter.setSportTypeStr(sportEvent.getType()) +"  "+ getCostInfo(sportEvent.getRmb())+" "+businessTag
					+" "+"邀请"+sportEvent.getNum()+"人");
			name.setText(sportEvent.getName());
			address.setText(sportEvent.getPlace());
			admin.setText(sportEvent.getContact());
			jioners.setText("报名 "+"("+sportEvent.getWantnum()+"人)"
					+ "    通过  ("+sportEvent.getNownum()+")");
			time.setText(sportEvent.getTimeStr()+"  "+SportEventAdapter.getWeekDayStr(sportEvent.getSweek()));
		
			if (dist>=0) {
				((View)distance.getParent()).setVisibility(View.VISIBLE);
				distance.setText(getDistanceStr(dist));
			}else {
				((View)distance.getParent()).setVisibility(View.GONE);
			}
			//String memberStr = "";
			if (sportUsers!=null) {
				setSportMemberInfoToUI(sportUsers);
			}
			
			
			
			if (sportEvent.getAddtext()==null) {
				return;
			}
			
			loadImage(HttpConfig.HTTP_BASE_URL+ sportEvent.getAddtext(), photo, roundOptions);
		} else {
			SportEventDetail sportEventDetail =(SportEventDetail) detail;
			this.sportEventDetail =sportEventDetail;
			SportEvent sportEvent =sportEventDetail.getAction();
			setDetailInfoToUI(sportEvent);
			//jioners.setText(text);
			time.setText(sportEvent.getTimeStr()+"  "+SportEventAdapter.getWeekDayStr(sportEvent.getSweek()));
			
			content.setText(Html.fromHtml(sportEventDetail.getContent()));
			
			if (content.getLayout()!=null&&content.getLayout().getLineCount()>2) {
				
				Drawable drawable = getResources().getDrawable(R.drawable.enter_gray);
				drawable.setBounds(0, 0, drawable.getMinimumWidth(),
						drawable.getMinimumHeight());// 必须设置图片大小，否则不显示
				content.setCompoundDrawables(null, null, drawable, null);
				content.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Util.go2DetailContent(SportEventDetailActivity.this,content.getText().toString());
					}
				});
			}else {
				content.setCompoundDrawables(null, null, null, null);
			}
		}
	}
	
	

	private SNS snsObj;
	private void setSNSInfo(String sns) {
		qq.setVisibility(View.GONE);
		weibo.setVisibility(View.GONE);
		if (TextUtils.isEmpty(sns)) {
			return;
		}
		try {
			 snsObj = ParseJsonData.getSNSData(sns);
			if (snsObj!=null) {
				
				if (snsObj.getQq()!=null) {
					qq.setVisibility(View.VISIBLE);
				}
				
				if (snsObj.getWeibo()!=null) {
					weibo.setVisibility(View.VISIBLE);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private MGridView memebersGridView;

	private void setSportMemberInfoToUI(List<SportUser> sportUsers) {
		int size =sportUsers.size();
		if (size == 0) {
			return;//no more
		}
		
		List<SportUser> showSportUsers=getReverseList(sportUsers);
		 
		headAdapter.setObjectList(showSportUsers);
		headAdapter.notifyDataSetChanged();
	
	}

	
	public static List<SportUser> getReverseList(List<SportUser> sportUsers){
		int size =sportUsers.size();
		int maxlen=21;//max 21
		List<SportUser> showSportUsers=null;
		if (size<=maxlen) {
			sportUsers.remove(0);//remove first.
			showSportUsers=sportUsers;
		}else {//len >21  ,. get last 20 already skip first.
			showSportUsers=sportUsers.subList(size-maxlen+1, size);
		}
		//list change end to start..
		 Collections.reverse(showSportUsers);
		 return showSportUsers;
	}



	private void loadImageByUrl(final SportUser sportUser, ImageView photo,int roundVal, int defualtIcon) {
		 DisplayImageOptions options = new DisplayImageOptions.Builder()
			.showImageOnLoading(defualtIcon)
			.showImageForEmptyUri(defualtIcon).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(roundVal))
			.cacheOnDisc(true).build();
		 final String slogoUrl =sportUser.getLogo();
		 if (TextUtils.isEmpty(slogoUrl)) {
			photo.setImageResource(defualtIcon);
		}else {
			loadImage(HttpConfig.HTTP_BASE_URL+ slogoUrl, photo, options);
		}
			
		
		photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Util.go2SeeUserHomePage(SportEventDetailActivity.this, sportUser.getId());
									
			}
		});
			
	}

	private List<SportEventPhoto> sportPhotos;
	private void setPhotosToUI(List<SportEventPhoto> sportPhotos) {
		if (sportPhotos.size()==0) {
			return ;
		}
		if (this.sportPhotos ==null) {
			this.sportPhotos = new ArrayList<SportEventPhoto>();
		}
		this.sportPhotos.clear();
		this.sportPhotos.addAll(sportPhotos);
	}
	
	private void setCommentsToUI(List<Comment> comments) {

		ViewGroup viewGroup = (ViewGroup) findViewById(R.id.event_comment_info);
		int size =comments.size();
		int len=0;
		if (size == 0) {
	
			return;//no more
		}
		viewGroup.removeViews(1, viewGroup.getChildCount()-1);
		if (size>5) {//show more page.
			len=5;
		}else {
			len =size ;
		}
		
		for (int i = 0; i < len; i++) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup vGroup =(ViewGroup) inflater.inflate(R.layout.comment_item, viewGroup, false);
			TextView name = (TextView) vGroup.findViewById(R.id.comment_item_name);
			TextView info = (TextView) vGroup.findViewById(R.id.comment_item_content);
			TextView  date = (TextView) vGroup.findViewById(R.id.comment_item_date);
			name.setText(comments.get(i).getUsernick());
			info.setText(Html.fromHtml(comments.get(i).getContent()));
			date.setText(comments.get(i).getCtstr());
			final Comment comment =comments.get(i);
			TextView replay=(TextView) vGroup.findViewById(R.id.comment_reply);
			replay.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
				Util.go2SendComment(SportEventDetailActivity.this,id,comment.getUserid(),comment.getUsernick(),ConstValues.TYPE_EVENT_VAL);
				}
			});
			viewGroup.addView(vGroup);
			 if (i<len-1) {
				 
				 View line =(View) inflater.inflate(R.layout.item_dot_line, viewGroup, false);
					viewGroup.addView(line);
			 }
		}
	
	}


	
	private void onDataChangeNotify(WebSportEventDetail webSportEventDetail) {
		SportEventDetail sportEventDetail =webSportEventDetail.getActionDetail();
		sportEventDetail.setAction(webSportEventDetail.getAction());
		
		setDetailInfoToUI(sportEventDetail);
		Account creator = webSportEventDetail.getCreator();
		
		if (creator!=null) {
			setSNSInfo(webSportEventDetail.getCreator().getSns());
			createrAge.setText(creator.getAge()+"");
			int sex = creator.getSex()==1?R.drawable.m:R.drawable.wm;
			SportUser sportUser = new SportUser();
			sportUser.setId(creator.getId());
			sportUser.setLogo(creator.getLogo());
			loadImageByUrl(sportUser, createrPhoto,10,sex);
			
		}
		
		setPhotosToUI(webSportEventDetail.getPhotos());
		setCommentsToUI(webSportEventDetail.getComments());
		
		commentNum.setText("留言"+"("+webSportEventDetail.getCommentCount()+")");
	}

	protected UIController joinController=new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			MobclickAgent.onEvent(SportEventDetailActivity.this, "activity_jion");
			showToast("报名成功，去【我的活动】查看发起人是否审核");
			doRefresh();
			currentSportChangeStatus=true;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			if (errorCode>-1) {
				
			showToast(getString(R.string.jion_failed_by_network));
			}
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG+"join";
		}
	};
	
	private boolean currentSportChangeStatus=false;
	
	private UIController favController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
	
			showToast("收藏活动成功！进入“个人面板-我的活动”查看我收藏的活动");
			currentSportChangeStatus=true;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
	if (errorCode>-1) {
				showToast(getString(R.string.get_detail_info_error));
			}
			
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG+"fav";
		}
	};
	
	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			showToast=true;
			WebSportEventDetail webSportEventDetail = (WebSportEventDetail) content;
			onDataChangeNotify(webSportEventDetail);
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebSportEventDetail>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			if(errorCode>-1&&showToast){
				
			showToast(getString(R.string.get_detail_info_error));
		}
			showToast=true;
		
			
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};

}
