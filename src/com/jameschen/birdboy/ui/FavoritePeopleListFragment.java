package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.AroundUserAdapter.ClickType;
import com.jameschen.birdboy.adapter.MyFriendAdapter;
import com.jameschen.birdboy.adapter.MyFriendAdapter.InviteItemClickListener;
import com.jameschen.birdboy.base.BaseListFragment;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.FavoriteFriend;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;


public class FavoritePeopleListFragment extends BaseListFragment<FavoriteFriend> implements
		OnItemClickListener, OnClickListener {


	private MyFriendAdapter myFriendAdapter;
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		if (!hidden) {
			//check  is get the location,,,
			//s
			
		}
	
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = null;
		
			view = inflater.inflate(R.layout.my_friend_ui, container, false);
		
		myFriendAdapter = new MyFriendAdapter(getBaseActivity(),inviteItemClickListener);
		initViews(view);
		
		return view;
	}

	private InviteItemClickListener inviteItemClickListener =new InviteItemClickListener() {
	
	@Override
	public void OnItemClick(int position, FavoriteFriend invitedUser,
			ClickType clickType) {
		
		//check is logOn
		if (LogInController.IsLogOn()) {
			fliterByClickType(clickType, invitedUser);
		}else {
			final FavoriteFriend uInvitedUser=invitedUser;
			final ClickType uClickType =clickType;
			Util.go2LogInDialog(getBaseActivity(), new OnLogOnListener() {
				@Override
				public void logon() {
					fliterByClickType(uClickType, uInvitedUser);
				}
			});
		}
		
			
	}
};
	void fliterByClickType(ClickType clickType, FavoriteFriend invitedUser){
		if (clickType==ClickType.FAV) {
			cancelMyFav(invitedUser);
		}else if (clickType == ClickType.INVITE) {
			inviteUser(invitedUser);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
			//load 
			getDataLoader(HttpConfig.REQUST_FAV_FRIEND_LIST_URL, false, createInitPage(), uiFreshController,new HttpClientStack(LogInController.getHttpClientInstance()));

	}
	
	
	
	protected void inviteUser(FavoriteFriend invitedUser) {
		Bundle extra=new Bundle();
		extra.putInt("flag", 1);
		extra.putParcelable("invite_one", invitedUser);
		Util.createSportEvent(getActivity(),null,extra);
	}

	protected void cancelMyFav(final FavoriteFriend invitedUser) {
			UIController cancelMyFavController = new UIController() {
				@Override
				public void responseNetWorkError(int errorCode, Page page) {
					// TODO Auto-generated method stub
					showToast("取消关注失败");
				}
				
				@Override
				public <E> void refreshUI(E content, Page page) {
					if (myFriendAdapter!=null) {
						myFriendAdapter.remove(invitedUser);
						myFriendAdapter.notifyDataSetChanged();
					}
					showToast("取消关注"+invitedUser.getUsername()+"成功!");
				}
				
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return TAG+"fav"+invitedUser.getId();
				}
				
				@Override
				public Type getEntityType() {
					return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
					}.getType();
				}
			};
		
		Map<String, String> map=getPublicParamRequstMap();
		map.put("userid", invitedUser.getUserid()+"");
	//	postDataLoader(map,HttpConfig.REQUST_CANCEL_FAV_URL, false,null,cancelMyFavController,new HttpClientStack(LogInController.getHttpClientInstance()));	
		getDataLoader(HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_CANCEL_FAV_URL, map), false,null,cancelMyFavController,new HttpClientStack(LogInController.getHttpClientInstance()));	

	}

	
	
	
	@Override
	protected   void initViews(View viewGroup) {
		// TODO Auto-generated method stub
		super.initData();
		mListView = (MyListView) viewGroup
				.findViewById(R.id.my_friend_list_view);

		mListView.setOnItemClickListener(this);
		initListAdapter(myFriendAdapter, uiFreshController);
		mListView.setMode(Mode.DISABLED);//not support page..
		
		
		final View emptyView = viewGroup.findViewById(R.id.list_empty);
		
		emptyView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
			}
		});
		setEmptyView(emptyView);
		
		
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
//		// TODO Auto-generated method stub
//		Intent intent = new Intent(getActivity(),
//				SportEventDetailActivity.class);
//		Object object = parent.getAdapter().getItem(position);
//		if (object == null) {
//			return;
//		}
//		Parcelable p = (FavoriteFriend) (object);
//		intent.putExtra(ConstValues.AROUND_USER, p);
//		startActivity(intent);
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		
		super.onDestroy();
	}

	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
		
				// refreash
			 List<FavoriteFriend> invitedUsers = (List<FavoriteFriend>) content;
				int size = invitedUsers.size();
				if (page.isLoadMore() != Page.CLEAR) {
					page.setLoadMore(size < numPerPage ? Page.NO_MORE
							: Page.LOAD_MORE);
				}
				refreshList(invitedUsers, page);

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<List<FavoriteFriend>>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			try {
				if(errorCode>-1)
				showToast(getString(R.string.network_error_time_out));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (page != null) {
				setPrevPage(page);
			}

		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};

	@Override
	protected String setRequestLoadMoreUrl(Page page) {
		
		return null;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
	
		default:
			break;
		}
	}

	@Override
	protected void refreshArea() {
		
	}

	

}
