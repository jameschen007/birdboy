package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SlapshActivity;
import com.jameschen.birdboy.adapter.AddMemberAdapter;
import com.jameschen.birdboy.adapter.AddMemberAdapter.OnMemberChangedListener;
import com.jameschen.birdboy.adapter.AroundUserAdapter;
import com.jameschen.birdboy.adapter.AroundUserAdapter.ClickType;
import com.jameschen.birdboy.adapter.AroundUserAdapter.InviteItemClickListener;
import com.jameschen.birdboy.adapter.CategoryAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.BaseList2Fragment;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.location.LocationManager;
import com.jameschen.birdboy.location.LocationManager.GetLastLocationListener;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebAround;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.FliterDialog;
import com.jameschen.birdboy.utils.FliterDialog.onFilerSelectedListener;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyList2View;
import com.jameschen.plugin.BindModel;
import com.umeng.analytics.MobclickAgent;


/***
 * 切换到该界面 判断是否获取得到当前坐标，获取不到则进行一次定位操作,参考陌陌 分页点击请求 上拉显示刷新时间。
 * 
 * */

public class AroundListFragment extends BaseList2Fragment<InvitedUser> implements
		OnItemClickListener, OnClickListener {

	public static final String AROUND_USER = ConstValues.AROUND_USER;
	public static long areaGetTime;
	private String keyword = "*:*";

	public static AroundListFragment newInstance(String search, int type,
			ArrayList<InvitedUser> mInvitedUsers) {
		AroundListFragment fragment = new AroundListFragment();
		fragment.keyword = search;
		fragment.mInfos = mInvitedUsers;
		return fragment;
	}

	private AroundUserAdapter aroundUserAdapter;
	
	
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		if (!hidden) {
			//check  is get the location,,,
			//s
			if (!LogInController.IsLogOn()) {
				((View)relocateView.getParent()).setVisibility(View.VISIBLE);
				Util.go2LogInDialog(getBaseActivity(), new OnLogOnListener() {
					@Override
					public void logon() {
						((View)relocateView.getParent()).setVisibility(View.GONE);

						if (TAG.equals(ConstValues.SEARCH)) {
							startSearch(keyword);
						} else {
							//load area
							Map<String, String> temp=getPublicParamRequstMap();
							sort ="distance";
							if (findLocation()) {
								startSearch(keyword, temp);
							} else {
								startLocate(getPublicParamRequstMap());
							}
						}
					}
				});
			
			}else {
				((View)relocateView.getParent()).setVisibility(View.GONE);

			}
		}
	
	}
	
	boolean  findLocation(){
		LocationSupport locSupport = LocationSupport
			.getLocationSupportInstance(getActivity().getApplicationContext());
		if (locSupport.getLatitude()>0&&locSupport.getLongtitude()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {

			if (savedInstanceState.containsKey(AROUND_USER)) {
				ArrayList<InvitedUser> invitedUsers = savedInstanceState
						.getParcelableArrayList(AROUND_USER);
				if (invitedUsers != null)
					mInfos = invitedUsers;
			}
			// restore..adapter data...
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = null;
		if (TAG.equals(ConstValues.SEARCH)) {
			view = inflater.inflate(R.layout.around_list, container, false);
		} else {
			view = inflater.inflate(R.layout.around_ui, container, false);
		}
		aroundUserAdapter = new AroundUserAdapter(getBaseActivity(),inviteItemClickListener);
		initViews(view);
		
		return view;
	}

	private InviteItemClickListener inviteItemClickListener =new InviteItemClickListener() {
	
	@Override
	public void OnItemClick(int position, InvitedUser invitedUser,final Drawable head,
			ClickType clickType) {
		
		//check is logOn
		if (LogInController.IsLogOn()) {
			fliterByClickType(clickType, invitedUser,head);
		}else {
			final InvitedUser uInvitedUser=invitedUser;
			final ClickType uClickType =clickType;
			Util.go2LogInDialog(getBaseActivity(), new OnLogOnListener() {
				@Override
				public void logon() {
					fliterByClickType(uClickType, uInvitedUser,head);
				}
			});
		}
		
			
	}
};
	void fliterByClickType(ClickType clickType, InvitedUser invitedUser, Drawable head){
		
		//check is myself
		if (invitedUser!=null&&invitedUser.getId()==LogInController.currentAccount.getId()) {
			if (clickType==ClickType.FAV) {
				showToast("不能关注自己！");
				return;
			}else if (clickType==ClickType.INVITE) {
				showToast("不能邀约自己！");
				return;
			}
			
			
		}
		
		if (clickType==ClickType.FAV) {
			Util.addMyFav(getBaseActivity(),invitedUser);
		}else if (clickType == ClickType.INVITE) {
			/**currently  max is 3
			 * */
			if (memberAdapter.getInviedCount()==3) {
				showToast("当前只能邀约3个人");
				return;
			}
			inviteUser(invitedUser,head);
		}else if (clickType==ClickType.SNS_QQ) {
			seeUserBlog(invitedUser.getSns().getMembers(),BindModel.QQ);
		}else if (clickType==ClickType.SNS_WEIBO) {
			seeUserBlog(invitedUser.getSns().getMembers(),BindModel.SINA);
		}
	}

	 
	
	
	private void seeUserBlog(SNS sns,int type) {
		Util.go2SeeSNS(getBaseActivity(),sns,type);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		SlapshActivity.IsNeedCheckedGuidePage(getBaseActivity(), ConstValues.AROUND_USER);
		
		if (mInfos != null) {
			aroundUserAdapter.setObjectList(mInfos);
		}

		if (LogInController.IsLogOn()) {
			if (TAG.equals(ConstValues.SEARCH)) {
				startSearch(keyword);
			} else {
				//load area
				Map<String, String> temp=getPublicParamRequstMap();
				sort ="distance";
				if (findLocation()) {
					startSearch(keyword, temp);
				} else {
					startLocate(getPublicParamRequstMap());
				}
				
			}
		}else {
			Util.go2LogInDialog(getBaseActivity(), new OnLogOnListener() {
				@Override
				public void logon() {
					if (TAG.equals(ConstValues.SEARCH)) {
						startSearch(keyword);
					} else {
						//load area
						Map<String, String> temp=getPublicParamRequstMap();
						sort ="distance";
						if (findLocation()) {
							startSearch(keyword, temp);
						} else {
							startLocate(getPublicParamRequstMap());
						}
					}
				}
			});
		}

	}
	
	
	
	protected void inviteUser(InvitedUser invitedUser, Drawable head) {
		//
		int addStatus = memberAdapter.addMember(invitedUser,head);
		if (addStatus==-2) {
			showToast("不能重复邀约，该用户已在邀约列表了");
		}
	}

	
	
	
	static float lastLat=-1 ,lastlng=-1 ;
	
	private Map<String, String> createDistanceMap(Map<String, String> reqMap) {
		LocationSupport locationSupport = LocationSupport
				.getLocationSupportInstance(getActivity()
						.getApplicationContext());
		if (lastLat==-1) {
			float lat = locationSupport.getLatitude();
			float lng = locationSupport.getLongtitude();
			if (lat <= 0 || lng <= 0 ) {
				//showToast("无法获取当前位置！");
				((View)relocateView.getParent()).setVisibility(View.VISIBLE);
				return null;
			}
			lastLat=lat;
			lastlng=lng;
		}
	
		((View)relocateView.getParent()).setVisibility(View.GONE);
		refreshAddress(locationSupport.getLocation(), locationSupport);
		Map<String, String> map=null;
		if (reqMap==null) {
			map = getPublicParamRequstMap();
		}else {
			map = reqMap;
		}
	
		map.put("lnt", "" + lastlng);
		map.put("lat", "" + lastLat);
		map.put("sort", "distance");
		map.put("d", "100");
		return map;
	}

	
	public void startSearch(String keyword) {
		startSearch(keyword, null);
	}

	public void startSearch(String keyword, Map<String, String> map) {
		this.keyword = keyword;
		if (map == null) {
			map = getPublicParamRequstMap();
		}
		String url =createUrl(HttpConfig.REQUST_AROUND_SPORT_USER_SEARCH_LIST_URL, map,1);
		if (url == null) {
			return ;
		}
		loadData(url, false,
				 createInitPage(), uiFreshController);
	}
	
	@Override
	protected void doFreshFromTop() {
		startLocate(getPublicParamRequstMap());
	}

	

	
	protected void startLocate(final Map<String, String> map) {
		LocationManager locationManager = new LocationManager();
		locationManager.findLocation(getActivity(), new GetLastLocationListener() {
			
			@Override
			public void onLocResult(float lat, float lont, String where) {
				if (getBaseActivity()==null||getBaseActivity().isFinishing()) {
					return;
				}
				getBaseActivity().cancelProgressDialog();
				lastLat = -1;
				lastlng=-1;
				startSearch(keyword, map);
			}
		}, true);
		
	}
	private int type = 1;//defualt  set 1 important  not set -1
	
	private boolean isDistance =false;
	
	private String createUrl(String url, Map<String, String> map,int pageNum) {

		if (TAG.equals(ConstValues.SEARCH)) {
			map.put("name", keyword );
			map.put("sort", "uptime");
		} else {

			String queryUrl = "";
			
			if (sort!=null) {
				
				if (sort.equals("distance")) {
					isDistance=true;
					Map<String, String> temp;
					if ((temp=createDistanceMap(map))!=null) {
						map=temp;
					}else {
						return null;
					}
				}else {
					isDistance=false;
				}
			}else {
					isDistance=false;
			}
			if (aroundUserAdapter!=null) {
				aroundUserAdapter.setDistanceShow(isDistance);
			}
			
			if (type > -1) {
				keyword = "";
				map.put("type", ""+type);
			}
			
			if (SportId>-1) {
				keyword = "";
				map.put("sporttype", ""+SportId);
			}
			if (sexId>-1) {
				keyword = "";
				map.put("sex", ""+sexId);
			}
			
		}
		
		map.put("pagenum", ""+numPerPage);
		map.put("pageno", ""+pageNum);
		
		url = HttpUtil.getRequestMapUrlStr(url, map);
		return url;
	}

	private View categorySportChoose;
	
	
	TextView relocateView;
	
	GridView addMemberView;
	private AddMemberAdapter memberAdapter;
	private View addMemberLayout;
	@Override
	protected   void initViews(View viewGroup) {
		// TODO Auto-generated method stub
		super.initData();
		mListView = (MyList2View) viewGroup
				.findViewById(R.id.around_list_view);

		mListView.setOnItemClickListener(this);
		initListAdapter(aroundUserAdapter, uiFreshController);
		
		if (TAG.equals(ConstValues.SEARCH)) {
			mListView.setMode(Mode.DISABLED);
			return;
		}
		
		
		relocateView=(TextView) viewGroup.findViewById(R.id.relocate_view);
		final View emptyView = viewGroup.findViewById(R.id.list_empty);
		addMemberView=(GridView) viewGroup.findViewById(R.id.gv_add_member);
		addMemberLayout = viewGroup.findViewById(R.id.around_add_user_layout);
		memberAdapter=new AddMemberAdapter(getBaseActivity());
		addMemberView.setAdapter(memberAdapter);
		memberAdapter.setOnMemberChangedListener(new OnMemberChangedListener(){
			@Override
			public void onMemberChanged(){
				if (memberAdapter.getInviedCount()<=0) {
					addMemberLayout.setVisibility(View.GONE);
				}else {
					addMemberLayout.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void startMemberSport(List<InvitedUser> uInvitedUsers) {
				Bundle extra = new Bundle();
				extra.putInt("flag", 2);
				int sportType =uInvitedUsers.get(0).getMylike();
				Log.i(TAG, "userLike = "+sportType);
				extra.putInt("sportType", sportType);
				extra.putParcelableArrayList("invite_members", (ArrayList<InvitedUser>)uInvitedUsers);
				Util.createSportEvent(getActivity(),null,extra);
				memberAdapter.clear();
				memberAdapter.notifyDataSetChanged();
				onMemberChanged();
				
			}
		});
		
		
		
		
		relocateView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!LogInController.IsLogOn()) {

					Util.go2LogInDialog(getBaseActivity(), new OnLogOnListener() {
						@Override
						public void logon() {
							emptyView.setVisibility(View.GONE);
						startLocate(getPublicParamRequstMap());}
					});
				return;
				
				}
				emptyView.setVisibility(View.GONE);
				startLocate(getPublicParamRequstMap());
			}
		});
		
		emptyView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//检查是否定位成功
				Map<String, String> map = getPublicParamRequstMap();
				startSearch(keyword, map);
				getBaseActivity().showProgressDialog("努力加载中", "请稍后...", null);
			}
		});
		setEmptyView(emptyView);
		
		String address= LocationSupport
				.getLocationSupportInstance(getActivity().getApplicationContext()).getLocation();
		if (address!=null) {
			 refreshAddress(address, null);
		}
		/**
		 * 筛选按钮
		 * */
		viewGroup.findViewById(R.id.filter).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (!LogInController.IsLogOn()) {
					Util.go2LogInDialog(getBaseActivity(), new OnLogOnListener() {
						@Override
						public void logon() {
							FliterDialog fliterDialog = new FliterDialog();
							fliterDialog.showFilterDialog(getBaseActivity(), new onFilerSelectedListener() {
								
								@Override
								public void onItemSelect(Category sex, Category sport) {
								
									getBaseActivity().showProgressDialog("正在查询", "请稍候....", null);
									doSearch(sex,sport);
								}
							},sexId,SportId);
						}
					});
				return;
				}
				
				FliterDialog fliterDialog = new FliterDialog();
				fliterDialog.showFilterDialog(getBaseActivity(), new onFilerSelectedListener() {
					
					@Override
					public void onItemSelect(Category sex, Category sport) {
					
						getBaseActivity().showProgressDialog("正在查询", "请稍候....", null);
						doSearch(sex,sport);
					}
				},sexId,SportId);
			}
		});
	}

	private int sexId=-1,SportId=-1;
	
	private void doSearch(Category sex, Category sport) {
		
		if (sex!=null) {
			sexId = sex.getType();
		}
		if (sport!=null) {
			SportId=sport.getType();
		}
		startSearch(keyword);
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (LogInController.IsLogOn()) {
			((View)relocateView.getParent()).setVisibility(View.GONE);
		}else {
			((View)relocateView.getParent()).setVisibility(View.VISIBLE);

		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (aroundUserAdapter != null) {
			outState.putParcelableArrayList(AROUND_USER,
					(ArrayList<InvitedUser>) aroundUserAdapter.getObjectInfos());
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
//		// TODO Auto-generated method stub
	
//		Object object = parent.getAdapter().getItem(position);
//		if (object == null) {
//			return;
//		}
//		InvitedUser invitedUser = (InvitedUser) object;
//		Util.go2SeeUserHomePage(getBaseActivity(), invitedUser.getId());
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		
		super.onDestroy();
	}

	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			((View)relocateView.getParent()).setVisibility(View.GONE);

				// refreash
			WebAround invitedUsers = (WebAround) content;
				int size = invitedUsers.getList().size();
				if (size==0) {
					Log.i(TAG, "fuck  get 0 size");
				
				}
				if (page.isLoadMore() != Page.CLEAR) {
					page.setLoadMore(size < numPerPage ? Page.NO_MORE
							: Page.LOAD_MORE);
				}
				
				if (page.isLoadMore()==Page.CLEAR) {
					clearAdapter();
				}
				
				refreshList(invitedUsers.getList(), page);

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebAround>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			try {
				if(errorCode>-1)
				showToast(getString(R.string.network_error_time_out));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (page != null) {
				setPrevPage(page);
			}

		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};

	@Override
	protected String setRequestLoadMoreUrl(Page page) {
		// TODO Auto-generated method stub
	
			int position = page.getCurrentPositon();
			if (position==0&&page.isLoadMore()!=Page.CLEAR) {//?
				page.setCurrentPositon(numPerPage);//
				page.setLoadMore(Page.LOAD_MORE);
				position=numPerPage;
			}else {
				position+=numPerPage;//next page
			}
	

		Map<String, String> map = getPublicParamRequstMap();
		String url = createUrl(
				HttpConfig.REQUST_AROUND_SPORT_USER_SEARCH_LIST_URL, map,position/numPerPage);
		return url;
	}

	
	private  void showCategorySport(final List<Category> categorySports) {
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow((View) (categorySportChoose.getParent()), getBaseActivity(), categorySports);
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			@Override
			public void onItemClick(int index) {
				Map<String, String> map = getPublicParamRequstMap();
			type = categorySports.get(index).getType();
			startSearch(keyword, map);
			getBaseActivity().showProgressDialog("排序", "请稍后...", null);
			CategoryAdapter.attchReourceToView((Button) categorySportChoose, categorySports.get(index),R.drawable.category_choose_trangle_state);
			
			}
		});
		categorySportChoose.setSelected(true);
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				categorySportChoose.setSelected(false);
			}
		});
	
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.category_sport: {
			List<Category> categorySports = SportType.initSportList();
			showCategorySport(categorySports);
		}
			break;

		default:
			break;
		}
	}

	@Override
	protected void refreshArea() {
		
	}

	
}
