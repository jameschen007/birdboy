package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.jameschen.birdboy.utils.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.internal.Utils;
import com.jameschen.birdboy.BindPlugActivity;
import com.jameschen.birdboy.CoachActivity;
import com.jameschen.birdboy.EditAccountInfoActivity;
import com.jameschen.birdboy.FavoritePeopleListActivity;
import com.jameschen.birdboy.FeedbackActivity;
import com.jameschen.birdboy.ImageDetailActivity;
import com.jameschen.birdboy.MainActivity;
import com.jameschen.birdboy.MessageReceiveListActivity;
import com.jameschen.birdboy.MyActivityInfo;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.Register0Activity;
import com.jameschen.birdboy.RegisterFinishActivity;
import com.jameschen.birdboy.SettingActivity;
import com.jameschen.birdboy.SlapshActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.Setting;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.Inbox;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.utils.UtilsUI;
import com.jameschen.birdboy.widget.BadgeView;
import com.jameschen.plugin.AuthBindController;
import com.jameschen.plugin.AuthUtil;
import com.jameschen.plugin.BindModel;
import com.jameschen.plugin.ShareModel;
import com.jameschen.plugin.AuthBindController.OnbindResultListener;
import com.jameschen.plugin.ShareUtil;
import com.jameschen.plugin.qq.TQq;
import com.jameschen.plugin.qq.TencentModel;
import com.jameschen.plugin.weibo.Constants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuth.AuthInfo;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.widget.LoginButton;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;

public class AccountFragment extends LoginFragment {
	protected static final int EDIT_ACCOUNT = 0x17;
	private static final int READ_MESSAGE = 0x12;
	private View feedbackBtn;
	private TextView nickname,sign, favPeople, receiveMsg, activity;
	private View settingBtn;
	private ImageView photo;
	private Button createSportBtn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (LogInController.IsLogOn()) {
			currentLogged = true;
			View view = inflater.inflate(R.layout.account, null, false);
			initviewAccountViews(view);
			return view;
		} else {
			currentLogged = false;
			return super.onCreateView(inflater, container, savedInstanceState);
		}

	}
	
	private DisplayImageOptions	roundOptions;
	
	private BadgeView numView;
	private ImageView mWeiBoLbind,qqBind;
	private void initviewAccountViews(View view) {
		nickname = (TextView) view.findViewById(R.id.nickname);
		favPeople = (TextView) view.findViewById(R.id.fav_people);
		receiveMsg = (TextView) view.findViewById(R.id.rev_msg);
		activity = (TextView) view.findViewById(R.id.activity);
		feedbackBtn =  view.findViewById(R.id.feedback);
		settingBtn = (View) view.findViewById(R.id.btn_setting);
		createSportBtn = (Button) view.findViewById(R.id.create_sport);
		photo =(ImageView) view.findViewById(R.id.head_logo);
		sign = (TextView) view.findViewById(R.id.sign);
		numView=(BadgeView)view.findViewById(R.id.num);
	
     
        mWeiBoLbind = (ImageView) view.findViewById(R.id.weibo_login_button);
       
         qqBind = (ImageView) view.findViewById(R.id.qq_login_button);
         checkBindStatus();
		view.findViewById(R.id.account_info).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				 Intent intent =new Intent(getActivity(),
							EditAccountInfoActivity.class);
					startActivityForResult(intent,EDIT_ACCOUNT);			
			}
		});
		
	view.findViewById(R.id.find_coach_layout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				 Intent intent =new Intent(getActivity(),
							CoachActivity.class);
					startActivity(intent);			
			}
		});
		
		view.findViewById(R.id.bind_container).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Util.go2Bind(AccountFragment.this);
		}
	});
	
	createSportBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.createSportEvent(getActivity(),null);
			}
		});
		final Account account =LogInController.currentAccount;
		nickname.setText(account.getNick());
		CharSequence text=getString(R.string.no_sign);
		if (!TextUtils.isEmpty(account.getSignstr())) {
			text =account.getSignstr();
		}
		sign.setText(text);
		int defaultResImg=R.drawable.icon_default;
		if (account.getSex()==1) {//girl
			
		}else {
			
		}
		roundOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(defaultResImg)
		.showImageForEmptyUri(defaultResImg)
		.showImageOnFail(defaultResImg).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(20))
		.cacheOnDisc(true).build();
		
		photo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (!TextUtils.isEmpty(account.getLogo())) {
					Intent intent=new Intent(getBaseActivity(),ImageDetailActivity.class);
					ArrayList<String> phoneList = new ArrayList<String>();
					String blogoUrl = account.getLogo().replace("slogo", "blogo");
					phoneList.add(HttpConfig.HTTP_BASE_URL+blogoUrl);
					intent.putStringArrayListExtra("photos", phoneList);
					//phoneList
					startActivity(intent);
				}
			}
		});
		
		getBaseActivity().loadImage(HttpConfig.HTTP_BASE_URL+ account.getLogo(), photo, roundOptions);
		settingBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), SettingActivity.class);
				MainActivity activity = (MainActivity) getBaseActivity();
				if (activity.version!=null) {
					intent.putExtra("ver", activity.version);
				}
				startActivityForResult(intent, Setting.REQUEST_SETTING);
			}
		});
		feedbackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				startActivity(new Intent(getActivity(), FeedbackActivity.class));

			}
		});
		view.findViewById(R.id.activity_layout).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(getActivity(), MyActivityInfo.class));

			}
		});
		
		view.findViewById(R.id.fav_people_layout).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(getActivity(), FavoritePeopleListActivity.class));

			}
		});
		
		view.findViewById(R.id.revmsg_layout).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(getActivity(),
						MessageReceiveListActivity.class),READ_MESSAGE);
				//set to 0
				setUnreadMsgNum(receiveMsg, 0);
			}
		});
		/**
		 * get the unread inbox message number
		 * */
		getDataLoader(HttpConfig.REQUST_INBOX_UNREAD_NUM_URL, false, new Page(), unreadInboxNumController, new HttpClientStack(LogInController.getHttpClientInstance()));
	}


	
	private void checkBindStatus() {
		if (LogInController.currentAccount==null) {
			return;
		}
		  if (LogInController.currentAccount.hasWeibo()) {
	 			mWeiBoLbind.setImageResource(R.drawable.weibo);
	 		 }else {
	 				mWeiBoLbind.setImageResource(R.drawable.weibo_disable);
	 	
	 		}
	         if (LogInController.currentAccount.hasQq()) {
	        	 qqBind.setImageResource(R.drawable.qq);
	 		 }else {
	 			 qqBind.setImageResource(R.drawable.qq_disable);
	 	
	 		}		
	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case Setting.REQUEST_SETTING: {
			if (resultCode == Setting.RESULT_SETTING_LOGOUT) {
				LogInController.setHttpClientNull();
				currentLogged = false;
				resetInitViews();
				//((MainActivity) getBaseActivity()).onNavigateItemSelected(0);// homepage
			} else if (resultCode == Setting.RESULT_SETTING_UPDATE_PASSWORD) {
				Register0Activity.removeUserPasswordFromPreference(getActivity());
				LogInController.setHttpClientNull();
				currentLogged = false;
				resetInitViews();
			}
		}
			break;
		case EDIT_ACCOUNT:
			if (resultCode==Activity.RESULT_OK) {
				if (LogInController.currentAccount==null) {
					return;
				}
				initviewAccountViews(getView());
			}
			break;
			
		case BindPlugActivity.REQ_BIND:
			
			if (resultCode==Activity.RESULT_OK) {
				checkBindStatus();
			}
			break;
		case READ_MESSAGE:
		{
			if (resultCode==MessageReceiveListActivity.Read_Msg) {
				/**
				 * get the unread inbox message number
				 * */
				//getDataLoader(HttpConfig.REQUST_INBOX_UNREAD_NUM_URL, false, new Page(), unreadInboxNumController, new HttpClientStack(LogInController.getHttpClientInstance()));
			}
		}
			break;
			
		default:
			break;
		}
		
		
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if (LogInController.IsLogOn()) {
			SlapshActivity.IsNeedCheckedGuidePage(getBaseActivity(), ConstValues.ACCOUNT);

		}
	}

	
	private void setUnreadMsgNum(View targetView,int unreadMessgeCount) {
		if (targetView==null) {
			return ;
		}
		if (unreadMessgeCount>0) {
			numView.setVisibility(View.VISIBLE);
		}else {
			numView.setVisibility(View.GONE);
		}
		numView.setBadgeCount(unreadMessgeCount);

	}
	
	
	private UIController unreadInboxNumController = new UIController() {
	
	@Override
	public void responseNetWorkError(int errorCode, Page page) {
		
	}
	
	@Override
	public <E> void refreshUI(E content, Page page) {
		Inbox inbox = (Inbox) content;
		int unreadMessgeCount = inbox.getNum();
		if (unreadMessgeCount>0) {
			if (getBaseActivity()==null||getBaseActivity().isFinishing()) {
				return;
			}
			
			setUnreadMsgNum(receiveMsg, unreadMessgeCount);
		}
	}
	
	@Override
	public String getTag() {
		return TAG+"unreadInboxNum";
	}
	
	@Override
	public Type getEntityType() {
		return new TypeToken<WebBirdboyJsonResponseContent<Inbox>>() {
		}.getType();
	}
};	
	
}
