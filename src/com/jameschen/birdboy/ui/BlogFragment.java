package com.jameschen.birdboy.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.utils.Log;
public class BlogFragment extends BaseFragment implements OnClickListener{
	
	private static final String TAG="BlogFragment";
	/**
	 *  this webView is for show starhub homepage
	 * */
	private WebView webSiteView;
	/**
	 * loading web page progress
	 * */
	private ProgressBar loadingBar;
	private String webSite="http://birdboy.cn/blog/";
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.blog, container, false);
	   (webSiteView= (WebView) view.findViewById(R.id.web_site)).setOnClickListener(this);
	   loadingBar =(ProgressBar) view.findViewById(R.id.loading_bar);
	   webSiteView.getSettings().setJavaScriptEnabled(true);  
	   webSiteView.getSettings().setLoadsImagesAutomatically(true);//auto load images
	   webSiteView.getSettings().setSupportZoom(true);
	   webSiteView.getSettings().setBuiltInZoomControls(true);//zoom
	   webSiteView.getSettings().setUseWideViewPort(true); //auto adjust screen
	   webSiteView.getSettings().setLoadWithOverviewMode(true); 
	   webSiteView.setWebViewClient(new GlobalWebViewClient());
	   loadingFirstTime = true;
		return view;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		webSiteView.loadUrl(webSite);
	}
boolean  loadingFirstTime=true;
	/**
	 *  webClient is for deal some loading state
	 *
	 */
	private class GlobalWebViewClient extends WebViewClient { 
		
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);
			//show  loading image
		//	loadingBar.setVisibility(View.VISIBLE);
		}
	
		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
		 // hidden loading image
			loadingBar.setVisibility(View.GONE);
		}
		
	    @Override 
	    // 在WebView中而不是默认浏览器中显示页面
	    public boolean shouldOverrideUrlLoading(WebView view, String url) { 
	        view.loadUrl(url); 
	        return true; 
	    } 
	}
	@Override
	public void onPause() {
	
		super.onPause();
	}
	public WebView getWebView(){
		return webSiteView;
	}
	
	@Override
	public void onResume() {
		
		super.onResume();
		
		
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		webSiteView.destroy();
	}


 
	
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.web_site:
		{}
			break;


		default:
			break;
		}

	}
@Override
public void onHiddenChanged(boolean hidden) {
	// TODO Auto-generated method stub
	super.onHiddenChanged(hidden);

	if (webSiteView==null) {
		return;
	}
	if (!hidden) {
		if (loadingFirstTime) {
			webSiteView.loadUrl(webSite);
			loadingFirstTime=false;
		}
		   webSiteView.resumeTimers();
		 
	}else {
		  webSiteView.pauseTimers();
	}

}


}
