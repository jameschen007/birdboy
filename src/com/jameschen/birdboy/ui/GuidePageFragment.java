package com.jameschen.birdboy.ui;

import java.io.InputStream;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jameschen.birdboy.MainActivity;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SlapshActivity;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.SoftwareInfo;

public class GuidePageFragment extends BaseFragment {
	Context context;
	int position;
	private int[] ResoureID =null;// new int[] { R.raw.guide1, R.raw.guide2,
		//	R.raw.guide3, R.raw.guide4, R.raw.guide5 };

	public static Fragment newInstance(int position) {
		GuidePageFragment guidePageFragment = new GuidePageFragment();
		guidePageFragment.position = position;
		return guidePageFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.guide_enter_ui, container, false);
		if (position == SlapshActivity.NUM_ITEMS - 1) {
			((ImageView) view.findViewById(R.id.guide_img))
		.setImageBitmap(readBitMap(getActivity(), ResoureID[position]));

//			view.findViewById(R.id.start_btn).setVisibility(View.VISIBLE);
//			view.findViewById(R.id.start_btn).setOnClickListener(
//					new OnClickListener() {
//
//						@Override
//						public void onClick(View v) {
//							SharedPreferences cfg = context
//									.getSharedPreferences(
//											SoftwareInfo.SharedName, 0);
//							cfg.edit().putBoolean(SoftwareInfo.guide, false).commit();
//
//							startMain();
//
//						}
//					});
		} else {
			try {
				((ImageView) view.findViewById(R.id.guide_img))
				.setImageBitmap(readBitMap(getActivity(), ResoureID[position]));

			} catch (Exception e) {
				// TODO: handle exception
			}
			}

		return view;
	}
	   public static Bitmap readBitMap(Context context, int resId){  
		        BitmapFactory.Options opt = new BitmapFactory.Options();  
		        opt.inPreferredConfig = Bitmap.Config.RGB_565;   
		       opt.inPurgeable = true;  
		       opt.inInputShareable = true;  
		          //获取资源图片  
		      InputStream is = context.getResources().openRawResource(resId);  
		           return BitmapFactory.decodeStream(is,null,opt);  
		   }

	private void startMain() {
		Intent intent = new Intent(getActivity(), MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		getActivity().overridePendingTransition(R.anim.welcome_begin_anim,
				R.anim.welcome_end_anim);
		startActivity(intent);
		getActivity().finish();

	}
	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

}
