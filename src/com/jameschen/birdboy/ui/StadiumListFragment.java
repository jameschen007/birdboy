package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.R.integer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow.OnDismissListener;

import com.actionbarsherlock.internal.widget.IcsListPopupWindow;
//import com.espian.showcaseview.ShowcaseView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SearchActivity;
import com.jameschen.birdboy.StadiumDetailActivity;
import com.jameschen.birdboy.adapter.CategoryAdapter;
import com.jameschen.birdboy.adapter.ChildAreaAdapter;
import com.jameschen.birdboy.adapter.StadiumAdapter;
import com.jameschen.birdboy.base.BaseListFragment;
import com.jameschen.birdboy.base.BaseActivity.LoadMode;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.environment.ConstValues.Setting;
import com.jameschen.birdboy.location.LocationManager;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.location.LocationUtil;
import com.jameschen.birdboy.location.LocationManager.GetLastLocationListener;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.QueryBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebChildAreas;
import com.jameschen.birdboy.model.WebDistanceStadiums;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.model.entity.City;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.FliterOptionItem;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.provider.ChildAreaHelper;
import com.jameschen.birdboy.provider.DBContent;
import com.jameschen.birdboy.utils.AreaMap;
import com.jameschen.birdboy.utils.AreaSelectPopupWindow;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.OptionsPopupWindow;
import com.jameschen.birdboy.utils.PopupWindowUtil;
import com.jameschen.birdboy.widget.MyListView;

public class StadiumListFragment extends BaseListFragment<Stadium> implements
		OnItemClickListener, OnClickListener {

	public static final String STADIUM = ConstValues.STADIUM;
	public static long areaGetTime;
	private String keyword = "*:*";

	public static StadiumListFragment newInstance(String search, int type,
			ArrayList<Stadium> mStadiums) {
		StadiumListFragment fragment = new StadiumListFragment();
		fragment.keyword = search;
		fragment.mInfos = mStadiums;
		return fragment;
	}
	PopupWindowUtil<City>cityPopupWindow = new PopupWindowUtil<City>();
	private StadiumAdapter gymnasiumAdapter;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {

			if (savedInstanceState.containsKey(STADIUM)) {
				ArrayList<Stadium> gymnasiums = savedInstanceState
						.getParcelableArrayList(STADIUM);
				if (gymnasiums != null)
					mInfos = gymnasiums;
			}
			// restore..adapter data...
		}
		// areas = AreaMap.initAreaList(getActivity());
	}

	private List<Area> areas = new ArrayList<Area>();
	protected AreaSelectPopupWindow areaPopupWindow = new AreaSelectPopupWindow();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = null;
		if (TAG.equals(ConstValues.SEARCH)) {
			view = inflater.inflate(R.layout.stadium_list, container, false);
		} else {
			view = inflater.inflate(R.layout.stadium_ui, container, false);
		}
		gymnasiumAdapter = new StadiumAdapter(getBaseActivity());
		initViews(view);
		checkCityChoose(city);
		return view;
	}

	
	//for check city last choose.
	private void checkCityChoose(TextView textView) {
			if (textView==null) {
			return ;
			}
			SharedPreferences settingPreferences = getActivity().getSharedPreferences(Setting.SharedName, 0);
			int id =settingPreferences.getInt(Setting.cityId, LocationUtil.cityIds[0]);
			setCityId(id);
			City city =LocationUtil.getCurrentCityById(id);
			textView.setText(city.getName());
	}
		
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	
		if (mInfos != null) {
			gymnasiumAdapter.setObjectList(mInfos);
		}

		if (TAG.equals(ConstValues.SEARCH)) {
			startSearch(keyword);
		} else {
			refreshArea();
			//loadDataLocation();
			((Button) categoryScoreChoose).setText("评分最高");
			Map<String, String> reqMap = getPublicParamRequstMap();
			sort="scores desc";
			String requrl = createUrl(HttpConfig.REQUST_STADIUM_SEARCH_URL, reqMap);
			initDataLoaderFromNativeCache(requrl, LoadMode.SAVE_CACHE, uiFreshController);
		}
	}

	@Override
	protected void refreshArea() {
		Map<String, String> map = getPublicParamRequstMap();
		areas.clear();
		// load area..
		map.put("type", "" + 1);
		if (type>-1) {
			map.put("typedesc", "" + type);
			String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_SERACH_AREA_URL, map);
			getDataLoader(url, false, new Page(), areaUIController);
		}else {
			String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_SERACH_AREA_URL, map);
			initDataLoaderFromNativeCache(url, LoadMode.SAVE_CACHE,
					areaUIController,1000*3600);	
		}
		
			
	}


	public void loadDataLocation() {
		((Button) categoryScoreChoose).setText("离我最近");
		loadDataByDistance(true,1, createInitPage());
	}

	public void startSearch(String keyword) {
		// TODO Auto-generated method stub
		startSearch(keyword, null);

	}

	public void startSearch(String keyword, Map<String, String> map) {
		// TODO Auto-generated method stub
		this.keyword = keyword;
		clearAdapter();
		if (map == null) {
			map = getPublicParamRequstMap();
		}
		loadData(createUrl(HttpConfig.REQUST_STADIUM_SEARCH_URL, map), false,
				null, uiFreshController);

	}

	private int area = -1, childArea = -1, type = -1, option = 0;

	private String createUrl(String url, Map<String, String> map) {
		if (TAG.equals(ConstValues.SEARCH)) {
			map.put("q", "name:*" + keyword + "*");
		} else {

			String queryUrl = "";
			if (sort!=null) {
				isDistance=false;
				keyword=map.put("sort", sort);
			}else {
				((Button) categoryScoreChoose).setText("最新发布");
			}
			
			if (area > -1) {
				keyword = "";
				queryUrl = parseParam(queryUrl, "positionarea", area + "",true);

			}

			if (childArea > -1) {
				keyword = "";
				queryUrl = parseParam(queryUrl, "childarea", childArea + "");
			}
			if (type > -1) {
				keyword = "";
				queryUrl = parseParam(queryUrl, "typedesc", type + "");
			}

			if (option > 0) {
				keyword ="";
				queryUrl = parseParam(queryUrl, "options", option + "");
			}
			//for city.
			int cityId = Integer.valueOf(map.get("cityId"));
			if (cityId>0&&cityId!=LocationUtil.cityIds[0]) {
				queryUrl = parseParam(queryUrl, "cityId", cityId + "");
			}
			
			if (TextUtils.isEmpty(queryUrl)) {
				map.put("q", "*:*");
			} else {
				map.put("q", queryUrl);
			}

		}
		map.put("rows", numPerPage + "");
		map.put("wt", "json");
		map.put("indent", "true");
		url = HttpUtil.getRequestMapUrlStr(url, map);
		return url;
	}

	private View fliterOptionItemChoose, categoryAreaChoose,
			categoryScoreChoose;
	

	@Override
	protected   void initViews(View viewGroup) {
		// TODO Auto-generated method stub
		super.initData();
		mListView = (MyListView) viewGroup
				.findViewById(R.id.gymnasium_list_view);

		initListAdapter( gymnasiumAdapter, uiFreshController);
		mListView.setOnItemClickListener(this);
		if (TAG.equals(ConstValues.SEARCH)) {
			mListView.setMode(Mode.DISABLED);
			return;
		}
		city = (TextView) viewGroup.findViewById(R.id.city);
		weatherTv = (TextView) viewGroup.findViewById(R.id.weather);
		city.setOnClickListener(this);
		View emptyView = viewGroup.findViewById(R.id.list_empty);
		emptyView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Map<String, String> map = getPublicParamRequstMap();
				startSearch(keyword, map);
				getBaseActivity().showProgressDialog("努力加载中", "请稍后...", null);
			}
		});
		setEmptyView(emptyView);
		fliterOptionItemChoose = viewGroup.findViewById(R.id.category_sport);
		fliterOptionItemChoose.setOnClickListener(this);
		categoryAreaChoose = viewGroup.findViewById(R.id.category_area);
		categoryAreaChoose.setOnClickListener(this);
		categoryScoreChoose = viewGroup.findViewById(R.id.category_score);
		categoryScoreChoose.setOnClickListener(this);
		(viewGroup.findViewById(R.id.btn_filter))
				.setOnClickListener(this);
		viewGroup.findViewById(R.id.btn_search).setOnClickListener(this);
		//final LocationManager locationManager = new LocationManager();
		 locationView = (TextView) viewGroup
				.findViewById(R.id.location);
		String address= LocationSupport
			.getLocationSupportInstance(getActivity().getApplicationContext()).getLocation();
		if (address!=null) {
			 refreshAddress(address, null);
		}
		
//		locationManager.findLocation(getActivity(), locationView);
//		viewGroup.findViewById(R.id.refresh_location).setOnClickListener(
//				new OnClickListener() {
//
//					@Override
//					public void onClick(View arg0) {
//						// TODO Auto-generated method stub
//
//						locationView.setText("正在定位...");
//						locationManager.cancel();
//						locationManager.findLocation(getActivity(),
//								locationView);
//					}
//				});

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (gymnasiumAdapter != null) {
			outState.putParcelableArrayList(STADIUM,
					(ArrayList<Stadium>) gymnasiumAdapter.getObjectInfos());
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(getActivity(), StadiumDetailActivity.class);
		Object object = parent.getAdapter().getItem(position);
		if (object == null) {
			return;
		}
		Parcelable p = (Stadium) (object);
		intent.putExtra(STADIUM, p);
		startActivity(intent);
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	private void showOptions(View view) {
		// TODO Auto-generated method stub
		final List<FliterOptionItem> optionsLists = OptionFilter.initList();

		OptionsPopupWindow optionsPopupWindow = new OptionsPopupWindow();
		optionsPopupWindow.showActionWindow(view, getBaseActivity(),
				optionsLists);
		optionsPopupWindow
				.setItemOnClickListener(new OptionsPopupWindow.OnItemClickListener() {

					@Override
					public void onItemClick(int index) {

					}
				});
		optionsPopupWindow
				.setOnDismissListener(new OptionsPopupWindow.OnDismissListener() {

					@Override
					public void onDismiss(int optionValue) {
						// TODO Auto-generated method stub
						option = optionValue;
						Log.i(TAG, "options values==" + optionValue);
						Map<String, String> map = getPublicParamRequstMap();
						startSearch(keyword, map);
						getBaseActivity().showProgressDialog("排序", "请稍后...",
								null);
					}
				});
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
	
		if (!hidden) {
			checkCityChoose(city);
		}
	}
	
	private void showChildArea(final List<ChildArea> childAreas,
			final Area mArea) {
		areaPopupWindow.getChildListView().setAdapter(
				new ChildAreaAdapter(getActivity(), childAreas));
		areaPopupWindow.showChildArea();
		areaPopupWindow.getChildListView().setOnItemClickListener(
				new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						Map<String, String> map = getPublicParamRequstMap();
						area = mArea.getId();
						childArea = childAreas.get(arg2).getId();
						isDistance=false;
						startSearch(keyword, map);
						getBaseActivity().showProgressDialog("排序", "请稍后...",
								null);
						areaPopupWindow.dismiss();
						if (childArea == -1) {
							((Button) categoryAreaChoose).setText(mArea
									.getName());
						} else {
							((Button) categoryAreaChoose).setText(childAreas
									.get(arg2).getName());
						}

					}

				});

	}

	private void showCategorySort(final List<Category> categorySorts) {
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow(
				(View) (categoryScoreChoose.getParent()), getBaseActivity(),
				categorySorts);

		customSelectPopupWindow
				.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {

					@Override
					public void onItemClick(int index) {
						Category mItem = categorySorts.get(index);
						((Button) categoryScoreChoose).setText(mItem.getName());
						if (index == 1) {
							startLocate();
							return;
						}
						Map<String, String> map = getPublicParamRequstMap();
						sort = mItem.getSortParam();
						startSearch(keyword, map);
						getBaseActivity().showProgressDialog("排序", "请稍后...",
								null);
					}
				});
		categoryScoreChoose.setSelected(true);

		customSelectPopupWindow
				.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

					@Override
					public void onDismiss() {
						// TODO Auto-generated method stub
						categoryScoreChoose.setSelected(false);
					}
				});

	}

	protected void startLocate() {
		//reset area to all
		this.area = -1;
		childArea = -1;
		((Button) categoryAreaChoose).setText("全部区域");
		
		LocationManager locationManager = new LocationManager();
		getBaseActivity().showProgressDialog("定位", "正在定位..", null);

		locationManager.findLocation(getActivity(), new GetLastLocationListener() {
			
			@Override
			public void onLocResult(float lat, float lont, String where) {
				if (getBaseActivity()==null||getBaseActivity().isFinishing()) {
					return;
				}
				getBaseActivity().cancelProgressDialog();
				loadDataByDistance(true,1, createInitPage());
			}
		}, true);
		
	}
	private void showCategorySport(final List<Category> fliterOptionItems) {

		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow(
				(View) (fliterOptionItemChoose.getParent()), getBaseActivity(),
				fliterOptionItems);

		customSelectPopupWindow
				.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {

					@Override
					public void onItemClick(int index) {
						Map<String, String> map = getPublicParamRequstMap();
						type = fliterOptionItems.get(index).getType();
						startSearch(keyword, map);
						getBaseActivity().showProgressDialog("排序", "请稍后...",
								null);
						CategoryAdapter.attchReourceToView((Button) fliterOptionItemChoose, fliterOptionItems.get(index),R.drawable.category_choose_trangle_state);
						//refresh stadium
						map = getPublicParamRequstMap();
						map.put("type", "" + 1);
						map.put("typedesc", "" + type);
						String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_SERACH_AREA_URL, map);
						Page page =  createInitPage();
						page.setCurrentPositon(type);
						getDataLoader(url, false, page, areaUIController);
					}
					// area selet

				});
		fliterOptionItemChoose.setSelected(true);

		customSelectPopupWindow
				.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

					@Override
					public void onDismiss() {
						// TODO Auto-generated method stub
						fliterOptionItemChoose.setSelected(false);
					}
				});

	}

	ChildAreaHelper childAreaHelper;

	private UIController areaUIController = new UIController() {

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			areas.clear();
		}
		Object locker = new Object();
		@Override
		public <E> void refreshUI(E content, Page page) {

			areas.clear();
			List<Area> areaLists = null;
			if (content!=null) {
				areaLists = new ArrayList<Area>( (List<Area>) content);
			}
				if (areaLists!=null) {
					Area area = new Area();
					area.setId(-1);
					area.setName("全部区域");
					int count = 0;
					for (Area areaItem : areaLists) {
						count += areaItem.getCount();
					}
					area.setCount(count);
					areas.addAll(areaLists);
					areas.add(0, area);
				}	
						
			// List<ChildArea> childAreaLists = childAreas.getClist();
			// if (childAreaLists.size()>0) {
			// ChildArea allChildArea = new ChildArea();
			// allChildArea.setId(-1);
			// allChildArea.setName("全部");
			// int count =0;
			// for (ChildArea childArea : childAreaLists) {
			// count+=childArea.getCount();
			// }
			// allChildArea.setCount(count);
			// childAreaLists.add(0, allChildArea);
			// }
			//
			// showChildArea(childAreaLists,page.getCurrentPositon());
			//
			// //save data to database
			// if (childAreaLists.size()>0) {
			// DBContent dbContent = new DBContent();
			// dbContent.setId(page.getCurrentPositon());
			// Gson gson =new Gson();
			// Type lisType = new TypeToken<ArrayList<ChildArea>>(){}.getType();
			// dbContent.setContent(gson.toJson(childAreaLists,lisType));
			// if (childAreaHelper!=null) {
			// childAreaHelper.save(dbContent);
			// }
			//
			// }
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "staduim_Area";
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<List<Area>>>() {
			}.getType();
		}
	};
	// private UIController childAreaUIController = new UIController() {
	//
	// @Override
	// public void responseNetWorkError(int errorCode, Page page) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public <E> void refreshUI(E content, Page page) {
	// // TODO Auto-generated method stub
	// WebChildAreas childAreas = (WebChildAreas) content;
	// List<ChildArea> childAreaLists = childAreas.getClist();
	// if (childAreaLists.size()>0) {
	// ChildArea allChildArea = new ChildArea();
	// allChildArea.setId(-1);
	// allChildArea.setName("全部");
	// int count =0;
	// for (ChildArea childArea : childAreaLists) {
	// count+=childArea.getCount();
	// }
	// allChildArea.setCount(count);
	// childAreaLists.add(0, allChildArea);
	// }
	//
	// showChildArea(childAreaLists,page.getCurrentPositon());
	//
	// //save data to database
	// if (childAreaLists.size()>0) {
	// DBContent dbContent = new DBContent();
	// dbContent.setId(page.getCurrentPositon());
	// Gson gson =new Gson();
	// Type lisType = new TypeToken<ArrayList<ChildArea>>(){}.getType();
	// dbContent.setContent(gson.toJson(childAreaLists,lisType));
	// if (childAreaHelper!=null) {
	// childAreaHelper.save(dbContent);
	// }
	//
	// }
	// }
	//
	// @Override
	// public String getTag() {
	// // TODO Auto-generated method stub
	// return "staduim_childArea";
	// }
	//
	// @Override
	// public Type getEntityType() {
	// // TODO Auto-generated method stub
	// return new TypeToken<WebBirdboyJsonResponseContent<WebChildAreas>>() {
	// }.getType();
	// }
	// };

	private UIController distanceUIFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			//if (!isDetached()) {
				// refreash
				WebDistanceStadiums stadiumLists = (WebDistanceStadiums) content;
				page.setTag(ConstValues.DISTANCE);
				int size =stadiumLists.getBuildings().size();
				if (page.isLoadMore()!=Page.CLEAR) {
					page.setLoadMore(size<numPerPage?Page.NO_MORE:Page.LOAD_MORE);
				}
				refreshList(stadiumLists.getBuildings(), page);
		//	}

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebDistanceStadiums>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			try {
				if(errorCode>-1)
				showToast(getString(R.string.network_error_time_out));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (page != null) {
				setErrorPage(errorCode,page);
			}
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG + "dis";
		}
	};
	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
		//	if (!isDetached()) {
				// refreash
				refreshList(content, page);
		//	}

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<QueryBirdboyJsonResponseContent<Stadium>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			showToast(getString(R.string.network_error_time_out));
			if (page != null) {
				setPrevPage(page);
			}
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};
	
	
	private void loadDataByDistance(boolean showDialog,int pageNo,Page page) {
		sort=null;
		isDistance=true;
		String url = createDistanceUrl(pageNo);
		if (TextUtils.isEmpty(url)) {
			return;
		}
		if (showDialog) {
			clearAdapter();
			getBaseActivity().showProgressDialog("已定位", "正在获取数据请稍候...", null);
		}

		if (page.isLoadMore() == Page.CLEAR) {
			loadData(url, false, page,
					distanceUIFreshController);
		}else {
			getDataLoader(url, false, page,
					distanceUIFreshController);
		}

	}
	boolean isDistance=false;
	private String createDistanceUrl(int pageNo) {
		LocationSupport locationSupport = LocationSupport
				.getLocationSupportInstance(getActivity()
						.getApplicationContext());
		float lat = locationSupport.getLatitude();
		float lng = locationSupport.getLongtitude();
		if (lat <= 0 || lng <= 0 ||locationSupport.getLocation()==null) {
			refreshAddress(null,locationSupport);
			showToast("无法获取当前位置！");
			return null;
		}
		refreshAddress(locationSupport.getLocation(), locationSupport);
		
		Map<String, String> map = getPublicParamRequstMap();
		map.put("lnt", "" + lng);
		map.put("lat", "" + lat);
		map.put("sort", "distance");
		map.put("d", "100");
		if (type>-1) {
			map.put("type", type+"");	
		}
		
		map.put("pagenum", ""+numPerPage);
		map.put("pageno", ""+pageNo);
		return HttpUtil
				.getRequestMapUrlStr("http://birdboy.cn/SearchBAPI", map);
	}

	@Override
	protected String setRequestLoadMoreUrl(Page page) {
		if (isDistance) {
			int position = page.getCurrentPositon();
			if (position==0&&page.isLoadMore()!=Page.CLEAR) {//?
				page.setCurrentPositon(numPerPage);//
				page.setLoadMore(Page.LOAD_MORE);
				position=numPerPage;
			}else {
				position+=numPerPage;//next page
				
			}
		
			loadDataByDistance(false,position/numPerPage,page);
			return ConstValues.DISTANCE;
		}
		
		int position = page.getCurrentPositon();
		Map<String, String> map = getPublicParamRequstMap();
		map.put("start", position + "");
		String url = createUrl(HttpConfig.REQUST_STADIUM_SEARCH_URL, map);
		return url;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.city:
		{
			
			
			cityPopupWindow.showActionWindow(v, getActivity(), LocationUtil.Citys);
			cityPopupWindow.setItemOnClickListener(new PopupWindowUtil.OnItemClickListener() {

				@Override
				public void onItemClick(int index) {
						City cityObj = LocationUtil.Citys.get(index);
						city.setText(cityObj.getName());
						setCityId(cityObj.getId());
						Map<String, String> map = getPublicParamRequstMap();
						startSearch(keyword, map);
						refreshArea();
						refreshWeather(cityObj, true,weatherTv);
						getBaseActivity().showProgressDialog("切换城市", "请稍候...", null);
				}
			});
		}
			break;
		case R.id.category_sport: {
			List<Category> fliterOptionItems = SportType.initSportList();
			showCategorySport(fliterOptionItems);
		}
			break;
		case R.id.btn_filter: {
			showOptions(v);
		}
			break;

		case R.id.category_area: {
			areaPopupWindow.showActionWindow(((View)v.getParent()), getBaseActivity(), areas);
			categoryAreaChoose.setSelected(true);
			areaPopupWindow
					.setItemOnClickListener(new AreaSelectPopupWindow.OnItemClickListener() {

						@Override
						public void onItemClick(int index) {
							// TODO Auto-generated method stub
							Area area = areas.get(index);
							// categoryAreaChoose.setText(area.getName());
							// 1 check from database not found then from
							// server..
							// if (childAreaHelper==null) {
							// childAreaHelper = new
							// ChildAreaHelper(getActivity());
							// }

							// DBContent childJsonContent =
							// childAreaHelper.getQuery(area.getId());
							//
							// if (childJsonContent!=null) {
							// Gson gson =new Gson();
							// Type lisType = new
							// TypeToken<List<ChildArea>>(){}.getType();
							// List<ChildArea>
							// childAreaLists=gson.fromJson(childJsonContent.getContent(),
							// lisType);
							// if (childAreaLists.size()>0) {
							// showChildArea(childAreaLists,area.getId());
							// return;
							// }
							//
							// }
							//
							if (area.getId() == -1) {// all area
								Map<String, String> map = getPublicParamRequstMap();
								StadiumListFragment.this.area = -1;
								childArea = -1;
								isDistance=false;
								startSearch(keyword, map);
								getBaseActivity().showProgressDialog("排序",
										"请稍后...", null);
								((Button) categoryAreaChoose).setText(area
										.getName());
								areaPopupWindow.dismiss();
								return;
							}
							List<ChildArea> childAreaLists = area.getChilds();
							if (childAreaLists.size() > 0) {
								if (childAreaLists.get(0).getId() != -1) {// not
																			// contains
																			// all
									ChildArea allChildArea = new ChildArea();
									allChildArea.setId(-1);
									allChildArea.setName("全部");
									int count = area.getCount();
//									for (ChildArea childArea : childAreaLists) {
//										count += childArea.getCount();
//									}
									allChildArea.setCount(count);

									childAreaLists.add(0, allChildArea);
								}
							}else {
								ChildArea allChildArea = new ChildArea();
								allChildArea.setId(-1);
								allChildArea.setName("全部");
								allChildArea.setCount(area.getCount());
								childAreaLists.add(0, allChildArea);
							}
							showChildArea(childAreaLists, area);
							// Page page =  createInitPage();
							// page.setCurrentPositon(area.getId());
							// getDataLoader(HttpConfig.REQUST_CHILD_AREA_URL
							// + area.getId(), false, page,
							// childAreaUIController);// later change true
						}
					});
			areaPopupWindow
					.setOnDismissListener(new AreaSelectPopupWindow.OnDismissListener() {

						@Override
						public void onDismiss() {
							categoryAreaChoose.setSelected(false);
						}
					});
		}
			break;

		case R.id.category_score: {
			final List<Category> menuItems = new ArrayList<Category>();
			Category menuItem = new Category();
			menuItem.setSortParam("scores desc");
			menuItem.setName("评分最高");
			menuItems.add(menuItem);
			menuItem = new Category();
			menuItem.setName("离我最近");
			menuItems.add(menuItem);
			showCategorySort(menuItems);

		}
			break;

		case R.id.btn_search: {
			Intent intent = new Intent(getActivity(), SearchActivity.class);
			intent.putExtra(ConstValues.SEARCH, ConstValues.STADIUM);
			startActivity(intent);
		}
			break;
		default:
			break;

		}
	}

	public static final class OptionFilter {

		private static HashMap<Integer, String> optionsMap = new HashMap<Integer, String>();
		// #if($op / 128==1) #set($op=$op % 128) <span title="有"
		// class="freeTag0 sp_cgmp">场馆卖品</span> #end
		// #if($op / 64==1) #set($op=$op % 64) <span title="有"
		// class="freeTag0 sp_qccz">器材租借</span> #end
		// #if($op / 32==1) #set($op=$op % 32) <span title="提供免费停车位"
		// class="freeTag0 sp_zypx">专业培训</span> #end
		//
		// #if($op / 16==1) #set($op=$op % 16) <span title="有"
		// class="freeTag0 mr5 sp_xyss">洗浴设施</span> #end
		// #if($op / 8==1) #set($op=$op % 8) <span title="有"
		// class="freeTag0 mr5 sp_xxqy">休息区域</span> #end
		// #if($op / 4==1) #set($op=$op % 4) <span title="有"
		// class="freeTag0 mr5 sp_xxqy">刷卡结算</span> #end
		// #if($op / 2==1) #set($op=$op % 2) <span title="提供免费停车位"
		// class="freeTag0 mr5 sp_tcc">停车场</span> #end
		static {
			optionsMap.put(2, "停车场");
			optionsMap.put(4, "刷卡结算");
			optionsMap.put(8, "休息区域");
			optionsMap.put(16, "淋浴设施");
			optionsMap.put(32, "专业培训");
			optionsMap.put(64, "器材租借");
			optionsMap.put(128, "场馆卖品");

		}
		private static List<FliterOptionItem> optionLists;

		public static List<FliterOptionItem> initList() {
			if (optionLists != null) {
				return optionLists;
			}
			optionLists = new ArrayList<FliterOptionItem>();

			for (Iterator it = optionsMap.entrySet().iterator(); it.hasNext();) {
				Entry entry = (Entry) it.next();
				String item = (String) entry.getValue(); // StatItem
															// 是我自己定义的一个简单的JAVABEAN
															// 对象类型
				FliterOptionItem fliterOptionItem = new FliterOptionItem();
				fliterOptionItem.setName(item);
				fliterOptionItem.setType((Integer) entry.getKey());
				optionLists.add(fliterOptionItem);
			}

			return optionLists;
		}
	}
}
