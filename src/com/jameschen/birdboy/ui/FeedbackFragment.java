package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.Map;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.FeedbackActivity;
import com.jameschen.birdboy.MainActivity;
import com.jameschen.birdboy.MyActivityInfo;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.MessageReceiveListActivity;
import com.jameschen.birdboy.Register0Activity;
import com.jameschen.birdboy.SettingActivity;
import com.jameschen.birdboy.adapter.FeedbackExpandableAdapter;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.Setting;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.WebInbox;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.utils.HttpUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

public class FeedbackFragment extends BaseFragment  {
	private int fromDetail =0;
	
	
	public static  FeedbackFragment getFeedbackFragmentInstance(int detail) {
		FeedbackFragment feedbackFragment = new FeedbackFragment();
		feedbackFragment.fromDetail =detail;
		return feedbackFragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		
		View view = inflater.inflate(R.layout.feedback, container, false);
		initViews(view);
		return view;
	}



ExpandableListView feedbackExpandableListView;


protected  void initViews(View view) {
	if (fromDetail==1) {//from detail
		view.findViewById(R.id.top_bar).setVisibility(View.GONE);
	}
	feedbackExpandableListView = (ExpandableListView)view. findViewById(R.id.feedback_expandableListView);
	feedbackExpandableListView.setAdapter(feedbackExpandableAdapter=new FeedbackExpandableAdapter(getBaseActivity(),new FeedbackExpandableAdapter.FeedbackCommitListener() {
		
		@Override
		public void done(String content, int type) {
			if (checkContent(content)) {
		/*
		 * userid int 用户id 匿名0
		 * 
		 * username String 用户名 匿名"guest"
		 * 
		 * content String 反馈内容
		 * 
		 * type int 4网站建议 1购买功能2账号问题3搜索问题
		 * 
		 * coninfo String 联系方式
		 */

		Map<String, String> map = getPublicParamRequstMap();
		 com.jameschen.birdboy.model.entity.Account account = LogInController.currentAccount;
		 if (LogInController.IsLogOn()) {
				map.put("userid", account.getId()+"");
				map.put("username",account.getNick());
		}else {
			map.put("userid", 0+"");
			map.put("username","guest");
		}
	
		map.put("content", content);
		map.put("type", type+"");
		if (account!=null) {
			map.put("coninfo", account.getMobile()+"");
		}
	
		String url = HttpUtil.getRequestMapUrlStr(
				HttpConfig.REQUST_FEEDBACK_URL, map);
		getDataLoader(url, false, null, uiFreshController);
	  getBaseActivity().showProgressDialog("意见反馈", "正在提交,请稍候...", null);
	}

}
		
	}));

	
	feedbackExpandableListView.setOnGroupExpandListener(new OnGroupExpandListener() {  
		  
        @Override  
        public void onGroupExpand(int groupPosition) {  
            // TODO Auto-generated method stub  
            for (int i = 0; i < feedbackExpandableAdapter.getGroupCount(); i++) {  
                if (groupPosition != i) {  
                	feedbackExpandableListView.collapseGroup(i);  
                }  
            }  

        }  

    });  
	feedbackExpandableListView.setOnGroupClickListener(new OnGroupClickListener() {  
		  
            @Override  
            public boolean onGroupClick(ExpandableListView parent, View v,  
                    int groupPosition, long id) {  
                // TODO Auto-generated method stub  
                if (sign== -1) {  
                    // 展开被选的group  
                	feedbackExpandableListView.expandGroup(groupPosition);  
                    // 设置被选中的group置于顶端  
                	feedbackExpandableListView.setSelectedGroup(groupPosition);  
                    sign= groupPosition;  
                } else if (sign== groupPosition) {  
                	feedbackExpandableListView.collapseGroup(sign);  
                    sign= -1;  
                } else {  
                	feedbackExpandableListView.collapseGroup(sign);  
                    // 展开被选的group  
                	feedbackExpandableListView.expandGroup(groupPosition);  
                    // 设置被选中的group置于顶端  
                	feedbackExpandableListView.setSelectedGroup(groupPosition);  
                    sign= groupPosition;  
                }  
                return true;  
            }  
        });    

}

	FeedbackExpandableAdapter feedbackExpandableAdapter;
	private boolean checkContent(CharSequence content) {
		// TODO Auto-generated method stub
		if (TextUtils.isEmpty(content)) {
			showToast("请输入内容！");
			return false;
		}
		return true;
	}

	
	private int sign= -1;//控制列表的展开  
	

	private UIController uiFreshController = new UIController() {

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub

		}

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			showToast("谢谢你的建议，我们会持续关注！");
			if (getBaseActivity() instanceof FeedbackActivity) {
				getBaseActivity().finish();
			}
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebFeedBack>>() {
			}.getType();
		}
	};

}
