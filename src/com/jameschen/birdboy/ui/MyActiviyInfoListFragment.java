package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.jameschen.birdboy.CoachDetailActivity;
import com.jameschen.birdboy.MySportDetailActivity;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.Register0Activity;
import com.jameschen.birdboy.SlapshActivity;
import com.jameschen.birdboy.SportEventDetailActivity;
import com.jameschen.birdboy.adapter.MyActivityInfoAdapter;
import com.jameschen.birdboy.adapter.MyActivityInfoAdapter.CommitActivityListener;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.base.BaseListFragment;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.ActivityInfo;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.widget.MyListView;

import android.R.id;
import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class MyActiviyInfoListFragment extends BaseListFragment<ActivityInfo> {
	private int type, position;
	private List<ActivityInfo> mActivityInfos;

	public static MyActiviyInfoListFragment newInstance(int type, int position,
			List<ActivityInfo> list) {
		MyActiviyInfoListFragment fragment = new MyActiviyInfoListFragment();
		fragment.type = type;
		fragment.position = position;
		fragment.mActivityInfos = list;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreateView(inflater, container, savedInstanceState);
		View view = null;
		view = inflater.inflate(R.layout.my_activity_list, container, false);
		initViews(view);
		return view;

	}

	MyActivityInfoAdapter activityInfoAdapter;

	@Override
	protected   void initViews(View view) {

		mListView = (MyListView) view
				.findViewById(R.id.my_activity_list_view);
		activityInfoAdapter = new MyActivityInfoAdapter(getBaseActivity());
		activityInfoAdapter.setType(position, new CommitActivityListener() {

			@Override
			public void done(int type,int id) {
				CommitDoneController commitDoneController= new CommitDoneController();
				commitDoneController.setId(type,id);
				String url = HttpConfig.REQUST_UNKEEP_EVENT_URL+id;
				if (MyActivityInfoAdapter.UNKEEP==type) {
					url = HttpConfig.REQUST_UNKEEP_EVENT_URL+id;
				}else if (MyActivityInfoAdapter.UNJIONACTION==type) {
					getBaseActivity().showProgressDialog("取消报名", "正在提交,请稍候...", null);
					url = HttpConfig.REQUST_UNJOIN_EVENT_URL+id;
				}else if (MyActivityInfoAdapter.JIONACTION==type) {
					getBaseActivity().showProgressDialog("参加报名", "正在提交,请稍候...", null);
					url = HttpConfig.REQUST_JOIN_EVENT_URL+id;
				}
				 getDataLoader(url, false, null, commitDoneController,new HttpClientStack(LogInController.getHttpClientInstance()));

			}
		});
		activityInfoAdapter.setObjectList(mActivityInfos);
		initListAdapter(activityInfoAdapter,
				uiFreshController);

		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(),
						MySportDetailActivity.class);
				Object object = arg0.getAdapter().getItem(arg2);
				if (object == null) {
					return;
				}
				ActivityInfo activityInfo = (ActivityInfo) (object);
				intent.putExtra("activity_id", activityInfo.getUactionid());
				if (position==2) {//my fav
					intent =new Intent(getActivity(),SportEventDetailActivity.class);
					intent.putExtra("activity_id", activityInfo.getUactionid());
				}else if(position==1){//create
					intent.putExtra("showPhone", activityInfo.getCheckstatus()==1);
					intent.putExtra("showOtherDetail", true);
				}else {//jion
					intent.putExtra("showPhone", activityInfo.getCheckstatus()==1);
				}
				
				
				// type...
				startActivity(intent);
			}
		});
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		TAG="MyactivityInfo";
		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("type")) {
				type = savedInstanceState.getInt("type");
			}
			if (savedInstanceState.containsKey("position")) {
				position = savedInstanceState.getInt("position");
			}
			if (savedInstanceState.containsKey("list")) {
				ArrayList<ActivityInfo> sportEvents = savedInstanceState
						.getParcelableArrayList("list");
				if (sportEvents != null)
					mActivityInfos = sportEvents;
			}
		}
		// no need clear resource....
		clear = false;
		needHttpStack = true;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putInt("type", type);
		outState.putInt("position", position);
		if (activityInfoAdapter != null) {
			outState.putParcelableArrayList("list",
					(ArrayList<ActivityInfo>) activityInfoAdapter
							.getObjectInfos());
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		//
		// 2、请求字段
		// http://birdboy.cn/myActionAPI?type=1
		//
		// 字段 类型 描述 备注 值条件
		//
		// type int 查看我参与的活动 1，我关注的活动2
		//
		// 3、返回字段
		//
		// { "success": true
		// "list":
		// }
		if (position == 0) {
			SlapshActivity.IsNeedCheckedGuidePage(getBaseActivity(), ConstValues.MY_SPORT_LIST);
			loadData();
		}

	}

	public void loadData() {
		if (activityInfoAdapter != null && activityInfoAdapter.getCount() == 0) {
			loadData(
					createUrl(1),
					false,
					createInitPage(),
					uiFreshController,
					new HttpClientStack(LogInController.getHttpClientInstance()));

		}

	}

	private String createUrl(int pageNo) {
		Map<String, String> map = getPublicParamRequstMap();
		switch (position) {
		case 0:// jion
			map.put("type", "1");
			break;
		case 1:// created
			map.put("type", "3");
			break;
		case 2:// collected
			map.put("type", "2");
			break;
		default:
			break;
		}
		map.put("pagenum", "" + numPerPage);
		map.put("pageno", "" + pageNo);
		String url = HttpUtil.getRequestMapUrlStr(
				"http://birdboy.cn/myActionAPI", map);
		return url;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (activityInfoAdapter != null) {
			activityInfoAdapter.recycle();// not clear list data.
		}
	}

//	private CommitDoneController commitDoneController = new CommitDoneController();

	class CommitDoneController implements UIController {
		private int id;
		int requetType=-1;
		public void setId(int requestType,int id) {
			this.id = id;
			this.requetType = requestType;
		}

		@Override
		public <E> void refreshUI(E content, Page page) {
			if (requetType==MyActivityInfoAdapter.UNKEEP) {
				 showToast("取消收藏成功!");
			}else if (requetType==MyActivityInfoAdapter.UNJIONACTION) {
				 showToast("很遗憾你放弃了该活动。请注意，如果频繁放弃活动可能会导致被封号!");
			}else if (requetType==MyActivityInfoAdapter.JIONACTION) {
				 showToast("报名成功!");
			}
			
			 activityInfoAdapter.removeActivtyById(id);
			 
		}

		@Override
		public Type getEntityType() {
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			if(errorCode>-1)
			showToast(getString(R.string.network_error_time_out));
		}

		@Override
		public String getTag() {

			return position + "id=" + id +TAG+ type;
		}

	}

	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {

			if (!isDetached()) {
				// refreash
				int size = ((List<ActivityInfo>) content).size();

				if (page.isLoadMore() != Page.CLEAR) {
					page.setLoadMore(size < numPerPage ? Page.NO_MORE
							: Page.LOAD_MORE);
				}
				refreshList((List<ActivityInfo>) content, page);

			}

		}

		@Override
		public Type getEntityType() {
			return new TypeToken<WebBirdboyJsonResponseContent<List<ActivityInfo>>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			try {
				if(errorCode>-1)
				showToast(getString(R.string.network_error_time_out));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		@Override
		public String getTag() {

			return position + "my_activity"+TAG + type;
		}
	};

	@Override
	protected String setRequestLoadMoreUrl(Page page) {
		int position = page.getCurrentPositon();
		if (position == 0 && page.isLoadMore() != Page.CLEAR) {// ?
			page.setCurrentPositon(numPerPage);//
			page.setLoadMore(Page.LOAD_MORE);
			position = numPerPage;
		} else {
			position += numPerPage;// next page
		}
		String url = createUrl(position / numPerPage);
		return url;
	}

	@Override
	protected void refreshArea() {
		// TODO Auto-generated method stub
		
	}

}
