package com.jameschen.birdboy.ui;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.Register0Activity;
import com.jameschen.birdboy.Register2Activity;
import com.jameschen.birdboy.RegisterFinishActivity;
import com.jameschen.birdboy.base.BaseBindActivity;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.base.BaseShareAcitivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Login;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Register;
import com.jameschen.birdboy.model.entity.Weibo;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.plugin.AuthUtil;
import com.jameschen.plugin.ShareModel;
import com.jameschen.plugin.ShareUtil;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;

public class LoginFragment extends BaseFragment {

//	private String account;
//
//	private String password;

	private EditText accountInput;

	private EditText passwordInput;

	private Button loginBtn;

	private TextView loginRegisterSign,forgetPassword;

	private BroadcastReceiver receiver=new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (isDetached()) {
				return;
			}
			
			if (intent.getAction().equals(ACTION_LOGON)) {
				try {
					Register0Activity.saveUserToPreference(getBaseActivity(), accountInput.getText().toString(), passwordInput.getText().toString(), true);
				
					resetInitViews();	
				} catch (NullPointerException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}else if (intent.getAction().equals(ACCOUNT_CHANGED)) {
				resetAccountInfo();
			}
			
			
			
		}
	};

	
	public static final String ACCOUNT = ConstValues.ACCOUNT;

	public static final String ACTION_LOGON = "com.jameshcen.birdboy.LOG_ON";

	public static final String ACCOUNT_CHANGED = "com.jameschen.birdboy.ACCOUNT_CHANGED";
	
	public static LoginFragment newInstance() {
		LoginFragment fragment = new LoginFragment();
		return fragment;
	}

	protected void resetAccountInfo() {
		if (accountInput!=null) {
			accountInput.setText("");
		}
		
		if (passwordInput!=null) {
			passwordInput.setText("");
		}
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			// restore..adapter data...
		}
	}

	protected static boolean currentLogged=false;
	
@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		//test share.
		BaseShareAcitivity acitivity =(BaseShareAcitivity) getBaseActivity();
//		if (LogInController.IsLogOn()) {
//			ShareModel shareModel = new ShareModel();
//			shareModel.summary=ShareUtil.shareBindContent;
//			shareModel.url=ShareUtil.ShareLink;
//			shareModel.imgUrl =ShareUtil.imgShareLink;	
//			shareModel.title =ShareUtil.shareTitle;
//			acitivity.sendQQTShareReq(shareModel, LogInController.currentAccount.getSnsObj().getQq(), acitivity);
//			if (true) {
//				return;
//			}
//			//
//			Weibo weibo =LogInController.currentAccount.getSnsObj().getWeibo();
//			Oauth2AccessToken accessToken=new Oauth2AccessToken();
//			//accessToken.setExpiresTime(mExpiresTime);
//			accessToken.setToken(weibo.getToken());
//			accessToken.setUid(weibo.getNick());
//			acitivity.sendWeiboShareMsg(acitivity, shareModel, accessToken, weibo.getToken());
//		}
		
		if (!hidden) {
			if (LogInController.IsLogOn()!=currentLogged) {//changed status
				resetInitViews();
			}
		}
}
	
	
	protected void resetInitViews() {
		if (isHidden()||isDetached()) {
			return;
		}
		FragmentTransaction ft = getFragmentManager()
				.beginTransaction();
		ft.detach(LoginFragment.this);
		ft.attach(LoginFragment.this);
		ft.commitAllowingStateLoss();
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.login, container, false);

		initViews(view);
		IntentFilter filter=new IntentFilter(ACTION_LOGON);
		filter.addAction(ACCOUNT_CHANGED);
		getBaseActivity().registerReceiver(receiver, filter);
		return view;
	}
	
	private boolean auto=false;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		if (!LogInController.IsLogOn()&&auto) {
			String[] accounts = Register0Activity
					.readAccountDataFromPreference(getActivity());
			LogInController.login(getBaseActivity(),accounts[0], accounts[1],new OnLogOnListener() {
				
				@Override
				public void logon() {
					currentLogged=true;
					resetInitViews();
				}
			});	
		}

	
	}






	public void initViews(View viewGroup) {
		accountInput = (EditText) viewGroup.findViewById(R.id.login_nickname);
		passwordInput = (EditText) viewGroup.findViewById(R.id.login_password);
		loginBtn = (Button) viewGroup.findViewById(R.id.login_btn);
		loginBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LogInController.login(getBaseActivity(),accountInput.getText(), passwordInput.getText(),new OnLogOnListener() {
					
					@Override
					public void logon() {
						currentLogged=true;
						Register0Activity.saveUserToPreference(getBaseActivity(), accountInput.getText().toString(), passwordInput.getText().toString(), true);
						resetInitViews();
					}
				});
			}
		});
		loginRegisterSign = (TextView) viewGroup
				.findViewById(R.id.login_to_register);
		
		forgetPassword= (TextView) viewGroup
				.findViewById(R.id.forget_password);
		loginRegisterSign.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(getActivity(),
						Register0Activity.class), Register.REQUST_REGISTER);
			}
		});
		forgetPassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Util.go2ForgetPassword(getBaseActivity());
			}
		});
		
		fillAccount();
	}

	private void fillAccount() {
		String[] accounts = Register0Activity
				.readAccountDataFromPreference(getActivity());
		if (accounts[0] != null) {
			accountInput.setText(accounts[0]);
				passwordInput.setText(accounts[1]);
		
		}
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Register.REQUST_REGISTER: {
			if (resultCode == Activity.RESULT_OK) {// refresh data.
				fillAccount();
				//...
				LogInController.login(getBaseActivity(),Register2Activity.accountStr, Register2Activity.passwordStr,new OnLogOnListener() {
					
					@Override
					public void logon() {
						currentLogged=true;
						resetInitViews();
					}
				});
			}
		}
			break;
		case Login.REQUST_PASSWORD_FORGET: {
			if (resultCode == Login.RESULT_PASSWORD_RESET) {

			}
		}
			break;
		default:
			break;
		}
	}


	

	@Override
	public void onResume() {
		super.onResume();
		if (LogInController.IsLogOn()!=currentLogged) {//changed status
			resetInitViews();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		
	}

	@Override
	public void onDestroy() {
		if (receiver!=null) {
			try {
				getBaseActivity().unregisterReceiver(receiver);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		
		super.onDestroy();
	}
	



	

}
