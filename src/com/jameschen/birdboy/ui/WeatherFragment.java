package com.jameschen.birdboy.ui;

import java.util.Calendar;
import java.util.List;

import android.os.Bundle;
import android.text.format.DateUtils;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.common.Lunar;
import com.jameschen.birdboy.environment.ConstValues.Setting;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.entity.SportWeatherInfo;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.weather.BDParseWeather;
import com.jameschen.birdboy.weather.BDResult;
import com.jameschen.birdboy.weather.BDWeather;
import com.jameschen.birdboy.weather.PM25;
import com.jameschen.birdboy.weather.BDParseWeather.OnWeatherResultCallbackListener;
import com.jameschen.birdboy.weather.BDWeatherData;
import com.jameschen.birdboy.weather.ParsePM25;
import com.jameschen.birdboy.weather.ParsePM25.OnPM25ResultCallbackListener;
public class WeatherFragment extends BaseFragment implements OnClickListener{
	
	private View weatherItem0,weatherItem1,weatherItem2;
	private  TextView weather,city,temp,date,advise;
	private ImageView weatherImg;
	long lastFreshTime,lastPM25FreshTime;
	private ImageView refreshWeatherImg;
	private View pm25Container;
	private TextView pm25Value;
//	if (force||System.currentTimeMillis()-lastFreshTime>7200*1000) {//2 h.
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.weather, container, false);
		initViews(view);
		refreshWeather(false);
		return view;
	}
	
	protected void showPm25(PM25 pm25) {
		hasPm=true;
		pm25Container.setVisibility(View.VISIBLE);
		if (pm25.getPm2_5_24h()==null) {
			pm25Value.setText(pm25.getPm2_5());
		}else {
			pm25Value.setText(pm25.getPm2_5_24h());
		}
	}
	
	private void updateItemWeather(int index) {
		if (bdResult!=null) {
			updateItemButtonWeather(bdResult.getWeather_data().get(index),index);
			BDWeatherData bdWeatherData = bdResult.getWeather_data().get(index);		
			updateWeather(bdWeatherData,index==0);
		}

	}
	private void updateItemButtonWeather(BDWeatherData bdWeatherData, int index) {
		TextView date,temp;
		ImageView weatherImg;
		if (index ==0){
			temp = (TextView) weatherItem0.findViewById(R.id.weather0_temp);
			weatherImg = (ImageView) weatherItem0.findViewById(R.id.weather0_img);
			date = (TextView) weatherItem0.findViewById(R.id.date0);
		}else if (index==1) {
			temp = (TextView) weatherItem1.findViewById(R.id.weather1_temp);
			weatherImg = (ImageView) weatherItem1.findViewById(R.id.weather1_img);
			date = (TextView) weatherItem1.findViewById(R.id.date1);
		}else {
			temp = (TextView) weatherItem2.findViewById(R.id.weather2_temp);
			weatherImg = (ImageView) weatherItem2.findViewById(R.id.weather2_img);
			date = (TextView) weatherItem2.findViewById(R.id.date2);
		}
		 SportWeatherInfo res = BDParseWeather.getWeatherImgRes(bdWeatherData.getWeather());
		
		if (res!=null&&res.getResourceId()>0) {
			weatherImg.setImageResource(res.getResourceId());	
			
		}
		

		temp.setText(bdWeatherData.getTemperature());
		if (index==0) {
			date.setText(bdWeatherData.getDate().subSequence(0, 2));
		}else {
			date.setText(bdWeatherData.getDate());
		}
		
		
	}
	BDResult bdResult;
	private String dateStr;
	protected boolean hasPm;
	private void updateALLInfo(List<BDWeatherData> list) {
			updateWeather(list.get(0),true);
			for (int i = 0; i < 3; i++) {
				updateItemButtonWeather(list.get(i), i);
			}
	}
	
	private void updateWeather(BDWeatherData bdWeatherData,boolean showYearDate) {
		if (bdWeatherData!=null) {
			weather.setText(bdWeatherData.getWeather());
			city.setText(bdResult.getCurrentCity());
			temp.setText(bdWeatherData.getTemperature());
			if (showYearDate&&dateStr!=null) {
				 Calendar today = Calendar.getInstance();
				  Lunar lunar = new Lunar(today);
				date.setText(dateStr+"("+lunar+")"+" "+bdWeatherData.getDate().substring(0,2));
			}else {
				date.setText(bdWeatherData.getDate());
			}
		
			 SportWeatherInfo res = BDParseWeather.getWeatherImgRes(bdWeatherData.getWeather());
			if (res!=null&&res.getResourceId()>0) {
				weatherImg.setImageResource(res.getResourceId());	
			}
			if (res!=null) {
				advise.setText(res.getAdvise());
			}
		}

	}
	private void initViews(View view) {
		weather = (TextView) view.findViewById(R.id.weather);
		city = (TextView) view.findViewById(R.id.city);
		temp = (TextView) view.findViewById(R.id.weather_temp);
		date = (TextView) view.findViewById(R.id.date);
		advise =(TextView) view.findViewById(R.id.sport_advise);
		pm25Container=view.findViewById(R.id.pm25_container);
		pm25Value=(TextView) view.findViewById(R.id.weather_pm25);
		weatherImg = (ImageView) view.findViewById(R.id.weather_img);
		refreshWeatherImg = (ImageView) view.findViewById(R.id.refresh_btn);
		refreshWeatherImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				refreshWeather(true);
			}
		});
		weatherItem0 = view.findViewById(R.id.weather_item0);
		weatherItem0.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			if (hasPm) {
				 pm25Container.setVisibility(View.VISIBLE);
			}else {
				 pm25Container.setVisibility(View.GONE);
			}
			updateItemWeather(0);	
			}
		});
		weatherItem1 = view.findViewById(R.id.weather_item1);
		weatherItem1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		  pm25Container.setVisibility(View.GONE);
			updateItemWeather(1);	
			}
		});
		weatherItem2 = view.findViewById(R.id.weather_item2);
		weatherItem2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pm25Container.setVisibility(View.GONE);
			updateItemWeather(2);	
			}
		});
	}
	
	
	private int ansaysisWeather(BDWeather weather,String cityName,String content,boolean saveCache) {
		if (weather==null) {
			return -1;
		}
		if (getActivity()==null||getActivity().isFinishing()) {
			return -2;
		}
		if ("success".equals(weather.getStatus())) {
			//save wheather,
			if (weather.getResults().size()>0) {
				if (saveCache) {
					Util.saveJsonCache(getActivity(),Setting.SharedName,"weather"+cityName, content);
					Util.saveJsonCache(getActivity(),Setting.SharedName,"weatherTime", System.currentTimeMillis()+"");
				}
				Log.i(TAG, "weather....="+weather.getResults().get(0).getWeather_data().get(0).getWeather());
				bdResult = weather.getResults().get(0);
				dateStr=weather.getDate();
				updateALLInfo(bdResult.getWeather_data());
				return 0;
			}
		
		}
		return -1;
	
	
	}
	
	protected  void refreshWeather(boolean force) {
			LocationSupport locationSupport = LocationSupport.getLocationSupportInstance(getActivity());
			String city =locationSupport.city;
			boolean detalTime=System.currentTimeMillis()-lastFreshTime>6*3600*1000;//6h
			if (city==null) {
				city="成都市";//default
			}
			if (city!=null) {
				Log.i(TAG, "city=="+city);
				BDParseWeather parseWeather = new BDParseWeather();
				//check if has weather today.
				String content	=Util.loadJsonCacheData(getActivity(),Setting.SharedName,"weather"+city);
				long when=0;
				try {String secInMillStr =Util.loadJsonCacheData(getActivity(),Setting.SharedName,"weatherTime");
					when = Long.parseLong(secInMillStr==null?"0":secInMillStr);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			
				if (content!=null&&DateUtils.isToday(when)&&!detalTime&&!force) {
					BDWeather weather = parseWeather.getBdWeather(content);
					int status =ansaysisWeather(weather, city, content, false);
					if (status==-1) {//no data
						final String cityName= city;
						parseWeather.getCityWeater(city, new OnWeatherResultCallbackListener() {
							
							@Override
							public void weatherResult(BDWeather weather,String content) {
								ansaysisWeather(weather, cityName, content, true);
							}
						});
					
					}
				}else {
					final String cityName= city;
					parseWeather.getCityWeater(city, new OnWeatherResultCallbackListener() {
						
						@Override
						public void weatherResult(BDWeather weather,String content) {
							ansaysisWeather(weather, cityName, content, true);
						}
					});
				}
				ansysisPM25(city);
			}

			lastFreshTime = System.currentTimeMillis();
	}
	
	private void ansysisPM25(String city) {
		boolean updateTime=System.currentTimeMillis()-lastPM25FreshTime>2*3600*1000;//2h
		if (updateTime) {
			hasPm=false;
			ParsePM25 parsePM25 = new ParsePM25();
			parsePM25.getCityPM25(city, new OnPM25ResultCallbackListener() {
				
				@Override
				public void PM25Result(PM25 pm25, String jsonContent) {
					if (pm25!=null) {
						showPm25(pm25);
					}
				}
			});
		}
		
		lastPM25FreshTime = System.currentTimeMillis();
	}

	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onPause() {
	
		super.onPause();
	}
	
	@Override
	public void onResume() {
		
		super.onResume();
		refreshWeather(false);
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
	}


 
	
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.web_site:
		{}
			break;


		default:
			break;
		}

	}

@Override
public void onHiddenChanged(boolean hidden) {
	// TODO Auto-generated method stub
	super.onHiddenChanged(hidden);
	if (!hidden) {
		refreshWeather(false);
	}

}


}
