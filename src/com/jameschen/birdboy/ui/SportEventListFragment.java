package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

//import com.espian.showcaseview.ShowcaseView;
//import com.espian.showcaseview.targets.ViewTarget;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SearchActivity;
import com.jameschen.birdboy.SlapshActivity;
import com.jameschen.birdboy.SportEventCreate0Activity;
import com.jameschen.birdboy.SportEventDetailActivity;
import com.jameschen.birdboy.adapter.CategoryAdapter;
import com.jameschen.birdboy.adapter.ChildAreaAdapter;
import com.jameschen.birdboy.adapter.SportEventAdapter;
import com.jameschen.birdboy.base.BaseActivity.LoadMode;
import com.jameschen.birdboy.base.BaseListFragment;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.location.LocationManager;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.location.LocationManager.GetLastLocationListener;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebSportEvents;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.provider.ChildAreaHelper;
import com.jameschen.birdboy.utils.AreaSelectPopupWindow;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.FliterDialog;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;

public class SportEventListFragment extends BaseListFragment<SportEvent> implements
		OnItemClickListener, OnClickListener {

	public static final String SPORT_EVENT = ConstValues.SPORT_EVENT;
	public static long areaGetTime;
	private String keyword = "*:*";

	public static SportEventListFragment newInstance(String search, int type,
			ArrayList<SportEvent> mSportEvents) {
		SportEventListFragment fragment = new SportEventListFragment();
		fragment.keyword = search;
		fragment.mInfos = mSportEvents;
		return fragment;
	}

	private SportEventAdapter sportEventAdapter;

//	 ShowcaseView guideView;
	
	 private int cityId=-1;//if cityid == -1 remove key , or  set cityId first..
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {

			if (savedInstanceState.containsKey(SPORT_EVENT)) {
				ArrayList<SportEvent> coachs = savedInstanceState
						.getParcelableArrayList(SPORT_EVENT);
				if (coachs != null)
					mInfos = coachs;
			}
			// restore..adapter data...
		}

	//	areas = AreaMap.initAreaList(getActivity());
	}

	private List<Area> areas = new ArrayList<Area>();
	protected AreaSelectPopupWindow areaPopupWindow = new AreaSelectPopupWindow();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = null;
		if (TAG.equals(ConstValues.SEARCH)) {
			view = inflater.inflate(R.layout.sportevent_list, container, false);
		} else {
			view = inflater.inflate(R.layout.sportevent_ui, container, false);
		}
		sportEventAdapter = new SportEventAdapter(getBaseActivity());
		initViews(view);
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		SlapshActivity.IsNeedCheckedGuidePage(getBaseActivity(), ConstValues.SPORT_EVENT);

//		   ShowcaseView.ConfigOptions co = new ShowcaseView.ConfigOptions();
//	        co.hideOnClickOutside = true;
//	        guideView = ShowcaseView.insertShowcaseView(new ViewTarget(createView), getActivity(), "创建活动", "你可以创建一个活动", co);
//		
		
		if (mInfos != null) {
			sportEventAdapter.setObjectList(mInfos);
		}

		if (TAG.equals(ConstValues.SEARCH)) {
			startSearch(keyword);
		} else {
			//load area
			Map<String, String> map = getPublicParamRequstMap();
			map.put("type", ""+3);
			 String url = createUrl(HttpConfig.REQUST_SERACH_AREA_URL, map,1);			
			initDataLoaderFromNativeCache(url, LoadMode.SAVE_CACHE,
					areaUIController,1000*3600);
			Map<String, String> temp=getPublicParamRequstMap();
			
			((Button) categoryScoreChoose).setText("最新创建");
			sort ="uptime";
			startSearch(keyword, temp);
			
		}

	}
	
	public static boolean sortByCreateTime = true;
	public void sortByCreateTime() {
		sortByCreateTime=false;
		isDistance=false;
		area=-1;
		childArea=-1;
		type=-1;
		Log.i(TAG, "begin sort.. by uptime");
		Map<String, String> temp=getPublicParamRequstMap();
		List<Category> categorySports = SportType.initSportList();
		CategoryAdapter.attchReourceToView((Button) categorySportChoose, categorySports.get(0),R.drawable.category_choose_trangle_state);
		((Button) categoryAreaChoose).setText("所有区域");
		((Button) categoryScoreChoose).setText("最新创建");
		sort ="uptime";
		getBaseActivity().showProgressDialog("排序", "请稍候...", null);
		startSearch(keyword, temp);
	}
	
	
	static float lastlng=-1,lastlat=-1;
	
	private Map<String, String> createDistanceMap(Map<String, String> reqMap) {
		LocationSupport locationSupport = LocationSupport
				.getLocationSupportInstance(getActivity()
						.getApplicationContext());
		float lat = locationSupport.getLatitude();
		float lng = locationSupport.getLongtitude();
		if (lastlng==-1) {
			if (lat <= 0 || lng <= 0 ||locationSupport.getLocation()==null) {
				showToast("无法获取当前位置！");
				refreshAddress(null,locationSupport);
				return null;
			}
			lastlat =lat;
			lastlng = lng;
			refreshAddress(locationSupport.getLocation(), locationSupport);
		
		}else {
			
		}
		Map<String, String> map=null;
		if (reqMap==null) {
			map = getPublicParamRequstMap();
		}else {
			map = reqMap;
		}
	
		map.put("lnt", "" + lastlng);
		map.put("lat", "" + lastlat);
		map.put("sort", "distance");
		map.put("d", "100");
		return map;
	}

	
	public void startSearch(String keyword) {
		startSearch(keyword, null);
	}

	public void startSearch(String keyword, Map<String, String> map) {
		this.keyword = keyword;
		clearAdapter();
		if (map == null) {
			map = getPublicParamRequstMap();
		}
		String url =createUrl(HttpConfig.REQUST_SPORTEVENT_URL, map,1);
		if (url == null) {
			return ;
		}
		loadData(url, false,
				 createInitPage(), uiFreshController);
	}
	protected void startLocate(final Map<String, String> map) {
		this.area = -1;
		childArea = -1;
		((Button) categoryAreaChoose).setText("全部区域");
		
			getBaseActivity().showProgressDialog("定位", "正在定位..", null);
		LocationManager locationManager = new LocationManager();
		locationManager.findLocation(getActivity(), new GetLastLocationListener() {
			
			@Override
			public void onLocResult(float lat, float lont, String where) {
				if (getBaseActivity()==null||getBaseActivity().isFinishing()) {
					return;
				}
				lastlat=-1;
				lastlng=-1;
				getBaseActivity().cancelProgressDialog();
				getBaseActivity().showProgressDialog("已定位", "获取数据中..", null);

				startSearch(keyword, map);
			}
		}, true);
		
	}
	private int area=-1,childArea = -1, type = -1;
	
	private boolean isDistance =false;
	
	private String createUrl(String url, Map<String, String> map,int pageNum) {

		if (TAG.equals(ConstValues.SEARCH)) {
			map.put("name", keyword );
			map.put("sort", "uptime");
		} else {

			String queryUrl = "";
			
			if (sort!=null) {
				keyword=map.put("sort", sort);
				if (sort.equals("distance")) {
					isDistance=true;
					Map<String, String> temp;
					if ((temp=createDistanceMap(map))!=null) {
						map=temp;
					}else {
						return null;
					}
				}else {
					isDistance=false;
				}
			}else {
					isDistance=false;
				//((Button) categoryScoreChoose).setText("");
			}
			if (sportEventAdapter!=null) {
				sportEventAdapter.setDistanceShow(isDistance);
			}
			if (area>-1) {
				keyword = "";
				map.put("area", ""+area);
			}
			
			if (childArea > -1) {
				keyword = "";
				map.put("childarea", ""+childArea);
			}
			if (type > -1) {
				keyword = "";
				map.put("type", ""+type);
			}
			
			
//			if (TextUtils.isEmpty(queryUrl)) {
//				map.put("q", "*:*");
//			} else {
//				map.put("q", queryUrl);
//			}

		}
		map.put("pagenum", ""+numPerPage);
		map.put("pageno", ""+pageNum);
		
		map.put("rows", numPerPage + "");
		map.put("wt", "json");
		map.put("indent", "true");
		url = HttpUtil.getRequestMapUrlStr(url, map);
		return url;
	}

	private View categorySportChoose, categoryAreaChoose, categoryScoreChoose;
	private View createView;
	

	@Override
	protected   void initViews(View viewGroup) {
		// TODO Auto-generated method stub
		super.initData();
		mListView = (MyListView) viewGroup
				.findViewById(R.id.sportevent_list_view);

		mListView.setOnItemClickListener(this);
		initListAdapter(sportEventAdapter, uiFreshController);
		
		if (TAG.equals(ConstValues.SEARCH)) {
			mListView.setMode(Mode.DISABLED);
			return;
		}
		View emptyView = viewGroup.findViewById(R.id.list_empty);
		emptyView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Map<String, String> map = getPublicParamRequstMap();
				startSearch(keyword, map);
				getBaseActivity().showProgressDialog("努力加载中", "请稍后...", null);
			}
		});
		setEmptyView(emptyView);

		categorySportChoose = viewGroup.findViewById(R.id.category_sport);
		categorySportChoose.setOnClickListener(this);
		categoryAreaChoose = viewGroup.findViewById(R.id.category_area);
		categoryAreaChoose.setOnClickListener(this);
		categoryAreaChoose.setVisibility(View.GONE);
		viewGroup.findViewById(R.id.category_area_split).setVisibility(View.GONE);
		
		categoryScoreChoose = viewGroup.findViewById(R.id.category_score);
		categoryScoreChoose.setOnClickListener(this);
		viewGroup.findViewById(R.id.btn_search).setOnClickListener(this);
		 createView = viewGroup.findViewById(R.id.create_sport);
		locationView = (TextView) viewGroup.findViewById(R.id.location);
		String address= LocationSupport
				.getLocationSupportInstance(getActivity().getApplicationContext()).getLocation();
		if (address!=null) {
			 refreshAddress(address, null);
		}
		createView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				 if (!LogInController.IsLogOn()) {
						Util.go2LogInDialog(getBaseActivity(),new OnLogOnListener() {
							
							@Override
							public void logon() {
								Util.createSportEvent(getActivity(),null);
							}
						});
					 return ;
				}
				 Util.createSportEvent(getActivity(),null);
			}
		});


		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (sportEventAdapter != null) {
			outState.putParcelableArrayList(SPORT_EVENT,
					(ArrayList<SportEvent>) sportEventAdapter.getObjectInfos());
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		final AdapterView<?> mAdapterView = parent;
		final int item = position;
		if (!LogInController.IsLogOn()) {
			Util.go2LogInDialog(getBaseActivity(), new OnLogOnListener() {
				
				@Override
				public void logon() {
					Intent intent = new Intent(getActivity(),
							SportEventDetailActivity.class);
					Object object = mAdapterView.getAdapter().getItem(item);
					if (object == null) {
						return;
					}
					Parcelable p = (SportEvent) (object);
					intent.putExtra(ConstValues.SPORT_EVENT, p);
					startActivity(intent);
				}
			});
			return;
		}
		
		Intent intent = new Intent(getActivity(),
				SportEventDetailActivity.class);
		Object object = parent.getAdapter().getItem(position);
		if (object == null) {
			return;
		}
		Parcelable p = (SportEvent) (object);
		intent.putExtra(ConstValues.SPORT_EVENT, p);
		startActivity(intent);
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		
		super.onDestroy();
	}

	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
		//	if (!isDetached()) {
				// refreash
				WebSportEvents webSportEvents = (WebSportEvents) content;
				int size = webSportEvents.getActions().size();
				if (size==0) {
					Log.i(TAG, "list size==0");
					
				}
				if (page.isLoadMore() != Page.CLEAR) {
					page.setLoadMore(size < numPerPage ? Page.NO_MORE
							: Page.LOAD_MORE);
				}
				refreshList(webSportEvents.getActions(), page);
	//		}
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebSportEvents>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			try {
				if(errorCode>-1)
				showToast(getString(R.string.network_error_time_out));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (page != null) {
				setPrevPage(page);
			}

		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};

	@Override
	protected String setRequestLoadMoreUrl(Page page) {
		// TODO Auto-generated method stub
	
			int position = page.getCurrentPositon();
			if (position==0&&page.isLoadMore()!=Page.CLEAR) {//?
				page.setCurrentPositon(numPerPage);//
				page.setLoadMore(Page.LOAD_MORE);
				position=numPerPage;
			}else {
				position+=numPerPage;//next page
			}
	

		Map<String, String> map = getPublicParamRequstMap();
		map.put("start", position + "");
		
		String url = createUrl(
				HttpConfig.REQUST_SPORTEVENT_URL, map,position/numPerPage);
		return url;
	}

	private void showChildArea(final List<ChildArea> childAreas,final Area mArea) {
		areaPopupWindow.getChildListView().setAdapter(new ChildAreaAdapter(getActivity(), childAreas));
		areaPopupWindow.showChildArea();
		areaPopupWindow.getChildListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				Map<String, String> map = getPublicParamRequstMap();
				area =mArea.getId();
				childArea = childAreas.get(arg2).getId();
				startSearch(keyword, map);
				getBaseActivity().showProgressDialog("排序", "请稍后...", null);
				areaPopupWindow.dismiss();
				if (childArea==-1) {//show parent name
					((Button) categoryAreaChoose).setText(mArea.getName());
				}else {
					((Button) categoryAreaChoose).setText(childAreas.get(arg2)
							.getName());
				}
				
			}
		});
		
	}

	private void showCategorySort(final List<Category> categorySorts) {

		
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow((View) (categoryScoreChoose.getParent()), getBaseActivity(), categorySorts);
		
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			
			@Override
			public void onItemClick(int index) {
				Category mItem = categorySorts.get(index);
			((Button) categoryScoreChoose).setText(mItem.getName());
			Map<String, String> map = getPublicParamRequstMap();
			sort = mItem.getSortParam();
			if (index==0) {//distance.
				startLocate(map);
				return;
			}
			
			startSearch(keyword, map);
			getBaseActivity().showProgressDialog("排序", "请稍后...", null);
			categoryScoreChoose.setSelected(false);}
		});
		categoryScoreChoose.setSelected(true);
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				categoryScoreChoose.setSelected(false);
			}
		});
	
		
	}

	private  void showCategorySport(final List<Category> categorySports) {
		
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow((View) (categorySportChoose.getParent()), getBaseActivity(), categorySports);
		
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			
			@Override
			public void onItemClick(int index) {
				Map<String, String> map = getPublicParamRequstMap();
			type = categorySports.get(index).getType();
			startSearch(keyword, map);
			getBaseActivity().showProgressDialog("排序", "请稍后...", null);
			CategoryAdapter.attchReourceToView((Button) categorySportChoose, categorySports.get(index),R.drawable.category_choose_trangle_state);
			//sportEvent num
			map = getPublicParamRequstMap();
			map.put("type", "" + 3);
			map.put("typedesc", "" + type);
			String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_SERACH_AREA_URL, map);
			Page page =  createInitPage();
			page.setCurrentPositon(type);
			getDataLoader(url, false, page, areaUIController);
			}
		});
		categorySportChoose.setSelected(true);
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				categorySportChoose.setSelected(false);
			}
		});
	}

	private UIController areaUIController = new UIController() {

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
		}
		Object locker = new Object();
		@Override
		public <E> void refreshUI(E content, Page page) {
				// TODO Auto-generated method stub
			List<Area> areaLists = null;
			if (content!=null) {
				areaLists = new ArrayList<Area>( (List<Area>) content);
			}
				if (areaLists!=null) {
					areas.clear();
					Area area=new Area();
					area.setId(-1);
					area.setName("全部区域");
					int count = 0;
					for (Area areaItem : areaLists) {
						count+=areaItem.getCount();
					}
					area.setCount(count);
					areas.addAll(areaLists);
					areas.add(0, area);
				}
			
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "sport_Area";
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<List<Area>>>() {
			}.getType();
		}
	};
	
	
	

	
	
	ChildAreaHelper childAreaHelper;
	//
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.category_sport: {
			List<Category> categorySports = FliterDialog.getLikedSportCategories();
			showCategorySport(categorySports);
		}
			break;
		case R.id.category_area: {
			areaPopupWindow.showActionWindow((View) (v.getParent()),
					getBaseActivity(), areas);
			categoryAreaChoose.setSelected(true);
			areaPopupWindow
					.setItemOnClickListener(new AreaSelectPopupWindow.OnItemClickListener() {

						@Override
						public void onItemClick(int index) {
							Area area = areas.get(index);
							if (area.getId() == -1) {// all area
								Map<String, String> map = getPublicParamRequstMap();
								SportEventListFragment.this.area=-1;
								childArea = -1;
								startSearch(keyword, map);
								getBaseActivity().showProgressDialog("排序",
										"请稍后...", null);
								((Button)categoryAreaChoose).setText(area.getName());
								areaPopupWindow.dismiss();
								return;
							}
							// List<ChildArea> childAreaLists = area.getChilds();
							 List<ChildArea> childAreaLists = area.getChilds();
							 List<ChildArea> tempAreas = new ArrayList<ChildArea>();
							if (childAreaLists.size()>0) {
								if (childAreaLists.get(0).getId()!=-1) {//not contains all
									ChildArea allChildArea = new ChildArea();
									allChildArea.setId(-1);
									allChildArea.setName("全部");
									int count = area.getCount();
//									for (ChildArea childArea : childAreaLists) {
//										count+=childArea.getCount();
//									}
									allChildArea.setCount(count);
									tempAreas.add(allChildArea);
								}
						}else {
							ChildArea allChildArea = new ChildArea();
							allChildArea.setId(-1);
							allChildArea.setName("全部");
							allChildArea.setCount(area.getCount());
							childAreaLists.add(0, allChildArea);
						}
						
							showChildArea(tempAreas, area);
						
						}
					});
			areaPopupWindow
					.setOnDismissListener(new AreaSelectPopupWindow.OnDismissListener() {

						@Override
						public void onDismiss() {
							categoryAreaChoose.setSelected(false);
						}
					});

		}
			break;

		case R.id.category_score: {
			final List<Category> menuItems = new ArrayList<Category>();
			
			Category menuItem = new Category();
			menuItem.setName("离我最近");
			menuItem.setSortParam("distance");
			menuItems.add(menuItem);
			 menuItem = new Category();
				menuItem.setSortParam("uptime");
				menuItem.setName("最新创建");
				menuItems.add(menuItem);
			menuItem = new Category();
			menuItem.setSortParam("nownum");
			menuItem.setName("报名最多");
			menuItems.add(menuItem);
			showCategorySort(menuItems);
		}
			break;
		

		case R.id.btn_search: {
			Intent intent = new Intent(getActivity(), SearchActivity.class);
			intent.putExtra(ConstValues.SEARCH, ConstValues.SPORT_EVENT);
			startActivity(intent);
		}
			break;
		default:
			break;
		}
	}

	@Override
	protected void refreshArea() {
		// TODO Auto-generated method stub
		
	}

	

}
