package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.jameschen.birdboy.CoachDetailActivity;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SearchActivity;
import com.jameschen.birdboy.adapter.CategoryAdapter;
import com.jameschen.birdboy.adapter.ChildAreaAdapter;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.base.BaseActivity.LoadMode;
import com.jameschen.birdboy.base.BaseListFragment;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.location.LocationManager;
import com.jameschen.birdboy.location.LocationManager.GetLastLocationListener;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.QueryBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebDistanceCoachs;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.provider.ChildAreaHelper;
import com.jameschen.birdboy.utils.AreaSelectPopupWindow;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.widget.MyListView;

public class CoachListFragment extends BaseListFragment<Coach> implements
		OnItemClickListener, OnClickListener {

	public static final String COACH = ConstValues.COACH;
	public static long areaGetTime;
	private String keyword = "*:*";


	public static CoachListFragment newInstance(String search, int type,
			ArrayList<Coach> mCoachs) {
		CoachListFragment fragment = new CoachListFragment();
		fragment.keyword = search;
		fragment.mInfos = mCoachs;
		return fragment;
	}

	private CoachAdapter coachAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {

			if (savedInstanceState.containsKey(COACH)) {
				ArrayList<Coach> coachs = savedInstanceState
						.getParcelableArrayList(COACH);
				if (coachs != null)
					mInfos = coachs;
			}
			// restore..adapter data...
		}

	//	areas = AreaMap.initAreaList(getActivity());
	}

	private List<Area> areas = new ArrayList<Area>();
	protected AreaSelectPopupWindow areaPopupWindow = new AreaSelectPopupWindow();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = null;
		if (TAG.equals(ConstValues.SEARCH)) {
			view = inflater.inflate(R.layout.coach_list, container, false);
		} else {
			view = inflater.inflate(R.layout.coach_ui, container, false);
		}

		coachAdapter = new CoachAdapter(getBaseActivity());
		initViews(view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		if (mInfos != null) {
			coachAdapter.setObjectList(mInfos);
		}
		if (TAG.equals(ConstValues.SEARCH)) {
			startSearch(keyword);
		} else {
			Map<String, String> map = getPublicParamRequstMap();
			
			//load area
			map.put("type", ""+2);
			 String url = createUrl(HttpConfig.REQUST_SERACH_AREA_URL, map);			
			initDataLoaderFromNativeCache(url, LoadMode.SAVE_CACHE,
					areaUIController,1000*3600);
		//	((Button) categoryScoreChoose).setText("离我最近");
			((Button) categoryScoreChoose).setText("评分最高");
			Map<String, String> reqMap = getPublicParamRequstMap();
			sort="scores desc";
			String requrl = createUrl(HttpConfig.REQUST_COACH_SEARCH_URL, reqMap);
			initDataLoaderFromNativeCache(requrl, LoadMode.SAVE_CACHE, uiFreshController);
		}

	}
	
	

	
	public void startSearch(String keyword) {
		startSearch(keyword, null);
	}

	public void startSearch(String keyword, Map<String, String> map) {
		this.keyword = keyword;
		clearAdapter();
		if (map == null) {
			map = getPublicParamRequstMap();
		}
		loadData(createUrl(HttpConfig.REQUST_COACH_SEARCH_URL, map), false,
				 createInitPage(), uiFreshController);
	}

	private int area=-1,childArea = -1, type = -1, jobtype = -1;
	private boolean isDistance=false;
	private String createUrl(String url, Map<String, String> map) {

		if (TAG.equals(ConstValues.SEARCH)) {
			map.put("q", "name:*" + keyword + "*");
		} else {

			String queryUrl = "";
			
			if (sort!=null) {
				isDistance=false;
				keyword=map.put("sort", sort);
			}else {
				((Button) categoryScoreChoose).setText("最新发布");
			}
			
			if (area>-1) {
				keyword = "";
				queryUrl = parseParam(queryUrl, "area", area + "",true);
				
			}
			
			if (childArea > -1) {
				keyword = "";
				queryUrl = parseParam(queryUrl, "childarea", childArea + "");
			}
			if (type > -1) {
				keyword = "";
				queryUrl = parseParam(queryUrl, "type", type + "");
			}
			if (jobtype > -1) {
				keyword = "";
				// &fq=jobtype : [2 TO *]
				map.put("fq", "jobtype:" + "[" + jobtype + " TO " + "*" + "]");
				// queryUrl = parseParam(queryUrl, "jobtype", jobtype+"");
			}
			
		
			
			if (TextUtils.isEmpty(queryUrl)) {
				map.put("q", "*:*");
			} else {
				map.put("q", queryUrl);
			}

		}
		map.put("rows", numPerPage + "");
		map.put("wt", "json");
		map.put("indent", "true");
		url = HttpUtil.getRequestMapUrlStr(url, map);
		return url;
	}

	private View categorySportChoose, categoryAreaChoose, categoryScoreChoose;
	private View courseBtn, personalBtn;

	private int coachType;

	@Override
	protected void initViews(View viewGroup) {
		super.initData();
		mListView = (MyListView) viewGroup
				.findViewById(R.id.coach_list_view);

		initListAdapter(coachAdapter, uiFreshController);
		
		mListView.setOnItemClickListener(this);

		if (TAG.equals(ConstValues.SEARCH)) {
			mListView.setMode(Mode.DISABLED);
			return;
		}
		View emptyView = viewGroup.findViewById(R.id.list_empty);
		emptyView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Map<String, String> map = getPublicParamRequstMap();
				startSearch(keyword, map);
				getBaseActivity().showProgressDialog("努力加载中", "请稍后...", null);
			}
		});
		setEmptyView(emptyView);

		categorySportChoose = viewGroup.findViewById(R.id.category_sport);
		categorySportChoose.setOnClickListener(this);
		categoryAreaChoose = viewGroup.findViewById(R.id.category_area);
		categoryAreaChoose.setOnClickListener(this);
		categoryScoreChoose = viewGroup.findViewById(R.id.category_score);
		categoryScoreChoose.setOnClickListener(this);
		viewGroup.findViewById(R.id.btn_search).setOnClickListener(this);
//		final LocationManager locationManager = new LocationManager();
		locationView = (TextView) viewGroup
				.findViewById(R.id.location);
		String address= LocationSupport
				.getLocationSupportInstance(getActivity().getApplicationContext()).getLocation();
		
			 refreshAddress(address, null);
		
//		
//		locationManager.findLocation(getActivity(), locationView);
//		viewGroup.findViewById(R.id.refresh_location).setOnClickListener(
//				new OnClickListener() {
//
//					@Override
//					public void onClick(View arg0) {
//						// TODO Auto-generated method stub
//						
//						locationView.setText("正在定位...");
//						locationManager.cancel();
//						locationManager.findLocation(getActivity(),
//								locationView);
//					}
//				});

		courseBtn = viewGroup.findViewById(R.id.course_coach);
		courseBtn.setOnClickListener(this);
		courseBtn.setSelected(true);
		
		personalBtn = viewGroup.findViewById(R.id.personal_coach);
		personalBtn.setOnClickListener(this);
//		if (coachType == 0) {
//			courseBtn.performClick();
//			mCoachListView.setRefreshing();
//		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (coachAdapter != null) {
			outState.putParcelableArrayList(COACH,
					(ArrayList<Coach>) coachAdapter.getObjectInfos());
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(getActivity(), CoachDetailActivity.class);
		Object object = parent.getAdapter().getItem(position);
		if (object == null) {
			return;
		}
		Parcelable p = (Coach) (object);
		intent.putExtra(COACH, p);
		startActivity(intent);
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		
		super.onDestroy();
	}

	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			//if (!isDetached()) {
				// refreash
				refreshList(content, page);
		//	}

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<QueryBirdboyJsonResponseContent<Coach>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			try {
				if(errorCode>-1)
				showToast(getString(R.string.network_error_time_out));
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (page != null) {
				setErrorPage(errorCode,page);
			}

		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};

	@Override
	protected String setRequestLoadMoreUrl(Page page) {
		// TODO Auto-generated method stub
		if (isDistance) {
			int position = page.getCurrentPositon();
			if (position==0&&page.isLoadMore()!=Page.CLEAR) {//?
				page.setCurrentPositon(numPerPage);//
				page.setLoadMore(Page.LOAD_MORE);
				position=numPerPage;
			}else {
				position+=numPerPage;//next page
			}
		
			loadDataByDistance(false,position/numPerPage,page);
			return ConstValues.DISTANCE;
		}

		int position = page.getCurrentPositon();

		Map<String, String> map = getPublicParamRequstMap();
		map.put("start", position + "");
		
		String url = createUrl(HttpConfig.REQUST_COACH_SEARCH_URL, map);
		return url;
	}

	private void showChildArea(final List<ChildArea> childAreas,final Area mArea) {
		areaPopupWindow.getChildListView().setAdapter(new ChildAreaAdapter(getActivity(), childAreas));
		areaPopupWindow.showChildArea();
		areaPopupWindow.getChildListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				Map<String, String> map = getPublicParamRequstMap();
				area =mArea.getId();
				childArea = childAreas.get(arg2).getId();
				isDistance=false;
				startSearch(keyword, map);
				getBaseActivity().showProgressDialog("排序", "请稍后...", null);
				areaPopupWindow.dismiss();
				if (childArea==-1) {//show parent name
					((Button) categoryAreaChoose).setText(mArea.getName());
				}else {
					((Button) categoryAreaChoose).setText(childAreas.get(arg2)
							.getName());
				}
				
			}
		});
		
	}

	private void showCategorySort(final List<Category> categorySorts) {

		
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow((View) (categoryScoreChoose.getParent()), getBaseActivity(), categorySorts);
		
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			
			@Override
			public void onItemClick(int index) {
				Category mItem = categorySorts.get(index);
			((Button) categoryScoreChoose).setText(mItem.getName());
			if (index == 2) {
				//重新定位...
				startLocate();
				return;
			}

			Map<String, String> map = getPublicParamRequstMap();
			sort = mItem.getSortParam();
			startSearch(keyword, map);
			getBaseActivity().showProgressDialog("排序", "请稍后...", null);
			categoryScoreChoose.setSelected(false);}
		});
		categoryScoreChoose.setSelected(true);
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				categoryScoreChoose.setSelected(false);
			}
		});
	
		
	}

	protected void startLocate() {
		this.area = -1;
		childArea = -1;
		((Button) categoryAreaChoose).setText("全部区域");
		
		LocationManager locationManager = new LocationManager();
		getBaseActivity().showProgressDialog("定位", "正在定位..", null);

		locationManager.findLocation(getActivity(), new GetLastLocationListener() {
			
			@Override
			public void onLocResult(float lat, float lont, String where) {
				if (getBaseActivity()==null||getBaseActivity().isFinishing()) {
					return;
				}
				getBaseActivity().cancelProgressDialog();
				loadDataByDistance(true,1, createInitPage());
			}
		}, true);
		
	}

	private  void showCategorySport(final List<Category> categorySports) {
		
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow((View) (categorySportChoose.getParent()), getBaseActivity(), categorySports);
		
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			
			@Override
			public void onItemClick(int index) {
				Map<String, String> map = getPublicParamRequstMap();
			type = categorySports.get(index).getType();
			startSearch(keyword, map);
			getBaseActivity().showProgressDialog("排序", "请稍后...", null);
			CategoryAdapter.attchReourceToView((Button) categorySportChoose, categorySports.get(index),R.drawable.category_choose_trangle_state);
			//refresh coach
			map=getPublicParamRequstMap();
			map.put("type", ""+2);
			map.put("typedesc",""+ type);
			String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_SERACH_AREA_URL, map);
			 Page page = createInitPage();
			 page.setCurrentPositon(type);
			 getDataLoader(url, false, page, areaUIController);
			}
		});
		categorySportChoose.setSelected(true);
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				categorySportChoose.setSelected(false);
			}
		});
	}

	private UIController areaUIController = new UIController() {

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub

		}
		Object locker = new Object();
		@Override
		public <E> void refreshUI(E content, Page page) {
			
			List<Area> areaLists = null;
			if (content!=null) {
				areaLists = new ArrayList<Area>( (List<Area>) content);
			}
				if (areaLists!=null) {
					areas.clear();
					Area area=new Area();
					area.setId(-1);
					area.setName("全部区域");
					int count = 0;
					for (Area areaItem : areaLists) {
						count+=areaItem.getCount();
					}
					area.setCount(count);
					areas.addAll(areaLists);
					areas.add(0, area);
				}
			
			
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "coach_Area";
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<List<Area>>>() {
			}.getType();
		}
	};
	
	
	
	
	private void loadDataByDistance(boolean showDialog,int pageNo,Page page) {
		sort=null;
		isDistance=true;
		String url =createDistanceUrl(pageNo);
		if (TextUtils.isEmpty(url)) {
			return;
		}
		if (showDialog) {
			clearAdapter();
			getBaseActivity().showProgressDialog("已定位", "正在获取数据请稍候...", null);
		}
		if (page.isLoadMore() == Page.CLEAR) {
			loadData(url, false, page,
					distanceUIFreshController);
		}else {
			getDataLoader(url, false, page,
					distanceUIFreshController);
		}
	

	}
	
	private String  createDistanceUrl(int pageNo) {
		LocationSupport locationSupport = LocationSupport
				.getLocationSupportInstance(getActivity()
						.getApplicationContext());
		float lat = locationSupport.getLatitude();
		float lng = locationSupport.getLongtitude();
		if (lat <= 0 || lng <= 0||locationSupport.getLocation()==null) {
			refreshAddress(null,locationSupport);
			showToast("无法获取当前位置！");
			return null;
		}
		refreshAddress(locationSupport.getLocation(), locationSupport);
		
		Map<String, String> map = getPublicParamRequstMap();
		map.put("lnt", "" + lng);
		map.put("lat", "" + lat);
		map.put("sort", "distance");
		if (type>-1) {
			map.put("type", type+"");
		}
		map.put("d", "100");
		map.put("pagenum", ""+numPerPage);
		map.put("pageno", ""+pageNo);
		return HttpUtil.getRequestMapUrlStr(
				"http://birdboy.cn/searchcoachAPI", map);
	}
	
	ChildAreaHelper childAreaHelper;
	//
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.category_sport: {
			List<Category> categorySports = SportType.initSportList();
			showCategorySport(categorySports);
		}
			break;
		case R.id.category_area: {
			areaPopupWindow.showActionWindow((View) (v.getParent()),
					getBaseActivity(), areas);
			categoryAreaChoose.setSelected(true);
			areaPopupWindow
					.setItemOnClickListener(new AreaSelectPopupWindow.OnItemClickListener() {

						@Override
						public void onItemClick(int index) {
							Area area = areas.get(index);
							if (area.getId() == -1) {// all area
								Map<String, String> map = getPublicParamRequstMap();
								CoachListFragment.this.area=-1;
								childArea = -1;
								isDistance=false;
								startSearch(keyword, map);
								getBaseActivity().showProgressDialog("排序",
										"请稍后...", null);
								((Button)categoryAreaChoose).setText(area.getName());
								areaPopupWindow.dismiss();
								return;
							}
							 List<ChildArea> childAreaLists = area.getChilds();
							if (childAreaLists.size()>0) {
								if (childAreaLists.get(0).getId()!=-1) {//not contains all
									ChildArea allChildArea = new ChildArea();
									allChildArea.setId(-1);
									allChildArea.setName("全部");
									int count = area.getCount();
//									for (ChildArea childArea : childAreaLists) {
//										count+=childArea.getCount();
//									}
									allChildArea.setCount(count);
									
					 				childAreaLists.add(0, allChildArea);
								}
						}else {
							ChildArea allChildArea = new ChildArea();
							allChildArea.setId(-1);
							allChildArea.setName("全部");
							allChildArea.setCount(area.getCount());
							childAreaLists.add(0, allChildArea);
						}
							showChildArea(childAreaLists, area);
						
						}
					});
			areaPopupWindow
					.setOnDismissListener(new AreaSelectPopupWindow.OnDismissListener() {

						@Override
						public void onDismiss() {
							categoryAreaChoose.setSelected(false);
						}
					});

		}
			break;

		case R.id.category_score: {
			final List<Category> menuItems = new ArrayList<Category>();
			Category menuItem = new Category();
			menuItem.setSortParam("level" + " desc");
			menuItem.setName("水平最高");
			menuItems.add(menuItem);
			menuItem = new Category();
			menuItem.setSortParam("scores" + " desc");
			menuItem.setName("评分最高");
			menuItems.add(menuItem);
			menuItem = new Category();
			menuItem.setName("离我最近");
			menuItems.add(menuItem);
			showCategorySort(menuItems);
		}
			break;
		case R.id.course_coach: {
			Map<String, String> map = getPublicParamRequstMap();
			jobtype = -1;
			startSearch(keyword, map);
			getBaseActivity().showProgressDialog("排序", "请稍后...", null);
			v.setSelected(true);
			personalBtn.setSelected(false);
		}
			break;
		case R.id.personal_coach: {
			Map<String, String> map = getPublicParamRequstMap();
			jobtype = 2;
			startSearch(keyword, map);
			getBaseActivity().showProgressDialog("排序", "请稍后...", null);
			courseBtn.setSelected(false);
			v.setSelected(true);
		}
			break;

		case R.id.btn_search: {
			Intent intent = new Intent(getActivity(), SearchActivity.class);
			intent.putExtra(ConstValues.SEARCH, ConstValues.COACH);
			startActivity(intent);
		}
			break;
		default:
			break;
		}
	}

	private UIController distanceUIFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
		//	if (!isDetached()) {
				// refreash
				WebDistanceCoachs distanceCoachs = (WebDistanceCoachs) content;
				page.setTag(ConstValues.DISTANCE);
				int size =distanceCoachs.getCoachs().size();
				if (page.isLoadMore()!=Page.CLEAR) {
					page.setLoadMore(size<numPerPage?Page.NO_MORE:Page.LOAD_MORE);
				}
				Log.i(TAG, "get getCoachs distannce list size==="+ size);
				refreshList(distanceCoachs.getCoachs(), page);
		//	}

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebDistanceCoachs>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			showToast(getString(R.string.network_error_time_out));
			if (page != null) {
				setPrevPage(page);
			}
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG + "dis";
		}
	};


	@Override
	protected void refreshArea() {
		// TODO Auto-generated method stub
		
	}


	
}
