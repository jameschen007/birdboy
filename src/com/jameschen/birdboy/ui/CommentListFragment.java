package com.jameschen.birdboy.ui;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SportEventDetailActivity;
import com.jameschen.birdboy.adapter.CommentAdapter;
import com.jameschen.birdboy.base.BaseListFragment;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;

//
//返回字段说明
//
//字段 类型 描述 备注 值条件
//
//
//fromtype int 来源 1场馆2教练3活动 必填
//id int 场馆或者教练或者活动的id 必填
//
//pageno int 当前第几页 默认从0页开始
//
//pagenum int 每页展示的个数 pageno和pagenum要一起传，默认一页10个

public  final class CommentListFragment extends BaseListFragment<Comment> {

	private long id;
	private int fromtype;
	
	public CommentListFragment() {
		// TODO Auto-generated constructor stub
	}

	public static CommentListFragment getCommentListFragment(long id, int fromtype) {
		CommentListFragment commentListFragment=new CommentListFragment();
		commentListFragment.id = id;
		commentListFragment.fromtype = fromtype;
		return commentListFragment;
	}

	@Override
	protected   void initViews(View view) {
		mListView = (MyListView) view.findViewById(R.id.comment_list_view);
		if (mCommentAdapter==null) {
			mCommentAdapter = new CommentAdapter(getBaseActivity(),id);
			
		}
		mCommentAdapter.setStarVisable(fromtype!=3);//sport
		mListView.setAdapter(mCommentAdapter);
		
		initListAdapter(mCommentAdapter, uiFreshCommentListController);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (fromtype!=3) {
					return;
				}
				
				Object object = arg0.getAdapter().getItem(arg2);
				if (object == null) {
					return;
				}
				Comment comment =(Comment) object;
				Util.go2SendComment(getBaseActivity(),id,comment.getUserid(),comment.getUsernick(),ConstValues.TYPE_EVENT_VAL);

				
			}
		});
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 super.onCreateView(inflater, container, savedInstanceState);
		 View view = inflater.inflate(R.layout.comment_list, container, false);
		 initViews(view);
		
		 return view;
	}
	
	private CommentAdapter mCommentAdapter;
//	private int currentPage,perPageNum=10;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		 getDataLoader(createUrl(0), false, createInitPage(), uiFreshCommentListController);
	}
	
	private String createUrl(int pageNo) {
		Map<String, String> map = getPublicParamRequstMap();
		map.put("id", ""+id);
		map.put("fromtype", ""+fromtype);
		map.put("pagenum", ""+numPerPage);
		map.put("pageno", ""+pageNo);
		 String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_COMMENT_LIST_URL, map);
		 return url;
	}
	
	
	public void refreshListFromTop() {
			loadData(createUrl(0), false,createInitPage(), uiFreshCommentListController,null );

		
	}
	
	protected UIController uiFreshCommentListController = new UIController() {

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			if (errorCode>-1) {
				
			}
			if (page != null) {
				setErrorPage(errorCode,page);
			}
		}

		@Override
		public <E> void refreshUI(E content, Page page) {
	//		if (!isDetached()) {
				List<Comment> mComments = (List<Comment>) content;
				// refreash
				Log.i(TAG, "page=="+page.getCurrentPositon()+";total="+page.getTotalNum());
				int size =mComments.size();
				
				if (page.isLoadMore()!=Page.CLEAR) {
					page.setLoadMore(size<numPerPage?Page.NO_MORE:Page.LOAD_MORE);
				}
				
				refreshList(mComments, page);
	//		}

		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<List<Comment>>>() {
			}.getType();
		}
	};

	@Override
	protected String setRequestLoadMoreUrl(Page page) {
		int position = page.getCurrentPositon();
		if (position==0&&page.isLoadMore()!=Page.CLEAR) {//?
			page.setCurrentPositon(numPerPage);//
			page.setLoadMore(Page.LOAD_MORE);
			position=numPerPage;
		}else if (page.isLoadMore()==Page.CLEAR) {
			//do nothing...
		}else {
			position+=numPerPage;//next page
		}
		String url =createUrl(position/numPerPage);
		return url;
	}

	@Override
	protected void refreshArea() {
		// TODO Auto-generated method stub
		
	}

	
}