package com.jameschen.birdboy.model;

import java.io.Serializable;

import com.jameschen.birdboy.model.entity.ReqParam;


import android.R.integer;
import android.os.Parcel;
import android.os.Parcelable;

public class Page implements Parcelable, Serializable{
	private int  currentPositon;
	private int  totalNum;
	public static final int LOAD_MORE=1,CLEAR=3,NO_MORE=2;
	private int loadMore ;//0,1
	private  String  tag;
	private ReqParam reqParam;
	public ReqParam getReqParam() {
		return reqParam;
	}
	public void setReqParam(ReqParam reqParam) {
		this.reqParam = reqParam;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public int  isLoadMore() {
		return loadMore;
	}
	public void setLoadMore(int loadMore) {
		this.loadMore = loadMore;
	}
	public int getCurrentPositon() {
		return currentPositon;
	}
	public void setCurrentPositon(int currentPositon) {
		this.currentPositon = currentPositon;
	}
	public int getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	private static final long serialVersionUID = 3637682994695160511L;
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
	
		dest.writeInt(currentPositon);
		dest.writeInt(totalNum);
		dest.writeInt(loadMore);
		dest.writeString(tag);
	}


	public static final Parcelable.Creator<Page> CREATOR = new Creator<Page>() {

		@Override
		public Page createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Page page = new Page();
			page.currentPositon = source.readInt();
			page.totalNum = source.readInt();
			page.loadMore = source.readInt();
			page.tag  = source.readString();
			return page;
		}

		@Override
		public Page[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Page[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


}
