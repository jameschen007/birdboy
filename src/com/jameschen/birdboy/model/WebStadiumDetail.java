package com.jameschen.birdboy.model;

import java.util.List;

import com.jameschen.birdboy.model.entity.AreaPriceRangs;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;
import com.jameschen.birdboy.model.entity.StadiumPhoto;

public class WebStadiumDetail {
	private StadiumDetailInfo bvo;
	private List<Comment> comments;
	private String areaprice;
	private List<AreaPriceRangs>areas;
	private List<StadiumPhoto> photos;
	
	public List<StadiumPhoto> getPhotos() {
		return photos;
	}
	public void setPhotos(List<StadiumPhoto> photos) {
		this.photos = photos;
	}
	public StadiumDetailInfo getBvo() {
		return bvo;
	}
	public void setBvo(StadiumDetailInfo bvo) {
		this.bvo = bvo;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public String getAreaprice() {
		return areaprice;
	}
	public void setAreaprice(String areaprice) {
		this.areaprice = areaprice;
	}
	public List<AreaPriceRangs> getAreas() {
		return areas;
	}
	public void setAreas(List<AreaPriceRangs> areas) {
		this.areas = areas;
	}
	
}
