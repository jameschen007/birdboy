package com.jameschen.birdboy.model.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class QQ implements Parcelable{
	private String token;
	private String openid;
	
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	private String nick;
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(token);
		dest.writeString(nick);
		dest.writeString(openid);
	}

	public static final Parcelable.Creator<QQ> CREATOR = new Creator<QQ>() {

		@Override
		public QQ createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			QQ qq = new QQ();
			qq.token =source.readString();
			qq.nick =source.readString();
			qq.openid =source.readString();
			return qq;
		}

		@Override
		public QQ[] newArray(int size) {
			// TODO Auto-generated method stub
			return new QQ[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


}
