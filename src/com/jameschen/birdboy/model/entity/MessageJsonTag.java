package com.jameschen.birdboy.model.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.jameschen.plugin.weibo.SNSUser;
import com.sina.weibo.sdk.openapi.models.Status;

public class MessageJsonTag {
	public int fromtype;
	public  int aid;
	
	public static MessageJsonTag parse(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return MessageJsonTag.parse(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return null;
    }
	 public static MessageJsonTag parse(JSONObject jsonObject) {
	        if (null == jsonObject) {
	            return null;
	        }
	        
	        MessageJsonTag messageJsonTag = new MessageJsonTag();
	        messageJsonTag.fromtype                = jsonObject.optInt("fromtype",-1);
	        messageJsonTag.aid              = jsonObject.optInt("aid",-1);
	     
	        return messageJsonTag;
	    }
    
}
