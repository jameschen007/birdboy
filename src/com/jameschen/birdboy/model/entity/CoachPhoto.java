package com.jameschen.birdboy.model.entity;

public class CoachPhoto {
	//{"id":47,"coachid":2,"title":"","info":"","uptime":"Jul 12, 2013 10:57:29 AM","url":"/swebs/upload/CP_500_zhouhui.jpg","surl":""}
private int id;
private int coachid;

private String title;

private  String info;


private String uptime;

private String url;


private String surl;


public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public int getCoachid() {
	return coachid;
}


public void setCoachid(int coachid) {
	this.coachid = coachid;
}


public String getTitle() {
	return title;
}


public void setTitle(String title) {
	this.title = title;
}


public String getInfo() {
	return info;
}


public void setInfo(String info) {
	this.info = info;
}


public String getUptime() {
	return uptime;
}


public void setUptime(String uptime) {
	this.uptime = uptime;
}


public String getUrl() {
	return url;
}


public void setUrl(String url) {
	this.url = url;
}


public String getSurl() {
	return surl;
}


public void setSurl(String surl) {
	this.surl = surl;
}

}
