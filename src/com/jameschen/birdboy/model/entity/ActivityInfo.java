package com.jameschen.birdboy.model.entity;

import android.R.integer;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ActivityInfo  implements Parcelable{

	private String name;

	private String uname;

	private int checkstatus;// 字段标示状态，checkstatus=1 表示报名通过审核，其他状态都是未审核状态。
	

	private String place;

	private String stimeStr;

	private String etimeStr;
	
	private int type;

	private int uactionid;
	
	
	
	public String getUname() {
		return uname;
	}




	public void setUname(String uname) {
		this.uname = uname;
	}
	
	public int getUactionid() {
		return uactionid;
	}




	public void setUactionid(int uactionid) {
		this.uactionid = uactionid;
	}




	public int getUserid() {
		return userid;
	}




	public void setUserid(int userid) {
		this.userid = userid;
	}

	private int userid;

	private String position;
	
	private  int nownum;
	
	private  int num;
	
	public String getStimeStr() {
		return stimeStr;
	}




	public int getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(int checkstatus) {
		this.checkstatus = checkstatus;
	}

	public void setStimeStr(String stimeStr) {
		this.stimeStr = stimeStr;
	}




	public String getEtimeStr() {
		return etimeStr;
	}




	public void setEtimeStr(String etimeStr) {
		this.etimeStr = etimeStr;
	}




	public int getNownum() {
		return nownum;
	}




	public void setNownum(int nownum) {
		this.nownum = nownum;
	}




	public int getNum() {
		return num;
	}




	public void setNum(int num) {
		this.num = num;
	}




	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(uactionid);
		dest.writeString(name);
		dest.writeString(stimeStr);
		dest.writeString(etimeStr);
		dest.writeInt(type);
		dest.writeString(place);
		dest.writeString(position);
		dest.writeInt(userid);
	}

	public static final Parcelable.Creator<ActivityInfo> CREATOR = new Creator<ActivityInfo>() {

		@Override
		public ActivityInfo createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			ActivityInfo mSportEvent = new ActivityInfo();
			mSportEvent.uactionid = source.readInt();
			mSportEvent.name = source.readString();		
			mSportEvent.stimeStr = source.readString();
			mSportEvent.etimeStr = source.readString();
			mSportEvent.type = source.readInt();
			mSportEvent.place = source.readString();
			mSportEvent.position = source.readString();
			mSportEvent.userid = source.readInt();
			return mSportEvent;
		}

		@Override
		public ActivityInfo[] newArray(int size) {
			// TODO Auto-generated method stub
			return new ActivityInfo[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getId() {
		return uactionid;
	}

	public void setId(int id) {
		this.uactionid = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getStime() {
		return stimeStr;
	}

	public void setStime(String stime) {
		this.stimeStr = stime;
	}

	public String getEtime() {
		return etimeStr;
	}

	public void setEtime(String etime) {
		this.etimeStr = etime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}


	private int commentCount;

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}


	public int getCommentCount() {
		// TODO Auto-generated method stub
		return commentCount;
	}



}
