
package com.jameschen.birdboy.model.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ChatMsgEntity implements Parcelable{

    public static final String Tag = "chatMsg";

	private String name;

    private String date;

    private String text;
    
    private String recommedId;
 
	private String randomCode;
    
    private String uid;
    
    public String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(String randomCode) {
		this.randomCode = randomCode;
	}

	public String getUid() {
		return uid;
	}
	   
    public String getRecommedId() {
		return recommedId;
	}

	public void setRecommedId(String recommedId) {
		this.recommedId = recommedId;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public static final int FROM=0,TO=1;
	
	public int isComMeg() {
		return isComMeg;
	}

	public void setComMeg(int isComMeg) {
		this.isComMeg = isComMeg;
	}

	private String id;
    
	
	
    private int isComMeg = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getMsgType() {
        return isComMeg;
    }

    public void setMsgType(int isComMsg) {
    	isComMeg = isComMsg;
    }

    public ChatMsgEntity() {
    //..
    }

    public ChatMsgEntity(String name, String date, String text, int isComMsg) {
        super();
        this.name = name;
        this.date = date;
        this.text = text;
        this.isComMeg = isComMsg;
    }

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(text);
		dest.writeString(name);
		dest.writeString(date);
		dest.writeInt(isComMeg);
	}
	public static final Parcelable.Creator<ChatMsgEntity> CREATOR=new Creator<ChatMsgEntity>() {

		@Override
		public ChatMsgEntity createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			ChatMsgEntity chatMsgEntity = new ChatMsgEntity();
			chatMsgEntity.text=source.readString();
			chatMsgEntity.name=source.readString();
			chatMsgEntity.date=source.readString();
			chatMsgEntity.isComMeg=source.readInt();
			return chatMsgEntity;
		}

		@Override
		public ChatMsgEntity[] newArray(int size) {
			// TODO Auto-generated method stub
			return new ChatMsgEntity[size];
		}
		
	};
}
