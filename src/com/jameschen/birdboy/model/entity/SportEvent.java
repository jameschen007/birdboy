package com.jameschen.birdboy.model.entity;

import android.R.integer;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SportEvent  implements Parcelable{

	private String name;

	private String place;

	private String stimeStr;

	private String timeStr;
	
	public String getTimeStr() {
		return timeStr;
	}


	public void setTimeStr(String timeStr) {
		this.timeStr = timeStr;
	}

	private String etimeStr;

	private double dist=-1;
	
	private int  keeps;
	
	private int comments;
	private String contact;
	private int addint;
	public int getAddint() {
		return addint;
	}


	public void setAddint(int addint) {
		this.addint = addint;
	}

	private String  users;
	
	public String getUsers() {
		return users;
	}


	public void setUsers(String users) {
		this.users = users;
	}


	public String getContact() {
		return contact;
	}


	public void setContact(String contact) {
		this.contact = contact;
	}

	private int sweek;

	private int type;

	private int id;
	
	private float rmb; 
	
	private String addtext;

	private String position;
	
	private  int nownum;
	
	private  int num;
	
	public int getComments() {
		return comments;
	}


	public void setComments(int comments) {
		this.comments = comments;
	}
	
	public int getKeeps() {
		return keeps;
	}




	public void setKeeps(int keeps) {
		this.keeps = keeps;
	}




	public int getSweek() {
		return sweek;
	}




	public void setSweek(int sweek) {
		this.sweek = sweek;
	}

	
	public double getDist() {
		return dist*1000;
	}




	public void setDist(double dist) {
		this.dist = dist;
	}
	
	public String getStimeStr() {
		return stimeStr;
	}




	public void setStimeStr(String stimeStr) {
		this.stimeStr = stimeStr;
	}




	public String getEtimeStr() {
		return etimeStr;
	}




	public void setEtimeStr(String etimeStr) {
		this.etimeStr = etimeStr;
	}


	private int wantnum;

	public int getWantnum() {
		return wantnum;
	}


	public void setWantnum(int wantnum) {
		this.wantnum = wantnum;
	}


	public int getNownum() {
		return nownum;
	}




	public void setNownum(int nownum) {
		this.nownum = nownum;
	}




	public int getNum() {
		return num;
	}




	public void setNum(int num) {
		this.num = num;
	}




	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(timeStr);
		dest.writeString(stimeStr);
		dest.writeString(etimeStr);
		dest.writeInt(type);
		dest.writeInt(comments);
		dest.writeInt(keeps);		
		dest.writeInt(addint);
		dest.writeInt(wantnum);
		dest.writeString(place);
		dest.writeString(position);
		dest.writeString(addtext);
		dest.writeString(contact);
		dest.writeString(users);
		dest.writeFloat(rmb);
		dest.writeDouble(dist);
	}

	public static final Parcelable.Creator<SportEvent> CREATOR = new Creator<SportEvent>() {

		@Override
		public SportEvent createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			SportEvent mSportEvent = new SportEvent();
			mSportEvent.id = source.readInt();
			mSportEvent.name = source.readString();		
			mSportEvent.timeStr = source.readString();
			mSportEvent.stimeStr = source.readString();
			mSportEvent.etimeStr = source.readString();
			mSportEvent.type = source.readInt();
			mSportEvent.comments = source.readInt();
			mSportEvent.keeps = source.readInt();
			mSportEvent.addint = source.readInt();
			mSportEvent.wantnum = source.readInt();
			mSportEvent.place = source.readString();
			mSportEvent.position = source.readString();
			mSportEvent.addtext = source.readString();
			mSportEvent.contact = source.readString();
			mSportEvent.users = source.readString();
			mSportEvent.rmb =source.readFloat();
			mSportEvent.dist = source.readDouble();
			return mSportEvent;
		}

		@Override
		public SportEvent[] newArray(int size) {
			// TODO Auto-generated method stub
			return new SportEvent[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getStime() {
		return stimeStr;
	}

	public void setStime(String stime) {
		this.stimeStr = stime;
	}

	public String getEtime() {
		return etimeStr;
	}

	public void setEtime(String etime) {
		this.etimeStr = etime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getAddtext() {
		return addtext;
	}

	public void setAddtext(String addtext) {
		this.addtext = addtext;
	}


	public float getRmb() {
		return rmb;
	}


	public void setRmb(float rmb) {
		this.rmb = rmb;
	}

}
