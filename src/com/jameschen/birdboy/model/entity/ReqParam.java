package com.jameschen.birdboy.model.entity;

import java.util.Map;

import com.android.volley.Request;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;

public class ReqParam {
//	null, Request.Method.GET, requstUrl, cacheSave, page,
//	uiController, httpStack
	
	private String reqMapStr;
	private int  method;
	private String url;
	private boolean cacheSave;
	private boolean hasHttpStack;
	public boolean isHasHttpStack() {
		return hasHttpStack;
	}
	public void setHasHttpStack(boolean hasHttpStack) {
		this.hasHttpStack = hasHttpStack;
	}
	public Map<String, String> getReqMap() {
		if (reqMapStr==null) {
			return null;
		}
		return  null;
//		String params[]=reqMapStr.split("|");
//		if (cacheSave) {
//			
//		}
//		return reqMap;
	}
	public void setReqMap(Map<String, String> reqMap) {
		//this.reqMap = reqMap;
	}
	public int getMethod() {
		return method;
	}
	public void setMethod(int method) {
		this.method = method;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isCacheSave() {
		return cacheSave;
	}
	public void setCacheSave(boolean cacheSave) {
		this.cacheSave = cacheSave;
	}

}
