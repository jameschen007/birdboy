package com.jameschen.birdboy.model.entity;

public class SportUser {
	private int sex ;
	private int id;
	private String phone ;
	private String name;
	private String logo;
	private String blogo;
	public String getBlogo() {
		return blogo;
	}
	public void setBlogo(String blogo) {
		this.blogo = blogo;
	}
	public String getLogo() {
		return logo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
