package com.jameschen.birdboy.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class SNS implements Parcelable{

	private Weibo weibo;
	
	public Weibo getWeibo() {
		return weibo;
	}

	public void setWeibo(Weibo weibo) {
		this.weibo = weibo;
	}

	public QQ getQq() {
		return qq;
	}

	public void setQq(QQ qq) {
		this.qq = qq;
	}

	private QQ qq;
	

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(weibo, flags);
		dest.writeParcelable(qq, flags);
	}

	public static final Parcelable.Creator<SNS> CREATOR = new Creator<SNS>() {

		@Override
		public SNS createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			SNS sns = new SNS();
			sns.weibo =source.readParcelable(Weibo.class.getClassLoader());
			sns.qq =source.readParcelable(QQ.class.getClassLoader());
			return sns;
		}

		@Override
		public SNS[] newArray(int size) {
			// TODO Auto-generated method stub
			return new SNS[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

}
