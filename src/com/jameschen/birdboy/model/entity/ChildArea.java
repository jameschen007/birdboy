package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

public class ChildArea implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int childarea;

	private String childname;

	public int getChildarea() {
		return childarea;
	}

	public void setChildarea(int childarea) {
		this.childarea = childarea;
	}

	public String getChildname() {
		return childname;
	}

	public void setChildname(String childname) {
		this.childname = childname;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return childname;
	}

//new api 
	private int id;
	private String name;
	private int count;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
