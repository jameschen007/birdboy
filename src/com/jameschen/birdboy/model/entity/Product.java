package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

//"id": 33, 
//"content": "8次课，每次课一小时。基础培训正手、反手、发球、截击、切削、高压球、挑高球、放小球等技术。(报名人数4~6人。500元/人，可以免费试听!押金50元)", 
//"url": "http://birdboy.cn/coach/11.html", 
//"slogo": "/swebs/upload/CP_500_zhaopai.jpg", 
//"blogo": "/swebs/upload/CP_500_zhaopai.jpg", 
//"uptime": "Aug 7, 2013 1:41:31 PM", 
//"name": "网球普训班", 
//"type": 3

public class Product  implements Parcelable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;

	private String info;

	private String total;

	private int status;

	private int num;
	
	private float rmb;

	private String name;

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeInt(status);
		dest.writeString(total);
		dest.writeInt(num);
		dest.writeFloat(rmb);
		dest.writeString(info);
	}

	public static final Parcelable.Creator<Product> CREATOR = new Creator<Product>() {

		@Override
		public Product createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Product mProduct = new Product();
			mProduct.id = source.readInt();
			mProduct.name = source.readString();		
			mProduct.status = source.readInt();
			mProduct.total = source.readString();
			mProduct.num = source.readInt();
			mProduct.rmb = source.readFloat();
			mProduct.info = source.readString();
			return mProduct;
		}

		@Override
		public Product[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Product[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	public float getRmb() {
		return rmb;
	}

	public void setRmb(float rmb) {
		this.rmb = rmb;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
