package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;
//"position":"104.032341,30.684013",
//"phone":"028-82940388",
//"typedesc":4,
//"type":1,
//"info":"球场塑胶新旧程度：比较旧；灯光效果：不详；是否有观众席位：无；能否承办篮球比赛：不详；能否提供比赛裁判：不详。",
//"id":7,
//"childarea":606,
//"name":"荣德网球场",
//"positionsubway":1,
//"notice":"体育中心停车位众多，停车方便。休闲广场还有羽毛球、乒乓球、游泳运动场所。",
//"worktime":"09:00--23:00",
//"mobile":"02882940388",
//"positiondesc":"青羊区清溪东路99号",
//"_version_":1454189618926714880},

public class Stadium extends Entity implements Parcelable, Serializable{

	private  String position;
	
	private  int type;
	
	private int positionsubway;
	
	private int typedesc;
	
	private String logo;
	
	private String phone;
	
	private  String worktime;
	
	private  String notice;	
	
	private  String info;
	
	private String  name;
	
	private String  positiondesc;
	
	private String  mobile;
	
	private int childarea=-1;
	
	private  int id=-1;
	private float scores;
	private int positionarea=-1;
	public int getPositionarea() {
		return positionarea;
	}

	public void setPositionarea(int positionarea) {
		this.positionarea = positionarea;
	}


	private int options;
	
	private String _version_;
	//default we set 0, if distance >0 we not need calcu the distance everytime
	private float  dist=.0f;
	
	public float getDist() {
			return dist*1000;
		}

		public void setDist(float dist) {
			this.dist = dist;
		}

	
	public int getOptions() {
		return options;
	}

	public void setOptions(int options) {
		this.options = options;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public float getScores() {
		return scores;
	}

	public void setScores(float scores) {
		this.scores = scores;
	}
	

	private static final long serialVersionUID = 3637682994695160511L;
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getPositionsubway() {
		return positionsubway;
	}

	public void setPositionsubway(int positionsubway) {
		this.positionsubway = positionsubway;
	}

	public int getTypedesc() {
		return typedesc;
	}

	public void setTypedesc(int typedesc) {
		this.typedesc = typedesc;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWorktime() {
		return worktime;
	}

	public void setWorktime(String worktime) {
		this.worktime = worktime;
	}

	public String getPositiondesc() {
		return positiondesc;
	}

	public void setPositiondesc(String positiondesc) {
		this.positiondesc = positiondesc;
	}

	public String getWorkTime() {
		return worktime;
	}

	public void setWorkTime(String worktime) {
		this.worktime = worktime;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPositionDesc() {
		return positiondesc;
	}

	public void setPositionDesc(String positiondesc) {
		this.positiondesc = positiondesc;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	public int getChildarea() {
		return childarea;
	}

	public void setChildarea(int childarea) {
		this.childarea = childarea;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String get_version_() {
		return _version_;
	}

	public void set_version_(String _version_) {
		this._version_ = _version_;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(_version_);
		dest.writeString(worktime);
		dest.writeString(notice);
		dest.writeInt(childarea);
		dest.writeString(info);
		dest.writeString(positiondesc);
		dest.writeString(mobile);
		dest.writeInt(type);
		dest.writeInt(positionsubway);
		dest.writeString(position);
		dest.writeInt(typedesc);
		dest.writeString(phone);
		dest.writeString(logo);
		dest.writeFloat(dist);
		dest.writeFloat(scores);
		dest.writeInt(options);
		dest.writeInt(positionarea);
	}

	public static final Parcelable.Creator<Stadium> CREATOR = new Creator<Stadium>() {

		@Override
		public Stadium createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Stadium mGymnasium = new Stadium();
			mGymnasium.id = source.readInt();
			mGymnasium.name = source.readString();
			mGymnasium._version_ = source.readString();
			mGymnasium.worktime = source.readString();
			mGymnasium.notice = source.readString();
			mGymnasium.childarea = source.readInt();
			mGymnasium.info = source.readString();
			mGymnasium.positiondesc = source.readString();
			mGymnasium.mobile = source.readString();
			mGymnasium.type = source.readInt();
			mGymnasium.positionsubway = source.readInt();
			mGymnasium.position = source.readString();
			mGymnasium.typedesc = source.readInt();
			mGymnasium.phone = source.readString();
			mGymnasium.logo = source.readString();
			mGymnasium.dist =source.readFloat();
			mGymnasium.scores =source.readFloat();
			mGymnasium.options =source.readInt();
			mGymnasium.positionarea =source.readInt();
			return mGymnasium;
		}

		@Override
		public Stadium[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Stadium[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
