package com.jameschen.birdboy.model.entity;

import java.io.Serializable;


public class Version implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2547834665736179457L;
	private int anc ;
	private boolean isforce;
	private boolean isupdate;
	private String content;
	private String url;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getAnc() {
		return anc;
	}
	public void setAnc(int anc) {
		this.anc = anc;
	}
	public boolean isIsforce() {
		return isforce;
	}
	public void setIsforce(boolean isforce) {
		this.isforce = isforce;
	}
	public boolean isIsupdate() {
		return isupdate;
	}
	public void setIsupdate(boolean isupdate) {
		this.isupdate = isupdate;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
