package com.jameschen.birdboy.model.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;


public class Message implements Parcelable{

	private  int sid;
	
	private int id;
	private String jsoninfo;//maybe null
	public String getJsoninfo() {
		return jsoninfo;
	}

	public void setJsoninfo(String jsoninfo) {
		this.jsoninfo = jsoninfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	private String sname;
	
	private int  revid;
	
	private String revname;
	
	private String content;
	
	private int fromtype;
	
	private int status;
	
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public int getRevid() {
		return revid;
	}

	public void setRevid(int revid) {
		this.revid = revid;
	}

	public String getRevname() {
		return revname;
	}

	public void setRevname(String revname) {
		this.revname = revname;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getFromtype() {
		return fromtype;
	}

	public void setFromtype(int fromtype) {
		this.fromtype = fromtype;
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(content);
		dest.writeString(revname);
		dest.writeString(sname);
		dest.writeString(jsoninfo);
		dest.writeInt(fromtype);
		dest.writeInt(sid);
		dest.writeInt(revid);
		dest.writeInt(status);
		dest.writeInt(id);
	}
	public static final Parcelable.Creator<Message> CREATOR=new Creator<Message>() {

		@Override
		public Message createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Message chatMsgEntity = new Message();
			chatMsgEntity.content=source.readString();
			chatMsgEntity.revname=source.readString();
			chatMsgEntity.sname=source.readString();
			chatMsgEntity.jsoninfo=source.readString();
			chatMsgEntity.fromtype=source.readInt();
			chatMsgEntity.sid=source.readInt();
			chatMsgEntity.revid=source.readInt();
			chatMsgEntity.status=source.readInt();
			chatMsgEntity.id=source.readInt();
			return chatMsgEntity;
		}

		@Override
		public Message[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Message[size];
		}
		
	};
}
