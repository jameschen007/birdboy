package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

import android.text.TextUtils;

public class Location implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String address;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLont() {
		return lont;
	}
	public void setLont(double lont) {
		this.lont = lont;
	}
	private double lat,lont;
	
	public static Location CreateLocation(String address, String positionStr) {
		if (TextUtils.isEmpty(positionStr)) {
			return null;
		}
		String position[] = positionStr.split("\\s+");
		if (position==null||position.length<2) {
			return null;
		}
		Location location = new Location();
		location.setAddress(address);
		try {
			location.setLat(Double.parseDouble(position[1]));
			location.setLont(Double.parseDouble(position[0]));
		} catch (NumberFormatException e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	
		return location;
	}
	
}
