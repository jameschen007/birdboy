package com.jameschen.birdboy.model.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Weibo implements Parcelable{
	private String token;
	private long expires_in;
	
	public long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	private String nick;
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(token);
		dest.writeString(nick);
		dest.writeLong(expires_in);
	}

	public static final Parcelable.Creator<Weibo> CREATOR = new Creator<Weibo>() {

		@Override
		public Weibo createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Weibo weibo = new Weibo();
			weibo.token =source.readString();
			weibo.nick =source.readString();
			weibo.expires_in =source.readLong();
			return weibo;
		}

		@Override
		public Weibo[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Weibo[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


}
