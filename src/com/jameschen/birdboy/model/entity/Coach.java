package com.jameschen.birdboy.model.entity;

import java.io.Serializable;


import android.R.integer;
import android.os.Parcel;
import android.os.Parcelable;
public class Coach  extends Entity implements Parcelable, Serializable {

	private  String classes;
	
	private  String logo;
	
	private  float 	scores;
	
	private  String qq;
	
	private String  name;
	
	private String  skill;
	
	private String  addtext;
	
	private String  mobile;
	
	private int  area;
	
	private int status;
	
	private int addint;
	
	private int type;
	
	private int childarea;

	private float level;
	
	private  int id;
	
	private String _version_;
	
	private String position;
	//default we set 0, if distance >0 we not need calcu the distance everytime
		private float  dist=.0f;

		private int jobtype;
	
	public float getDist() {
			return dist*1000;
		}

		public int getJobtype() {
		return jobtype;
	}

	public void setJobtype(int jobtype) {
		this.jobtype = jobtype;
	}

		public void setDist(float dist) {
			this.dist = dist;
		}

	public String getPosition() {
			return position;
		}

		public void setPosition(String position) {
			this.position = position;
		}

		
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getAddint() {
		return addint;
	}

	public void setAddint(int addint) {
		this.addint = addint;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getAddtext() {
		return addtext;
	}

	public void setAddtext(String addtext) {
		this.addtext = addtext;
	}

	private static final long serialVersionUID = 3637682994695160511L;
	
	public String getClasses() {
		return classes;
	}

	public void setClasses(String classes) {
		this.classes = classes;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public float getScores() {
		return scores;
	}

	public void setScores(float score) {
		this.scores = score;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public int getChildarea() {
		return childarea;
	}

	public void setChildarea(int childarea) {
		this.childarea = childarea;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String get_version_() {
		return _version_;
	}

	public void set_version_(String _version_) {
		this._version_ = _version_;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(_version_);
		dest.writeInt(area);
		dest.writeString(classes);
		dest.writeString(logo);
		dest.writeInt(childarea);
		dest.writeFloat(level);
		dest.writeString(qq);
		dest.writeString(skill);
		dest.writeInt(type);
		dest.writeInt(status);
		dest.writeInt(addint);
		dest.writeFloat(scores);
		dest.writeFloat(dist);
		dest.writeString(mobile);
		dest.writeString(addtext);
	}

	public static final Parcelable.Creator<Coach> CREATOR = new Creator<Coach>() {

		@Override
		public Coach createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Coach mCoach = new Coach();
			mCoach.id = source.readInt();
			mCoach.name = source.readString();
			mCoach._version_ = source.readString();
			mCoach.area = source.readInt();
			mCoach.classes = source.readString();
			mCoach.logo = source.readString();
			mCoach.childarea = source.readInt();
			mCoach.level = source.readFloat();
			mCoach.qq = source.readString();
			mCoach.skill = source.readString();
			mCoach.type = source.readInt();
			mCoach.status = source.readInt();
			mCoach.addint = source.readInt();
			mCoach.scores = source.readFloat();
			mCoach.dist =source.readFloat();
			mCoach.mobile = source.readString();
			mCoach.addtext = source.readString();
			return mCoach;
		}

		@Override
		public Coach[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Coach[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
