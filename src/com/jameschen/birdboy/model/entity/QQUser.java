/*
 * Copyright (C) 2010-2013 The SINA WEIBO Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jameschen.birdboy.model.entity;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.jameschen.birdboy.utils.Log;
import com.jameschen.plugin.qq.QQStatus;
import com.jameschen.plugin.weibo.SNSUser;
import com.sina.weibo.sdk.openapi.models.Status;

/**
 * 用户信息结构体。
 * 
 *qquserinfo={
    "data": {
        "birth_day": 1,
        "birth_month": 11,
        "birth_year": 1987,
        "city_code": "1",
        "comp": null,
        "country_code": "1",
        "edu": null,
        "email": "",
        "exp": 2485,
        "fansnum": 116,
        "favnum": 0,
        "head": "http:\/\/app.qlogo.cn\/mbloghead\/8a68da6ae3af17f7559c",
        "homecity_code": "",
        "homecountry_code": "",
        "homepage": "",
        "homeprovince_code": "",
        "hometown_code": "",
        "https_head": "https:\/\/app.qlogo.cn\/mbloghead\/8a68da6ae3af17f7559c",
        "idolnum": 110,
        "industry_code": 0,
        "introduction": "",
        "isent": 0,
        "ismyblack": 0,
        "ismyfans": 0,
        "ismyidol": 0,
        "isrealname": 1,
        "isvip": 0,
        "level": 3,
        "location": "中国 四川 成都",
        "mutual_fans_num": 20,
        "name": "jameschen007",
        "nick": "陈杨",
        "openid": "",
        "province_code": "51",
        "regtime": 1273754578,
        "send_private_flag": 2,
        "sex": 1,
        "tag": null,
        "tweetinfo": [
            {
                "city_code": "",
                "country_code": "",
                "emotiontype": 0,
                "emotionurl": "",
                "from": "腾讯微博",
                "fromurl": "http:\/\/t.qq.com\/\u000a",
                "geo": "",
                "id": "233096025789440",
                "image": [
                    "http:\/\/app.qpic.cn\/mblogpic\/cf9a7c008e0372d10f6a"
                ],
                "latitude": "0",
                "location": "未知",
                "longitude": "0",
                "music": null,
                "origtext": "&quot;I am what I am, 我喜欢我,让蔷薇开出一种结果。&quot; 《我》-by张国荣",
                "province_code": "",
                "self": 1,
                "status": 0,
                "text": "&quot;I am what I am, 我喜欢我,让蔷薇开出一种结果。&quot; 《我》-by张国荣",
                "timestamp": 1396197605,
                "type": 1,
                "video": null
            }
        ],
        "tweetnum": 331,
        "verifyinfo": ""
    },
    "errcode": 0,
    "msg": "ok",
    "ret": 0,
    "seqid": 5996891565001294552
}
 */
public class QQUser {
	
	private SNSUser user;
	
	
    public SNSUser getSNSUser() {
		return user;
	}

	public void setSNSUser(SNSUser user) {
		this.user = user;
	}


    /** 字符串型的用户 年月日 */
    public int birth_day,birth_month,birth_year;
   
    public String head,nick,location;
    
    public int fansnum,idolnum,tweetnum;
    
    public int sex;//1 boy

	private ArrayList<Status> tweetinfo;

	public int age=18;
   
    
    public static QQUser parse(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return QQUser.parse(jsonObject.optJSONObject("data"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    private static QQUser parse(JSONObject jsonObject) {
        if (null == jsonObject) {
            return null;
        }
        
        QQUser user = new QQUser();
        user.birth_day                 = jsonObject.optInt("birth_day", -1);
        user.birth_month              = jsonObject.optInt("birth_month", -1);
        user.birth_year        = jsonObject.optInt("birth_year", -1);
        user.fansnum               = jsonObject.optInt("fansnum", 0);
        user.head           = jsonObject.optString("head", null);
        user.location           = jsonObject.optString("location", null);
        
        user.idolnum               = jsonObject.optInt("idolnum", 0);
        user.tweetnum           = jsonObject.optInt("tweetnum", 0);
        user.sex        = jsonObject.optInt("sex", 1);
        user.nick                = jsonObject.optString("nick", "");
        Calendar calendar = Calendar.getInstance();
        // year
         int year = calendar.get(Calendar.YEAR);
         if (user.birth_year>1900&&(year-user.birth_year)>0&&(year-user.birth_year)<110) {
        	 user.age =year-user.birth_year;
		}
       
        
      SNSUser sSNSUser = new SNSUser();
      
      sSNSUser.followers_count=user.fansnum;
      sSNSUser.friends_count=user.idolnum;
      sSNSUser.statuses_count=user.tweetnum;
      sSNSUser.location = user.location;
      sSNSUser.avatar_large=user.head+"/100";
      sSNSUser.avatar_hd=user.head+"/100";
      sSNSUser.name =user.nick;
      if (user.sex==2) {
    	  sSNSUser.gender="f";
	}else {
		sSNSUser.gender="m";
	}
      
      JSONArray statusArray = jsonObject.optJSONArray("tweetinfo");
      if (statusArray != null && statusArray.length() > 0) {
          int length = statusArray.length();
          user.tweetinfo = new ArrayList<Status>(length);
          JSONObject tmpObject = null;
          for (int ix = 0; ix < length; ix++) {
              tmpObject = statusArray.optJSONObject(ix);
              if (tmpObject != null) {
            	  user.tweetinfo.add(QQStatus.parse(tmpObject));
              }
          }
      }else {
		Log.e("no status", "no status...");
	}
      
      if (user.tweetinfo!=null&&user.tweetinfo.size()>0) {
          sSNSUser.status  =user.tweetinfo.get(0);
      	}
    
      user.setSNSUser(sSNSUser);
        return user;
    }
}
