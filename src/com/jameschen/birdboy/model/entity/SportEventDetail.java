package com.jameschen.birdboy.model.entity;

import android.R.integer;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SportEventDetail  implements Parcelable{

	private String name;

	private String place;

	private String stimeStr;

	private String etimeStr;
	


	private String timeStr;
	
	public String getTimeStr() {
		return timeStr;
	}


	public void setTimeStr(String timeStr) {
		this.timeStr = timeStr;
	}

	private double dist;
	
	
	private SportEvent action;
	
	public SportEvent getAction() {
		return action;
	}



	public void setAction(SportEvent action) {
		this.action = action;
	}

	private String content;
	


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}

	private int sweek;

	private int type;

	private int id;
	
	private String addtext;

	private String position;
	
	private  int nownum;
	
	private  int num;


	public int getSweek() {
		return sweek;
	}




	public void setSweek(int sweek) {
		this.sweek = sweek;
	}

	
	public double getDist() {
		return dist*1000;
	}




	public void setDist(double dist) {
		this.dist = dist;
	}
	
	public String getStimeStr() {
		return stimeStr;
	}




	public void setStimeStr(String stimeStr) {
		this.stimeStr = stimeStr;
	}




	public String getEtimeStr() {
		return etimeStr;
	}




	public void setEtimeStr(String etimeStr) {
		this.etimeStr = etimeStr;
	}




	public int getNownum() {
		return nownum;
	}




	public void setNownum(int nownum) {
		this.nownum = nownum;
	}




	public int getNum() {
		return num;
	}




	public void setNum(int num) {
		this.num = num;
	}




	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(timeStr);
		dest.writeString(stimeStr);
		dest.writeString(etimeStr);
		dest.writeInt(type);
		dest.writeString(place);
		dest.writeString(position);
		dest.writeString(addtext);
	}

	public static final Parcelable.Creator<SportEventDetail> CREATOR = new Creator<SportEventDetail>() {

		@Override
		public SportEventDetail createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			SportEventDetail mSportEvent = new SportEventDetail();
			mSportEvent.id = source.readInt();
			mSportEvent.name = source.readString();		
			mSportEvent.timeStr = source.readString();
			mSportEvent.stimeStr = source.readString();
			mSportEvent.etimeStr = source.readString();
			mSportEvent.type = source.readInt();
			mSportEvent.place = source.readString();
			mSportEvent.position = source.readString();
			mSportEvent.addtext = source.readString();
			return mSportEvent;
		}

		@Override
		public SportEventDetail[] newArray(int size) {
			// TODO Auto-generated method stub
			return new SportEventDetail[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getStime() {
		return stimeStr;
	}

	public void setStime(String stime) {
		this.stimeStr = stime;
	}

	public String getEtime() {
		return etimeStr;
	}

	public void setEtime(String etime) {
		this.etimeStr = etime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getAddtext() {
		return addtext;
	}

	public void setAddtext(String addtext) {
		this.addtext = addtext;
	}

}
