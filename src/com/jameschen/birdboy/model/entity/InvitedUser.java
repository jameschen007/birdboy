package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

import android.R.integer;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class InvitedUser implements Parcelable, Serializable {

	private String name;
	private String dist;
	private String position;
	private String nick;
	private InvitePlugMember sns;
	
	public InvitePlugMember getSns() {
		return sns;
	}

	public void setSns(InvitePlugMember sns) {
		this.sns = sns;
	}

	private long uptime;
	

	public long getUptime() {
		return uptime;
	}

	public void setUptime(long uptime) {
		this.uptime = uptime;
	}

	public Drawable head;
	
	private String password;
	private String signstr;
	private String logo;
	private int mylike;
	public int getMylike() {
		return mylike;
	}

	public void setMylike(int mylike) {
		this.mylike = mylike;
	}

	private long mobile;
	private int type;
	private int id;
	private int age;
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	private String email;
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private int sex;
	
	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getNick() {
		return nick;
	}

	public void setNick(String nickname) {
		this.nick = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	private static final long serialVersionUID = 3637682994695160511L;
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
	
		dest.writeString(name);
		dest.writeString(nick);
		dest.writeString(password);
		dest.writeLong(mobile);
		dest.writeInt(type);
		dest.writeInt(id);
		dest.writeInt(sex);
		dest.writeInt(age);
		dest.writeInt(mylike);
		dest.writeString(email);
		dest.writeString(signstr);
		dest.writeString(position);
		dest.writeString(dist);
		dest.writeString(logo);
		dest.writeLong(uptime);
		dest.writeParcelable(sns, flags);
	}

	public static final Parcelable.Creator<InvitedUser> CREATOR = new Creator<InvitedUser>() {

		@Override
		public InvitedUser createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			InvitedUser inviteduser = new InvitedUser();
			inviteduser.name = source.readString();
			inviteduser.nick = source.readString();
			inviteduser.password = source.readString();
			inviteduser.mobile = source.readLong();
			inviteduser.type = source.readInt();
			inviteduser.id = source.readInt();
			inviteduser.sex = source.readInt();
			inviteduser.age = source.readInt();
			inviteduser.mylike = source.readInt();
			inviteduser.email = source.readString();
			inviteduser.signstr = source.readString();
			inviteduser.position = source.readString();
			inviteduser.dist = source.readString();
			inviteduser.logo = source.readString();
			inviteduser.uptime = source.readLong();
			inviteduser.sns = source.readParcelable(InvitePlugMember.class.getClassLoader());
			return inviteduser;
		}

		@Override
		public InvitedUser[] newArray(int size) {
			// TODO Auto-generated method stub
			return new InvitedUser[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getSex() {
		// TODO Auto-generated method stub
		return sex;
	}

	public double  getDist() {
		if (TextUtils.isEmpty(dist)) {
			return -1;
		}
		double dis =0;
		try {
			dis =Double.parseDouble(dist);
			dis = dis*1000;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dis;
	}

	public void setDist(String dist) {
		this.dist = dist;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSignstr() {
		return signstr;
	}

	public void setSignstr(String signstr) {
		this.signstr = signstr;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

}
