package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

public class StadiumDetailInfo     implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private  String classes;
	
	private  String logo;
	
	private String info;
	private String position;
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}


	private float  score;
	
	private int offset;
	
	private int limit;
	
	private  float 	avescore;
	
	private  String qq;
	
	private String  name;
	
	private String  skill;
	
	private String  mobile;
	
	private int  type;
	
	private int  buildid;
	
	private int commentCount;

	private float level;
	
	private  int id;
	
	private int status;
	
	private int jobtype;
	
	private String uptime;
	
	private int addint;
	
	private String addtext;
	
	private int area;
	
	private int  childarea;
	
	private int options;
	
	private String _version_;
	
	private String worktime;
	
	private String  positiondesc;
	
	public String getPositiondesc() {
		return positiondesc;
	}

	public void setPositiondesc(String positiondesc) {
		this.positiondesc = positiondesc;
	}



	public String getWorktime() {
		return worktime;
	}

	public void setWorktime(String worktime) {
		this.worktime = worktime;
	}

	public int getOptions() {
		return options;
	}

	public void setOptions(int options) {
		this.options = options;
	}

	public String getClasses() {
		return classes;
	}

	public void setClasses(String classes) {
		this.classes = classes;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}
	
	
	private String phone;
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public int getChildarea() {
		return childarea;
	}

	public void setChildarea(int childarea) {
		this.childarea = childarea;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String get_version_() {
		return _version_;
	}

	public void set_version_(String _version_) {
		this._version_ = _version_;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public float getAvescore() {
		return avescore;
	}

	public void setAvescore(float avescore) {
		this.avescore = avescore;
	}
	private int  typedesc;
	public int getTypedesc() {
		return typedesc;
	}

	public void setTypedesc(int typedesc) {
		this.typedesc = typedesc;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getBuildid() {
		return buildid;
	}

	public void setBuildid(int buildid) {
		this.buildid = buildid;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getJobtype() {
		return jobtype;
	}

	public void setJobtype(int jobtype) {
		this.jobtype = jobtype;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public int getAddint() {
		return addint;
	}

	public void setAddint(int addint) {
		this.addint = addint;
	}

	public String getAddtext() {
		return addtext;
	}

	public void setAddtext(String addtext) {
		this.addtext = addtext;
	}

	
}
