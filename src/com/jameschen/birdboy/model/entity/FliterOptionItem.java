package com.jameschen.birdboy.model.entity;

public class FliterOptionItem {

	private String name;
	private int type;
	private int iconResource;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getIconResource() {
		return iconResource;
	}
	public void setIconResource(int iconResource) {
		this.iconResource = iconResource;
	}
	
}
