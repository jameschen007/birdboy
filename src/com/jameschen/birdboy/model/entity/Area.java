package com.jameschen.birdboy.model.entity;

import java.util.List;

import android.R.integer;

public class Area {
private int id;

private String name;


public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

//new api 
private int count ;

private List<ChildArea> childs;


public int getCount() {
	return count;
}
public void setCount(int count) {
	this.count = count;
}
public List<ChildArea> getChilds() {
	return childs;
}
public void setChilds(List<ChildArea> childs) {
	this.childs = childs;
}
}
