package com.jameschen.birdboy.model.entity;


public class Comment {

	private int id;

	private String usernick;

	private int userid;
	
	private String content;

	private String uptime;

	private int score;

	private String createtime;

	private String ctstr;

	private int fromtype;

	private int buildid;

	private int offset;

	private int limit;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsernick() {
		return usernick;
	}

	public void setUsernick(String usernick) {
		this.usernick = usernick;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getCtstr() {
		return ctstr;
	}

	public void setCtstr(String ctstr) {
		this.ctstr = ctstr;
	}

	public int getFromtype() {
		return fromtype;
	}

	public void setFromtype(int fromtype) {
		this.fromtype = fromtype;
	}

	public int getBuildid() {
		return buildid;
	}

	public void setBuildid(int buildid) {
		this.buildid = buildid;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
