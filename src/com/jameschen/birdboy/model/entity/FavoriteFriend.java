package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

import android.R.integer;
import android.os.Parcel;
import android.os.Parcelable;

public class FavoriteFriend implements Parcelable, Serializable {

	private String username;
	private String dist;
	private String position;
	private String nick;
	

	private String password;
	private String signstr;
	private String logo;
	private int mylike;
	public int getMylike() {
		return mylike;
	}

	public void setMylike(int mylike) {
		this.mylike = mylike;
	}

	private long mobile;
	private int type;
	private int id;
	private int myuserid;
	private int age;
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	private String email;
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getMyuserid() {
		return myuserid;
	}

	public void setMyuserid(int myuserid) {
		this.myuserid = myuserid;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private int sex;
	private int userid;
	
	
	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	
	public String getNick() {
		return nick;
	}

	public void setNick(String nickname) {
		this.nick = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	private static final long serialVersionUID = 3637682994695160511L;
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
	
		dest.writeString(username);
		dest.writeString(nick);
		dest.writeString(password);
		dest.writeLong(mobile);
		dest.writeInt(type);
		dest.writeInt(id);
		dest.writeInt(sex);
		dest.writeInt(age);
		dest.writeInt(mylike);
		dest.writeInt(myuserid);
		dest.writeInt(userid);
		dest.writeString(email);
		dest.writeString(signstr);
		dest.writeString(position);
		dest.writeString(dist);
		dest.writeString(logo);
	}

	public static final Parcelable.Creator<FavoriteFriend> CREATOR = new Creator<FavoriteFriend>() {

		@Override
		public FavoriteFriend createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			FavoriteFriend inviteduser = new FavoriteFriend();
			inviteduser.username = source.readString();
			inviteduser.nick = source.readString();
			inviteduser.password = source.readString();
			inviteduser.mobile = source.readLong();
			inviteduser.type = source.readInt();
			inviteduser.id = source.readInt();
			inviteduser.sex = source.readInt();
			inviteduser.age = source.readInt();
			inviteduser.mylike = source.readInt();
			inviteduser.myuserid = source.readInt();
			inviteduser.userid = source.readInt();
			inviteduser.email = source.readString();
			inviteduser.signstr = source.readString();
			inviteduser.position = source.readString();
			inviteduser.dist = source.readString();
			inviteduser.logo = source.readString();
			return inviteduser;
		}

		@Override
		public FavoriteFriend[] newArray(int size) {
			// TODO Auto-generated method stub
			return new FavoriteFriend[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getSex() {
		// TODO Auto-generated method stub
		return sex;
	}

	public String  getDist() {
		return dist;
	}

	public void setDist(String dist) {
		this.dist = dist;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSignstr() {
		return signstr;
	}

	public void setSignstr(String signstr) {
		this.signstr = signstr;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

}
