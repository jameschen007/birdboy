package com.jameschen.birdboy.model.entity;

import java.io.Serializable;

import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.ParseJsonData;

import android.R.integer;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class Account implements Parcelable, Serializable {

	private String name;
	private String nick;
	private String password;
	private String signstr;
	private String logo;
	private int mylike;
	private int age;
	private long mobile;
	private int type;
	private int id;
	private String position;
	private String email;
	private String sns;
	
	public void setSns(String sns) {
		this.sns = sns;
//		Log.i("sns~~~", "sss=="+sns);
//		try {
//			if (TextUtils.isEmpty(sns)) {
//				return;
//			}
//			setSns(ParseJsonData.getSNSData(sns));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	public String getSns() {
		return sns;
	}

	private SNS snsObj;
	
	public SNS getSnsObj() {
		return snsObj;
	}

	public void setSns(SNS sns) {
		this.snsObj = sns;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private int sex;
	
	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nickname) {
		this.nick = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	private static final long serialVersionUID = 3637682994695160511L;
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
	
		dest.writeString(name);
		dest.writeString(nick);
		dest.writeString(password);
		dest.writeLong(mobile);
		dest.writeInt(type);
		dest.writeInt(id);
		dest.writeInt(sex);
		dest.writeInt(mylike);
		dest.writeInt(age);
		dest.writeString(email);
		dest.writeString(signstr);
		dest.writeString(logo);
		dest.writeParcelable(snsObj, flags);
	}

	public static final Parcelable.Creator<Account> CREATOR = new Creator<Account>() {

		@Override
		public Account createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Account account = new Account();
			account.name = source.readString();
			account.nick = source.readString();
			account.password = source.readString();
			account.mobile = source.readLong();
			account.type = source.readInt();
			account.id = source.readInt();
			account.sex = source.readInt();
			account.mylike = source.readInt();
			account.age = source.readInt();
			account.email = source.readString();
			account.signstr = source.readString();
			account.logo = source.readString();
			account.snsObj =source.readParcelable(SNS.class.getClassLoader());
			return account;
		}

		@Override
		public Account[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Account[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMylike() {
		return mylike;
	}

	public void setMylike(int mylike) {
		this.mylike = mylike;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSex() {
		// TODO Auto-generated method stub
		return sex;
	}

	public String getSignstr() {
		return signstr;
	}

	public void setSignstr(String signstr) {
		this.signstr = signstr;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
	public boolean hasWeibo() {
		if (getSnsObj()!=null) {
			return getSnsObj().getWeibo()!=null?true:false;
		}
		return false;
	}

	public boolean hasQq() {
		if (getSnsObj()!=null) {
			return getSnsObj().getQq()!=null?true:false;
		}
		return false;
	}

	
}
