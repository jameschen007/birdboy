package com.jameschen.birdboy.model.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CoachDetailInfo    implements Parcelable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private  String classes;
	
	private  String logo;
	
	private String info;
	
	private float  scores;
	
	private int offset;
	
	private int limit;
	
	private  float 	avescore;
	
	private  String qq;
	
	private String  name;
	
	private String  skill;
	
	private String  mobile;
	
	private int  type;
	
	private int  buildid;
	
	private int commentCount;

	private float level;
	
	private  int id;
	
	private int status;
	
	private int jobtype;
	
	private String uptime;
	
	private int addint;
	
	private String addtext;
	
	private int area;
	
	private int  childarea;
	
	private String _version_;
	
	private String position;
	
	private String blogo;
	private String frontuserid;
	
	public String getFrontuserid() {
		return frontuserid;
	}
	public void setFrontuserid(String frontuserid) {
		this.frontuserid = frontuserid;
	}
	//default we set 0, if distance >0 we not need calcu the distance everytime
		private float  distance=.0f;
	
		
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			// TODO Auto-generated method stub
			dest.writeInt(id);
			dest.writeString(name);
			dest.writeString(_version_);
			dest.writeInt(area);
			dest.writeString(classes);
			dest.writeString(logo);
			dest.writeInt(childarea);
			dest.writeFloat(level);
			dest.writeString(qq);
			dest.writeString(skill);
			dest.writeInt(type);
			dest.writeInt(status);
			dest.writeInt(addint);
			dest.writeFloat(scores);
			dest.writeString(mobile);
			dest.writeString(addtext);
			dest.writeString(info);
		}

		public static final Parcelable.Creator<CoachDetailInfo> CREATOR = new Creator<CoachDetailInfo>() {

			@Override
			public CoachDetailInfo createFromParcel(Parcel source) {
				// TODO Auto-generated method stub
				CoachDetailInfo mCoach = new CoachDetailInfo();
				mCoach.id = source.readInt();
				mCoach.name = source.readString();
				mCoach._version_ = source.readString();
				mCoach.area = source.readInt();
				mCoach.classes = source.readString();
				mCoach.logo = source.readString();
				mCoach.childarea = source.readInt();
				mCoach.level = source.readFloat();
				mCoach.qq = source.readString();
				mCoach.skill = source.readString();
				mCoach.type = source.readInt();
				mCoach.status = source.readInt();
				mCoach.addint = source.readInt();
				mCoach.scores = source.readFloat();
				mCoach.mobile = source.readString();
				mCoach.addtext = source.readString();
				mCoach.info = source.readString();
				return mCoach;
			}

			@Override
			public CoachDetailInfo[] newArray(int size) {
				// TODO Auto-generated method stub
				return new CoachDetailInfo[size];
			}
		};

		@Override
		public int describeContents() {
			// TODO Auto-generated method stub
			return 0;
		}
		
	public String getBlogo() {
			return blogo;
		}

		public void setBlogo(String blogo) {
			this.blogo = blogo;
		}

	public String getPosition() {
			return position;
		}

		public void setPosition(String position) {
			this.position = position;
		}

		public float getDistance() {
			return distance;
		}

		public void setDistance(float distance) {
			this.distance = distance;
		}

	public String getClasses() {
		return classes;
	}

	public void setClasses(String classes) {
		this.classes = classes;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public float getScores() {
		return scores;
	}

	public void setScores(float score) {
		this.scores = score;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public int getChildarea() {
		return childarea;
	}

	public void setChildarea(int childarea) {
		this.childarea = childarea;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String get_version_() {
		return _version_;
	}

	public void set_version_(String _version_) {
		this._version_ = _version_;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public float getAvescore() {
		return avescore;
	}

	public void setAvescore(float avescore) {
		this.avescore = avescore;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getBuildid() {
		return buildid;
	}

	public void setBuildid(int buildid) {
		this.buildid = buildid;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getJobtype() {
		return jobtype;
	}

	public void setJobtype(int jobtype) {
		this.jobtype = jobtype;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public int getAddint() {
		return addint;
	}

	public void setAddint(int addint) {
		this.addint = addint;
	}

	public String getAddtext() {
		return addtext;
	}

	public void setAddtext(String addtext) {
		this.addtext = addtext;
	}

	
}
