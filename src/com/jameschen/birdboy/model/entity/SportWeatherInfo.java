package com.jameschen.birdboy.model.entity;

public class SportWeatherInfo {
	
	public SportWeatherInfo(String advise,int resourceId) {
		this.advise = advise;
		this.resourceId = resourceId;
	}
	
 private String advise;
 public String getAdvise() {
	return advise;
}
public void setAdvise(String advise) {
	this.advise = advise;
}
public int getResourceId() {
	return resourceId;
}
public void setResourceId(int resourceId) {
	this.resourceId = resourceId;
}
private int resourceId;
}
