package com.jameschen.birdboy.model.entity;
//"id": 33, 
//"content": "8次课，每次课一小时。基础培训正手、反手、发球、截击、切削、高压球、挑高球、放小球等技术。(报名人数4~6人。500元/人，可以免费试听!押金50元)", 
//"url": "http://birdboy.cn/coach/11.html", 
//"slogo": "/swebs/upload/CP_500_zhaopai.jpg", 
//"blogo": "/swebs/upload/CP_500_zhaopai.jpg", 
//"uptime": "Aug 7, 2013 1:41:31 PM", 
//"name": "网球普训班", 
//"type": 3

public class Notice {

	private int id;

	private String content;

	private String url;

	private String slogo;

	private String blogo;

	private String uptime;

	private int type;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSlogo() {
		return slogo;
	}

	public void setSlogo(String slogo) {
		this.slogo = slogo;
	}

	public String getBlogo() {
		return blogo;
	}

	public void setBlogo(String blogo) {
		this.blogo = blogo;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
