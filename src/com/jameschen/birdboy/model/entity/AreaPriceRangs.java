package com.jameschen.birdboy.model.entity;
//"md": "12-17",
//"ymd": "2013-12-17",
//"priceRange": "40~40",
//"desc": "暂无打折",
//"weekStr": "星期二"

public class AreaPriceRangs {

	private  String  md;
	private  String  ymd;
	private  String  priceRange;
	private  String  desc;
	private  String  weekStr;
	public String getMd() {
		return md;
	}
	public void setMd(String md) {
		this.md = md;
	}
	public String getYmd() {
		return ymd;
	}
	public void setYmd(String ymd) {
		this.ymd = ymd;
	}
	public String getPriceRange() {
		return priceRange;
	}
	public void setPriceRange(String priceRange) {
		this.priceRange = priceRange;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getWeekStr() {
		return weekStr;
	}
	public void setWeekStr(String weekStr) {
		this.weekStr = weekStr;
	}
}
