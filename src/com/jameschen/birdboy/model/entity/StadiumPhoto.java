package com.jameschen.birdboy.model.entity;

public class StadiumPhoto {
//	 "id": 55, 
//     "buildid": 4, 
//     "title": "cover", 
//     "info": "", 
//     "uptime": "Jun 3, 2013 10:24:32 AM", 
//     "url": "/swebs/upload/500_20130516_130044.jpg", 
//     "surl": "/swebs/upload/75_20130516_130044.jpg"
private int id;
private int buildid;

private String title;

private  String info;


private String uptime;

private String url;


private String surl;


public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public int getBuildid() {
	return buildid;
}


public void setBuildidid(int buildid) {
	this.buildid = buildid;
}


public String getTitle() {
	return title;
}


public void setTitle(String title) {
	this.title = title;
}


public String getInfo() {
	return info;
}


public void setInfo(String info) {
	this.info = info;
}


public String getUptime() {
	return uptime;
}


public void setUptime(String uptime) {
	this.uptime = uptime;
}


public String getUrl() {
	return url;
}


public void setUrl(String url) {
	this.url = url;
}


public String getSurl() {
	return surl;
}


public void setSurl(String surl) {
	this.surl = surl;
}

}
