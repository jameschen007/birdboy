package com.jameschen.birdboy.model.entity;

public class Category {

	private String name;
	private int type;
	private int iconResource;
	public String getSortParam() {
		return sortParam;
	}

	public void setSortParam(String sortParam) {
		this.sortParam = sortParam;
	}

	private String sortParam;
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getIconResource() {
		return iconResource;
	}
	public void setIconResource(int iconResource) {
		this.iconResource = iconResource;
	}
	
}
