package com.jameschen.birdboy.model.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class InvitePlugMember implements Parcelable{
private SNS members;

public SNS getMembers() {
	return members;
}

public void setMembers(SNS members) {
	this.members = members;
}
@Override
public void writeToParcel(Parcel dest, int flags) {
	dest.writeParcelable(members, flags);
}

public static final Parcelable.Creator<InvitePlugMember> CREATOR = new Creator<InvitePlugMember>() {

	@Override
	public InvitePlugMember createFromParcel(Parcel source) {
		// TODO Auto-generated method stub
		InvitePlugMember sns = new InvitePlugMember();
		sns.members =source.readParcelable(SNS.class.getClassLoader());
		return sns;
	}

	@Override
	public InvitePlugMember[] newArray(int size) {
		// TODO Auto-generated method stub
		return new InvitePlugMember[size];
	}
};

@Override
public int describeContents() {
	// TODO Auto-generated method stub
	return 0;
}
}
