package com.jameschen.birdboy.model;

public class QueryBirdboyJsonResponseContent<T> {
	
	private QueryResponseHeader responseHeader;
	
	private QueryResponse<T> response;

	public QueryResponseHeader getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(QueryResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}

	public QueryResponse<T> getResponse() {
		return response;
	}

	public void setResponse(QueryResponse<T> response) {
		this.response = response;
	}
	
	
	//兼容之前格式
	
	
	private boolean success;
	private T result;
	private String info;
	
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean sucess) {
		this.success = sucess;
	}
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
}
