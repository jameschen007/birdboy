package com.jameschen.birdboy.model;

import java.util.List;

import com.jameschen.birdboy.model.entity.AreaPriceRangs;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;

public class WebDistanceStadiums {
	private List<Stadium> buildings;


	public List<Stadium> getBuildings() {
		return buildings;
	}

	public void setBuildings(List<Stadium> buildings) {
		this.buildings = buildings;
	}

}
