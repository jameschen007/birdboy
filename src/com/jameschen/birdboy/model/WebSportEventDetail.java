package com.jameschen.birdboy.model;

import java.util.List;

import android.provider.ContactsContract.CommonDataKinds.Phone;

import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.model.entity.SportEventDetail;
import com.jameschen.birdboy.model.entity.SportEventPhoto;

public class WebSportEventDetail {
	private SportEventDetail actionDetail;
	private SportEvent action;
	private List<Comment> comments;
	private List<SportEventPhoto> photos;
	private int[] checkUserids;
	
	

	public int[] getCheckUserids() {
		return checkUserids;
	}


	public void setCheckUserids(int[] checkUserids) {
		this.checkUserids = checkUserids;
	}


	private Account creator;	
	
	public Account getCreator() {
		return creator;
	}


	public void setCreator(Account creator) {
		this.creator = creator;
	}


	private String sns;
	
	public String getSns() {
		return sns;
	}


	public void setSns(String sns) {
		this.sns = sns;
	}
	
	
	private int commentCount;

	public int getCommentCount() {
		return commentCount;
	}


	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	

	public SportEventDetail getActionDetail() {
		return actionDetail;
	}
	public void setActionDetail(SportEventDetail actionDetail) {
		this.actionDetail = actionDetail;
	}
	public SportEvent getAction() {
		return action;
	}
	public void setAction(SportEvent action) {
		this.action = action;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public List<SportEventPhoto> getPhotos() {
		return photos;
	}
	public void setPhotos(List<SportEventPhoto> photos) {
		this.photos = photos;
	}
	
}
