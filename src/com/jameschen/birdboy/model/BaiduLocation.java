package com.jameschen.birdboy.model;

public class BaiduLocation {

	private String status;
	
	private BaiduLocationResult result;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BaiduLocationResult getResult() {
		return result;
	}

	public void setResult(BaiduLocationResult result) {
		this.result = result;
	}
	
}
