package com.jameschen.birdboy.model;

public class QueryResponseHeader {
		
	private int status;
	
	private int QTime;
	
	private QueryParams params;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getQTime() {
		return QTime;
	}

	public void setQTime(int qTime) {
		QTime = qTime;
	}

	public QueryParams getParams() {
		return params;
	}

	public void setParams(QueryParams params) {
		this.params = params;
	}
	
}
