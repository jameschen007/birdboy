package com.jameschen.birdboy.model;

public class WebBirdboyJsonResponseContent<T> {
	
	private boolean success;
	private T result;
	private String info;
	private   int  code;
	
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean sucess) {
		this.success = sucess;
	}
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
}
