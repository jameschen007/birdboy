package com.jameschen.birdboy.model;

import java.util.List;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Product;

public class WebCoachDetail {
	private CoachDetailInfo coach;
	private List<Notice> notice;
	private List<Comment> comment;
	private List<CoachPhoto> photos;
	private List<Product> product;
	
	public List<Product> getProduct() {
		return product;
	}
	public void setProduct(List<Product> product) {
		this.product = product;
	}
	public List<CoachPhoto> getPhotos() {
		return photos;
	}
	public void setPhotos(List<CoachPhoto> photos) {
		this.photos = photos;
	}
	public CoachDetailInfo getCoach() {
		return coach;
	}
	public void setCoach(CoachDetailInfo coach) {
		this.coach = coach;
	}
	public List<Notice> getNotice() {
		return notice;
	}
	public void setNotice(List<Notice> notice) {
		this.notice = notice;
	}
	public List<Comment> getComment() {
		return comment;
	}
	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}
}
