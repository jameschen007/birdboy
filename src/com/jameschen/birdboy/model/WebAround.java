package com.jameschen.birdboy.model;

import java.util.List;

import com.jameschen.birdboy.model.entity.AreaPriceRangs;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;

public class WebAround {
	private List<InvitedUser> list;

	public List<InvitedUser> getList() {
		return list;
	}

	public void setList(List<InvitedUser> list) {
		this.list = list;
	}



}
