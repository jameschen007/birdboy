package com.jameschen.birdboy.model;

public class BaiduLocationResult {

	private String formatted_address;
	private   BaiduAddressComponent addressComponent;
	public BaiduAddressComponent getAddressComponent() {
		return addressComponent;
	}

	public void setAddressComponent(BaiduAddressComponent addressComponent) {
		this.addressComponent = addressComponent;
	}

	public String getFormatted_address() {
		return formatted_address;
	}

	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}
	
	
}
