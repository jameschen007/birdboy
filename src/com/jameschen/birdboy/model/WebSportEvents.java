package com.jameschen.birdboy.model;

import java.util.List;

import com.jameschen.birdboy.model.entity.AreaPriceRangs;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;

public class WebSportEvents {
	private List<SportEvent> actions;

	public List<SportEvent> getActions() {
		return actions;
	}

	public void setActions(List<SportEvent> actions) {
		this.actions = actions;
	}



}
