package com.jameschen.birdboy.model;

import java.util.List;

import com.jameschen.birdboy.model.entity.AreaPriceRangs;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;

public class WebDistanceCoachs {
	private List<Coach> coachs;


	public List<Coach> getCoachs() {
		return coachs;
	}

	public void setCoachs(List<Coach> coachs) {
		this.coachs = coachs;
	}

}
