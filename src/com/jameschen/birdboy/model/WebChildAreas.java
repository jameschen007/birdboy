package com.jameschen.birdboy.model;

import java.util.List;

import com.jameschen.birdboy.model.entity.AreaPriceRangs;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;

public class WebChildAreas {
	private List< ChildArea> clist;

	public List<ChildArea> getClist() {
		return clist;
	}

	public void setClist(List<ChildArea> clist) {
		this.clist = clist;
	}
}
