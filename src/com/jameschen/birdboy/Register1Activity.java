package com.jameschen.birdboy;
import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.RegisterBaseActivity;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Register;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.Code;
import com.jameschen.birdboy.utils.CheckValid;
import com.jameschen.birdboy.utils.HttpUtil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
public class Register1Activity extends RegisterBaseActivity {

	private EditText verifycode;
	private  String mobile,code ;
	private Button aquireCode;
	private TextView phoneNum;
	private CountDownTimer countDownTimer;
	private boolean enable;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register1);
		
			initViews();	
			setTitle("输入验证码 (2/4)");
			Intent intent = getIntent();
			  mobile = intent.getStringExtra("mobile");
			  code = intent.getStringExtra("code");
//			  if (code!=null) {
//				  verifycode.setText(code);
//			}
			  phoneNum.setText(getString(R.string.phone_num, mobile));
			  setTopBarLeftBtnVisiable(View.GONE);
			setTopBarRightBtnListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					register(mobile, verifycode.getText());
				}
			});
			initDataLoaderFromNativeCache(null, LoadMode.NONE, uiFreshController);		
			//count down timer..
			startCountDownTimer();
	}
	private void startCountDownTimer() {
		countDownTimer = new CountDownTimer(120000, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub#777777
//				aquireCode.setTextColor(Color.rgb(0x77, 0x77,
//						0x77));
				aquireCode.setText("获取验证码"
						+ "(" + millisUntilFinished / 1000 + "s)");
			}

			@Override
			public void onFinish() {
				resetReceiveBtn();
			}
		};
		countDownTimer.start();
		enable= false;
	}
	private void resetReceiveBtn() {
//		int Color =getResources().getColor(R.drawable.label_text_color);
//		aquireCode.setTextColor(Color);
		aquireCode.setText("免费获取验证码");
		enable=true;
	}
	private void initViews() {
		// TODO Auto-generated method stub
		phoneNum =(TextView)findViewById(R.id.phone_num_tv);
		verifycode = (EditText) findViewById(R.id.edtVerifyCode);
		aquireCode = (Button) findViewById(R.id.btn_get_code);
		aquireCode.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!enable) {
					return;
				}
				startCountDownTimer();
				//showProgressDialog("获取验证码", "请稍候...", null);
				Map<String, String> map = getPublicParamRequstMap();
				map.put("mobile", mobile);
				getDataLoader(HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_REG_GET_SMS_VERIFY_CODE_URL, map), false, null, codeController);
			}
		});
		
	}
 
	private UIController codeController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode,Page page) {
			if (countDownTimer != null) {
				countDownTimer.cancel();
				resetReceiveBtn();
			}
		}
		
	

		@Override
		public <E> void refreshUI(E content,Page page) {
			//showToast(msg)
			//Code code =(Code) content;
			 // verifycode.setText(code.getCode());
//				if (countDownTimer != null) {
//					countDownTimer.cancel();
//					resetReceiveBtn();
//				}
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG+"code";
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return  new TypeToken<WebBirdboyJsonResponseContent<Code> >(){}.getType();
		}
	};
	
	private void register(CharSequence phone,	CharSequence verifyCode) {
		boolean nextCheckStep =true;
		if (nextCheckStep) {
			nextCheckStep = CheckValid. verifyCodeCheckValid(this,verifyCode,6, 6);
		}
			

		if (nextCheckStep) {
			closeInputMethod();			
			showProgressDialog("验证中", "请稍候", null);
			Map<String, String> map = getPublicParamRequstMap();
			map.put("mobile", phone.toString());
			map.put("code", verifyCode.toString());			
			getDataLoader(HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_CHECK_SMS_VERIFY_CODE_URL, map), false, null, uiFreshController);
			

		}
	}
	

	private UIController uiFreshController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode,Page page) {
			// TODO Auto-generated method stub
		}
		
		@Override
		public <E> void refreshUI(E content,Page page) {
			Intent intent =new Intent(Register1Activity.this,Register2Activity.class);
			intent.putExtra("mobile", mobile);
			intent.putExtra("code", code);
			
			startActivityForResult(intent, Register.REQUST_REGISTER);
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return  new TypeToken<WebBirdboyJsonResponseContent<Account> >(){}.getType();
		}
	};
	
	

}
