package com.jameschen.birdboy;
import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import com.jameschen.birdboy.utils.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MKMapTouchListener;
import com.baidu.mapapi.map.MKMapViewListener;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKGeocoderAddressComponent;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKShareUrlResult;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.BaiduLocation;
import com.jameschen.birdboy.model.entity.Location;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.jameschen.birdboy.utils.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class MapAddressActivity extends BaseActivity implements OnClickListener {
	

	// 定位图层	
	locationOverlay myLocationOverlay = null;
	

	// 地图相关，使用继承MapView的MyLocationMapView目的是重写touch事件实现泡泡处理
	// 如果不处理touch事件，则无需继承，直接使用MapView即可
	MyLocationMapView mMapView = null; // 地图View
	private MapController mMapController = null;

	// UI相关
	boolean isRequest = false;// 是否手动触发请求定位
	boolean isFirstLoc = true;// 是否首次定位

	private MKMapTouchListener mapTouchListener;

	private MKMapViewListener mapViewListener;

	private TextView locMarkAddress;

	private ImageView locMarkImg;

	private LocationData locData;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("查看地图");
		/**
		 * 使用地图sdk前需先初始化BMapManager. BMapManager是全局的，可为多个MapView共用，它需要地图模块创建前创建，
		 * 并在地图地图模块销毁后销毁，只要还有地图模块在使用，BMapManager就不应该销毁
		 */
		BirdboyApplication app = (BirdboyApplication) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplicationContext());
			/**
			 * 如果BMapManager没有初始化则初始化BMapManager
			 */
			app.mBMapManager.init(BirdboyApplication.strKey,
					new BirdboyApplication.MyGeneralListener());
		}
		setContentView(R.layout.location_map);
		
		

		// 地图初始化
		mMapView = (MyLocationMapView) findViewById(R.id.bmapView);
		mMapController = mMapView.getController();
		mMapView.getController().setZoom(15);
		mMapView.getController().enableClick(true);
		mMapView.setBuiltInZoomControls(true);

		locMarkAddress = (TextView) findViewById(R.id.loc_mark_tv);

		locMarkImg = (ImageView) findViewById(R.id.loc_mark_img);
	
		locMarkAddress.setVisibility(View.GONE);
		locMarkImg.setVisibility(View.VISIBLE);
		locMarkImg.setEnabled(false);
		locMarkImg.setClickable(false);
		

		// 定位初始化
		locData = new LocationData();
		Location location = (Location) getIntent().getSerializableExtra(ADDRESS);
		// 定位图层初始化
		myLocationOverlay = new locationOverlay(mMapView);
		// 设置定位数据
		myLocationOverlay.setData(locData);
		// 添加定位图层
		mMapView.getOverlays().add(myLocationOverlay);
		myLocationOverlay.enableCompass();
		// 修改定位数据后刷新图层生效
		mMapView.refresh();
		markPosition(location);
		initListener();
	}
	
	private void markPosition(Location location) {
		// 更新定位数据

		locData.latitude = location.getLat();
		locData.longitude = location.getLont();
		
		myLocationOverlay.setData(locData);
		MKGeocoderAddressComponent cAddressComponent = new MKGeocoderAddressComponent();
		
		myLocationOverlay.setAddress(location.getAddress(),
				cAddressComponent);
		// 更新图层数据执行刷新后生效
		mMapView.refresh();

		// 移动地图到定位点
		Log.d("LocationOverlay", "receive location, animate to it");
		mMapController.animateTo(new GeoPoint(
				(int) (locData.latitude * 1e6),
				(int) (locData.longitude * 1e6)));	
		locMarkAddress.setVisibility(View.VISIBLE);
		locMarkAddress.setText("当前位置：" + location.getAddress());

			locMarkAddress.setTag(location);
	}
	private void initListener() {
		/**
		 * 设置地图点击事件监听
		 */
		mapTouchListener = new MKMapTouchListener() {
			@Override
			public void onMapClick(GeoPoint point) {
				Log.i(TAG, "single tap~~~~");

			}

			@Override
			public void onMapDoubleClick(GeoPoint point) {
				updateMapState(DOUBLE, point);
			}

			@Override
			public void onMapLongClick(GeoPoint point) {
				updateMapState(LONG_PRESS, point);
			}
		};

		mapViewListener = new MKMapViewListener() {

			@SuppressLint("Recycle")
			@Override
			public void onMapMoveFinish() {
				updateMapState(SINGLE, mMapView.getMapCenter());

			}

			@Override
			public void onMapLoadFinish() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMapAnimationFinish() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onGetCurrentMap(Bitmap arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onClickMapPoi(MapPoi arg0) {
				// TODO Auto-generated method stub

			}
		};

		mMapView.regMapTouchListner(mapTouchListener);
		mMapView.regMapViewListener(
				BirdboyApplication.getInstance().mBMapManager, mapViewListener);
		// 初始化搜索模块，注册事件监听
		mSearch = new MKSearch();
		mSearch.init(BirdboyApplication.getInstance().mBMapManager,
				new MKSearchListener() {
					@Override
					public void onGetPoiDetailSearchResult(int type, int error) {
						if (error != 0) {
							String str = String.format("错误号：%d", error);
							locMarkAddress.setText("查询失败。");
							Log.i(TAG, str);
							return;
						}
					}

					public void onGetAddrResult(MKAddrInfo res, int error) {
						if (error != 0) {
							String str = String.format("错误号：%d", error);
							locMarkAddress.setText("查询失败。");
							Log.i(TAG, str);
							return;
						}
						// 地图移动到该点
						mMapView.getController().animateTo(res.geoPt);

						if (res.type == MKAddrInfo.MK_REVERSEGEOCODE) {
							// 反地理编码：通过坐标点检索详细地址及周边poi
							String strInfo = res.strAddr;
							// Toast.makeText(MapActivity.this, strInfo,
							// Toast.LENGTH_LONG).show();

							createAddressOverLay(res.geoPt, res.strAddr,
									res.addressComponents);
						}

					}

					public void onGetPoiResult(MKPoiResult res, int type,
							int error) {
						if (error != 0) {
							String str = String.format("错误号：%d", error);
							locMarkAddress.setText("查询失败。");
							Log.i(TAG, str);
							return;
						}

					}

					public void onGetDrivingRouteResult(
							MKDrivingRouteResult res, int error) {
					}

					public void onGetTransitRouteResult(
							MKTransitRouteResult res, int error) {
					}

					public void onGetWalkingRouteResult(
							MKWalkingRouteResult res, int error) {
					}

					public void onGetBusDetailResult(MKBusLineResult result,
							int iError) {
					}

					@Override
					public void onGetSuggestionResult(MKSuggestionResult res,
							int arg1) {
					}

					@Override
					public void onGetShareUrlResult(MKShareUrlResult result,
							int type, int error) {
						// TODO Auto-generated method stub

					}

				});
	}

	// 搜索相关
	MKSearch mSearch = null; // 搜索模块，也可去掉地图模块独立使用

	protected void updateMapState(int toutchType, GeoPoint point) {
		switch (toutchType) {
		case LONG_PRESS: {

			// GeoPoint ptCenter = new GeoPoint((int)(point.getLatitudeE6()),
			// (int)(point.getLongitudeE6()));
			// createAddressOverLay(point,null,null);
			// //反Geo搜索
			// mSearch.reverseGeocode(ptCenter);
		}
			break;
		case SINGLE: {
			Log.i(TAG, "search......");

			locMarkAddress.setText("正在查询...");
			locMarkAddress.setTag(null);
			final GeoPoint ptCenter = new GeoPoint(
					(int) (point.getLatitudeE6()),
					(int) (point.getLongitudeE6()));
			// createAddressOverLay(point,null,null);
			// 反Geo搜索
			// mSearch.reverseGeocode(ptCenter);
			String url = "http://api.map.baidu.com/geocoder?output=json&location="
					+ ptCenter.getLatitudeE6()
					* 1e-6
					+ ",%20"
					+ ptCenter.getLongitudeE6()
					* 1e-6
					+ "&key=3712a21fd73c350d7c322e6d834f20a3";
			AsyncHttpClient client = new AsyncHttpClient();
			client.get(url, new AsyncHttpResponseHandler() {
				@Override
				@Deprecated
				public void onSuccess(String content) {
					// TODO Auto-generated method stub
					super.onSuccess(content);

					BaiduLocation baiduLocation = null;
					try {
						baiduLocation = ParseJsonData
								.getBaiduLocationWebData(content,
										new TypeToken<BaiduLocation>() {
										}.getType());
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if (baiduLocation == null
							|| baiduLocation.getResult() == null) {
						return;

					}
					String address = "";
					try {
						address = baiduLocation.getResult()
								.getFormatted_address();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

					createAddressOverLay(ptCenter, address, null);
					Log.i("url", "pos----" + content);
				}

				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					locMarkAddress.setText("查询失败。");
				}
			});
		}
			break;

		default:
			break;
		}
	}

	private void createAddressOverLay(GeoPoint geoPoint, String address,
			MKGeocoderAddressComponent addressComponents) {
		if (isFinishing()) {
			return;
		}
		
		myLocationOverlay.setData(locData);
		myLocationOverlay.setAddress(address, addressComponents);
		// 清除地图其他图层
		if (mMapView == null) {
			return;
		}
		mMapView.getOverlays().clear();
		// mMapView.getOverlays().add(itemLocationOverlay);
		mMapView.getOverlays().add(myLocationOverlay);
		locMarkAddress.setText("详细地址:" + address);
		Location location = new Location();
		location.setAddress(address);
		location.setLat(locData.latitude);
		location.setLont(locData.longitude);
		locMarkAddress.setTag(location);
		// 执行刷新使生效
		mMapView.refresh();
	}

	

	private static final int SINGLE = 0, DOUBLE = 1, LONG_PRESS = 2;

	public static final String ADDRESS = "address";


	/**
	 * 修改位置图标
	 * 
	 * @param marker
	 */
	public void modifyLocationOverlayIcon(Drawable marker) {
		// 当传入marker为null时，使用默认图标绘制
		myLocationOverlay.setMarker(marker);
		// 修改图层，需要刷新MapView生效
		mMapView.refresh();
	}
	


	// 继承MyLocationOverlay重写dispatchTap实现点击处理
	public class locationOverlay extends MyLocationOverlay {

		public locationOverlay(MapView mapView) {
			super(mapView);
			// TODO Auto-generated constructor stub
		}

		private String mAddr;
		private MKGeocoderAddressComponent addressComponents;

		public void setAddress(String mAddr,
				MKGeocoderAddressComponent addressComponents) {
			// TODO Auto-generated method stub
			this.mAddr = mAddr;
			this.addressComponents = addressComponents;
		}

		private int alignTop = 8;

		public void setAlignTop(int alignTop) {
			this.alignTop = alignTop;
		}

		@Override
		protected boolean dispatchTap() {
			// TODO Auto-generated method stub
			if (mAddr == null) {
				showToast("无法获取当前详细地址，请重新选址");
				return true;
			}
			// 处理点击事件,弹出泡泡
			Util.CommitDialog(MapAddressActivity.this, "地图选点", "确认选择此处作为活动地点吗?\n",
					new Dialog.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Stadium stadium = new Stadium();
							stadium.setPosition(getMyLocation().longitude + " "
									+ getMyLocation().latitude);
							stadium.setPositiondesc(mAddr);
							Intent intent = new Intent();
							intent.putExtra(ConstValues.STADIUM,
									(Parcelable) stadium);
							setResult(RESULT_OK, intent);
							finish();
						}
					});
			// popupText.setBackgroundResource(R.drawable.popup);
			// popupText.setText("选择当前位置");
			// pop.showPopup(getBitmapFromView(popupText),
			// new GeoPoint((int)(getMyLocation().latitude*1e6),
			// (int)(getMyLocation().longitude*1e6)),
			// alignTop);
			return true;
		}

	}

	/**
	 * 从view 得到图片
	 * 
	 * @param view
	 * @return
	 */
	public static Bitmap getBitmapFromView(View view) {
		view.destroyDrawingCache();
		view.measure(View.MeasureSpec.makeMeasureSpec(0,
				View.MeasureSpec.UNSPECIFIED), View.MeasureSpec
				.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
		view.setDrawingCacheEnabled(true);
		Bitmap bitmap = view.getDrawingCache(true);
		return bitmap;
	}

	@Override
	protected void onPause() {
		mMapView.onPause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		mMapView.onResume();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// 退出时销毁定位
		
		mMapView.destroy();
		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mMapView.onSaveInstanceState(outState);

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mMapView.onRestoreInstanceState(savedInstanceState);
	}

}
