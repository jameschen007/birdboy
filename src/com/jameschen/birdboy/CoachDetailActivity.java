package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.base.BaseDetailActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebCoachDetail;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class CoachDetailActivity extends BaseDetailActivity {

	private ImageView coachLogo,coachHeadPhoto;
	private TextView coachName,photoScan,commentCount,AveScore_tv,info;
	ImageView coachIndentify;
	ImageView coachSkill;
	ImageView coachTechType;
	private RatingBar coachAveScore;


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case ConstValues.TYPE_COACH_VAL:
		case ConstValues.TYPE_USER_VAL:
		{
			if (resultCode == RESULT_OK) {
				showProgressDialog("刷新", "请稍候...", null);
				getDataLoader(HttpConfig.REQUST_COACH_DETAIL_ACTION_URL+id, false,new Page(), uiFreshController);

			}
		}
			break;

		default:
			break;
		}
		
	}

	@Override
			protected void onCreate(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				super.onCreate(savedInstanceState);
				setContentView(R.layout.coach_detail);
				Intent intent =getIntent();
				//
				Coach coach =intent.getParcelableExtra(CoachListFragment.COACH);
				if (coach!=null) {
					id=coach.getId();
				}else {
					id = intent.getIntExtra("id", -1);
				}
				setTopBarRightBtnListener(R.drawable.share, new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (mCoachDetailInfo!=null) {
							String content ="你的朋友使用了“鸟孩运动”推荐了"+CoachAdapter.setSportTypeStr(mCoachDetailInfo.getType())+"教练"+mCoachDetailInfo.getName()+"给你。快来下载吧。在这里，你能找到周边的教练，场馆。";
							v.setTag(content);
							share(v,ConstValues.TYPE_COACH_VAL);
						}
						
					}
				});
				setTitle("教练详情");
				initViews();
				setCoachInfoToUI(coach);
				coachTechType.setVisibility(View.GONE);
				showProgressDialog("刷新", "请稍候...", null);
				getDataLoader(HttpConfig.REQUST_COACH_DETAIL_ACTION_URL+id, false,new Page(), uiFreshController);
				getDataLoader(HttpConfig.REQUST_COACH_PHOTO_URL+id, false,new Page(), uiFreshPhotoController);
				
	}
	
	long id;
	String frontuserid=null;
	private ImageView personal;
	private TextView coachLevel;
	private RatingBar commentScore;
	private TextView commentScore_tv;
	private Button contactButton;
	private TextView position;
	
	ProgressBar loadImgBar;
	private void initViews() {
		coachLogo = (ImageView) findViewById(R.id.coach_detail_iv_icon);
		coachHeadPhoto = (ImageView) findViewById(R.id.coach_portrait);
		coachName = (TextView) findViewById(R.id.coach_name);
		coachLevel = (TextView) findViewById(R.id.coach_level);
		personal = (ImageView) findViewById(R.id.coach_personal_icon);
		coachSkill = (ImageView) findViewById(R.id.coach_skill_icon);
		coachTechType = (ImageView) findViewById(R.id.coach_category_icon);
		coachIndentify = (ImageView) findViewById(R.id.coach_identify_icon);
		loadImgBar = (ProgressBar) findViewById(R.id.loading_img);
		coachAveScore = (RatingBar) findViewById(R.id.score_rating);
		commentScore = (RatingBar) findViewById(R.id.comment_score);
		commentScore_tv = (TextView) findViewById(R.id.comment_score_tv);
		contactButton= (Button) findViewById(R.id.contact_btn);
		AveScore_tv = (TextView) findViewById(R.id.score_tv);
		commentCount = (TextView) findViewById(R.id.total_comment_num_tv);
		info = (TextView) findViewById(R.id.coach_brief_text);
		position =(TextView) findViewById(R.id.coach_distance);
		photoScan = (TextView) findViewById(R.id.coach_photos);
		coachLogo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (coachPhotos==null||coachPhotos.size() == 0) {
					return;
				}
				Intent intent=new Intent(CoachDetailActivity.this,ImageDetailActivity.class);
				ArrayList<String> phoneList = new ArrayList<String>();
				for (int i = 0; i < coachPhotos.size(); i++) {
					phoneList.add(HttpConfig.HTTP_BASE_URL+coachPhotos.get(i).getUrl());
				}
	
				intent.putStringArrayListExtra("photos", phoneList);
				//phoneList
				startActivity(intent);
			}
		});
		
		roundOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.icon_default)
		.showImageForEmptyUri(R.drawable.icon_default)
		.showImageOnFail(R.drawable.icon_default).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(20))
		.cacheOnDisc(true).build();
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.icon_default_large)
		.showImageForEmptyUri(R.drawable.icon_default_large)
		.showImageOnFail(R.drawable.icon_default_large).considerExifParams(true).cacheInMemory(true) 
		.cacheOnDisc(true).build();
		
		findViewById(R.id.comment_btn).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				Util.go2SendComment(CoachDetailActivity.this,id,ConstValues.TYPE_COACH_VAL);
			}
		});
		
		findViewById(R.id.coach_comment_scores_info).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.go2CommentList(CoachDetailActivity.this,ConstValues.TYPE_COACH_VAL,id);
			}
		});
		
	findViewById(R.id.send_message_btn).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (frontuserid!=null) {
					Util.go2SendMessage(CoachDetailActivity.this,coachName.getText().toString(),frontuserid,0);
				
				}
			}
		});
	findViewById(R.id.detail_btn).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if (mCoachDetailInfo!=null) {
				Intent intent =new Intent(CoachDetailActivity.this,CoachDetailIntroActivity.class);
				intent.putExtra(ConstValues.COACH, mCoachDetailInfo);
				intent.putExtra("frontuid", frontuserid);
				startActivity(intent);
			}
			
		}
	});
	contactButton.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (LogInController.IsLogOn()) {
				if (mCoachDetailInfo!=null) {
					Util.showContactDialog(CoachDetailActivity.this, mCoachDetailInfo.getName(), mCoachDetailInfo.getMobile(), null);
					
				}
			}else {
				Util.go2LogInDialog(CoachDetailActivity.this,new OnLogOnListener() {
					
					@Override
					public void logon() {
						if (mCoachDetailInfo!=null) {
							Util.showContactDialog(CoachDetailActivity.this, mCoachDetailInfo.getName(), mCoachDetailInfo.getMobile(), null);
							
						}						
					}
				});
			}
		}
	});
	}

	
	private String getPosition(String positionStr) {
		LocationSupport locationSupport = LocationSupport
				.getLocationSupportInstance(this);
		float lat = locationSupport.getLatitude();
		float lng = locationSupport.getLongtitude();
		if (lat<=0||lng<=0) {
			return "无法定位";
		}
		if (positionStr != null) {
			Log.i("positionStr", positionStr);
			String position[] = positionStr.split("\\s+");
			double distance = Util.gps2m(lat, lng,
					Float.valueOf(position[1]), Float.valueOf(position[0]));
			DecimalFormat df = null;
			if (distance >= 1000) {// show in km
				df = new DecimalFormat(".0");
				return df.format(distance / 1000) + "km";
			} else {

				return (int) distance + "m";
			}
			
		} else {
			return null;
		}
	}
	private DisplayImageOptions roundOptions;

	private DisplayImageOptions options;
	private <T> void setCoachInfoToUI(T coach) {
			if (coach==null) {
				return ;
			}
		if (coach instanceof Coach) {
			Coach mCoach = (Coach) coach;
			coachName.setText(mCoach.getName());
			coachSkill.setVisibility(CoachAdapter.sportSkillImg(mCoach.getAddint()));
			coachIndentify.setVisibility(CoachAdapter.sportIndentifyImg(mCoach.getStatus()));
			//coachTechType.setVisibility(CoachAdapter.setSportTypeImgRes(mCoach.getType()));
			personal.setVisibility(CoachAdapter.sportPersonalImg(mCoach.getJobtype()));	
			coachLevel.setText(CoachAdapter.getLevelStr(mCoach.getLevel()));
			String positionStr =getPosition(mCoach.getPosition());
			if (TextUtils.isEmpty(positionStr)) {
				position.setVisibility(View.GONE);
			}else {
				position.setVisibility(View.VISIBLE);
				position.setText(positionStr);
			}
			setTitle(mCoach.getName());
			loadImage(HttpConfig.HTTP_BASE_URL+ mCoach.getLogo(), coachHeadPhoto, roundOptions);
		} else {// coach detail
			 mCoachDetailInfo = (CoachDetailInfo) coach;
			 setTitle(mCoachDetailInfo.getName());
			coachName.setText(mCoachDetailInfo.getName());
			coachLevel.setText(CoachAdapter.getLevelStr(mCoachDetailInfo.getLevel()));
			personal.setVisibility(CoachAdapter.sportPersonalImg(mCoachDetailInfo.getJobtype()));
			coachSkill.setVisibility(CoachAdapter.sportSkillImg(mCoachDetailInfo.getAddint()));
			coachIndentify.setVisibility(CoachAdapter.sportIndentifyImg(mCoachDetailInfo.getStatus()));
			//coachTechType.setVisibility(CoachAdapter.setSportTypeImgRes(mCoachDetailInfo.getType()));
			coachAveScore.setRating((float)mCoachDetailInfo.getAvescore()/2);
			commentScore.setRating((float)mCoachDetailInfo.getAvescore()/2);
			commentScore_tv.setText(mCoachDetailInfo.getAvescore()+"");
			AveScore_tv.setText(mCoachDetailInfo.getAvescore()+"");
			commentCount.setText(mCoachDetailInfo.getCommentCount()+"人点评");
			info.setText(Html.fromHtml(mCoachDetailInfo.getSkill()));
			String positionStr =getPosition(mCoachDetailInfo.getPosition());
			frontuserid = mCoachDetailInfo.getFrontuserid();
			if (TextUtils.isEmpty(positionStr)) {
				position.setVisibility(View.GONE);
			}else {
				position.setVisibility(View.VISIBLE);
				position.setText(positionStr);
			}
			
			if (mCoachDetailInfo.getBlogo()==null) {
				//
				return;
			}
			loadImage(HttpConfig.HTTP_BASE_URL+mCoachDetailInfo.getBlogo(), coachLogo, options,new ImageLoadingListener() {
				
				@Override
				public void onLoadingStarted(String arg0, View arg1) {
					// TODO Auto-generated method stub
					loadImgBar.setVisibility(View.VISIBLE);
				}
				
				@Override
				public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
					loadImgBar.setVisibility(View.GONE);
				}
				
				@Override
				public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
					loadImgBar.setVisibility(View.GONE);
	
				}
				
				@Override
				public void onLoadingCancelled(String arg0, View arg1) {
					// TODO Auto-generated method stub
					
				}
			});
	

		}
	}
	
	CoachDetailInfo mCoachDetailInfo;
	
	private List<CoachPhoto> coachPhotos;
	private void setPhotosToUI(List<CoachPhoto> coachPhotos) {
		if (coachPhotos.size()==0) {
			return ;
		}
		if (this.coachPhotos ==null) {
			this.coachPhotos = new ArrayList<CoachPhoto>();
		}
		this.coachPhotos.clear();
		this.coachPhotos.addAll(coachPhotos);
	}
	
	private void setCommentsToUI(List<Comment> comments) {

		ViewGroup viewGroup = (ViewGroup) findViewById(R.id.coach_last_comments_info);
		int size =comments.size();
		int len=0;
		if (size == 0) {
			return;//no more
		}
		viewGroup.removeViews(1, viewGroup.getChildCount()-1);
		if (size>5) {//show more page.
			len=5;
		}else {
			len =size ;
		}
		
		for (int i = 0; i < len; i++) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup vGroup =(ViewGroup) inflater.inflate(R.layout.comment_item, viewGroup, false);
			TextView name = (TextView) vGroup.findViewById(R.id.comment_item_name);
			TextView info = (TextView) vGroup.findViewById(R.id.comment_item_content);
			TextView  date = (TextView) vGroup.findViewById(R.id.comment_item_date);
			name.setText(comments.get(i).getUsernick());
			info.setText(comments.get(i).getContent());
			date.setText(comments.get(i).getCtstr());
			final Comment comment =comments.get(i);
			TextView replay=(TextView) vGroup.findViewById(R.id.comment_reply);
			replay.setVisibility(View.GONE);
			replay.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
				Util.go2SendComment(CoachDetailActivity.this,id,comment.getUserid(),comment.getUsernick(),ConstValues.TYPE_COACH_VAL);
				}
			});
			viewGroup.addView(vGroup);
			 if (i<len-1) {
				 View line =(View) inflater.inflate(R.layout.item_dot_line, viewGroup, false);
					viewGroup.addView(line);
			 }
		}
	
	}

	private void setNoticesToUI(List<Notice> notices) {

	}

	private void setProductsToUI(List<Product> products) {
		ViewGroup viewGroup = (ViewGroup) findViewById(R.id.coach_course_info);
		int size =products.size();
		int len=0;
		if (size == 0) {
			return;//no more
		}
		viewGroup.removeViews(1, viewGroup.getChildCount()-1);
		if (size>3) {//show more page.
			len=3;
		}else {
			len =size ;
		}
		
		for (int i = 0; i < len; i++) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup vGroup =(ViewGroup) inflater.inflate(R.layout.product_list_item, viewGroup, false);
			TextView name = (TextView) vGroup.findViewById(R.id.course_name);
			TextView info = (TextView) vGroup.findViewById(R.id.course_intro);
			TextView  price = (TextView) vGroup.findViewById(R.id.course_price);
			name.setText(products.get(i).getName());
			info.setText(products.get(i).getInfo());
			price.setText("￥"+products.get(i).getRmb()+"元");
			if (i==0) {
				 View line =(View) inflater.inflate(R.layout.item_line, viewGroup, false);//add top
					viewGroup.addView(line);
			}
			viewGroup.addView(vGroup);
			final Product product =products.get(i);
			vGroup.setOnClickListener( new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Intent prodcutIntent = new Intent(CoachDetailActivity.this,CoachDetailCourseActivity.class);
					prodcutIntent.putExtra("product", product);
					startActivity(prodcutIntent);
				}
			});
			 if (i<len-1) {
				 
				 View line =(View) inflater.inflate(R.layout.item_line, viewGroup, false);
					viewGroup.addView(line);
			 }
		}
	}
	
	private void onDataChangeNotify(CoachDetailInfo coachDetailInfo,
			List<Comment> comments, List<Notice> notices,List<Product>products) {
		setCoachInfoToUI(coachDetailInfo);
		setCommentsToUI(comments);
		setNoticesToUI(notices);
		setProductsToUI(products);
	}
	
	private UIController uiFreshPhotoController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			WebCoachDetail webCoachDetail = (WebCoachDetail) content;
			setPhotosToUI(webCoachDetail.getPhotos());

			
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebCoachDetail>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			if (errorCode>-1) {
				showToast(getString(R.string.get_photo_error_from_net));
			}
			
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG+"photo";
		}
	};
	
	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			WebCoachDetail webCoachDetail = (WebCoachDetail) content;
			onDataChangeNotify(webCoachDetail.getCoach(),
					webCoachDetail.getComment(), webCoachDetail.getNotice(),webCoachDetail.getProduct());
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebCoachDetail>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			if (errorCode>-1) {
			showToast(getString(R.string.get_detail_info_error));
		}
		}
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};
	
//	http://birdboy.cn/addcommentAPI    get请求
//
//		字段               类型                描述                           备注            值条件
//
//		score               int                 提交的分数
//
//		replycontent    string            点评的内容
//
//		userid              int                点评用户的id
//
//		nick                 string            点评用户的昵称
//
//		id                     int                 场馆或者教练的id
//
//		fromtype          int               评论的类型 1：场馆   2  教练
//
//		返回数据
//
//		{success:true;}


}
