package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.R.integer;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.BaseActivity.LoadMode;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.FavoriteFriend;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.utils.CheckValid;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;

public class SportEventCreate1Activity extends BaseActivity {

	private TextView contentEdit;
	private int sportType;

	private String askUsers=null;
	//askusers    String     收邀请的人                    格式：userid:name:logo|userid2:name:logo 非必填 logo is null set blank
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportevent_create1);
		Bundle data = getIntent().getBundleExtra("data");
		if (data!=null) {
			if (data.getInt("flag")==1) {//single
				FavoriteFriend friendInfo=data.getParcelable("invite_one");
				if (friendInfo!=null) {

					askUsers=getAskSingleString(friendInfo);
				}
				
			}else if (data.getInt("flag")==2) {
			List<InvitedUser>memeberInvitedUsers = data.getParcelableArrayList("invite_members");
				askUsers=getAskUsersString(memeberInvitedUsers);
			}
			}
			
		
		setTopBarRightBtnListener("完成", new OnClickListener() {

			@Override
			public void onClick(View v) {
				// place String 活动地点 必填
				// position String 活动的坐标位置 必填
				// buildid int 活动所选的场馆 可选
				// area int 活动的一级城市区域 可选
				// childarea int 活动的二级城市区域 可选
				// if (month<currentMonth &&month!=0) {
				// return;
				// }
				// if (month==currentMonth) {
				// if (day<currentDay) {//must>=currentDay
				// return;
				// }
				// }else if (month==0&&currentMonth==11) {//maybe next year,but
				// only support 1 moth
				// daltaDay=31-currentDay+day>DayRange return;
				// else currentYear+1;
				//
				// }
				// calendar.set(year, month, day, hourOfDay, minute);
				// calendar.getTimeInMillis();
				// http://birdboy.cn/adduactionAPI
				//
				// get请求
				//
				// 返回字段说明
				//
				// 字段 类型 描述 备注 值条件
				//
				//
				// stime int 活动开始时间 yyyy-MM-dd HH:mm 必填
				// etime int 活动结束时间 yyyy-MM-dd HH:mm 必填
				//
				// signstime int 活动报名开始时间 yyyy-MM-dd HH:mm 必填
				// signetime int 活动报名结束时间 yyyy-MM-dd HH:mm 必填
				// name String 活动的名称 必填
				// type int 体育类别 4网球 必填
				// cmobile long 联系电话 用户手机号 必填
				// place String 活动地点 必填
				// rmb float 活动报名费用 可选
				// position String 活动的坐标位置 必填
				// buildid int 活动所选的场馆 可选
				// area int 活动的一级城市区域 可选
				// childarea int 活动的二级城市区域 可选
				// num

				if (TextUtils.isEmpty(contentEdit.getText())) {
					contentEdit.setText("暂无");
//					showToast("请填写活动内容!");
//					return;
				}
				
				if (!CheckValid.checkPhoneValid(SportEventCreate1Activity.this, phone.getText(), 11, 11) ){
					return;
				}
				

				
				if (!LogInController.IsLogOn()) {
					Util.go2LogInDialog(SportEventCreate1Activity.this,new OnLogOnListener() {
						
						@Override
						public void logon() {
							commitDone();
						}
					});
					return;
				}
			
				commitDone();
			}
		});
		Intent intent = getIntent();
		Stadium stadium=null;
		if ((stadium = intent.getParcelableExtra(ConstValues.STADIUM))!=null) {
			place = stadium.getPositionDesc();
			position = stadium.getPosition();
			areaId = stadium.getPositionarea();
			childAreaId = stadium.getChildarea();
			buildid =stadium.getId();	
			sportType=stadium.getTypedesc();
		}else {
			sportType = intent.getIntExtra("sportType", -1);
			place = intent.getStringExtra("place");
			position = intent.getStringExtra("position");
			areaId = intent.getIntExtra("areaId", -1);
			buildid= intent.getIntExtra("buildid", -1);
			childAreaId = intent.getIntExtra("childAreaId", -1);
		}
		
		
		initviews();
		if (LogInController.IsLogOn()) {
			phone.setText(LogInController.currentAccount.getMobile()+"");
		}
		setTitle("编辑内容(2/2)");

	}

	
	public static String getAskSingleString(FavoriteFriend friendInfo ){
		String askSinge =friendInfo.getUserid()+":"+friendInfo.getUsername()+":"+(TextUtils.isEmpty(friendInfo.getLogo())?"blank":friendInfo.getLogo());
		Log.i("single", friendInfo.getUsername()+"askSinglge=="+askSinge);
		return askSinge;
	}
	
	public static String getAskUsersString(List<InvitedUser> memeberInvitedUsers) {
		if (memeberInvitedUsers==null) {
			return null;
		}
		int size =memeberInvitedUsers.size();

		String uesers ="";
		for (int i = 0; i < size; i++) {
			 InvitedUser askUserInfo = memeberInvitedUsers.get(i);
			 String user=askUserInfo.getId()+":"+askUserInfo.getNick()+":"+(TextUtils.isEmpty(askUserInfo.getLogo())?"blank":askUserInfo.getLogo());
			 Log.e("ss", "user"+user);
			 uesers+=user;
			 
			 if (size>1) {
				if (i<size-1) {
					uesers+="|";
				}
			}
			
		}
		if (TextUtils.isEmpty(uesers)) {
			return null;
		}
		
		Log.i("ask users", "memeber size info ???"+uesers);

		return uesers;
	}

	protected void commitDone() {
		Map<String, String> map = getPublicParamRequstMap();
		String startTime = yearVal + "-" + getTimeFormat(monthVal + 1)
				+ "-" + getTimeFormat(dayVal) + " "
				+ getTimeFormat(hourVal) + ":" + getTimeFormat(minVal);

		String endTime = yearVal + "-" + getTimeFormat(monthEndVal + 1)
				+ "-" + getTimeFormat(dayEndVal) + " "
				+ getTimeFormat(endHourVal) + ":"
				+ getTimeFormat(endMinVal);
		com.jameschen.birdboy.model.entity.Account account = LogInController.currentAccount;
		// load area
		map.put("stime", "" + startTime);
		map.put("etime", "" + endTime);
		map.put("signstime", "" + startTime);
		map.put("signetime", "" + endTime);
		
		int numVal=Integer.valueOf(num.getText().toString());
		if (numVal<=0) {
			numVal=1;
		}
		map.put("num", "" +numVal);
		if (TextUtils.isEmpty(title.getText())) {
			showToast("输入活动标题");
			return;
		}
		map.put("name", "" + title.getText().toString());
		map.put("type", "" + sportType);
		map.put("sex", ""+account.getSex());
		// buildid int 活动所选的场馆 可选
		// area int 活动的一级城市区域 可选
		// childarea int 活动的二级城市区域 可选
	if (buildid!=-1) {
		
		map.put("buildid", "" + buildid);
	}
		if (areaId!=-1) {
			map.put("area", "" + areaId);
		}
	if (childAreaId!= -1) {
		map.put("childarea", "" + childAreaId);
	}
	
		map.put("cmobile", "" + phone.getText().toString());
		map.put("place", "" + place);
		map.put("content", contentEdit.getText().toString());
		map.put("position", position);
		if (!TextUtils.isEmpty(askUsers)) { 
		map.put("askusers", askUsers);
		}
		showProgressDialog("创建活动", "正在提交..", null);
		String url = HttpUtil.getRequestMapUrlStr(
				"http://birdboy.cn/adduactionAPI", map);
		getDataLoader(
				url,
				false,
				null,
				commitUIController,
				new HttpClientStack(LogInController
						.getHttpClientInstance()));		
	}

	private String place;
	private String position;

	protected int buildid=-1;
	private int areaId=-1, childAreaId =-1;
	int yearVal, monthVal, dayVal, monthEndVal, dayEndVal, hourVal, minVal,
			endHourVal, endMinVal;

	Calendar calendar = Calendar.getInstance();
	private TextView date;
	private TextView time;
	private EditText title;
	private EditText num;
	private EditText money;
	private EditText phone;
	private ImageView add, minus, chooseDate, chooseTime,costAdd,costMinus;
	private static final int CHOOSE_DATE = 0x01;
	private static final int CHOOSE_TIME = 0x02;
	private static final int CHOOSE_END_TIME = 0x03;
	protected static final int Max_Member_Num = 100;
	int numMember = 1;
	int costPrice = 0;
	// float money;
	private void initviews() {

		date = (TextView) findViewById(R.id.create_event_date_tv);
		time = (TextView) findViewById(R.id.create_event_time_tv);
		title = (EditText) findViewById(R.id.create_event_title_tv);
		num = (EditText) findViewById(R.id.create_event_member_num_tv);
		add = (ImageView) findViewById(R.id.add);
		minus = (ImageView) findViewById(R.id.minus);
		chooseDate = (ImageView) findViewById(R.id.choose_date);
		chooseTime = (ImageView) findViewById(R.id.choose_time);
		phone = (EditText) findViewById(R.id.create_event_phone_tv);
		costAdd = (ImageView) findViewById(R.id.cost_add);
		costMinus = (ImageView) findViewById(R.id.cost_minus);
		
		money = (EditText) findViewById(R.id.price);
		contentEdit = (TextView) findViewById(R.id.create_event_content_tv);
		((View)contentEdit.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent =new Intent(SportEventCreate1Activity.this,EditSportContentActivity.class);
				intent.putExtra("content", contentEdit.getText());
				startActivityForResult(intent, 0x11);
			}
		});
		money.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				if (!TextUtils.isEmpty(arg0)) {
					int price = Integer.parseInt(arg0.toString());
					if (price<0) {
						price=0;
					}
					costPrice = price;
					costMinus.setImageResource(R.drawable.minus);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		num.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				if (!TextUtils.isEmpty(arg0)) {
					int numVal = Integer.parseInt(arg0.toString());
					if (numVal<1) {
						num.setText("1");
						numMember=1;
						showToast("至少一人参与！");
						return;
					}
//					else if (numVal>Max_Member_Num) {
//						num.setText(""+Max_Member_Num);
//						numMember=Max_Member_Num;
//						showToast("至多"+Max_Member_Num+"人参与！");
//						return;
//					}
					add.setImageResource(R.drawable.add);
					minus.setImageResource(R.drawable.minus);
					numMember = numVal;
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		costAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				costPrice++;
				money.setText(costPrice + "");
			}
		});
		costMinus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (costPrice == 0) {
					costMinus.setImageResource(R.drawable.minus_disable);
					return;
				}
				costMinus.setImageResource(R.drawable.minus);
				costPrice--;
				money.setText(costPrice + "");
			}
		});
		add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				if (numMember == Max_Member_Num) {
//					add.setImageResource(R.drawable.add_disable);
//					showToast("人数达到上限！");
//					return;
//				}
				add.setImageResource(R.drawable.add);
				numMember++;
				num.setText(numMember + "");
			}
		});
		minus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (numMember <= 1) {
					numMember=1;
					
					minus.setImageResource(R.drawable.minus_disable);
					showToast("最少一个人参与！");
					return;
				}
				minus.setImageResource(R.drawable.minus);
				numMember--;
				
				num.setText(numMember + "");
			}
		});
		chooseDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(
						SportEventCreate1Activity.this, DateActivity.class),
						CHOOSE_DATE);
			}
		});
		chooseTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent =new Intent(
						SportEventCreate1Activity.this, TimeActivity.class);
				intent.putExtra("sameday", monthVal==monthEndVal&&dayVal==dayEndVal);
				startActivityForResult(intent,CHOOSE_TIME);
			}
		});

		date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(
						SportEventCreate1Activity.this, DateActivity.class),
						CHOOSE_DATE);
			}
		});
		time.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(
						SportEventCreate1Activity.this, TimeActivity.class),
						CHOOSE_TIME);
			}
		});

		yearVal = calendar.get(Calendar.YEAR);
		date.setText(forMatDate(monthVal = calendar.get(Calendar.MONTH),
				dayVal = calendar.get(Calendar.DAY_OF_MONTH))
				+ " - "
				+ forMatDate(monthEndVal = calendar.get(Calendar.MONTH),
						dayEndVal = calendar.get(Calendar.DAY_OF_MONTH)));
		hourVal = 10;
		if (hourVal>24) {
			hourVal=24;
		}
		endHourVal =18;
		if (endHourVal>24) {
			endHourVal=24;
		}
		time.setText(forMatTime(hourVal,
				(minVal = 0))
				+ " - "
				+ forMatTime(endHourVal,
						(endMinVal = 0)));
	}

	private String forMatTime(int hour, int min) {
		return getTimeFormat(hour) + ":" + getTimeFormat(min);
	}

	private String forMatDate(int month, int day) {
		return getTimeFormat(1 + month) + "月" + getTimeFormat(day) + "日";
	}


	String imageCachePath = ScanImageActivity.imageCachePath;


	private String getTimeFormat(int val) {
		if (val < 10) {
			return "0" + val;
		} else {
			return "" + val;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case CHOOSE_DATE:
				// check date is ok or not....
				monthVal = intent.getIntExtra("month", 1);
				dayVal = intent.getIntExtra("day", 1);
				monthEndVal = intent.getIntExtra("monthEnd", 1);
				dayEndVal = intent.getIntExtra("dayEnd", 1);
				date.setText(forMatDate(monthVal, dayVal) + " - "
						+ forMatDate(monthEndVal, dayEndVal));

				break;
			case CHOOSE_TIME:
				minVal = intent.getIntExtra("min", 1);
				hourVal = intent.getIntExtra("hour", 1);
				endMinVal = intent.getIntExtra("minEnd", 1);
				endHourVal = intent.getIntExtra("hourEnd", 1);
				time.setText(forMatTime(hourVal, minVal) + " - "
						+ forMatTime(endHourVal, endMinVal));
				// if (hour>20:00) {//????crazy weeked???
				//
				// }
				// this.startTime.setText(""+getTimeFormat(minVal)+":"+getTimeFormat(hourVal));
				break;

			case CHOOSE_END_TIME:
				endMinVal = intent.getIntExtra("min", 1);
				endHourVal = intent.getIntExtra("hour", 1);
				// if (end<start) {//fuck not right time
				//
				// }
				// this.endTime.setText(""+getTimeFormat(endHourVal)+":"+getTimeFormat(endMinVal));
				break;
			case 0x11:
				contentEdit.setText(intent.getStringExtra("content"));
				break;
			default:
				break;
			}

		}

	}

	String sendName = null;

	public static String getPhotoFileName() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"'IMG'_yyyy-MM-dd-HHmmss");
		return dateFormat.format(date) + ".jpg";
	}

	// get file absolute path.
	public static String Uri2Filename(Application application, Intent intent) {
		String file = null;

		Uri uriName = intent.getData();

		if (null != uriName) {
			if (0 == uriName.getScheme().toString().compareTo("content")) {
				Cursor c = application.getContentResolver().query(uriName,
						null, null, null, null);
				if (null != c && c.moveToFirst()) {
					try {
						int column_index = c
								.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
						file = c.getString(column_index);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
						try {
							int column_index = c
									.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA);
							file = c.getString(column_index);
						} catch (Exception e2) {
							e2.printStackTrace();
							Toast.makeText(application, "选择图片出错",
									Toast.LENGTH_LONG).show();
						}
					}

				}
			} else if (0 == uriName.getScheme().toString().compareTo("file")) {
				file = uriName.toString().replace("file://", "");
			}
		}
		return file;
	}

	private UIController commitUIController = new UIController() {

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub

		}

		@Override
		public <E> void refreshUI(E content, Page page) {
			Intent intent =new Intent(SportEventCreate1Activity.this,SportEventCreate2Activity.class);
			intent.putExtra("id", (Integer)content);
			intent.putExtra("title", title.getText().toString());
			Bundle data=getIntent().getBundleExtra("data");
			if (data!=null) {
				intent.putExtra("data", data);
			}
			startActivity(intent);
			setResult(Activity.RESULT_OK);
			finish();
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Integer>>() {
			}.getType();
		}
	};

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}

}
