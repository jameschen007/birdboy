package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.StadiumAdapter;
import com.jameschen.birdboy.adapter.StadiumImageAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.BaseDetailActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebStadiumDetail;
import com.jameschen.birdboy.model.WebStadiumDetail;
import com.jameschen.birdboy.model.entity.AreaPriceRangs;
import com.jameschen.birdboy.model.entity.Location;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;
import com.jameschen.birdboy.model.entity.StadiumPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyGallery;
import com.jameschen.birdboy.widget.MyListView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;


public class StadiumDetailActivity extends BaseDetailActivity {

	private ImageView stadiumLogo, stadiumHeadPhoto;
	private RatingBar stadiumAveScore;



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case ConstValues.TYPE_STADIUM_VAL:
		case ConstValues.TYPE_USER_VAL:
		{
			if (resultCode == RESULT_OK) {
				showProgressDialog("刷新", "请稍候...", null);
				getDataLoader(HttpConfig.REQUST_STADIUM_DETAIL_ACTION_URL+id, false,new Page(), uiFreshController);

			}
		}
			break;
		case 0x11:
		{
			if (resultCode == RESULT_OK) {
				
				finish();
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stadium_detail);
		Intent intent = getIntent();
		final Stadium stadium = intent.getParcelableExtra(StadiumListFragment.STADIUM);
		if (stadium!=null) {
			id = stadium.getId();
		}else {
			id =intent.getIntExtra("id",-1);
		}
		
//		setTopBarRightBtnListener(R.drawable.share, new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				share(v);
//			}
//		});
		setTitle("场馆详情");
		setTopBarRightBtnListener(getString(R.string.create_event), new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				closeInputMethod();
				if (!LogInController.IsLogOn()) {
					Util.go2LogInDialog(StadiumDetailActivity.this,new OnLogOnListener() {
						
						@Override
						public void logon() {
							Util.createSportEvent(StadiumDetailActivity.this,stadium);
														
						}
					});
					return;
				}
				Util.createSportEvent(StadiumDetailActivity.this,stadium);

				
			
			}
		});
		setTopButtonRightRes(0);
		initViews();
		setStadiumInfoToUI(stadium);
		showProgressDialog("刷新", "请稍候...", null);

		getDataLoader(
				HttpConfig.REQUST_STADIUM_DETAIL_ACTION_URL + id,
				false, new Page(), uiFreshController);
		getDataLoader(HttpConfig.REQUST_BUILDING_PHOTO_URL + id,
				false, new Page(), uiFreshPhotoController);

	}

	private ImageView stop, postcard, rest, wash, exercise, rent, sale,
			product, fix, box;
	private TextView stadiumName;
	private TextView AveScore_tv;
	private TextView commentCount;
	private Button tel;
	private TextView addr;
	private HorizontalScrollView mPager;
	protected long id;
	private RatingBar commentScore;
	private TextView commentScore_tv;
	private TextView time;
	private TextView sportType;
	private TextView price;
	private void initViews() {

		stadiumName = (TextView) findViewById(R.id.stadium_name);

		Button share = (Button) findViewById(R.id._bt_share);
		share.setVisibility(View.VISIBLE);
		share.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (mStadiumDetailInfo==null) {
					return;
				}
				String msg = "你的朋友分享了"+CoachAdapter.setSportTypeStr(mStadiumDetailInfo.getTypedesc())+" "+"场馆 "+mStadiumDetailInfo.getName()+"的介绍给你，快来使用鸟孩运动APP吧。在这里，你能找到周边的场馆。"+
						"立刻体验:\nhttp://apps.wandoujia.com/apps/com.jameschen.birdboy/download";
				arg0.setTag(msg);
				share(arg0,ConstValues.TYPE_STADIUM_VAL);
			}
		});
	
		stadiumAveScore = (RatingBar) findViewById(R.id.score_rating);
		AveScore_tv = (TextView) findViewById(R.id.score_tv);

		commentScore = (RatingBar) findViewById(R.id.comment_score);
		commentScore_tv = (TextView) findViewById(R.id.comment_score_tv);
		commentCount = (TextView) findViewById(R.id.total_comment_num_tv);
		tel = (Button) findViewById(R.id.contact_btn);
		tel.setVisibility(View.GONE);
		addr = (TextView) findViewById(R.id.stadium_address_info);
		addr.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mStadiumDetailInfo!=null) {
					Util.go2Map(getBaseContext(), Location.CreateLocation(mStadiumDetailInfo.getPositiondesc(),mStadiumDetailInfo.getPosition()));
				}
			}
		});
		time = (TextView) findViewById(R.id.stadium_date);
		sportType = (TextView) findViewById(R.id.stadium_sport_type);
		price = (TextView) findViewById(R.id.stadium_price);
		
		stop = (ImageView) findViewById(R.id.stadium_stop_icon);
		postcard = (ImageView) findViewById(R.id.stadium_postcard_icon);
		rest = (ImageView) findViewById(R.id.stadium_rest_icon);
		wash = (ImageView) findViewById(R.id.stadium_wash_icon);
		exercise = (ImageView) findViewById(R.id.stadium_exercise_icon);
		rent = (ImageView) findViewById(R.id.stadium_rent_icon);
		sale = (ImageView) findViewById(R.id.stadium_sale_icon);
		product = (ImageView) findViewById(R.id.stadium_product_icon);
		fix = (ImageView) findViewById(R.id.stadium_fix_icon);
		box = (ImageView) findViewById(R.id.stadium_box_icon);
		// not support currently
		box.setVisibility(View.GONE);
		sale.setVisibility(View.GONE);
		fix.setVisibility(View.GONE);

		findViewById(R.id.comment_btn).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				Util.go2SendComment(StadiumDetailActivity.this,id,ConstValues.TYPE_STADIUM_VAL);
			}
		});

		mPager = (HorizontalScrollView) findViewById(R.id.perview_pics_pages);
			findViewById(R.id.stadium_intro_container).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (mStadiumDetailInfo!=null) {
						Intent intent = new Intent(StadiumDetailActivity.this,StadiumDetailIntroActivity.class);
						intent.putExtra(ConstValues.STADIUM, mStadiumDetailInfo);
						startActivity(intent );
					}
				
				}
			});
			
			findViewById(R.id.stadium_comment_scores_info).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Util.go2CommentList(StadiumDetailActivity.this,ConstValues.TYPE_STADIUM_VAL,id);
				}
			});
			tel.setOnClickListener( new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					if (LogInController.IsLogOn()) {
						if (mStadiumDetailInfo!=null) {
							Util.showContactDialog(mStadiumDetailInfo.getTypedesc(),1,StadiumDetailActivity.this, mStadiumDetailInfo.getName(), mStadiumDetailInfo.getMobile(), null);

						}
					}else {
						Util.go2LogInDialog(StadiumDetailActivity.this,new OnLogOnListener() {
							
							@Override
							public void logon() {
								if (mStadiumDetailInfo!=null) {
									Util.showContactDialog(mStadiumDetailInfo.getTypedesc(),1,StadiumDetailActivity.this, mStadiumDetailInfo.getName(), mStadiumDetailInfo.getMobile(), null);
								}								
							}
						});
					}
				
				}
			});
	}

	private void serviceSupprt(int option) {
		stop.setVisibility(option / 2 == 0 ? View.GONE : View.VISIBLE);
		postcard.setVisibility(option / 4 == 0 ? View.GONE : View.VISIBLE);
		rest.setVisibility(option / 8 == 0 ? View.GONE : View.VISIBLE);
		wash.setVisibility(option / 16 == 0 ? View.GONE : View.VISIBLE);
		exercise.setVisibility(option / 32 == 0 ? View.GONE : View.VISIBLE);
		rent.setVisibility(option / 64 == 0 ? View.GONE : View.VISIBLE);
		product.setVisibility(option / 128 == 0 ? View.GONE : View.VISIBLE);

	}
	StadiumDetailInfo mStadiumDetailInfo;
	private <T> void setStadiumInfoToUI(T stadium) {
		if (stadium==null) {
			return ;
		}
		if (stadium instanceof Stadium) {
			Stadium mStadium = (Stadium) stadium;
			stadiumName.setText(mStadium.getName());
			serviceSupprt(mStadium.getOptions());
			addr.setText(mStadium.getPositionDesc());
			if (!TextUtils.isEmpty(mStadium.getMobile())) {
				tel.setVisibility(View.VISIBLE);
			}
		//	tel.setText(mStadium.getMobile());
			// loadImage(HttpConfig.HTTP_BASE_URL+ mStadium.getLogo(),
			// stadiumHeadPhoto, roundOptions);
		} else {// stadium detail
			 WebStadiumDetail webStadiumDetail = (WebStadiumDetail) stadium;
			 mStadiumDetailInfo = webStadiumDetail.getBvo();
			
			 if (!TextUtils.isEmpty(mStadiumDetailInfo.getMobile())) {
					tel.setVisibility(View.VISIBLE);
				}
			 
			serviceSupprt(mStadiumDetailInfo.getOptions());// make sure newest
															// serive not
															// changed?
			// addr.setText(mStadiumDetailInfo.getPositionDesc());
			// tel.setText(mStadiumDetailInfo.getPhone());
			stadiumName.setText(mStadiumDetailInfo.getName());
			stadiumAveScore.setRating(mStadiumDetailInfo.getScore() / 2);
			commentScore.setRating(mStadiumDetailInfo.getScore() / 2);
			AveScore_tv.setText(mStadiumDetailInfo.getScore() + "");
			commentScore_tv.setText(mStadiumDetailInfo.getScore()+"");
			addr.setText(mStadiumDetailInfo.getPositiondesc());
			//tel.setText(mStadiumDetailInfo.getMobile());
			if (mStadiumDetailInfo.getWorktime()!=null) {
				time.setText(Html.fromHtml(mStadiumDetailInfo.getWorktime()));
			}
		
			price.setText(webStadiumDetail.getAreaprice());
			sportType.setText(Html.fromHtml(CoachAdapter.setSportTypeStr(mStadiumDetailInfo.getTypedesc())));
			commentCount.setText(mStadiumDetailInfo.getCommentCount() + "人点评");
			// loadImage(HttpConfig.HTTP_BASE_URL+ mStadiumDetailInfo.getLogo(),
			// stadiumLogo, options);

		}
	}

	private List<StadiumPhoto> stadiumPhotos;

	private void setPhotosToUI(List<StadiumPhoto> stadiumPhotos) {
		if (stadiumPhotos.size() == 0) {
			return;
		}
		if (this.stadiumPhotos == null) {
			this.stadiumPhotos = new ArrayList<StadiumPhoto>();
		}
		this.stadiumPhotos.clear();
		this.stadiumPhotos.addAll(stadiumPhotos);
		StadiumImageAdapter stadiumImageAdapter = new StadiumImageAdapter(this);

		stadiumImageAdapter.setImgResources(this.stadiumPhotos);
		((MyGallery) (mPager.findViewById(R.id.gallery)))
				.setAdapter(stadiumImageAdapter);

	}

	private void setCommentsToUI(List<Comment> comments) {

		ViewGroup viewGroup = (ViewGroup) findViewById(R.id.stadium_last_comments_info);
		int size =comments.size();
		int len=0;
		if (size == 0) {
	
			return;//no more
		}
		viewGroup.removeViews(1, viewGroup.getChildCount()-1);

		if (size>5) {//show more page.
			len=5;
		}else {
			len =size ;
		}
		
		for (int i = 0; i < len; i++) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup vGroup =(ViewGroup) inflater.inflate(R.layout.comment_item, viewGroup, false);
			TextView name = (TextView) vGroup.findViewById(R.id.comment_item_name);
			TextView info = (TextView) vGroup.findViewById(R.id.comment_item_content);
			TextView  date = (TextView) vGroup.findViewById(R.id.comment_item_date);
			name.setText(comments.get(i).getUsernick());
			info.setText(comments.get(i).getContent());
			date.setText(comments.get(i).getCtstr());
			final Comment comment =comments.get(i);
			View replay=(View) vGroup.findViewById(R.id.comment_reply);
			replay.setVisibility(View.GONE);
			replay.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
				Util.go2SendComment(StadiumDetailActivity.this,id,comment.getUserid(),comment.getUsernick(),ConstValues.TYPE_STADIUM_VAL);
				}
			});
			viewGroup.addView(vGroup);
			 if (i<len-1) {
				 
				 View line =(View) inflater.inflate(R.layout.item_dot_line, viewGroup, false);
					viewGroup.addView(line);
			 }
		}
	
	}

	private void setNoticesToUI(List<Notice> notices) {

	}

	private void setBooksToUI(List<AreaPriceRangs> list) {
		ViewGroup viewGroup = (ViewGroup) findViewById(R.id.stadium_course_info);
		if (list==null) {
			return;
		}
		int size = list.size();
		int len = size;
		if (size == 0) {

			return;// no more
		}
		viewGroup.removeViews(1, viewGroup.getChildCount()-1);

		for (int i = 0; i < len; i++) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup vGroup = (ViewGroup) inflater.inflate(
					R.layout.book_list_item, viewGroup, false);

			TextView date = (TextView) vGroup.findViewById(R.id.book_date);
			TextView intro = (TextView) vGroup.findViewById(R.id.book_intro);
			TextView price = (TextView) vGroup.findViewById(R.id.book_price);
			TextView mark = (TextView) vGroup.findViewById(R.id.book_mark);
			date.setText(list.get(i).getMd() + "  " + list.get(i).getWeekStr());
			intro.setText(list.get(i).getDesc());
			price.setText(list.get(i).getPriceRange());
			// vGroup.setBackgroundResource(R.drawable.list_bottom_selector);
			if (i == 0) {
				View line = (View) inflater.inflate(R.layout.item_line,
						viewGroup, false);// add top
				viewGroup.addView(line);
			}
			viewGroup.addView(vGroup);
			if (i < len - 1) {

				View line = (View) inflater.inflate(R.layout.item_line,
						viewGroup, false);
				viewGroup.addView(line);
			}

		}
	}

	private void onDataChangeNotify(WebStadiumDetail webStadiumDetail) {
		setStadiumInfoToUI(webStadiumDetail);
		setCommentsToUI(webStadiumDetail.getComments());
		setBooksToUI(webStadiumDetail.getAreas());
	}

	private UIController uiFreshPhotoController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			WebStadiumDetail webStadiumDetail = (WebStadiumDetail) content;
			setPhotosToUI(webStadiumDetail.getPhotos());

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebStadiumDetail>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			if(errorCode>-1)
			showToast(getString(R.string.get_photo_error_from_net));
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG + "photo";
		}
	};

	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			WebStadiumDetail webStadiumDetail = (WebStadiumDetail) content;
			onDataChangeNotify(webStadiumDetail);
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebStadiumDetail>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			showToast(getString(R.string.get_detail_info_error));
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};
}
