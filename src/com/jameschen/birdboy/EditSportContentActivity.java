package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;

//http://birdboy.cn/addcommentAPI    get请求
//
//字段               类型                描述                           备注            值条件
//
//score               int                 提交的分数
//
//replycontent    string            点评的内容
//
//userid              int                点评用户的id
//
//nick                 string            点评用户的昵称
//
//id                     int                 场馆或者教练的id
//
//fromtype          int               评论的类型 1：场馆   2  教练
//
//返回数据
//
//{success:true;}


public class EditSportContentActivity extends BaseActivity{
	EditText inputEditText;
	String content ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sport_event_content_edit_ui);
		setTitle("编辑活动介绍");
		Intent intent = getIntent();
		 content = intent.getStringExtra("content");
	
		initViews();
		
		
		setTopBarRightBtnListener("完成", new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				closeInputMethod();
				// TODO Auto-generated method stub
				if (TextUtils.isEmpty(inputEditText.getText())||inputEditText.getText().toString().trim().equals("")) {
					showToast("请输入活动内容");
					return;
				}else {
					Intent intent = new Intent();
					intent.putExtra("content", inputEditText.getText().toString());
					setResult(RESULT_OK, intent);
					finish();
				}
			
			}
		});
	}

	private void initViews() {
		 inputEditText = (EditText) findViewById(R.id.comment_Edit);
		 if (content!=null) {
			 inputEditText.setText(content);
		}
		
	}

	
}
