package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.WebInbox;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.Message;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;


public class DetailContentActivity extends BaseActivity{
	TextView contentText;
	private String content;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_content_ui);
		setTitle("详细内容");
		Intent intent = getIntent();
		content = intent.getStringExtra("content");
		
		initViews();
		
	}
	
	
	
	
	private void initViews() {
		 contentText = (TextView) findViewById(R.id.detail_content);
		 if (TextUtils.isEmpty(content)) {
			return;
		}
		 contentText.setText(Html.fromHtml(content));
	}

	
}
