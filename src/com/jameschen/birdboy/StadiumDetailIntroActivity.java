package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.internal.Utils;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.CoachDetailAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebCoachDetail;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Location;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.model.entity.StadiumDetailInfo;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;


public class StadiumDetailIntroActivity extends BaseActivity {

	private TextView name;
	private TextView address;
	private Button phone;
	private TextView intro;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stadium_detail_intro);
		Intent intent = getIntent();
		stadiumDetailInfo= (StadiumDetailInfo) intent
				.getSerializableExtra(ConstValues.STADIUM);
		setTitle("详细资料");
		if (stadiumDetailInfo==null) {
			return;
		}
		initViews();
		setStadiumInfoToUI(stadiumDetailInfo);

	}
	StadiumDetailInfo stadiumDetailInfo;
	private void initViews() {
		name = (TextView) findViewById(R.id.stadium_name);
		address = (TextView) findViewById(R.id.stadium_address);
		address.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (stadiumDetailInfo!=null) {
					Util.go2Map(getBaseContext(), Location.CreateLocation(stadiumDetailInfo.getPositiondesc(),stadiumDetailInfo.getPosition()));
				}
			}
		});
		phone = (Button) findViewById(R.id.contact_btn);
		phone.setVisibility(View.GONE);
		if (!TextUtils.isEmpty(stadiumDetailInfo.getMobile())) {
			phone.setVisibility(View.VISIBLE);
		}
		intro = (TextView) findViewById(R.id.stadium_detail_intro);
		phone.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				if (LogInController.IsLogOn()) {
					if (stadiumDetailInfo!=null) {
						Util.showContactDialog(stadiumDetailInfo.getTypedesc(),1,StadiumDetailIntroActivity.this, stadiumDetailInfo.getName(), stadiumDetailInfo.getMobile(), null);

					}
				}else {
					Util.go2LogInDialog(StadiumDetailIntroActivity.this,new OnLogOnListener() {
						
						@Override
						public void logon() {
							// TODO Auto-generated method stub
							if (stadiumDetailInfo!=null) {
								Util.showContactDialog(stadiumDetailInfo.getTypedesc(),1,StadiumDetailIntroActivity.this, stadiumDetailInfo.getName(), stadiumDetailInfo.getMobile(), null);

							}	
						}
					});
				}
			
			}
		});
	}

	private <T> void setStadiumInfoToUI(T stadiumDetailInfo) {

		StadiumDetailInfo mStadiumDetailInfo = (StadiumDetailInfo) stadiumDetailInfo;
		name.setText(mStadiumDetailInfo.getName());
		address.setText(mStadiumDetailInfo.getPositiondesc());
		if (mStadiumDetailInfo.getInfo()!=null) {
			intro.setText(Html.fromHtml(mStadiumDetailInfo.getInfo()));
		}
	}

}
