package com.jameschen.birdboy.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.environment.ConstValues.IntentAction;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.User;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.model.entity.SportUser;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class SportEventAdapter extends ObjectAdapter<SportEvent> {
	private Context context;

	public SportEventAdapter(BaseActivity activity) {
		super(activity);
		context = activity;
		initImageDisplayOptions(activity,defaultImageResId=0);
		
	}


	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		SportEventHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(
					R.layout.sportevent_item, paramViewGroup, false);
			item = new SportEventHoldView();
			item.initChildView(convertView);
			// item.setListener(addCartListener);
			convertView.setTag(item);
		} else {
			item = (SportEventHoldView) convertView.getTag();
		}
		
		SportEvent sportEvent = getItem(paramInt);
		item.setInfo(sportEvent);
		item.setDistVisible(distanceShow);
	

		return convertView;

	}
	
	public static List<SportUser> getUsers(String users) {
		if (TextUtils.isEmpty(users)) {
			return null;
		}
		List<SportUser> sportUsers = new ArrayList<SportUser>();
		String []userInfo = users.split("\\|");
		int size = userInfo.length;
		for (int i = 0; i < size; i++) {
			SportUser sportUser = new SportUser();
			System.err.println("===="+userInfo[i]);
			String infos[] = userInfo[i].split(":");
			if (infos==null || infos.length==1) {
				return null;
			}
			sportUser.setName(infos[0]);
			sportUser.setPhone(infos[1]);
			if (infos.length==3) {//no logo
				if (i==0) {
					sportUser.setSex(Integer.parseInt(infos[2]));
				}else {
					sportUser.setId(Integer.parseInt(infos[2]));
				}
			}else if (infos.length==4) {//has user logo
				if (i==0) {
					sportUser.setSex(Integer.parseInt(infos[2]));
				}else {
					sportUser.setId(Integer.parseInt(infos[2]));
				}
				sportUser.setLogo(infos[3]);
				String blogo=infos[3].replace("slogo", "blogo");
				sportUser.setBlogo(blogo);
			}
			
			sportUsers.add(sportUser);
		}
		return sportUsers;
		
	}
	
	
private boolean distanceShow =false;
	public boolean isDistanceShow() {
	return distanceShow;
}


public void setDistanceShow(boolean distanceShow) {
	this.distanceShow = distanceShow;
}


	@Override
	public void recycle() {
		
		// super.recycle();

	}

	public static final int SELL_OUT = 1;

	public class AddCartListener implements OnClickListener {

		@Override
		public void onClick(View v) {

		}

	}
	
	public static String getWeekDayStr(int date) {
		if (date==0) {
			return "";
		}
		return dayWeek[date - 1];
	}

	private final  class SportEventHoldView extends HoldView<SportEvent> {
		private TextView name, distance,adminer,extraInfo,numFlag;
		private ImageView sex,createrPhoto;
		private TextView fav, comment, num;
		private TextView address, time;
		private SportEvent sportEvent;

		public SportEvent getObjectInfo() {
			return sportEvent;
		}

		private Context context;

		public void setVisible(int Flag) {

		}

		public void setDistVisible(boolean distVisible) {
			if (!distVisible) {
				((View)distance.getParent()).setVisibility(View.GONE);
			} else {
				((View)distance.getParent()).setVisibility(View.VISIBLE);
				distance.setText(getDistance());
			}
		}

		public void initChildView(View convertView) {
			context = convertView.getContext();

			name = (TextView) convertView.findViewById(R.id.event_item_name);
			address = (TextView) convertView
					.findViewById(R.id.event_item_address);
			numFlag=(TextView)convertView.findViewById(R.id.event_memer_max_flag);
			distance = (TextView) convertView
					.findViewById(R.id.event_item_location_distance);
			time = (TextView) convertView
					.findViewById(R.id.event_item_date_time);
			fav = (TextView) convertView.findViewById(R.id.event_fav_num);
			createrPhoto =(ImageView)convertView.findViewById(R.id.event_item_creater_photo);
			num = (TextView) convertView.findViewById(R.id.event_memer_num);
			sex = (ImageView) convertView.findViewById(R.id.event_item_admin_sex_info);
			comment = (TextView) convertView.findViewById(R.id.event_comment_num);
			adminer = (TextView) convertView.findViewById(R.id.event_item_admin_name);
			extraInfo = (TextView) convertView.findViewById(R.id.event_item_admin_extra_info);

		}

		

		private String getCostInfo() {
			return sportEvent.getRmb()==0?"免费":"￥"+sportEvent.getRmb();
		}
		
		
		
		@Override
		public void setInfo(SportEvent object) {
			// TODO Auto-generated method stub
			this.sportEvent = (SportEvent) object;
			name.setText(sportEvent.getName());
			address.setText(sportEvent.getPlace());
			adminer.setText(sportEvent.getContact());
			List<SportUser> sportUsers =getUsers(sportEvent.getUsers());
			
		
			String businessTag =sportEvent.getAddint()>=2?"商家":"个人";
			
			if (sportEvent.getAddint()<2) {//personal 
				int sexRes=R.drawable.boy;
				int defualtIcon = R.drawable.m;
				if (sportUsers!=null) {
					sexRes=sportUsers.get(0).getSex()==1?R.drawable.boy:R.drawable.girl;
					defualtIcon =sportUsers.get(0).getSex()==1?R.drawable.m:R.drawable.wm;
				}
				sex.setVisibility(View.VISIBLE);
				sex.setImageResource(sexRes);
				DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(defualtIcon)
	    		.showImageForEmptyUri(defualtIcon)
	    		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(20))
	    		.cacheOnDisc(true).build();
				if (sportUsers!=null&&sportUsers.size()>0) {
					if (!TextUtils.isEmpty(sportUsers.get(0).getLogo())) {
						loadImage(HttpConfig.HTTP_BASE_URL+sportUsers.get(0).getLogo(), createrPhoto,mOptions);
					}else {
						createrPhoto.setImageResource(defualtIcon);
					}
				}else {
					createrPhoto.setImageResource(defualtIcon);
				}
			}else {
				sex.setVisibility(View.GONE);
				createrPhoto.setImageResource(R.drawable.icon_default);
			}
			
			
			
			Drawable drawable = context.getResources().getDrawable(R.drawable.has_pic);
			drawable.setBounds(0, 0, drawable.getMinimumWidth(),
					drawable.getMinimumHeight());// 必须设置图片大小，否则不显示
			
			if (sportEvent.getAddtext()==null) {
				name.setCompoundDrawables(null, null, null, null);
			}else {
				name.setCompoundDrawablePadding(Util.getPixByDPI(context, 5));
				name.setCompoundDrawables(null, null, drawable, null);
			}
			extraInfo.setText(CoachAdapter.setSportTypeStr(sportEvent.getType()) +"  "+ getCostInfo()+" "+businessTag);
			time.setText(sportEvent.getTimeStr()+"   "+getWeekDayStr(sportEvent.getSweek()));
			num.setText(sportEvent.getWantnum()+"报名");
			comment.setText(sportEvent.getComments()+"留言");
			fav.setText(sportEvent.getKeeps() + "收藏");
			if (sportEvent.getNownum()>0&&sportEvent.getNownum()==sportEvent.getNum()) {
				numFlag.setVisibility(View.VISIBLE);
			}else {
				numFlag.setVisibility(View.GONE);
			}
			if (sportEvent.getDist()>0) {
				distance.setText(getDistanceFromServer(true));
			}else {
				if (sportEvent.getPosition() == null) {
					((View) distance.getParent()).setVisibility(View.GONE);
				} else {
					
//					if (lat == -1 && lng == -1) {
//						((View) distance.getParent()).setVisibility(View.GONE);
//					} else {
//						((View) distance.getParent()).setVisibility(View.VISIBLE);
//						distance.setText(getDistance());
//					}
					
				}
			}
			
		

		}

		private CharSequence getDistance() {
		
				return getDistanceFromServer(true);

		}

		private CharSequence getDistanceFromServer(boolean getCurrentLocation) {
			DecimalFormat df = null;
			if (!getCurrentLocation) {

				return "定位失败";
			}
			double distance = sportEvent.getDist();
			if (distance >= 1000) {// show in km
				df = new DecimalFormat(".0");
				return df.format(distance / 1000) + "km";
			} else {
				return (int) distance + "m";
			}
		}

	}

	private static String dayWeek[] = new String[] { "周一", "周二", "周三", "周四",
			"周五", "周六", "周日" };
}
