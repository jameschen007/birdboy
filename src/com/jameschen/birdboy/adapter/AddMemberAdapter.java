package com.jameschen.birdboy.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class AddMemberAdapter extends ObjectAdapter<InvitedUser> {
	private Context context;
	private final int START=-100;
	public AddMemberAdapter(BaseActivity activity) {
		super(activity);
		this.context = activity;
		initImageDisplayOptions(activity,defaultImageResId=0);
		createStart();
	}
private void createStart() {
	InvitedUser startInvitedUser = new InvitedUser();
	startInvitedUser.setId(START);
	addObject(startInvitedUser);// TODO Auto-generated method stub

}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView != null) {
			holder = (ViewHolder) convertView.getTag();
			attchReourceToView(holder, getItem(position));
			return convertView;
		}
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = (ViewGroup) inflater.inflate(R.layout.add_member_item,
				parent, false);
		holder = new ViewHolder();
		holder.logo = (ImageView) convertView.findViewById(R.id.memberPhoto);
		holder.delete = (ImageView) convertView.findViewById(R.id.delete_member);
		holder.startSport = convertView.findViewById(R.id.start_item);
		holder.addItemView = convertView.findViewById(R.id.additem);

		convertView.setTag(holder);
		attchReourceToView(holder, getItem(position));
		return convertView;
	}

	public  void attchReourceToView(ViewHolder holder, final InvitedUser item) {
		
		if (item.getId()==START) {

			holder.addItemView.setVisibility(View.GONE);
			holder.startSport.setVisibility(View.VISIBLE);
			holder.startSport.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (memberChangedListener!=null) {
						List<InvitedUser> mInvitedUsers=new ArrayList<InvitedUser>(getObjectInfos());
						mInvitedUsers.remove(mInvitedUsers.size()-1);//remove last
						
						memberChangedListener.startMemberSport(mInvitedUsers);
					}
				}
			});

		}else {
			holder.startSport.setVisibility(View.GONE);
			holder.addItemView.setVisibility(View.VISIBLE);
			//
			 int defualtIcon = item.getSex()==1?R.drawable.m:R.drawable.wm;
			DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
			.showImageOnLoading(defualtIcon)
    		.showImageForEmptyUri(defualtIcon)
    		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(10))
    		.cacheOnDisc(true).build();
			if (!TextUtils.isEmpty(item.getLogo())) {
				if (item.head!=null) {
					holder.logo.setImageDrawable(item.head);
				}else {
					loadImage(HttpConfig.HTTP_BASE_URL+item.getLogo(), holder.logo,mOptions);
				}
				
			
			}else {
				 holder.logo.setImageResource(defualtIcon);
			}
		}
		
		holder.delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				remove(item);
				notifyDataSetChanged();
				inovkeListener();
			}
		});
	}
	

	public int getInviedCount() {
		return super.getCount()-1;
	}
	
	@Override
	public void clear() {
		getObjectInfos().clear();
		createStart();
	}
	
	
	final class ViewHolder {
		View startSport,addItemView;
		ImageView logo;
		ImageView delete;
	}
	private void inovkeListener() {
		if (memberChangedListener!=null) {
			memberChangedListener.onMemberChanged();
		}
	}

	

	public int addMember(InvitedUser invitedUser, Drawable head) {
			if (getObjectInfos().contains(invitedUser)) {
				return -2;
			}
			invitedUser.head=head;
			insert(invitedUser, getInviedCount());
			notifyDataSetChanged();
			inovkeListener();
			return 0;
			
	}
	public interface OnMemberChangedListener {

		void onMemberChanged();
		
		void startMemberSport(List<InvitedUser> uInvitedUsers);

	}
	private OnMemberChangedListener memberChangedListener;

	public void setOnMemberChangedListener(
			OnMemberChangedListener onMemberChangedListener) {
		// TODO Auto-generated method stub
		this.memberChangedListener = onMemberChangedListener;
	}

}