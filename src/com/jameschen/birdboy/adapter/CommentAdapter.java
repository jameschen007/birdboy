package com.jameschen.birdboy.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SportEventDetailActivity;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.utils.Util;

public class CommentAdapter extends ObjectAdapter<Comment> {
	private BaseActivity context;
	private long id;
	public CommentAdapter(BaseActivity activity,long id) {
		super(activity);
		context = activity;
		this.id = id;
	}


	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		CommentHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(R.layout.comment_list_item,
					paramViewGroup, false);
			item = new CommentHoldView(context,id);
			item.initChildView(convertView);
			convertView.setTag(item);
		} else {
			item = (CommentHoldView) convertView.getTag();
		}

		Comment comment = getItem(paramInt);
		item.setInfo(comment);
	
		return convertView;

	}

	@Override
	public void recycle() {
		// TODO Auto-generated method stub
		super.recycle();

	}

	public static final int SELL_OUT = 1;

	public class AddCartListener implements OnClickListener {

		@Override
		public void onClick(View v) {

		}

	}

	private final  class CommentHoldView extends HoldView<Comment> {

		private TextView name,date,content;
		private RatingBar commentRatingBar;
		private Comment comment;
		private View replay;
		
		public Comment getObjectInfo() {
			return comment;
		}

		private BaseActivity context;
		private long id;
		public CommentHoldView(BaseActivity cActivity, long id){
			this.context =cActivity;
			this.id =id;
		}
		
		public void setVisible(int Flag) {

		}

		public void initChildView(View convertView) {
			replay = (View) convertView.findViewById(R.id.comment_reply);
			name = (TextView) convertView.findViewById(R.id.comment_item_name);
			date = (TextView) convertView.findViewById(R.id.comment_item_date);
			content = (TextView) convertView.findViewById(R.id.comment_item_content);
			commentRatingBar = (RatingBar) convertView.findViewById(R.id.comment_rating);
			replay.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Util.go2SendComment(context,id,comment.getUserid(),comment.getUsernick(),ConstValues.TYPE_EVENT_VAL);

				}
			});
		}

		@Override
		public void setInfo(Comment object) {
			// TODO Auto-generated method stub
			this.comment = (Comment) object;
			name.setText(comment.getUsernick());
			date.setText(comment.getCtstr());
			content.setText(Html.fromHtml(comment.getContent()));
			commentRatingBar.setRating((float)comment.getScore()/2);
			if (starVisiable) {
				commentRatingBar.setVisibility(View.VISIBLE);
				replay.setVisibility(View.GONE);
			} else {
				commentRatingBar.setVisibility(View.GONE);
				replay.setVisibility(View.VISIBLE);
			}

		}

	}

	private int position = 0;

	public void setCurrentSelection(int position) {
		// TODO Auto-generated method stub
		this.position = position;
		notifyDataSetChanged();
	}
	private boolean starVisiable =true;

	public void setStarVisable(boolean b) {
		// TODO Auto-generated method stub
		starVisiable =b;
	}

}
