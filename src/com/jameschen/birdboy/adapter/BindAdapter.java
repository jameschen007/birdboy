package com.jameschen.birdboy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.plugin.BindModel;

public class BindAdapter extends ObjectAdapter<BindModel> {
	private Context context;

	public BindAdapter(BaseActivity activity,BindItemClickListener bindItemClickListener) {
		super(activity);
		context = activity;
		itemClickListener = bindItemClickListener;
		initImageDisplayOptions(activity,defaultImageResId=0);
		
	}

	

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		BindModelHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(
					R.layout.bind_item, paramViewGroup, false);
			item = new BindModelHoldView();
			item.initChildView(convertView);
			// item.setListener(addCartListener);
			convertView.setTag(item);
		} else {
			item = (BindModelHoldView) convertView.getTag();
		}
		
		BindModel bindModel = getItem(paramInt);
		item.setPosition(paramInt);
		item.setInfo(bindModel);
	

		return convertView;

	}
	


	@Override
	public void recycle() {
		
		super.recycle();

	}

	
	private BindItemClickListener itemClickListener;
	
	
	public interface BindItemClickListener{
		void OnItemClick(int position,BindModel bindModel);
	}
	
	private final  class BindModelHoldView extends HoldView<BindModel> {
		private TextView name;
		private Button bindBtn;
		private ImageView logo;
		
		private BindModel bindModel;

		public BindModel getObjectInfo() {
			return bindModel;
		}

		private Context context;

		public void setVisible(int Flag) {

		}


		public void initChildView(View convertView) {
			context = convertView.getContext();

			name = (TextView) convertView.findViewById(R.id.bind_name);
			logo =(ImageView)convertView.findViewById(R.id.bind_logo);
			bindBtn = (Button) convertView.findViewById(R.id.bind_btn);
			
			bindBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					itemClickListener.OnItemClick(position, bindModel);
				}
			});
			
		}
		int position=0;
	

		public void setPosition(int position) {
			this.position = position;
		}

		@Override
		public void setInfo(BindModel object) {
			// TODO Auto-generated method stub
			this.bindModel = (BindModel) object;
			if (bindModel.fromtype==BindModel.QQ) {
				name.setText("腾讯微博");
				logo.setImageResource(R.drawable.qq);
			}else if  (bindModel.fromtype==BindModel.SINA) {
				name.setText("新浪微博");
				logo.setImageResource(R.drawable.weibo);

			}
			
		
			if (bindModel.token!=null) {
				bindBtn.setText("已绑定");
				bindBtn.setBackgroundResource(R.drawable.wodebangding_03);
				bindBtn.setEnabled(false);
			}else {
				bindBtn.setText("绑定");
				bindBtn.setBackgroundResource(R.drawable.invite_btn_state);
				bindBtn.setEnabled(true);
			}

		}


	}

}
