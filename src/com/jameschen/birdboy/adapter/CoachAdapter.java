package com.jameschen.birdboy.adapter;

import java.text.DecimalFormat;

import android.R.integer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.environment.ConstValues.IntentAction;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;

public class CoachAdapter extends ObjectAdapter<Coach> {
	private Context context;

	public CoachAdapter(BaseActivity activity) {
		super(activity);
		context = activity;
		initImageDisplayOptions(activity,20);
	}

	

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		CoachHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(R.layout.coach_item,
					paramViewGroup, false);
			
			item = new CoachHoldView();
			item.initChildView(convertView);
			convertView.setTag(item);
		} else {
			item = (CoachHoldView) convertView.getTag();
		}
		
		Coach coach = getItem(paramInt);
		item.setInfo(coach);
		item.setDistVisible(showDistance);
		
			if (coach.getLogo()==null) {
				item.icon.setImageResource(R.drawable.icon_default);
			}else {
				loadImage(HttpConfig.HTTP_BASE_URL + coach.getLogo(), item.icon);
			}
	

		return convertView;

	}

	@Override
	public void recycle() {
	
		super.recycle();

	}

	public static final int SELL_OUT = 1;

	public class AddCartListener implements OnClickListener {

		@Override
		public void onClick(View v) {

		}

	}

	private final static class CoachHoldView extends HoldView<Coach> {
		private TextView name, distance;
		private ImageView skill, indentify, type,personal;
		private TextView score;
		private ImageView icon, split;
		private TextView techAddress,level;
		private Coach coach;

		public Coach getObjectInfo() {
			return coach;
		}

		private Context context;

		public void setDistVisible(boolean distVisable) {
			if (!distVisable) {
				split.setVisibility(View.GONE);
				distance.setVisibility(View.GONE);
			} else {
				split.setVisibility(View.VISIBLE);
				distance.setVisibility(View.VISIBLE);
				distance.setText(getDistance());
			}
			
		}

		
		public void initChildView(View convertView) {
			context = convertView.getContext();

			name = (TextView) convertView.findViewById(R.id.coach_item_name);
			level = (TextView) convertView.findViewById(R.id.coach_item_level);
			skill = (ImageView) convertView
					.findViewById(R.id.coach_item_skill_icon);
			type = (ImageView) convertView
					.findViewById(R.id.coach_item_category_icon);
			personal = (ImageView) convertView
					.findViewById(R.id.coach_item_personal_icon);
			indentify = (ImageView) convertView
					.findViewById(R.id.coach_item_identify_icon);
			score = (TextView) convertView.findViewById(R.id.coach_item_score);
			distance = (TextView) convertView
					.findViewById(R.id.coach_item_distance);
			techAddress = (TextView) convertView
					.findViewById(R.id.coach_item_tech_address);

			split = (ImageView) convertView.findViewById(R.id.item_split);
			icon = (ImageView) convertView
					.findViewById(R.id.coach_item_portrait);
		}

		@Override
		public void setInfo(Coach object) {
			// TODO Auto-generated method stub
			this.coach = (Coach) object;
			name.setText(coach.getName());
			score.setText(coach.getScores() + "");
			if (coach.getAddtext() == null) {
				techAddress.setText("授课地点：待定");
			} else {
				techAddress.setText("授课地点：" + coach.getAddtext());
			}
			level.setText(getLevelStr(coach.getLevel()));
			type.setImageResource(setSportTypeImgRes(coach.getType()));
			personal.setVisibility(sportPersonalImg(coach.getJobtype()));
			indentify.setVisibility(sportIndentifyImg(coach.getStatus()));
			skill.setVisibility(sportSkillImg(coach.getAddint()));
		

		}

	

		private CharSequence getDistance() {
				return getDistanceFromServer(true);
		}

		private CharSequence getDistanceFromServer(boolean getCurrentLocation) {
			DecimalFormat df = null;
			if (!getCurrentLocation) {

				return "定位失败";
			}
			double distance = coach.getDist();
			if (distance >= 1000) {// show in km
				df = new DecimalFormat(".0");
				return df.format(distance / 1000) + "km";
			} else {
				return (int) distance + "m";
			}
		}



		@Override
		public void setVisible(int Flag) {
			// TODO Auto-generated method stub
			
		}

	}

	public static int sportPersonalImg(int jobtype) {
		// TODO Auto-generated method stub
		return jobtype >= 2 ? View.VISIBLE : View.GONE;
	}
	
	public static CharSequence getLevelStr(float level) {
		String levelStr =null;
		if (level<=1) {
			levelStr="初级";
		}else if (level<=2) {
			levelStr = "中级";
		}else if (level<=3) {
			levelStr = "高级";
		}else if (level<=4) {
			levelStr = "资深";
		}else {
			levelStr="特级";
		}
		return levelStr;
	}

	public static int sportSkillImg(int hasSkill) {
		return hasSkill == 1 ? View.VISIBLE : View.GONE;
	}

	public static int sportIndentifyImg(int isIndentify) {
		return isIndentify == 1 ? View.VISIBLE : View.GONE;
	}

	public static String setSportTypeStr(int type) {
		String name = "未知";
		switch (type) {
		case SportType.TABLETENIS:
			name = "乒乓球";
			break;
		case SportType.BADMINTON:
			name = "羽毛球";
			break;
		case SportType.SWIMMING:
			name = "游泳";
			break;
		case SportType.TENNIS:
			name = "网球";
			break;
		case SportType.FOOTBALL:
			name = "足球";
			break;
		case SportType.FITNESS:
			name = "健身";
			break;
		case SportType.BASKET_BALL:
			name = "篮球";
			break;
		default:
			
			break;
		}
		return name;
	}
	
	public static int setSportTypeImgRes(int type) {
		int resource = R.drawable.icon_swimming;
		switch (type) {
		case SportType.TABLETENIS:
			 resource = R.drawable.tabletennis;
			break;
		case SportType.BADMINTON:
			 resource = R.drawable.badminton;
			break;
		case SportType.SWIMMING:
			resource = R.drawable.icon_swimming;
			break;
		case SportType.TENNIS:
			resource = R.drawable.tennis;
			break;
		case SportType.FOOTBALL:
			resource = R.drawable.football;
			break;
		case SportType.FITNESS:
			resource = R.drawable.fit_ness;
			break;
		case SportType.BASKET_BALL:
			resource = R.drawable.basketball;
			break;
		default:
			resource = R.drawable.translant;
			break;
		}
		return resource;
	}

}
