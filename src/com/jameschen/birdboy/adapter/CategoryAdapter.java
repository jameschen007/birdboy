package com.jameschen.birdboy.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.utils.Util;

public class CategoryAdapter extends ArrayAdapter<Category> {
	private Context context;

	public CategoryAdapter(Context context, final List<Category> objects) {
		super(context, -1, objects);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView != null) {
			holder = (ViewHolder) convertView.getTag();
			attchReourceToView(holder.item, getItem(position));
			return convertView;
		}
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = (ViewGroup) inflater.inflate(R.layout.category_item,
				parent, false);
		holder = new ViewHolder();
		holder.item = (TextView) convertView.findViewById(R.id.item_text1);
		convertView.setTag(holder);
		attchReourceToView(holder.item, getItem(position));
		return convertView;
	}

	public static void attchReourceToView(TextView view, Category item) {
		view.setText(item.getName());
		view.setCompoundDrawablePadding(Util.getPixByDPI(view.getContext(), 10));
		if (item.getIconResource() == 0) {
			return;
		}
		Drawable drawable = view.getResources().getDrawable(
				item.getIconResource());
		drawable.setBounds(0, 0, drawable.getMinimumWidth(),
				drawable.getMinimumHeight());// 必须设置图片大小，否则不显示
		view.setCompoundDrawables(drawable, null, null, null);
	}

	public static void attchReourceToView(TextView view, Category item,int rightRes) {
		view.setText(item.getName());
		view.setCompoundDrawablePadding(Util.getPixByDPI(view.getContext(), 1));
		if (item.getIconResource() == 0) {
			return;
		}
		Drawable drawable = view.getResources().getDrawable(
				item.getIconResource());
		drawable.setBounds(0, 0, drawable.getMinimumWidth(),
				drawable.getMinimumHeight());// 必须设置图片大小，否则不显示
		Drawable drawableRight = view.getResources().getDrawable(rightRes);
		drawableRight.setBounds(0, 0, drawableRight.getMinimumWidth(),
				drawableRight.getMinimumHeight());// 必须设置图片大小，否则不显示
		view.setCompoundDrawables(drawable, null, drawableRight, null);
	}
	
	
	final class ViewHolder {
		TextView item;
	}

}