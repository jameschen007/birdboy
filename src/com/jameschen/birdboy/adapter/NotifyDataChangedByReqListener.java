package com.jameschen.birdboy.adapter;

import com.jameschen.birdboy.model.Page;

public interface NotifyDataChangedByReqListener {

	void notifydataChanged(Page page);
}
