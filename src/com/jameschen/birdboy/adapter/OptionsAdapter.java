package com.jameschen.birdboy.adapter;

import java.util.HashMap;
import java.util.List;

import android.R.integer;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.FliterOptionItem;

public class OptionsAdapter extends ObjectAdapter<FliterOptionItem> {
	private Context context;

	public OptionsAdapter(BaseActivity activity) {
		super(activity);
		context = activity;
	}

	public OptionsAdapter(BaseActivity activity, List<FliterOptionItem> list) {
		super(activity, ImageLoad.NONE);
		context = activity;
		addObjectList(list);
		notifyDataSetChanged();
	}

	@Override
	public void addObjectList(List<FliterOptionItem> mList) {
		// TODO Auto-generated method stub
		super.addObjectList(mList);

		initSelectItem(mList.size());
	}

	@Override
	public void addObject(FliterOptionItem object) {
		// TODO Auto-generated method stub
		super.addObject(object);
		initSelectItem(1);
	}

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		OptionHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.option_item, null);
			item = new OptionHoldView();
			item.initChildView(convertView);
			convertView.setTag(item);
		} else {
			item = (OptionHoldView) convertView.getTag();
		}

		FliterOptionItem fliterOptionItem = getItem(paramInt);
		item.setInfo(fliterOptionItem, getIsSelected().get(paramInt));

		return convertView;

	}

	@Override
	public void recycle() {
		// TODO Auto-generated method stub
		super.recycle();

	}

	public class OptionCheckListener implements OnCheckedChangeListener {
		int position;

		public void setCheckOption(int positon) {
			this.position = positon;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub

			getIsSelected().put(position, isChecked);

		}

	}

	// 用来控制CheckBox的选中状况
	private HashMap<Integer, Boolean> isSelected;

	// 初始化isSelected的数据
	private void initSelectItem(int size) {
		if (isSelected == null) {
			isSelected = new HashMap<Integer, Boolean>();
		} else {
		}
		int lastLen = isSelected.size();
		for (int i = 0 + lastLen; i < size + lastLen; i++) {
			getIsSelected().put(i + lastLen, false);
		}
	}

	public HashMap<Integer, Boolean> getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(HashMap<Integer, Boolean> selected) {
		isSelected = selected;
	}

	private final static class OptionHoldView extends
			HoldView<FliterOptionItem> {

		private TextView name;
		private FliterOptionItem optionItem;
		private CheckBox choose;

		public FliterOptionItem getObjectInfo() {
			return optionItem;
		}

		private Context context;

		public void setVisible(int Flag) {

		}

		public void initChildView(View convertView) {
			context = convertView.getContext();

			name = (TextView) convertView.findViewById(R.id.item_tv);
			choose = (CheckBox) convertView.findViewById(R.id.item_cb);
		}

		public void setInfo(FliterOptionItem object, boolean isChecked) {
			this.optionItem = (FliterOptionItem) object;
			choose.setChecked(isChecked);
			setInfo(optionItem);
		}

		@Override
		public void setInfo(FliterOptionItem object) {
			// TODO Auto-generated method stub
			name.setText(optionItem.getName());
		}

	}

	public void setItemChecked(int position, Object viewHolder) {
		// TODO Auto-generated method stub
		// 取得ViewHolder对象，这样就省去了通过层层的findViewById去实例化我们需要的cb实例的步骤
		OptionHoldView holder = (OptionHoldView) viewHolder;
		// 改变CheckBox的状态
		holder.choose.toggle();
		getIsSelected().put(position, holder.choose.isChecked());
		notifyDataSetChanged();
	}

	public int getAllCheckOptionsValue() {
		int value = 0;
		List<FliterOptionItem> optionsFilters = getObjectInfos();
		for (int i = 0; i < optionsFilters.size(); i++) {
			if (getIsSelected().get(i)) {
				value += optionsFilters.get(i).getType();
			}
		}

		if (value > 0) {
			value += 1;
		}

		return value;
	}

}
