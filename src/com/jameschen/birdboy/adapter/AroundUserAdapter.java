package com.jameschen.birdboy.adapter;

import java.text.DecimalFormat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.entity.InvitePlugMember;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.utils.UtilsTime;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class AroundUserAdapter extends ObjectAdapter<InvitedUser> {
	private BaseActivity context;

	public AroundUserAdapter(BaseActivity activity,InviteItemClickListener inviteItemClickListener) {
		super(activity);
		context = activity;
		itemClickListener = inviteItemClickListener;
		initImageDisplayOptions(activity,defaultImageResId=0);
		
	}

	
	public static String getSportTypeStr(int type){
		String sportType = "未知";
		//跑步65 ,自行车 66   瑜伽 67 爬山 68
		switch (type) {
		case 65:
			sportType="跑步";
			break;
case 66:
	sportType="自行车";
			break;
case 67:
	sportType="瑜伽";
	break;
case 68:
	sportType="爬山";
	break;
		default:
			sportType=CoachAdapter.setSportTypeStr(type);
			break;
		}
		return sportType;
	}
	

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		InviteUserEventHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(
					R.layout.around_item, paramViewGroup, false);
			item = new InviteUserEventHoldView();
			item.initChildView(convertView);
			// item.setListener(addCartListener);
			convertView.setTag(item);
		} else {
			item = (InviteUserEventHoldView) convertView.getTag();
		}
		
		InvitedUser invitedUser = getItem(paramInt);
		item.setPosition(paramInt);
		item.setInfo(invitedUser);
		item.setDistVisible(distanceShow);
	

		return convertView;

	}
	
private boolean distanceShow =false;
	public boolean isDistanceShow() {
	return distanceShow;
}


public void setDistanceShow(boolean distanceShow) {
	this.distanceShow = distanceShow;
}


	@Override
	public void recycle() {
		
		super.recycle();

	}
	

	public class AddCartListener implements OnClickListener {

		@Override
		public void onClick(View v) {

		}

	}
	
	public static String getWeekDayStr(int date) {
		if (date==0) {
			return "";
		}
		return dayWeek[date - 1];
	}

	private InviteItemClickListener itemClickListener;
	
	public  enum ClickType{
		FAV,INVITE,SNS_QQ,SNS_WEIBO
	}
	
	public interface InviteItemClickListener{
		void OnItemClick(int position,InvitedUser invitedUser,Drawable head,ClickType clickType);
	}
	
	private final  class InviteUserEventHoldView extends HoldView<InvitedUser> {
		private TextView name, distance,age,appearTime,sign,sportLike;
		private ImageView sex,headPhoto,weibo,qq;
		private View markFavBtn,bindPlugInView,sexAgeContainer;
		private Button inviteBtn;
		private InvitedUser invitedUser;

		public InvitedUser getObjectInfo() {
			return invitedUser;
		}


		public void setVisible(int Flag) {

		}

		public void setDistVisible(boolean distVisible) {
			if (!distVisible) {
				((View)distance.getParent()).setVisibility(View.GONE);
			} else {
				((View)distance.getParent()).setVisibility(View.VISIBLE);
				distance.setText(getDistance());
			}
		}

		public void initChildView(View convertView) {

			name = (TextView) convertView.findViewById(R.id.around_name);
			sign = (TextView) convertView
					.findViewById(R.id.sign);
			appearTime=(TextView)convertView.findViewById(R.id.time);
			distance = (TextView) convertView
					.findViewById(R.id.distance);
			age = (TextView) convertView.findViewById(R.id.age);
			sportLike = (TextView) convertView.findViewById(R.id.sport_like);
			headPhoto =(ImageView)convertView.findViewById(R.id.around_logo);
			markFavBtn =  convertView.findViewById(R.id.mark_fav);
			sex = (ImageView) convertView.findViewById(R.id.sex_flag);
			weibo = (ImageView) convertView.findViewById(R.id.weibo_img);
			qq = (ImageView) convertView.findViewById(R.id.tencent_img);
			sexAgeContainer = convertView.findViewById(R.id.sex_age_container);
			inviteBtn = (Button) convertView.findViewById(R.id.invite_btn);
			bindPlugInView = convertView.findViewById(R.id.plugin_container);
			qq.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					itemClickListener.OnItemClick(position, invitedUser,headPhoto.getDrawable() ,ClickType.SNS_QQ);
				}
			});
			weibo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					itemClickListener.OnItemClick(position, invitedUser,headPhoto.getDrawable() ,ClickType.SNS_WEIBO);
				}
			});
			markFavBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					itemClickListener.OnItemClick(position, invitedUser,headPhoto.getDrawable() ,ClickType.FAV);
				}
			});
			inviteBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					itemClickListener.OnItemClick(position, invitedUser, headPhoto.getDrawable() ,ClickType.INVITE);
				}
			});
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				
				
			}
		});
		}
		int position=0;
	

		public void setPosition(int position) {
			this.position = position;
		}
		
		private void checkPlugIn(InvitePlugMember sns) {
			if (sns!=null) {
				if (sns.getMembers()!=null) {

					bindPlugInView.setVisibility(View.VISIBLE);
					qq.setVisibility(View.GONE);
					weibo.setVisibility(View.GONE);
					if (sns.getMembers().getQq()!=null) {
						qq.setVisibility(View.VISIBLE);
					}
					
					if (sns.getMembers().getWeibo()!=null) {
						weibo.setVisibility(View.VISIBLE);
					}
					
				
				}else {
					bindPlugInView.setVisibility(View.GONE);
				}
				
			}else {

				bindPlugInView.setVisibility(View.GONE);
			}
		}
		
		@Override
		public void setInfo(InvitedUser object) {
			// TODO Auto-generated method stub
			this.invitedUser = (InvitedUser) object;
			name.setText(invitedUser.getNick());
			if (TextUtils.isEmpty(invitedUser.getSignstr())) {
				sign.setText(context.getString(R.string.no_sign));
			}else {
				sign.setText(invitedUser.getSignstr());
			}
		
			age.setText(invitedUser.getAge()+"");
			sportLike.setText(getSportTypeStr(invitedUser.getMylike()));
			appearTime.setText(UtilsTime.convertTimeFormartByMillsec(System.currentTimeMillis()-invitedUser.getUptime()));
			//personal 
			int sexRes=R.drawable.boy;
			int defualtIcon = R.drawable.m;
			int sexId=invitedUser.getSex();//default
			
			sexRes=sexId==1?R.drawable.fujin_boy:R.drawable.fujin_girl;
			defualtIcon =sexId==1?R.drawable.m:R.drawable.wm;
			sexAgeContainer.setBackgroundResource(sexId==1?R.drawable.round_bg_blue:R.drawable.round_bg_pink);
			sex.setImageResource(sexRes);
			DisplayImageOptions mOptions = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.showImageOnLoading(defualtIcon)
    		.showImageForEmptyUri(defualtIcon)
    		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(10))
    		.cacheOnDisc(true).build();
			if (!TextUtils.isEmpty(invitedUser.getLogo())) {
				loadImage(HttpConfig.HTTP_BASE_URL+invitedUser.getLogo(), headPhoto,mOptions);
				
			}else {
				headPhoto.setImageResource(defualtIcon);
				//headPhoto.setOnClickListener(null);
			}
			
			headPhoto.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (LogInController.IsLogOn()) {
						Util.go2SeeUserHomePage(context, invitedUser.getId());
					}else {
//					
					}
//					String blogoUrl=invitedUser.getLogo().replace("slogo", "blogo");
//					Util.go2SeeBigPic(context, HttpConfig.HTTP_BASE_URL+blogoUrl);
				}
			});
			
			
			checkPlugIn(invitedUser.getSns());
			
			
		}

		private CharSequence getDistance() {
		
				return getDistanceFromServer(true);

		}

		private CharSequence getDistanceFromServer(boolean getCurrentLocation) {
			DecimalFormat df = null;
			if (!getCurrentLocation) {

				return "定位失败";
			}
			if (invitedUser.getDist()==-1) {
				return "位置未知";
			}
			
			double distance =0;
			try {
			 distance = invitedUser.getDist();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			
			if (distance >= 1000) {// show in km
				df = new DecimalFormat(".0");
				return df.format(distance / 1000) + "km";
			} else {
				return (int) distance + "m";
			}
		}

	}

	private static String dayWeek[] = new String[] { "周一", "周二", "周三", "周四",
			"周五", "周六", "周日" };
}
