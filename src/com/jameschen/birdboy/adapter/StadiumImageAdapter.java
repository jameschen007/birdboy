package com.jameschen.birdboy.adapter;

import java.util.ArrayList;
import java.util.List;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.ObjectAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.entity.StadiumPhoto;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
public class StadiumImageAdapter extends ObjectAdapter<StadiumPhoto> {

	private ArrayList<StadiumPhoto> mStadiumPhotos = new ArrayList<StadiumPhoto>();

	private Context context;

	private int layoutId = R.layout.image_small_item;

	public StadiumImageAdapter(BaseActivity context) {
		super(context);
		this.context = context;
		initImageDisplayOptions(context,10);
		this.context = context;
	}

	public StadiumImageAdapter(BaseActivity context, int layoutId) {
		this(context);
		this.layoutId = layoutId;
	}

	public void setImgResources(List<StadiumPhoto> stadiumPhotos) {

		this.mStadiumPhotos = (ArrayList<StadiumPhoto>) stadiumPhotos;
		notifyDataSetChanged();
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mStadiumPhotos.size();
	}

	@Override
	public StadiumPhoto getItem(int position) {
		// TODO Auto-generated method stub
		return mStadiumPhotos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		switch (layoutId) {
		case R.layout.image_small_item: {
			MyHolder holder;
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = (ViewGroup) inflater.inflate(layoutId,
						parent, false);
				holder = new MyHolder();
				holder.img = (ImageView) convertView.findViewById(R.id.imageView);
				convertView.setTag(holder);
			} else {
				holder = (MyHolder) convertView.getTag();
			}
			if ( getItem(position).getSurl()==null) {
				holder.img.setImageResource(R.drawable.icon_default);
			}else {
				loadImage(HttpConfig.HTTP_BASE_URL +getItem(position).getSurl(), holder.img);
			}
			//setItemResource(holder, mBitmaps.get(position));
		}
			break;

	
		default:

			break;
		}

		return convertView;
	}

	

	
	static final class MyMenuHolder {

		public TextView name;

	}

	static final class MyHolder {

		public TextView name;
		public ImageView img;

	}
}
