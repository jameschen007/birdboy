package com.jameschen.birdboy.adapter;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.CategoryAdapter.ViewHolder;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.utils.Util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

/***
 * 数据源
 * 
 * @author Administrator
 * 
 */
public class FeedbackExpandableAdapter extends BaseExpandableListAdapter {
	private Context context;
	private LayoutInflater inflater;
	//4网站建议 1购买功能2账号问题3搜索问题      
	int feedbackTypes[] = new int[] {4,1,2,3,5};
	String feedbackTitles[] = new String[] { "功能建议", "场馆、教练信息纠错", "问题咨询",
			"账号问题", "其它" };

	public FeedbackExpandableAdapter(Context context,FeedbackCommitListener feedbackCommitListener) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.mCommitListener =feedbackCommitListener;
	}

	// 返回父列表个数
	@Override
	public int getGroupCount() {
		return 5;
	}

	// 返回子列表个数
	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {

		return feedbackTitles[groupPosition];
	}

	@Override
	public Integer getChild(int groupPosition, int childPosition) {
		return feedbackTypes[groupPosition];
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {

		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		GroupHolder groupHolder = null;
		if (convertView == null) {
			groupHolder = new GroupHolder();
			convertView = inflater.inflate(R.layout.feedback_group_item,
					parent, false);
			groupHolder.item = (TextView) convertView
					.findViewById(R.id.feedback_title);
			groupHolder.imageView = (ImageView) convertView
					.findViewById(R.id.group_tag);
			convertView.setTag(groupHolder);
		} else {
			groupHolder = (GroupHolder) convertView.getTag();
		}

		groupHolder.item.setText(getGroup(groupPosition).toString());

		if (isExpanded)// ture is Expanded or false is not isExpanded
		{
			convertView.findViewById(R.id.feedback_container).setBackgroundResource(R.drawable.list_center_selector_pressed);
			groupHolder.imageView.setImageResource(R.drawable.flag_up);
		} else {
			groupHolder.imageView.setImageResource(R.drawable.flag_down);
			convertView.findViewById(R.id.feedback_container).setBackgroundResource(R.drawable.list_center_selector);
		}
		return convertView;
	}

	final class GroupHolder {
		public ImageView imageView;
		TextView item;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.feedback_item, parent,
					false);
		}
			final EditText content = (EditText)
				 convertView.findViewById(R.id.feedback_content);
		 Button commit = (Button)
		 convertView.findViewById(R.id.btn_commit);
		 final int position =groupPosition;
		commit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCommitListener.done(content.getText().toString(), getChild(position, 0));
			}
		});
		return convertView;
	}

private FeedbackCommitListener mCommitListener;
	public interface FeedbackCommitListener{
		void done(String content,int type);
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}