package com.jameschen.birdboy.adapter;

import java.text.DecimalFormat;
import java.util.List;

import android.R.integer;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.HttpClientStack;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SportEventDetailActivity;
import com.jameschen.birdboy.adapter.CategoryAdapter.ViewHolder;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.environment.ConstValues.IntentAction;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.entity.ActivityInfo;
import com.jameschen.birdboy.model.entity.ActivityInfo;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;

public class MyActivityInfoAdapter extends ObjectAdapter<ActivityInfo> {
	private Context context;

	public MyActivityInfoAdapter(BaseActivity activity) {
		super(activity);
		context = activity;
	}

	int type;

	public interface CommitActivityListener {
		void done(int type, int id);
	}

	private CommitActivityListener commitActivityListener;

	public void setType(int type, CommitActivityListener commitActivityListener) {
		this.type = type;
		this.commitActivityListener = commitActivityListener;
	}

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		ActivityInfoHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(
					R.layout.my_activity_item, paramViewGroup, false);
			item = new ActivityInfoHoldView();
			item.initChildView(convertView);
			item.setListener(commitActivityListener);
			convertView.setTag(item);
			item.setType(type);
		} else {
			item = (ActivityInfoHoldView) convertView.getTag();
		}

		ActivityInfo activityInfo = getItem(paramInt);
		item.setInfo(activityInfo);

		return convertView;

	}

	@Override
	public void recycle() {

	}

	private final static class ActivityInfoHoldView extends
			HoldView<ActivityInfo> {
		private TextView name;
		private TextView status, comment, num;
		private int type;
		private ImageView delete,split;
		private ActivityInfo activityInfo;

		public ActivityInfo getObjectInfo() {
			return activityInfo;
		}

		private Context context;

		public void setVisible(int Flag) {

		}

		private View extraLayout;

		public void initChildView(View convertView) {
			context = convertView.getContext();

			name = (TextView) convertView
					.findViewById(R.id.my_activity_item_name);

			num = (TextView) convertView
					.findViewById(R.id.my_activity_memer_num);
			
			comment = (TextView) convertView
					.findViewById(R.id.my_activity_comment_num);
			status = (TextView) convertView
					.findViewById(R.id.my_activity_status_btn);
			extraLayout = convertView
					.findViewById(R.id.my_activity_extra_container);
			delete = (ImageView) convertView
					.findViewById(R.id.delete_btn);
			split = (ImageView) convertView
					.findViewById(R.id.item_split);
		
		}

		public void setType(int type) {
			this.type = type;

		}

		@Override
		public void setInfo(ActivityInfo object) {
			// TODO Auto-generated method stub
			this.activityInfo = (ActivityInfo) object;
			name.setText(activityInfo.getUname());
			num.setText(activityInfo.getNownum() + "/" + activityInfo.getNum()
					+ "");
			comment.setText(activityInfo.getCommentCount()+"");
			extraLayout.setVisibility(View.GONE);
			delete.setVisibility(View.GONE);
			extraLayout.setVisibility(View.VISIBLE);
			split.setVisibility(View.VISIBLE);
			if (type == 1) {// create..
				
			} else if(type==2) {//keep 
					delete.setVisibility(View.VISIBLE);
			}else if (type==0) {
				split.setVisibility(View.GONE);
				((View)status.getParent()).setVisibility(View.INVISIBLE);
			}
			// comment.setText(activityInfo.getNownum()/activityInfo.getNum()+"");
			// fav.setText();
			attchReourceToView();
		}

		private void attchReourceToView() {
			status.setCompoundDrawablePadding(Util.getPixByDPI(context, 2));
			status.setText(titles[type]);
			Drawable drawable = status.getResources().getDrawable(
					Resources[type]);
			drawable.setBounds(0, 0, drawable.getMinimumWidth(),
					drawable.getMinimumHeight());// 必须设置图片大小，否则不显示
			status.setCompoundDrawables(drawable, null, null, null);

		}

		public void setListener(final CommitActivityListener listener) {
			((View) status.getParent())
					.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// cancel or do other thing..
							if (listener != null) {
//								int opType = s;
								if (type == 0) {
									Util.CommitDialog(context,"取消报名",
											"请注意，如果频繁放弃活动可能会导致被封号" ,new Dialog.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													listener.done(UNJIONACTION, activityInfo.getId());
												}
												
											});
									
								}else if (type == 1) {
									Toast.makeText(context, "暂时不支持截至报名!", Toast.LENGTH_SHORT).show();
								}else if(type==2) {
									listener.done(JIONACTION, activityInfo.getId());
								}
//								listener.done(opType, activityInfo.getId());
							}
						}
					});
			delete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// cancel or do other thing..
					if (listener != null) {
						int opType = UNKEEP;
						listener.done(opType, activityInfo.getId());
					
						
					}
				}
			});
		}

	}

	public static final int UNKEEP = 1;
	public static final int UNJIONACTION = 2;
	public static final int JIONACTION = 3;

	static String titles[] = new String[] { "取消报名", "终止报名", "报名" };
	static int Resources[] = new int[] { R.drawable.cancel_event,
			R.drawable.stop_sport_event, R.drawable.join_icon };

	public void removeActivtyById(int id) {
		List<ActivityInfo> infos = getObjectInfos();
		if (infos == null) {
			return;
		}
		for (ActivityInfo activityInfo : infos) {
			if (activityInfo.getId() == id) {
				infos.remove(activityInfo);
				break;
			}

		}

	}

}
