package com.jameschen.birdboy.adapter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.MapActivity;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SportEventCreate0Activity;
import com.jameschen.birdboy.SportEventCreate1Activity;
import com.jameschen.birdboy.adapter.CategoryAdapter.ViewHolder;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.QueryBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.utils.AreaSelectPopupWindow;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MNestListView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/***
 * 数据源
 * 
 * @author Administrator
 * 
 */
public class SportEventCreateExpandableAdapter extends BaseExpandableListAdapter {
	private BaseActivity context;
	private LayoutInflater inflater;
	//4网站建议 1购买功能2账号问题3搜索问题      
	int createTypes[] = new int[] {123};
	String createTitles[] = new String[] { "地图选择活动地点", "上传当前你所在的位置","选择附近的场馆",};

	public SportEventCreateExpandableAdapter(BaseActivity context,CommitListener feedbackCommitListener) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.mCommitListener =feedbackCommitListener;
	}

	public CommitListener getCommitListener() {
		return mCommitListener;
	}
	
	
	private int sportType = -1;
	public void setSportType(int sportType) {
		this.sportType = sportType;
	}
	// 返回父列表个数
	@Override
	public int getGroupCount() {
		return 3;
	}

	// 返回子列表个数
	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {

		return createTitles[groupPosition];
	}

	@Override
	public Integer getChild(int groupPosition, int childPosition) {
		return createTypes[groupPosition];
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {

		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		GroupHolder groupHolder = null;
		if (convertView == null) {
			groupHolder = new GroupHolder();
			convertView = inflater.inflate(R.layout.sportevent_create_group_item,
					parent, false);
			groupHolder.item = (TextView) convertView
					.findViewById(R.id.sportevent_create_title);
			groupHolder.imageView = (ImageView) convertView
					.findViewById(R.id.group_tag);
			convertView.setTag(groupHolder);
		} else {
			groupHolder = (GroupHolder) convertView.getTag();
		}

		attchReourceToView(groupHolder,groupPosition);
		if (groupPosition==0||groupPosition==1) {//first one
			if (groupPosition==0) {
				groupHolder.imageView.setImageResource(R.drawable.enter_gray);
			}else {
				groupHolder.imageView.setImageResource(R.drawable.flag_down);
			}
			
			convertView.findViewById(R.id.sportevent_create_container).setBackgroundResource(R.drawable.list_center_selector);
		}else {
			if (isExpanded)// ture is Expanded or false is not isExpanded
			{
				convertView.findViewById(R.id.sportevent_create_container).setBackgroundResource(R.drawable.list_center_selector_pressed);
				groupHolder.imageView.setImageResource(R.drawable.flag_down);
			} else {
				groupHolder.imageView.setImageResource(R.drawable.flag_down);
				convertView.findViewById(R.id.sportevent_create_container).setBackgroundResource(R.drawable.list_center_selector);
			}
		}
	
		return convertView;
	}
	 static int Resources[]=new int[]{R.drawable.map_choose_icon,R.drawable.location_foucs,R.drawable.stadium_blue_icon};
	private void attchReourceToView(GroupHolder groupHolder,int position) {

		
		groupHolder.item.setCompoundDrawablePadding(Util.getPixByDPI(context, 2));
		groupHolder.item.setText(getGroup(position).toString());
		groupHolder.item.setPadding(Util.getPixByDPI(context, 5), 0, 0, 0);
		Drawable drawable =groupHolder.item.getResources().getDrawable(Resources[position]);
		 drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());//必须设置图片大小，否则不显示
		 groupHolder.item.setCompoundDrawables(drawable, null,null, null);
	}
	
	final class GroupHolder {
		public ImageView imageView;
		TextView item;
	}
@Override
public int getChildType(int groupPosition, int childPosition) {
	// TODO Auto-generated method stub
	return groupPosition;
}
@Override
public int getChildTypeCount() {
	// TODO Auto-generated method stub
	return 3;
}
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		if (convertView==null) {
			switch (groupPosition) {
			case 2:
			
				convertView=new View(context);
				break;
			case 1:
				convertView = inflater.inflate(R.layout.sportevent_create_location_item1, parent,
						false);
				convertView = getLocationView(convertView);
				break;
			case 0:
				convertView=new View(context);
//				convertView = inflater.inflate(R.layout.sportevent_create_location_item, parent,
//						false);
				break;
			default:
				break;
			}
			
		}else {
			switch (groupPosition) {
			case 0:
				convertView=new View(context);
				//convertView =getAreaView(convertView, areaList);
				break;
			case 1:
				convertView =getLocationView(convertView);
				break;

			default:
				break;
			}
		}
		
	
	

		
		return convertView;
	}

	private View getLocationView(View convertView) {
	TextView location = (TextView) convertView.findViewById(R.id.location);
	final LocationSupport locationSupport = LocationSupport
			.getLocationSupportInstance(convertView.getContext());
	final 	float lat = locationSupport.getLatitude();
	final  	float lng = locationSupport.getLongtitude();
	if (lat <= 0 || lng <= 0||locationSupport.getLocation()==null) {
		location.setText("无法获取当前位置！");
		return convertView;
	}
	location.setText("你当前位置:"+locationSupport.getLocation());
	convertView.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Util.CommitDialog(context,"坐标定位","确认选择此处作为活动地点吗?"
					,new Dialog.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Stadium stadium = 	new Stadium();
					stadium.setPosition(lng+" "+lat);
					String location = locationSupport.getLocation();
					if (!TextUtils.isEmpty(location)) {
						stadium.setPositiondesc(location);
					}
					
					if (mCommitListener!=null) {
						mCommitListener.done(stadium);
					}
				}
			});
			
			
		}
	});
	return convertView;
	}
private CommitListener mCommitListener;
	public interface CommitListener{
		void done(Stadium stadium);
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	private List<Area> areaList=new ArrayList<Area>();
	public void setAreas(List<Area> areas) {
			notifyDataSetInvalidated();
			areaList.clear();
			areaList.addAll(areas);
			notifyDataSetChanged();
	}
	private ProgressBar loadingbar;
	private Button loadingRefresh;
	private MNestListView childListView;
	private void showLoadingFinished() {
		childListView.setVisibility(View.VISIBLE);
		loadingbar.setVisibility(View.GONE);
		loadingRefresh.setVisibility(View.GONE);
	}
	private void showNodata() {
		loadingRefresh.setVisibility(View.VISIBLE);
		loadingbar.setVisibility(View.GONE);
	}
	private void beginLoadingData() {
		loadingbar.setVisibility(View.VISIBLE);
		loadingRefresh.setVisibility(View.GONE);
		childListView.setVisibility(View.GONE);
	}
	public void showChildArea() {
		if (childListView.getAdapter()==null) {
			beginLoadingData();
			return;
		}
		if (childListView.getAdapter().getCount()>0) {
			showLoadingFinished();
		}else {
			showNodata();
		}
		
		
	}
	private TextView area;
	
	
}