package com.jameschen.birdboy.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jameschen.birdboy.ImageDetailActivity;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.AroundUserAdapter.ClickType;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.entity.FavoriteFriend;
import com.jameschen.birdboy.utils.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class MyFriendAdapter extends ObjectAdapter<FavoriteFriend> {
	private BaseActivity context;

	public MyFriendAdapter(BaseActivity activity,InviteItemClickListener inviteItemClickListener) {
		super(activity);
		context = activity;
		itemClickListener = inviteItemClickListener;
		initImageDisplayOptions(activity,defaultImageResId=0);
		
	}

	

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		InviteUserEventHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(
					R.layout.my_friend_item, paramViewGroup, false);
			item = new InviteUserEventHoldView();
			item.initChildView(convertView);
			// item.setListener(addCartListener);
			convertView.setTag(item);
		} else {
			item = (InviteUserEventHoldView) convertView.getTag();
		}
		
		FavoriteFriend invitedUser = getItem(paramInt);
		item.setPosition(paramInt);
		item.setInfo(invitedUser);
	

		return convertView;

	}
	
private boolean distanceShow =false;
	public boolean isDistanceShow() {
	return distanceShow;
}


public void setDistanceShow(boolean distanceShow) {
	this.distanceShow = distanceShow;
}


	@Override
	public void recycle() {
		
		super.recycle();

	}

	
	private InviteItemClickListener itemClickListener;
	
	
	public interface InviteItemClickListener{
		void OnItemClick(int position,FavoriteFriend favoriteFriend,ClickType clickType);
	}
	
	private final  class InviteUserEventHoldView extends HoldView<FavoriteFriend> {
		private TextView name, inviteBtn ,cancelFav;
		private ImageView headPhoto;
		
		private FavoriteFriend invitedUser;

		public FavoriteFriend getObjectInfo() {
			return invitedUser;
		}


		public void setVisible(int Flag) {

		}


		public void initChildView(View convertView) {

			name = (TextView) convertView.findViewById(R.id.user_name);
			headPhoto =(ImageView)convertView.findViewById(R.id.around_logo);
			cancelFav =  (TextView) convertView.findViewById(R.id.cancel_fav);
			inviteBtn = (TextView) convertView.findViewById(R.id.invite);
			
			cancelFav.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					itemClickListener.OnItemClick(position, invitedUser, ClickType.FAV);
				}
			});
			inviteBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					itemClickListener.OnItemClick(position, invitedUser, ClickType.INVITE);
				}
			});
		}
		int position=0;
	

		public void setPosition(int position) {
			this.position = position;
		}

		@Override
		public void setInfo(FavoriteFriend object) {
			// TODO Auto-generated method stub
			this.invitedUser = (FavoriteFriend) object;
			name.setText(invitedUser.getUsername());
		//personal 
			int defualtIcon = R.drawable.m;
			int sexId=invitedUser.getSex();//default
			
			defualtIcon =sexId==1?R.drawable.m:R.drawable.wm;
			DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
			.showImageOnLoading(defualtIcon)
    		.showImageForEmptyUri(defualtIcon).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
    		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(10))
    		.cacheOnDisc(true).build();
			if (!TextUtils.isEmpty(invitedUser.getLogo())) {
				loadImage(HttpConfig.HTTP_BASE_URL+invitedUser.getLogo(), headPhoto,mOptions);

			}else {
				headPhoto.setImageResource(defualtIcon);
				headPhoto.setOnClickListener(null);
			}
			name.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Util.go2SeeUserHomePage(context, invitedUser.getUserid());
				}
			} );
			headPhoto.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Util.go2SeeUserHomePage(context, invitedUser.getUserid());
				}
			}
					);		

		}


	}

}
