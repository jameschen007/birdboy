package com.jameschen.birdboy.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.utils.Util;

public class ChildAreaAdapter extends ArrayAdapter<ChildArea> {
	private Context context;
	public ChildAreaAdapter(Context context, 	final List<ChildArea> objects) {
		super(context, -1, objects);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView != null) {
			holder = (ViewHolder) convertView.getTag();
			attchReourceToView(holder, getItem(position));
			return convertView;
		}
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = (ViewGroup) inflater.inflate(
				R.layout.childarea_item, parent, false);
		holder = new ViewHolder();
		holder.item = (TextView) convertView.findViewById(R.id.item_text1);
		holder.count = (TextView) convertView.findViewById(R.id.item_count);
		convertView.setTag(holder);
		attchReourceToView(holder, getItem(position));
		return convertView;
	}

	private void attchReourceToView(ViewHolder holder, ChildArea item) {
		holder.item.setText(item.getName());
		holder.count.setText(item.getCount()+"");
	}

	final class ViewHolder {
		TextView item;
		TextView count;
	}

}