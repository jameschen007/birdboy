package com.jameschen.birdboy.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jameschen.birdboy.ImageDetailActivity;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.SportEventDetailActivity;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.entity.SportUser;
import com.jameschen.birdboy.utils.Util;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class HeadAdapter extends ObjectAdapter<SportUser> {
	private BaseActivity context;
	public HeadAdapter(BaseActivity activity) {
		super(activity);
		this.context = activity;
		initImageDisplayOptions(activity,defaultImageResId=0);
	}

	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView != null) {
			holder = (ViewHolder) convertView.getTag();
			attchReourceToView(holder, getItem(position));
			return convertView;
		}
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = (ViewGroup) inflater.inflate(R.layout.member_item,
				parent, false);
		holder = new ViewHolder();
		holder.logo = (ImageView) convertView.findViewById(R.id.memberPhoto);

		convertView.setTag(holder);
		attchReourceToView(holder, getItem(position));
		return convertView;
	}

	public  void attchReourceToView(ViewHolder holder, final SportUser item) {
		
			 int defualtIcon = item.getSex()==2?R.drawable.icon_default:R.drawable.icon_default;
			DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
			.showImageOnLoading(defualtIcon)
    		.showImageForEmptyUri(defualtIcon).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
    		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(10))
    		.cacheOnDisc(true).build();
			if (!TextUtils.isEmpty(item.getLogo())) {
				
					loadImage(HttpConfig.HTTP_BASE_URL+item.getLogo(), holder.logo,mOptions);
					holder.logo.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							Util.go2SeeUserHomePage(context, item.getId());
						}
					});
				
			
			}else {
				 holder.logo.setImageResource(defualtIcon);
				 holder.logo.setOnClickListener(null);
			}
		
		
	}
	

	public int getInviedCount() {
		return super.getCount()-1;
	}
	
	
	
	final class ViewHolder {
		ImageView logo;
	}
	
	

	

}