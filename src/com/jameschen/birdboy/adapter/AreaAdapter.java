package com.jameschen.birdboy.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.model.entity.Area;

public class AreaAdapter extends ObjectAdapter<Area> {
	private Context context;

	public AreaAdapter(BaseActivity activity) {
		super(activity);
		context = activity;
		initImageDisplayOptions(activity);
		// TODO Auto-generated constructor stub
	}

	public AreaAdapter(BaseActivity activity, List<Area> list) {
		super(activity, ImageLoad.NONE);
		context = activity;
		addObjectList(list);
		notifyDataSetChanged();
	}

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		AreaHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(R.layout.area_item,
					paramViewGroup, false);
			item = new AreaHoldView();
			item.initChildView(convertView);
			convertView.setTag(item);
		} else {
			item = (AreaHoldView) convertView.getTag();
		}

		Area area = getItem(paramInt);
		item.setInfo(area);
		if (position == paramInt) {
			convertView.setBackgroundColor(context.getResources().getColor(
					R.color.area_item_select));
		}else {
			convertView.setBackgroundResource(R.drawable.menu_selector);
		}
		return convertView;

	}

	@Override
	public void recycle() {
		// TODO Auto-generated method stub
		super.recycle();

	}

	public static final int SELL_OUT = 1;

	public class AddCartListener implements OnClickListener {

		@Override
		public void onClick(View v) {

		}

	}

	private final static class AreaHoldView extends HoldView<Area> {

		private TextView name,count;
		private Area area;

		public Area getObjectInfo() {
			return area;
		}

		private Context context;

		public void setVisible(int Flag) {

		}

		public void initChildView(View convertView) {
			context = convertView.getContext();

			name = (TextView) convertView.findViewById(R.id.area_item_name);
			count = (TextView) convertView.findViewById(R.id.area_item_count);

		}

		@Override
		public void setInfo(Area object) {
			// TODO Auto-generated method stub
			this.area = (Area) object;
			name.setText(area.getName());
			if (area.getId()==-1) {
				count.setText("  "+area.getCount()+"  ");
			}else {
				count.setText("  "+area.getCount()+"  >  ");
			}
		

		}

	}

	private int position = 0;

	public void setCurrentSelection(int position) {
		// TODO Auto-generated method stub
		this.position = position;
		notifyDataSetChanged();
	}

}
