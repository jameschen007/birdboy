package com.jameschen.birdboy.adapter;

import java.text.DecimalFormat;

import android.R.integer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.IntentAction;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;

public class StadiumAdapter extends ObjectAdapter<Stadium> {
	private Context context;

	public StadiumAdapter(BaseActivity activity) {
		super(activity);
		context = activity;
		initImageDisplayOptions(activity,10);
	}

	@Override
	public View getView(int paramInt, View convertView, ViewGroup paramViewGroup) {
		StadiumHoldView item = null; // TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(R.layout.stadium_item,
					paramViewGroup, false);
			item = new StadiumHoldView();
			item.initChildView(convertView);
			convertView.setTag(item);
		} else {
			item = (StadiumHoldView) convertView.getTag();
		}

		Stadium stadium = getItem(paramInt);
		item.setInfo(stadium);
		item.setDistVisible(showDistance);
		if ( stadium.getLogo()==null) {
			item.icon.setImageResource(R.drawable.icon_default);
		}else {
			loadImage(HttpConfig.HTTP_BASE_URL +stadium.getLogo(), item.icon);
		}

		return convertView;

	}

	@Override
	public void recycle() {
	
		super.recycle();

	}

	private final static class StadiumHoldView extends HoldView<Stadium> {
		private TextView name;
		private ImageView icon;
		private TextView distance;
		private TextView score;
		private Stadium stadium;
		private TextView address;
		
		private ImageView stop,postcard,rest,wash,
		exercise,rent,sale,product,fix,box;
		
		
		public Stadium getObjectInfo() {
			return stadium;
		}

		private Context context;
		private ImageView split;
		public void setDistVisible(boolean distVisible){
			if (!distVisible) {
				split.setVisibility(View.GONE);
				distance.setVisibility(View.GONE);
			}else {
				split.setVisibility(View.VISIBLE);
				distance.setVisibility(View.VISIBLE);
				distance.setText(getDistance());
			}
		}
		
		public void setVisible(int Flag) {

		}

		public void initChildView(View convertView) {
			context = convertView.getContext();

			name = (TextView) convertView.findViewById(R.id.stadium_item_name);
			distance = (TextView) convertView.findViewById(R.id.stadium_item_distance);
			address = (TextView) convertView.findViewById(R.id.stadium_item_address);
			score = (TextView) convertView.findViewById(R.id.stadium_item_score);
			icon = (ImageView) convertView.findViewById(R.id.stadium_item_portrait);
			split = (ImageView) convertView.findViewById(R.id.item_split);	
			
			stop = (ImageView) convertView.findViewById(R.id.stadium_item_stop_icon);
			postcard = (ImageView) convertView.findViewById(R.id.stadium_item_postcard_icon);
			rest = (ImageView) convertView.findViewById(R.id.stadium_item_rest_icon);
			wash = (ImageView) convertView.findViewById(R.id.stadium_item_wash_icon);
			exercise = (ImageView) convertView.findViewById(R.id.stadium_item_exercise_icon);
			rent = (ImageView) convertView.findViewById(R.id.stadium_item_rent_icon);
			sale = (ImageView) convertView.findViewById(R.id.stadium_item_sale_icon);
			product = (ImageView) convertView.findViewById(R.id.stadium_item_product_icon);
			fix = (ImageView) convertView.findViewById(R.id.stadium_item_fix_icon);
			box = (ImageView) convertView.findViewById(R.id.stadium_item_box_icon);
			
			//not support currently
			box.setVisibility(View.GONE);
			sale.setVisibility(View.GONE);
			fix.setVisibility(View.GONE);
		}

		
		
		@Override
		public void setInfo(Stadium stadium) {
			// TODO Auto-generated method stub
			this.stadium = stadium;
			name.setText(stadium.getName());			
		
			score.setText("评分:"+stadium.getScores());
			address.setText("详细地址:"+stadium.getPositionDesc());
			int option =stadium.getOptions();
			stop.setVisibility(option/2==0?View.GONE:View.VISIBLE);
			postcard.setVisibility(option/4==0?View.GONE:View.VISIBLE);
			rest.setVisibility(option/8==0?View.GONE:View.VISIBLE);
			wash.setVisibility(option/16==0?View.GONE:View.VISIBLE);
			exercise.setVisibility(option/32==0?View.GONE:View.VISIBLE);
			rent.setVisibility(option/64==0?View.GONE:View.VISIBLE);
			product.setVisibility(option/128==0?View.GONE:View.VISIBLE);
		}

		private  CharSequence getDistance() {
				return getDistanceFromServer(true);
		}
		private  CharSequence getDistanceFromServer(boolean getCurrentLocation){
			DecimalFormat df=null;
			if (!getCurrentLocation) {
				
				return "定位失败";
			}
			double distance =stadium.getDist();
			if (distance>=1000) {//show in km
				df = new DecimalFormat(".0"); 
				return df.format(distance/1000)+"km";
			}else {
				return (int)distance + "m";
			}
		}

	
	}

}
