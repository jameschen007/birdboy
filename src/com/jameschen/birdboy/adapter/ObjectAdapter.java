package com.jameschen.birdboy.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.Worker.MyWorkerable;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.utils.Log;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public abstract class ObjectAdapter<T> extends BaseAdapter implements
		MyWorkerable ,NotifyDataChangedByReqListener{
	private Worker mObjectWorker;
	/**
	 * Lock used to modify the content of mObjects. Any write operation
	 * performed on the array should be synchronized on this lock. This lock is
	 * also used by the filter (see getFilter() to make a synchronized copy of
	 * the original array of data.
	 * */
	private final Object mLock = new Object();

	private ArrayList<T> mObjectInfos = new ArrayList<T>();

	public List<T> getObjectInfos() {
		return mObjectInfos;
	}

	protected boolean showDistance=true;
	
	public boolean isShowDistance() {
		return showDistance;
	}
	public void setShowDistance(boolean showDistance) {
		this.showDistance = showDistance;
	}

	private Page  page;
	
//	protected float lat, lng;	
//	public void refreshCurrentPosition(float lat , float lng) {
//		// TODO Auto-generated method stub
//		this.lat = lat;
//		this.lng =lng;
//
//	}
	
	public void setCurrentPage(Page page) {
		this.page = page;	
		needShowDistance();
	}
	@Override
	public void notifydataChanged(Page page) {
		setCurrentPage(page);
		notifyDataSetChanged();
	}
	
	private void needShowDistance( ) {
		if (page!=null&&ConstValues.DISTANCE.equals(page.getTag())) {
			showDistance=true;
		}else {
			showDistance=false;
		}
	}
	
	boolean isRecycle = false;

	public void recycle() {
		isRecycle = true;
		// exit the activity.....
		mObjectInfos.clear();

	}
	

	
	public interface  ImageLoad{
		public static final int NONE=-1;
	}

	/**
	 * @param context
	 * 
	 *            for ObjectmangeMent
	 */
	public ObjectAdapter(BaseActivity activity) {
		
		this(activity, R.drawable.translant);
	}
 
	public ObjectAdapter(BaseActivity activity,int defaultImage) {
			defaultImageResId=defaultImage;	
	}

    protected int defaultImageResId=ImageLoad.NONE;
	
	protected ImageLoader imageLoader;
	DisplayImageOptions options;

	
	public void loadImage(String url, ImageView imageView) {
		//
	//	imageView.setBackgroundResource(R.drawable.default_img);// setloading	
		if (imageLoader!=null) {
			Log.i("get img rul", url);
			String urls[]=url.split(HttpConfig.HTTP_BASE_URL);
			if (urls.length>=2&&urls[1].startsWith("http")) {
				url = urls[1];
			}
			imageLoader.displayImage(url, imageView, options, animateFirstListener);	
		}
		
			
	}

	public void loadImage(String url, ImageView imageView, DisplayImageOptions mOptions) {
		//
	//	imageView.setBackgroundResource(R.drawable.default_img);// setloading	
		if (imageLoader!=null) {
			Log.i("get img rul", url);
			String urls[]=url.split(HttpConfig.HTTP_BASE_URL);
			if (urls.length>=2&&urls[1].startsWith("http")) {
				url = urls[1];
			}
			imageLoader.displayImage(url, imageView, mOptions, animateFirstListener);	
		}
		
			
	}
	
	public void initImageDisplayOptions(BaseActivity activity) {
		// TODO Auto-generated method stub
		initImageDisplayOptions(activity, 20);
	}
	
	
	public void initImageDisplayOptions(BaseActivity activity,int roundVal) {
		// TODO Auto-generated method stub
		if (defaultImageResId!=ImageLoad.NONE) {
			initImageFetcher(activity,defaultImageResId,roundVal);
		}
	}
	
//    .showImageOnLoading(R.drawable.ic_stub)
//    .showImageForEmptyUri(R.drawable.ic_empty)
//    .showImageOnFail(R.drawable.ic_error)
	private void initImageFetcher(BaseActivity activity, int loadImg,int roundVal) {
			imageLoader =activity.getImageLoader();
	        if (roundVal>0) {
	        	options = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.IN_SAMPLE_INT)
	    		.showImageOnLoading(R.drawable.icon_default)
	    		.showImageForEmptyUri(R.drawable.icon_default)
	    		.showImageOnFail(R.drawable.icon_default).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(roundVal))
	    		.cacheOnDisc(true).build();
			}else {
				
				options = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.IN_SAMPLE_INT)
				.showImageOnLoading(R.drawable.icon_default)
	    		.showImageForEmptyUri(R.drawable.icon_default)
	    		.showImageOnFail(R.drawable.icon_default).considerExifParams(true).cacheInMemory(true) .cacheOnDisc(true).build();
			}
		animateFirstListener = new AnimateFirstDisplayListener();

	}

	private ImageLoadingListener animateFirstListener;

	public static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		static public void clearDisplayImageUris() {
			displayedImages.clear();
		}

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
	public ObjectAdapter() {

	}

	
	public void onResume() {
		notifyDataSetChanged();
	}

	public void onPause() {

	}

	public void setImageFetcherPauseWork(boolean pauseWork) {
		if (imageLoader==null) {
			return;
		}
		if (pauseWork) {
			imageLoader.pause();
		} else {
			imageLoader.resume();
		}
		
	}
	public void clear() {
		clear(false);
	}

	public void clear(boolean fresh) {
		synchronized (mLock) {
			mObjectInfos.clear();
		}
		if (fresh) {
			notifyDataSetInvalidated();
		}

	}

	/**
	 * remove the adapter index object from arraylist.
	 * 
	 * @param index
	 */
	public void remove(int index) {
		synchronized (mLock) {
			mObjectInfos.remove(index);
		}
	}

	public void remove(T object) {
		synchronized (mLock) {
			mObjectInfos.remove(object);
		}
	}

	public void addObject(T object) {
		synchronized (mLock) {
			mObjectInfos.add(object);
		}

	}

	
	  public void insert(T object, int index) {
	        synchronized (mLock) {
	        	mObjectInfos.add(index, object);
	        }
	    }
	
	public void setObjectList(List<T> mList) {
		synchronized (mLock) {
			mObjectInfos = (ArrayList<T>) mList;
		}

	}

	public void addObjectList(List<T> mList) {
		synchronized (mLock) {
			mObjectInfos.addAll(mList);
		}

	}

	@Override
	public int getCount() {
		synchronized (mLock) {
			count = mObjectInfos.size();
			return count;
		}
	}

	@Override
	public T getItem(int paramInt) {
		// TODO Auto-generated method stub
		synchronized (mLock) {
			return mObjectInfos.get(paramInt);
		}
	}

	@Override
	public long getItemId(int paramInt) {
		// TODO Auto-generated method stub
		return paramInt;
	}

	@Override
	public Worker getWorker() {
	
		return mObjectWorker;
	}



	int count = 0;

	/**
	 * Observer to get notified when the content observer's data down load
	 * finished
	 */

	public abstract static class HoldView<T> {

		public abstract T getObjectInfo();

		public abstract void setVisible(int Flag);

		public abstract void initChildView(View convertView);

		public abstract void setInfo(T object);

	}



}
