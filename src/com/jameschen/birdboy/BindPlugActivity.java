package com.jameschen.birdboy;

import android.os.Bundle;
import android.widget.ListView;

import com.jameschen.birdboy.adapter.BindAdapter;
import com.jameschen.birdboy.adapter.BindAdapter.BindItemClickListener;
import com.jameschen.birdboy.base.BaseBindActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.model.entity.QQ;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.model.entity.Weibo;
import com.jameschen.plugin.AuthBindController;
import com.jameschen.plugin.BindModel;

public class BindPlugActivity extends BaseBindActivity {
	private ListView bindListView;
	private BindAdapter bindAdapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bind_list);
		setTitle("绑定账号");
		initViews();

	}

	private void initViews() {
		bindListView = (ListView) findViewById(R.id.bind_list_view);
		bindAdapter=new BindAdapter(BindPlugActivity.this, new BindItemClickListener() {
			@Override
			public void OnItemClick(int position, BindModel bindModel) {
				
				if (bindModel.fromtype==BindModel.QQ) {
					doQQAuth(new AuthBindController.OnbindResultListener() {
						
						@Override
						public void bindSucc(BindModel bindModel) {
							// TODO Auto-generated method stub
							//refresh ..
							bindAdapter.getItem(1).token="succ";
							bindAdapter.notifyDataSetChanged();
							//
						}
						
						@Override
						public void bindFailed(String reason) {
							// TODO Auto-generated method stub
							
						}
					});
				}else if (bindModel.fromtype==BindModel.SINA) {
					doWeiboAuth(new AuthBindController.OnbindResultListener() {
						
						@Override
						public void bindSucc(BindModel bindModel) {
							//refresh..
							bindAdapter.getItem(0).token="succ";
							bindAdapter.notifyDataSetChanged();
						}
						
						@Override
						public void bindFailed(String reason) {
							// TODO Auto-generated method stub
							
						}
					});
				}
			}
		});
		//default is null
		SNS sns = LogInController.currentAccount.getSnsObj();
		BindModel qqBindModel = new BindModel();
		qqBindModel.fromtype=BindModel.QQ;
		BindModel sinaBindModel = new BindModel();
		sinaBindModel.fromtype = BindModel.SINA;
		if (sns!=null) {
			if (sns.getQq()!=null) {
				qqBindModel.token = sns.getQq().getToken();
			}
			
			if (sns.getWeibo()!=null) {
				sinaBindModel.token= sns.getWeibo().getToken();
			}
		
		}
		
		bindAdapter.addObject(sinaBindModel);
		bindAdapter.addObject(qqBindModel);
		bindListView.setAdapter(bindAdapter);
		
	}

}
