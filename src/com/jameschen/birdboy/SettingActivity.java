package com.jameschen.birdboy;

import java.lang.reflect.Type;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.Setting;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.User;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebInbox;
import com.jameschen.birdboy.model.entity.Version;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import com.umeng.update.UpdateStatus;

public class SettingActivity extends BaseActivity implements OnClickListener {

	private static final int UPDATE_PASSWORD = 0x11;
	private View modifyPassword,aboutUs;
	private View version;
	private View logout;
	private Version ver;
	private boolean resume=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.system_setting);
		setTopBarRightBtnVisiable(View.GONE);
		initViews(LogInController.IsLogOn());
		setTitle("设置");
		ver = (Version) getIntent().getSerializableExtra("ver");
	}

	private void initViews(boolean isLogged) {
	//	(aboutUs =findViewById(R.id.about_us)).setOnClickListener(this);
		(modifyPassword =findViewById(R.id.rl_modify_password)).setOnClickListener(this);
		(version =findViewById(R.id.rl_version)).setOnClickListener(this);
		(logout =findViewById(R.id.btnExit)).setOnClickListener(this);
		if (isLogged) {			
		}else {
			modifyPassword.setVisibility(View.GONE);
			logout.setVisibility(View.GONE);
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		resume=true;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		resume=false;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {
	
//		case R.id.about_us:{
//			startActivity(new Intent(SettingActivity.this,AboutUsActivity.class));
//		}
//			break;
		case R.id.rl_modify_password:{
			startActivityForResult(new Intent(SettingActivity.this,ModifyPasswordActivity.class),UPDATE_PASSWORD);
		}
			break;
		case R.id.rl_version:{
			showProgressDialog("检查版本", "请稍候",null);
			 UmengUpdateAgent.forceUpdate(this);
			 UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {

					@Override
					public void onUpdateReturned(int updateStatus,UpdateResponse updateInfo) {
						cancelProgressDialog();
				        switch (updateStatus) {
				        case UpdateStatus.No: // has no update
				        if(resume) {
				        		Toast.makeText(SettingActivity.this, "已经是最新版本！", Toast.LENGTH_SHORT).show();
							}
				            break;
				     
				        }
				    						
					}
				});

//				if (ver==null) {
//					int ver =Util.getAppVersion(this);
//					showProgressDialog("检查版本", "请稍候",null);
//					getDataLoader(HttpConfig.REQUST_ACTION_VERSION_URL+ver, false, null, versionController);
//
//				}else {
//					checkVersion(ver);
//				}
		}
			break;
		case R.id.btnExit:
		{
			SharedPreferences user = getSharedPreferences(User.SharedName, 0);
			 user.edit().putBoolean(User.logon, false).commit();
			 user.edit().remove(User.password).commit();
			 //jpush ,,
		getDataLoader(HttpConfig.REQUST_LOGOUT_URL,false,null, logoutController,
					new HttpClientStack(LogInController.getHttpClientInstance()));
			setResult(Setting.RESULT_SETTING_LOGOUT);
			finish();
		}
		break;
		default:
			break;
		}
	}
	
	private UIController versionController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			 ver = (Version) content;
			if (isFinishing()) {
				return;
			}
			cancelProgressDialog();
			checkVersion(ver);
	
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Version>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG + "version";
		}
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

	switch (requestCode) {
	case UPDATE_PASSWORD:
		if (resultCode== RESULT_OK) {
			SharedPreferences user = getSharedPreferences(User.SharedName, 0);
			 user.edit().putBoolean(User.logon, false).commit();
			setResult(Setting.RESULT_SETTING_UPDATE_PASSWORD);
			finish();
		}
		break;

	default:
		break;
	}
	}
	
	protected void checkVersion(Version ver) {
		if (!ver.isIsupdate()) {
			showToast("已经是最新版本!");
			return;
		}
		Util.upgradeVersion(ver,SettingActivity.this);		
	}

	UIController logoutController = new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}
	};
}
