package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.actionbarsherlock.app.ActionBar;
import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Register;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.utils.CheckValid;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.MD5Encode;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.utils.UtilsUI;

public class ModifyPasswordActivity extends BaseActivity implements
		OnClickListener {
	private View commitBtn;
	private EditText newPassword, confirmPassword;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.password_reset);
		setTitle("修改密码");
		Category item0= new Category();
		item0.setName("女");
		item0.setType(2);
		categories.add(item0);
		Category item1= new Category();
		item1.setName("男");
		item1.setType(1);
		categories.add(item1);
		initView();
//		
		if (LogInController.currentAccount.getSex()==2) {
			sex.setTag(item0);
		}else {
			sex.setTag(item1);
		}
		
		sex.setText(LogInController.currentAccount.getSex()==2?"女":"男");
	}

	List<Category> categories = new ArrayList<Category>();
	private TextView sex;
	private void showSex(final List<Category> category) {

		
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		int width =UtilsUI.getWidth(getApplication())-Util.getPixByDPI(getBaseContext(), 20);
		customSelectPopupWindow.showActionWindow((View) (sex.getParent()),this, category);
		customSelectPopupWindow.getPopupWindow().update(width, LayoutParams.WRAP_CONTENT);
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			
			@Override
			public void onItemClick(int index) {
				Category mItem = category.get(index);
				sex.setText(mItem.getName());
				sex.setTag(mItem);
			}
		});
		sex.setSelected(true);
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
				sex.setSelected(false);
			}
		});
	
		
	}

	private void initView() {
		// TODO Auto-generated method stub
		commitBtn = findViewById(R.id.btn_done);
//		oldPassword = (EditText) findViewById(R.id.old_password);
		newPassword = (EditText) findViewById(R.id.edtPasswrod);
		confirmPassword = (EditText) findViewById(R.id.edtconfirmPasswrod);
		sex = (TextView) findViewById(R.id.edtSex);
		sex.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				showSex(categories);
				
			}
		});
		
		commitBtn.setOnClickListener(this);

		confirmPassword.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER&& event.getAction() == KeyEvent.ACTION_UP) {
					commitDone();
					InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

					imm.hideSoftInputFromWindow(
							confirmPassword.getWindowToken(), 0);
				}
				return false;
			}
		});
	}

	protected void commitDone() {
		// TODO Auto-generated method stub
		boolean nextCheckStep =true;
		if (nextCheckStep) {//check old password
			//nextCheckStep =  CheckValid.(this,nickname, 2, 6);
		}
		
		String password = newPassword.getText().toString();
		if (nextCheckStep) {
			if (password == null || password.length() == 0) {
				showToast("请输入新密码!");				
				nextCheckStep = false;
				return ;
		}
			nextCheckStep =  CheckValid.passwordCheckValid(this,password, 6, 15);
		}
		
		String confirmPasswordStr = confirmPassword.getText().toString();
		if (nextCheckStep) {
			if (TextUtils.isEmpty(confirmPasswordStr)) {
				nextCheckStep=false;
				showToast("请再次输入密码!");
				return ;
			}
			if (!password.equals(confirmPasswordStr)) {
				nextCheckStep=false;
				showToast("两次密码输入不一致!");
			}else {
				nextCheckStep=true;
			}
		}
			

		if (nextCheckStep) {
			closeInputMethod();			
			showProgressDialog("修改资料", "正在提交，请稍候...", null);
			Map<String, String> map = getPublicParamRequstMap();
			Category iCategory =(Category) sex.getTag();
			map.put("sex", ""+iCategory.getType());
			map.put("password", MD5Encode.encode(password));
			map.put("gcode", 1234+"");			
			postDataLoader(map,"http://birdboy.cn/updateMeAPI", uiFreshController,new HttpClientStack(LogInController.getHttpClientInstance()));
			

		}
	}

	private UIController uiFreshController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode,Page page) {
			// TODO Auto-generated method stub
	if (errorCode>-1) {
				
			}
			
		}
		
		@Override
		public <E> void refreshUI(E content,Page page) {
				showToast("修改密码成功，请重新输入密码登陆");
				setResult(RESULT_OK);
				finish();
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return  new TypeToken<WebBirdboyJsonResponseContent<Object> >(){}.getType();
		}
	};

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.btn_done: {
			commitDone();
		}
			break;

		default:
			break;
		}
	}


}
