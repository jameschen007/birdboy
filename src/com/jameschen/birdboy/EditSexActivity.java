package com.jameschen.birdboy;

import java.util.Map;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.toolbox.HttpClientStack;
import com.jameschen.birdboy.base.BaseEditAccountActivity;
import com.jameschen.birdboy.common.LogInController;

public class EditSexActivity extends BaseEditAccountActivity {

	private int sexId;
	
	private ImageView selectBoy,selectGirl;
	
	private void seletSex(int positon) {

		if (positon==0) {//boy
			selectBoy.setVisibility(View.VISIBLE);
			selectGirl.setVisibility(View.GONE);
			sexId=1;
		}else {//girl
			sexId=2;
			selectBoy.setVisibility(View.GONE);
			selectGirl.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_sex);
		setTitle("性别");
		initView();
		
		setTopBarRightBtnListener("保存", new OnClickListener() {
			
			@Override
			public void onClick(View arg) {
				//no change. do nothing.
				if (sexId == LogInController.currentAccount.getSex()) {
					finish();
					return;
				}

				commitDone();
			}
		});
	}

private void initView() {
	sexId = LogInController.currentAccount.getSex();
	selectBoy = (ImageView) findViewById(R.id.select_boy);
	selectGirl = (ImageView) findViewById(R.id.select_girl);

	findViewById(R.id.boy_layout).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			seletSex(0);
		}
	});
	findViewById(R.id.girl_layout).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			seletSex(1);
		}
	});
	seletSex(sexId==2?1:0);//girl under.
	}

@Override
protected void commitDone() {
	showProgressDialog("修改资料", "正在提交，请稍候...", null);
	Map<String, String> map = getPublicParamRequstMap();
	map.put("sex", ""+sexId);
	map.put("password", LogInController.currentAccount.getPassword());
	map.put("gcode", 1234+"");			
	postDataLoader(map,"http://birdboy.cn/updateMeAPI", updateAccountController,new HttpClientStack(LogInController.getHttpClientInstance()));
	
}

@Override
protected void editOk() {
LogInController.currentAccount.setSex(sexId);
}
}
