package com.jameschen.birdboy.provider;

import java.io.Serializable;

public class DBContent implements Serializable{

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	private int id;
	
	private String content;

}
