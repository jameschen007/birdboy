package com.jameschen.birdboy.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {
	private static final String DBNAME = "birdboy.db";
	private static final int VERSION = 1;

	public DBOpenHelper(Context context) {
		super(context, DBNAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		init(db);
	}

	private void init(SQLiteDatabase db) {
		db.execSQL(SqlConfig.getTableCreator(SqlConfig.TABLENAME_CHILDAREA,
				SqlConfig.getObjMap()));
	}

	private void drop(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + SqlConfig.TABLENAME_CHILDAREA);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion == newVersion) {
			return;
		}
		drop(db);
		onCreate(db);
	}

}
