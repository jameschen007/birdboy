package com.jameschen.birdboy.provider;

import java.io.IOException;
import java.util.List;

import com.jameschen.birdboy.model.entity.ChildArea;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


public class ChildAreaHelper extends SqlHelper {

	public ChildAreaHelper(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	/*
	 * objects[0]--list for result objects[1]--offset objects[2]--length
	 */
	public int getQuery(Object... objects) {
		// TODO Auto-generated method stub
		// return super.getQuery(objects);
		
		String sql = null;
		int count = 0;
		if (objects.length == 1) {
			sql = "select * from "+SqlConfig.TABLENAME_CHILDAREA;
		} else if (objects.length == 3) {
			sql = "select * from "+SqlConfig.TABLENAME_CHILDAREA+" limit " + objects[3] + " offset "
					+ objects[2];
		} else {
			return count;
		}
		db = openHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
		count = cursor.getCount();
		while (cursor.moveToNext()) {
			byte[] b = cursor.getBlob(cursor.getColumnIndex("obj"));
			try {
				DBContent content =(DBContent) SqlConfig.tObject(b);
				((List<Object>) objects[0]).add(content);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		cursor.close();
		db.close();
		return count;
	}

	public synchronized DBContent  getQuery(int objId) {
		db = openHelper.getWritableDatabase();
		db.beginTransaction();
		String id = String.valueOf(objId);
		int number = 1;
		Cursor cursor = db.rawQuery("select obj from "+SqlConfig.TABLENAME_CHILDAREA+" where id=?",
				new String[] { id });
		DBContent content=null;
		try {
			if (cursor.getCount() > 0) {
				cursor.moveToPosition(0);
				byte[] b = cursor.getBlob(cursor.getColumnIndex("obj"));
				try {
					 content =(DBContent) SqlConfig.tObject(b);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 	
			
			db.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
		cursor.close();
		db.close();
		
		
		return content;
	}
	
	public void save(DBContent content) {
		db = openHelper.getWritableDatabase();
		db.beginTransaction();
		String id = String.valueOf(content.getId());
		int number = 1;
		Cursor cursor = db.rawQuery("select obj from "+SqlConfig.TABLENAME_CHILDAREA+" where id=?",
				new String[] { id });
		try {
			ContentValues cv = new ContentValues();
			if (cursor.getCount() > 0) {
				cursor.moveToPosition(0);
				cv.put("obj", SqlConfig.toByte(content));
				db.update(SqlConfig.TABLENAME_CHILDAREA, cv, "id=?", new String[] { id });
			} else {
				cv.put("id", id);
				cv.put("obj", SqlConfig.toByte(content));
				//cv.put("number", 1);
				db.insert(SqlConfig.TABLENAME_CHILDAREA, null, cv);
			}
			db.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
		cursor.close();
		db.close();
	}

	public int getCount() {
		int count = 0;
		db = openHelper.getReadableDatabase();
		db.beginTransaction();
		Cursor cursor = db.rawQuery("select number from "+SqlConfig.TABLENAME_CHILDAREA, null);
		count = cursor.getCount();
		int totalnum = 0;
		try {
			if (count > 0) {
				for (int i = 0; i < count; i++) {
					cursor.moveToPosition(i);
					int number = cursor.getInt(cursor.getColumnIndex("number"));
					totalnum += number;
				}
			}
			db.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
		cursor.close();
		db.close();
		return totalnum;
	}

	public void update(String id, int number) {
		db = openHelper.getReadableDatabase();
		db.beginTransaction();
		try {
			ContentValues cv = new ContentValues();
			cv.put("number", number);
			db.update("cart", cv, "id=?", new String[] { id });
			db.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			db.endTransaction();
		}
	}
}
