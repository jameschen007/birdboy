package com.jameschen.birdboy.provider;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SqlHelper {
	protected DBOpenHelper openHelper = null;
	protected SQLiteDatabase db = null;

	public SqlHelper(Context context) {
		openHelper = new DBOpenHelper(context);
	}

	/*
	 * objects[0]--tableName objects[1]--list for result objects[2]--offset
	 * objects[3]--length
	 */
	@SuppressWarnings("unchecked")
	public int getQuery(Object... objects) {
		String sql = null;
		int count = 0;
		if (objects.length == 2) {
			sql = "select * from " + objects[0];
		} else if (objects.length == 4) {
			sql = "select * from " + objects[0] + " limit " + objects[3]
					+ " offset " + objects[2];
		} else {
			return count;
		}
		db = openHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery(sql, null);
		count = cursor.getCount();
		while (cursor.moveToNext()) {
			byte[] b = cursor.getBlob(cursor.getColumnIndex("obj"));
			try {
				((List<Object>) objects[1]).add(SqlConfig.tObject(b));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		cursor.close();
		db.close();
		return count;
	}

	public Object getSingle(String table, String id) {
		db = openHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where id=?",
				new String[] { String.valueOf(id) });
		Object o = null;
		while (cursor.moveToNext()) {
			byte[] b = cursor.getBlob(cursor.getColumnIndex("obj"));
			System.out.println("byte size=" + b.length);
			try {
				o = SqlConfig.tObject(b);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		}
		cursor.close();
		db.close();
		return o;
	}

	public void insertList(List<Object> l) {
		db = openHelper.getWritableDatabase();
	}

	public void insert(Object o) {
		db = openHelper.getWritableDatabase();
	}

	public void delete(String table, String id) {
		db = openHelper.getWritableDatabase();
		db.execSQL("delete from " + table + " where id=?", new Object[] { id });
		db.close();
	}

	public void deleteAll(String table) {
		SQLiteDatabase db = openHelper.getWritableDatabase();
		db.execSQL("delete from " + table + " where 1=1");
		db.close();
	}
}
