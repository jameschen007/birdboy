package com.jameschen.birdboy;

import java.util.Map;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.android.volley.toolbox.HttpClientStack;
import com.jameschen.birdboy.base.BaseEditAccountActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.MD5Encode;

public class EditNickNameActivity extends BaseEditAccountActivity {
		EditText nickname;
		String nick;
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.edit_nickname);
			setTitle("昵称");
			nickname.setHint(LogInController.currentAccount.getNick());
			setTopBarRightBtnListener("保存", new OnClickListener() {
				
				@Override
				public void onClick(View arg) {
					//no change. do nothing.
					if (TextUtils.isEmpty(nickname.getText())) {
						finish();
						return;
					}

					nick =nickname.getText().toString();
					commitDone();
				}
			});
		}
	
	@Override
	protected void commitDone() {
		showProgressDialog("修改资料", "正在提交，请稍候...", null);
		Map<String, String> map = HttpUtil.createEditAccountMap(EditNickNameActivity.this);		
		postDataLoader(map,"http://birdboy.cn/updateMeAPI", updateAccountController,new HttpClientStack(LogInController.getHttpClientInstance()));
		
	}

	@Override
	protected void editOk() {
	LogInController.currentAccount.setNick(nick);
	}
}
