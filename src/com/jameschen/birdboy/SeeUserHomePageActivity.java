package com.jameschen.birdboy;

import java.lang.reflect.Type;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.adapter.AroundUserAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.FavoriteFriend;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.model.entity.UserInfo;
import com.jameschen.birdboy.ui.AroundListFragment;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.plugin.BindModel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class SeeUserHomePageActivity extends BaseActivity{


	private TextView name, distance,age,appearTime,sign,sportLike;
	private ImageView sex,headPhoto,weibo,qq;
	private View markFavBtn,bindPlugInView,sexAgeContainer;
	private Button inviteBtn;
	private UserInfo userInfo;
	private int aid= -1;//user id
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading_empty);
		aid = getIntent().getIntExtra("id", -1);
		setTitle("个人主页");
		Log.i(TAG, "userId=="+aid);
//		if (aid == myself ) {//not allow for invite.
//			
//		}
		
		showProgressDialog("正在加载", "请稍候...", null);
		if (LogInController.getHttpClientInstance()!=null) {
			getDataLoader(HttpConfig.REQUST_SEE_USER_HOME_PAGE_URL+"?userid="+aid, false, null, seeUserHomePageController,new HttpClientStack(LogInController.getHttpClientInstance()));
		}else {
			getDataLoader(HttpConfig.REQUST_SEE_USER_HOME_PAGE_URL+"?userid="+aid, false, null, seeUserHomePageController);

		}
	}
	

	public void setVisible(int Flag) {

	}

	public void setDistVisible(boolean distVisible) {
		if (!distVisible) {
			((View)distance.getParent()).setVisibility(View.GONE);
		} else {
			((View)distance.getParent()).setVisibility(View.VISIBLE);
		}
	}

	private TextView jionNum,createNum;
	
	public void initView(final UserInfo userInfo) {
		if (userInfo==null) {
			return;
		}
		
		setContentView(R.layout.user_home_page);
		name = (TextView)  findViewById(R.id.user_home_page_name);
		sign = (TextView) findViewById(R.id.sign);
		appearTime=(TextView) findViewById(R.id.time);
		distance = (TextView)findViewById(R.id.distance);
		age = (TextView)  findViewById(R.id.age);
		sportLike = (TextView)  findViewById(R.id.sport_like);
		jionNum = (TextView)  findViewById(R.id.jion_num);
		createNum = (TextView)  findViewById(R.id.create_num);
		headPhoto =(ImageView) findViewById(R.id.user_home_page_logo);
		markFavBtn =   findViewById(R.id.mark_fav);
		sex = (ImageView)  findViewById(R.id.sex_flag);
		weibo = (ImageView)  findViewById(R.id.weibo_img);
		qq = (ImageView)  findViewById(R.id.tencent_img);
		sexAgeContainer =  findViewById(R.id.sex_age_container);
		inviteBtn = (Button)  findViewById(R.id.invite_btn);
		bindPlugInView =  findViewById(R.id.plugin_container);
		final Account userAccount = userInfo.getUser();
		qq.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.go2SeeSNS(SeeUserHomePageActivity.this,userAccount.getSnsObj(),BindModel.QQ);
					}
		});
		weibo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.go2SeeSNS(SeeUserHomePageActivity.this,userAccount.getSnsObj(),BindModel.SINA);

			}
		});
		markFavBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InvitedUser invitedUser=new InvitedUser();
				invitedUser.setId(userAccount.getId());
				invitedUser.setNick(userAccount.getNick());
				invitedUser.setLogo(userAccount.getNick());
				invitedUser.setSex(userAccount.getSex());
				Util.addMyFav(SeeUserHomePageActivity.this, invitedUser);
			}
		});
		inviteBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//check is logOn
				final FavoriteFriend invitedUser=new FavoriteFriend();
				invitedUser.setUserid(userAccount.getId());
				invitedUser.setUsername(userAccount.getNick());
				invitedUser.setLogo(userAccount.getLogo());
				if (LogInController.IsLogOn()) {
					inviteUser(invitedUser);
				}else {
					Util.go2LogInDialog(SeeUserHomePageActivity.this ,new OnLogOnListener() {
						@Override
						public void logon() {
							inviteUser(invitedUser);
						}
					});
				}
				
			}
		});
		setInfo(userInfo);
	}
	int position=0;

	protected void inviteUser(FavoriteFriend invitedUser) {
		if (invitedUser.getUserid()==LogInController.currentAccount.getId()) {
			showToast("不能邀约自己");
			return;
		}
		
		Bundle extra=new Bundle();
		extra.putInt("flag", 1);
		extra.putParcelable("invite_one", invitedUser);
		Util.createSportEvent(SeeUserHomePageActivity.this,null,extra);
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	
	private void checkPlugIn(SNS sns) {
		if (sns!=null) {

			bindPlugInView.setVisibility(View.VISIBLE);
			qq.setVisibility(View.GONE);
			weibo.setVisibility(View.GONE);
			if (sns.getQq()!=null) {
				qq.setVisibility(View.VISIBLE);
			}
			
			if (sns.getWeibo()!=null) {
				weibo.setVisibility(View.VISIBLE);
			}
			
		}else {

			bindPlugInView.setVisibility(View.GONE);
		}
	}
	

	public void setInfo(UserInfo userInfo) {
		
		this.userInfo = userInfo;
		final Account userAccountInfo = userInfo.getUser();
		
		name.setText(userAccountInfo.getNick());
		if (TextUtils.isEmpty(userAccountInfo.getSignstr())) {
			sign.setText(getString(R.string.no_sign));
		}else {
			sign.setText(userAccountInfo.getSignstr());
		}
	
		age.setText(userAccountInfo.getAge()+"");
		sportLike.setText(AroundUserAdapter.getSportTypeStr(userAccountInfo.getMylike()));
		//personal 
		int sexRes=R.drawable.boy;
		int defualtIcon = R.drawable.m;
		int sexId=userAccountInfo.getSex();//default
		
		jionNum.setText(userInfo.getJionNum()+"");
		createNum.setText(userInfo.getCreateNum()+"");
		
		sexRes=sexId==1?R.drawable.fujin_boy:R.drawable.fujin_girl;
		defualtIcon =sexId==1?R.drawable.m:R.drawable.wm;
		sexAgeContainer.setBackgroundResource(sexId==1?R.drawable.round_bg_blue:R.drawable.round_bg_pink);
		sex.setImageResource(sexRes);
		DisplayImageOptions mOptions = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.showImageOnLoading(defualtIcon)
		.showImageForEmptyUri(defualtIcon)
		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(10))
		.cacheOnDisc(true).build();
		if (!TextUtils.isEmpty(userAccountInfo.getLogo())) {
			loadImage(HttpConfig.HTTP_BASE_URL+userAccountInfo.getLogo(), headPhoto,mOptions);
			headPhoto.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					String blogoUrl = userAccountInfo.getLogo().replace("slogo", "blogo");
					Util.go2SeeBigPic(SeeUserHomePageActivity.this, HttpConfig.HTTP_BASE_URL+blogoUrl);
				}
			});
		}else {
			headPhoto.setImageResource(defualtIcon);
			headPhoto.setOnClickListener(null);
		}
	
		
		if (!TextUtils.isEmpty(userAccountInfo.getSns())) {
			try {
				userAccountInfo.setSns(ParseJsonData.getSNSData(userAccountInfo.getSns()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		checkPlugIn(userAccountInfo.getSnsObj());
		
		
	}




	
private	UIController seeUserHomePageController = new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			initView((UserInfo)content);
		}
		
		@Override
		public String getTag() {
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			return new TypeToken<WebBirdboyJsonResponseContent<UserInfo>>() {
			}.getType();
		}
	};
	
	
}
