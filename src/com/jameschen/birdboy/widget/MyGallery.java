package com.jameschen.birdboy.widget;

import java.util.ArrayList;

import com.jameschen.birdboy.CoachDetailActivity;
import com.jameschen.birdboy.ImageDetailActivity;
import com.jameschen.birdboy.adapter.ObjectAdapter;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.entity.StadiumPhoto;
import com.jameschen.birdboy.utils.Util;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

public class MyGallery extends LinearLayout {
	private AdapterView.OnItemClickListener onItemClickListener;

	public MyGallery(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOrientation(HORIZONTAL);
	}

	public MyGallery(Context context) {
		super(context);
		setOrientation(HORIZONTAL);
	}

	public void setAdapter(final ObjectAdapter<StadiumPhoto> adapter) {
		for (int i = 0; i < adapter.getCount(); i++) {
			View view = adapter.getView(i, null, this);
			final int position = i;
			final long id = adapter.getItemId(position);
			
			view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					//StadiumPhoto stadiumPhoto = adapter.getItem(position);
			
					Intent scanImageIntenet=new Intent(getContext(),ImageDetailActivity.class);
					ArrayList<String> phoneList = new ArrayList<String>();
						//phoneList.add(HttpConfig.HTTP_BASE_URL+stadiumPhoto.getUrl());
						for (int j = 0; j < adapter.getCount(); j++) {
							phoneList.add(HttpConfig.HTTP_BASE_URL+adapter.getItem(j).getUrl());
						}
					scanImageIntenet.putExtra(ImageDetailActivity.EXTRA_IMAGE, position);
					scanImageIntenet.putStringArrayListExtra("photos", phoneList);
					getContext().startActivity(scanImageIntenet);
					if (onItemClickListener != null) {
						onItemClickListener.onItemClick(null, v, position, id);
					}
				}
			});
			view.setPadding(5, 0, 5, 0);
			LinearLayout.LayoutParams layoutParams =new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			int size =Util.getPixByDPI(getContext(),60);
			layoutParams.width=size;
			layoutParams.height=size;
			this.addView(view,layoutParams);
		}
	}

	public void setOnItemClickListener(
			AdapterView.OnItemClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}

	public void refresh(ObjectAdapter<StadiumPhoto> adapter) {
		removeAllViews();
		setAdapter(adapter);
	}
}