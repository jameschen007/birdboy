package com.jameschen.birdboy.widget;

import com.jameschen.birdboy.R;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;


public class MExpandableListView extends ExpandableListView implements
		OnScrollListener, android.widget.AdapterView.OnItemLongClickListener {
	private Context context;
	private View footer;
	private RefreshListener refresh;
	private boolean refreshing = false;

	private boolean enableLongClick = false;

	public void setItemLongClickable(boolean enable) {
		// TODO Auto-generated method stub
		enableLongClick = enable;

	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		// ..

	}

	public MExpandableListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		this.context = context;
		setOnScrollListener(this);
		if (footer == null) {
			footer = View.inflate(context,
					R.layout.footer_list_loading_progress, null);
		}

		addFooterView(footer);
	}

	public MExpandableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.context = context;
		setOnScrollListener(this);
	}

	public MExpandableListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		setOnScrollListener(this);
	}


	private void setLoading(boolean loading, boolean refreshing) {

		// if (this.loading && loading) {
		// return;
		// }
		// this.loading = loading;
		if (loading) {
			if (footer == null) {
				footer = View.inflate(context,
						R.layout.footer_list_loading_progress, null);
				addFooterView(footer);
				
			}
			footer.findViewById(R.id.load_progress).setVisibility(View.VISIBLE);
			footer.setVisibility(View.VISIBLE);
		} else {
			if (footer != null) {
				footer.setVisibility(View.GONE);
			}
		}
		this.refreshing = refreshing;
	}

	public void onRefreshComplete(boolean loadmore) {
		if (loadmore) {
			System.out.println("loadmore......");
			setLoading(false, true);
		} else {
			setLoading(false, false);

			System.out.println("complete......");
			if (footer != null) {
				System.out.println("remove......");
				removeFooterView(footer);
			}
		}

	}

	@Override
	public void setAdapter(ExpandableListAdapter adapter) {
		// TODO Auto-generated method stub
		setLoading(true, false);
		super.setAdapter(adapter);
	}

	public RefreshListener getRefresh() {
		return refresh;
	}

	public void setRefreshListener(RefreshListener refresh) {
		this.refresh = refresh;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		if (refreshing) {
			int lastItem = firstVisibleItem + visibleItemCount - 1;
			if (lastItem >= getCount() - 1) {
				if (refresh != null) {
					refresh.onRefresh();
				}
				setLoading(true, false);
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
			setPauseImageLoadWork(true);
		} else {
			setPauseImageLoadWork(false);
		}

	}

	public void setPauseImageLoadWork(boolean pauseWork) {
		if (refresh == null) {
			return;
		}
		refresh.OnPauseImageLoadWork(pauseWork);

	}

	public interface RefreshListener {
		void onRefresh();

		void OnPauseImageLoadWork(boolean pauseWork);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		if (!enableLongClick) {
			return false;
		}

		return true;
	}

}
