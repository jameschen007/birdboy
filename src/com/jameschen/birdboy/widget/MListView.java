package com.jameschen.birdboy.widget;

import java.text.BreakIterator;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.XListView;
import com.jameschen.birdboy.R;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.jameschen.birdboy.utils.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.AbsListView.OnScrollListener;
public abstract class MListView extends PullToRefreshListView {
	private MyRefreshListener refresh;

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		// ..

	}

	public MListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
		setMode(Mode.PULL_FROM_START);
	}

	private Context context;
	// private boolean loading = false;
	private View footer;
	private boolean refreshing = false;

	public void setPauseImageLoadWork(boolean pauseWork) {
		if (refresh == null) {
			return;
		}
		refresh.OnPauseImageLoadWork(pauseWork);

	}

	private void init(Context context) {
		this.context = context;
		setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				// Pause fetcher to ensure smoother scrolling when flinging
				if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
					setPauseImageLoadWork(true);
				} else if(scrollState== AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
					setPauseImageLoadWork(false);
				}

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (refreshing) {
					if (visibleItemCount == totalItemCount) {//only one page
						setLoading(false, false);
						return;
					}
					int lastItem = firstVisibleItem + visibleItemCount - 1;
				
					if (lastItem>0&&lastItem >= getRefreshableView().getCount() - 1) {
						if (refresh != null) {
							refresh.OnRefreshFooterListener();
						}
						setLoading(true, false);
					}
				}
			}
		});
		if (footer == null) {
			footer = View.inflate(context,
					R.layout.footer_list_loading_progress, null);
			getRefreshableView().addFooterView(footer);
		}

	}

	public void setLoadingCancel() {
		if (footer != null) {
			getRefreshableView().removeFooterView(footer);
		}
	}
	public void setfreshing(boolean refreshing) {
		this.refreshing = refreshing;
	}
	private void setLoading(boolean refresh) {
		setLoading(false, refresh);
	}

	private void setLoading(boolean loading, boolean refreshing) {
		// if (this.loading && loading) {
		// return;
		// }
		// this.loading = loading;

		if (loading) {
			if (footer == null) {
				footer = View.inflate(context,
						R.layout.footer_list_loading_progress, null);
				getRefreshableView().addFooterView(footer);
			}
			footer.findViewById(R.id.load_progress).setVisibility(View.VISIBLE);
			footer.setVisibility(View.VISIBLE);
		} else {
			if (footer == null) {
				footer = View.inflate(context,
						R.layout.footer_list_loading_progress, null);
				getRefreshableView().addFooterView(footer);
			}
			footer.setVisibility(View.GONE);
		}

		this.refreshing = refreshing;
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		// TODO Auto-generated method stub
		// setLoading(true, false);
		super.setAdapter(adapter);
	}

	public MListView(Context context) {
		super(context);
		init(context);
	}

	public void onRefreshComplete(boolean loadmore) {
		if (loadmore) {
			setLoading(true);
		} else {
			setLoading(false);

			if (footer != null) {
				getRefreshableView().removeFooterView(footer);
				footer = null;
			}
		}
		Log.i(VIEW_LOG_TAG, "refresh list end!");
		if (isRefreshing()) {
			onRefreshComplete();
		}else {
			//force fresh
			onReset();
		}
	
	}

	public RefreshListener getRefresh() {
		return refresh;
	}

	public interface MyRefreshListener extends RefreshListener {
		void OnPauseImageLoadWork(boolean pauseWork);

		void OnRefreshFooterListener();
	}

	public void setRefreshListener(MyRefreshListener refresh) {
		this.refresh = refresh;
		setOnRefreshListener(new OnRefreshListener<XListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<XListView> refreshView) {
				// TODO Auto-generated method stub
				MListView.this.refresh.onRefresh(refreshView);
			}
		});
	}

	public interface RefreshListener {
		void onRefresh(PullToRefreshBase<XListView> refreshView);
	}

}
