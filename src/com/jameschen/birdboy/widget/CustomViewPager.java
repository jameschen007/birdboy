package com.jameschen.birdboy.widget;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

public class CustomViewPager extends ViewPager {
	private boolean disable = false;

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	// @Override
	// public boolean onInterceptTouchEvent(MotionEvent event) {
	// if (disable) {
	// requestDisallowInterceptTouchEvent(true);
	// }
	//
	// return super.onInterceptTouchEvent(event);
	// }

	@Override
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
		if (v != this && v instanceof ViewPager) {
			ViewPager childPager = (ViewPager) v;
			int childCurrentIndex = childPager.getCurrentItem();
			PagerAdapter childAdapter = childPager.getAdapter();
			if (childAdapter == null) {
				return super.canScroll(v, checkV, dx, x, y);
			}
			int childPageNum = childAdapter.getCount();
			// middle
			if (childPageNum > 1) {
				if (childCurrentIndex == 0) {// first child banner
					if (dx >= 0) {// left
						return super.canScroll(v, checkV, dx, x, y);
					}
				} else if (childCurrentIndex == childPageNum - 1) {// last
																	// child
																	// banner
					if (dx < 0) {// right
						return super.canScroll(v, checkV, dx, x, y);
					}
				}

				return true;
			}

		}
		return super.canScroll(v, checkV, dx, x, y);
	}

	public boolean isFirstPage() {
		if (getAdapter()==null) {
			return false;
		}
		return getCurrentItem() == 0 && getAdapter().getCount() >1;
	}

	public boolean isLastPage() {
		if (getAdapter()==null) {
			return false;
		}
		return getCurrentItem() == getAdapter().getCount() - 1;
	}
	
	public int getItemTotalCount(){
		return getAdapter().getCount();
	}
	
//	public void setInterceptDisable(boolean disable) {
//		if (this.disable != disable) {
//			this.disable = disable;
//		}
//	}
//
//	public boolean isInterceptDisable() {
//		return disable;
//	}
}