package com.jameschen.birdboy.widget;

import android.content.Context;
import android.util.AttributeSet;

public class MyList2View extends MList2View {

	public MyList2View(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub

	}

	public interface OnLoadTask2Listener{
		void reloadTask();
	}
	
	private OnLoadTask2Listener loadTaskListener;
	
	public void setLoadTaskListener(OnLoadTask2Listener loadTaskListener){
		this.loadTaskListener = loadTaskListener;
	}
	
	public void reloadTask() {
		// TODO Auto-generated method stub
		if (loadTaskListener!=null) {
			loadTaskListener.reloadTask();
		}
	}

}
