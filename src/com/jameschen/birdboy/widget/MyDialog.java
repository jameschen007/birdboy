package com.jameschen.birdboy.widget;

import com.jameschen.birdboy.R;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

public class MyDialog extends Dialog{
private Context context;
	public MyDialog(Context context, int theme) {
		super(context, theme);
		// TODO Auto-generated constructor stub
		this.context = context;
		setCancelable(true);
		setCanceledOnTouchOutside(false);
	}
@Override
public void show() {
	// TODO Auto-generated method stub
	Window dialogWindow = getWindow();
	WindowManager.LayoutParams lp = dialogWindow.getAttributes();
	lp.width = context.getResources().getDimensionPixelSize(R.dimen.dialog_witdh);//
	lp.dimAmount=.3f; 
	
	getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
	super.show();
	
}//
}
