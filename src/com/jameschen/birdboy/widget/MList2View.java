package com.jameschen.birdboy.widget;

import java.text.BreakIterator;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.XListView;
import com.jameschen.birdboy.R;

import android.R.integer;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.jameschen.birdboy.utils.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.AbsListView.OnScrollListener;
public abstract class MList2View extends PullToRefreshListView {
	private MyRefresh2Listener refresh;

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		// ..

	}

	public MList2View(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
		setMode(Mode.PULL_FROM_START);
	}

	private Context context;
	// private boolean loading = false;
	private View footer;
	private boolean refreshing = false;

	public void setPauseImageLoadWork(boolean pauseWork) {
		if (refresh == null) {
			return;
		}
		refresh.OnPauseImageLoadWork(pauseWork);

	}

	private void init(Context context) {
		this.context = context;
		setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				// Pause fetcher to ensure smoother scrolling when flinging
				if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
					setPauseImageLoadWork(true);
				} else if(scrollState== AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
					setPauseImageLoadWork(false);
				}

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (refreshing) {
					if (visibleItemCount == totalItemCount) {//only one page
						setLoading(false, false);
						return;
					}
					int lastItem = firstVisibleItem + visibleItemCount - 1;
				
					if (lastItem>0&&lastItem >= getRefreshableView().getCount() - 1) {
						if (refresh != null) {
							refresh.OnRefreshFooterListener();
						}
						setLoading(true, false);
					}
				}
			}
		});
		if (footer == null) {
			bindFooterClick();
			getRefreshableView().addFooterView(footer);
		}
		footer.findViewById(R.id.load_next).setVisibility(View.GONE);
		setTimeOutShowRule(false);
		footer.findViewById(R.id.load_progress).setVisibility(View.VISIBLE);


	}

	public void setLoadingCancel() {
		if (footer != null) {
			getRefreshableView().removeFooterView(footer);
		}
	}
	public void setfreshing(boolean refreshing) {
		this.refreshing = refreshing;
	}
	private void setLoading(boolean refresh) {
		setLoading(false, refresh);
	}

	private void setLoading(boolean loading, boolean refreshing) {
		// if (this.loading && loading) {
		// return;
		// }
		// this.loading = loading;

		if (loading) {
			if (footer == null) {
				bindFooterClick();
				getRefreshableView().addFooterView(footer);
			}else {
				footer.findViewById(R.id.load_next).setVisibility(View.VISIBLE);
				footer.findViewById(R.id.load_progress).setVisibility(View.GONE);
				
			}
			
			footer.setVisibility(View.VISIBLE);
		} else {
			if (footer == null) {
				bindFooterClick();
				getRefreshableView().addFooterView(footer);
			}else {
				footer.findViewById(R.id.load_next).setVisibility(View.VISIBLE);
				footer.findViewById(R.id.load_progress).setVisibility(View.GONE);
				
			}
			footer.setVisibility(View.GONE);
		}

		this.refreshing = refreshing;
	}

	private void bindFooterClick() {
		footer = View.inflate(context,
				R.layout.footer_list_click_loading, null);	
		footer.findViewById(R.id.load_next).setVisibility(View.VISIBLE);
		footer.findViewById(R.id.load_progress).setVisibility(View.GONE);
		footer.findViewById(R.id.load_next).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				v.setVisibility(View.GONE);
				footer.findViewById(R.id.load_progress).setVisibility(View.VISIBLE);
				setTimeOutShowRule(true);
				if (refresh != null) {
					refresh.onClickNextPageListener();
				}
			}
		});
	}

	private  Runnable  footerRefreshRunnable;
	
	
	
	private void setTimeOutShowRule(final boolean showNextPage) {
		
	}
	
	@Override
	public void setAdapter(ListAdapter adapter) {
		// TODO Auto-generated method stub
		// setLoading(true, false);
		super.setAdapter(adapter);
	}

	public MList2View(Context context) {
		super(context);
		init(context);
	}

	public void onRefreshComplete(boolean loadmore) {
		if (loadmore) {
			setLoading(true);
		} else {
			setLoading(false);

			if (footer != null) {
				getRefreshableView().removeFooterView(footer);
				footer = null;
			}
		}
		Log.i(VIEW_LOG_TAG, "refresh list end!");
		if (isRefreshing()) {
			onRefreshComplete();
		}else {
			//force fresh
			onReset();
		}
	
	}

	public Refresh2Listener getRefresh() {
		return refresh;
	}

	public interface MyRefresh2Listener extends Refresh2Listener {
		void OnPauseImageLoadWork(boolean pauseWork);

		void OnRefreshFooterListener();
		
		void onClickNextPageListener();
	}

	public void setRefreshListener(MyRefresh2Listener refresh) {
		this.refresh = refresh;
		setOnRefreshListener(new OnRefreshListener<XListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<XListView> refreshView) {
				// TODO Auto-generated method stub
				MList2View.this.refresh.onRefresh(refreshView);
			}
		});
	}

	public interface Refresh2Listener {
		void onRefresh(PullToRefreshBase<XListView> refreshView);
	}

}
