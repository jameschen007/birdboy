package com.jameschen.birdboy.widget;

import com.jameschen.birdboy.R;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoveView extends RelativeLayout{

	
	private ImageView loveImg;
	private TextView loveContent;
	public LoveView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		contentLen = myLoveContent.length();
		
	}

	//Bitmap loveBitmap;
	int picLen;
	public void begin(ImageView loveImg,TextView iTextView) {
		this.loveImg = loveImg;
		this.loveContent = iTextView;
//		if (loveBitmap==null) {
//			loveBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.choose_sex_hear1t);
//			picLen  = loveBitmap.getHeight()+20;
//		}
		
		invalidate();
		showPostDelay();
		
	}
	
	private void showPostDelay() {
	loveContent.postDelayed(new Runnable() {
			
			@Override
			public void run() {

				
					if (textStep<contentLen) {
						 currentContent =myLoveContent.substring(0, textStep);
					}else {
						isEnd=true;
						
					}
					textStep++;
				
				if (!isEnd&&currentContent!=null) {
					loveContent.setText(currentContent);
					showPostDelay();
				}else if (isEnd) {
					loveContent.setText(myLoveContent);
				}
				
				
			}
		}, 500);
	}
	
	int loopStep=1,textStep=1,contentLen;
	
	@Override
	protected void onDetachedFromWindow() {
		// TODO Auto-generated method stub
		super.onDetachedFromWindow();
		
	}
	
	int count=0;
	private String currentContent;
	private boolean isEnd,isCenter;
	public void checkIfNeedAnimate(){
		count+=60;
		if (isCenter) {
			
		}else {
			postInvalidateDelayed(60);
		}
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if (loveImg!=null) {
			int center =getWidth()/2-loveImg.getWidth()/2;
			int currentPos=loopStep;
			LayoutParams params =(LayoutParams) loveImg.getLayoutParams();
			params.topMargin=5;
			if (currentPos<center) {
				params.leftMargin=currentPos;
				loveImg.setLayoutParams(params);
			}else {
				isCenter=true;
			}
			loopStep++;
		
		if (isCenter) {
			params.leftMargin=center;
			loveImg.setLayoutParams(params);
		}
			
			checkIfNeedAnimate();	
		}
	
	}
	
	
	private String myLoveContent=" 亲爱的闹，\n"+"想了很久，最终还是决定以这样的方式\n来告诉你我心里的想法。\n我想娶你做我的老婆，" +
			"\n我想和你生一个孩子，\n我想有一个温馨的家庭，\n我想有一个你这样的好妻子，\n我想和你一起讨论我们的人生，\n我想和你在一起过一辈子！\n你愿意吗？" +
			"\n我会很努力的工作，\n很努力的挣钱，\n我会照顾你，\n我会关心你。\n我会给你幸福，\n我要让全世界用这个软件的用户都看到我的心意。\n" +
			"" +
			"I love you ,黄凤婷~\n"+"by james";
}
