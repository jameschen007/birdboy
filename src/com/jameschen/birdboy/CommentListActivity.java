package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;
import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.adapter.CommentAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.BaseListFragment;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.CommentListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.widget.MListView;
import com.jameschen.birdboy.widget.MyListView;

public class CommentListActivity extends BaseActivity {

	private Fragment mFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
		setTitle("全部点评");
		Intent intent = getIntent();
		long id = intent.getLongExtra("id", 0);
		int fromtype = intent.getIntExtra("type", 0);
		// Make sure fragment is created.
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		mFragment = fm.findFragmentByTag(ConstValues.COMMENT);
		if (mFragment == null) {
			mFragment = CommentListFragment.getCommentListFragment(id, fromtype);
			ft.add(R.id.fragment_content, mFragment, ConstValues.COMMENT);
		}

		ft.commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case ConstValues.TYPE_EVENT_VAL:
		{
			if (resultCode == RESULT_OK) {
				if (mFragment!=null) {
					((CommentListFragment)mFragment).refreshListFromTop();
				}

			}
		}
			break;

		default:
			break;
		
	}	
	}
}




// http://birdboy.cn/commentListAPI?fromtype=1&id=88&pagenum=2&pageno=2
//
// get请求
//
// 返回字段说明
//
// 字段 类型 描述 备注 值条件
//
//
// fromtype int 来源 1场馆2教练3活动 必填
// id int 场馆或者教练或者活动的id 必填
//
// pageno int 当前第几页 默认从0页开始
//
// pagenum int 每页展示的个数 pageno和pagenum要一起传，默认一页10个
