 package com.jameschen.birdboy;


import java.lang.reflect.Type;
import java.util.HashMap;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.SoftwareInfo;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.ui.GuidePageFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;
import com.tencent.mm.sdk.ConstantsUI.Contact;
import com.viewpagerindicator.IconPageIndicator;
import com.viewpagerindicator.IconPagerAdapter;

public class SlapshActivity extends BaseActivity {

	private ViewPager mPager;
	private MyAdapter mAdapter;

	public static final int NUM_ITEMS = 5;
     
	private static boolean loadGuidePage = false;
	private IconPageIndicator indicator;
	// 每次进入上传log，上传成功，完毕删除对应的log文件
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTheme(R.style.Theme_Sherlock_Light_NoActionBar);
		getSupportActionBar().hide();
		setContentView(R.layout.guide);
		mAdapter = new MyAdapter(getSupportFragmentManager());
		mPager = (ViewPager) findViewById(R.id.guide_pager);
		indicator = (IconPageIndicator)findViewById(R.id.circle_indicator);
		indicator.setVisibility(View.GONE);
		
		readCfgFromPreference();
		//check http is aviaable
	//	getDataLoader(HttpConfig.REQUST_FEEDBACK_URL, false, null, slapshUiController,new HttpClientStack(HttpUtil.getHttpClientInstance()));
	
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	 handler.removeCallbacksAndMessages(null);
	}
	private void readCfgFromPreference() {
		// TODO Auto-generated method stub
		SharedPreferences software = getSharedPreferences(
				SoftwareInfo.SharedName, 0);
		loadGuidePage = software.getBoolean(SoftwareInfo.guide, true);//later  new verion is for check
		int version = software.getInt(SoftwareInfo.version, -1);
		//if  appVersion > store version show guide ??
		int Appversion = Util.getAppVersion(this);
		if (Appversion>version) {
			software.edit().putInt(SoftwareInfo.version, Appversion).commit();
			software.edit().putBoolean(SoftwareInfo.guide, true).commit();
			loadGuidePage=true;
		}
		
		//if (!loadGuidePage) {
			go2main();
//		}else {
//			mPager.setAdapter(mAdapter);
//			indicator.setVisibility(View.VISIBLE);
//			indicator.setViewPager(mPager);
//			indicator.setCurrentItem(0);		
//		}

	}
	
	
	
	
	public static boolean IsNeedCheckedGuidePage(BaseActivity context,String pageTag) {
		if (!loadGuidePage) {
			return false;
		}
		SharedPreferences software = context.getSharedPreferences(
				SoftwareInfo.SharedName, 0);
		String pagestr=software.getString("page", "");
		
		if (pagestr.contains(pageTag)) {//yes checked
			return true;
		}else {//no check
			Intent intent =new Intent(context,GuideActivity.class);
			intent.putExtra("tag", pageTag);
			
			context.startActivity(intent);
			context.overridePendingTransition(0, 0);
			return false;
		}
	}
	
	private static void setCheckALLGuidePage(Context context) {
		SharedPreferences software = context.getSharedPreferences(
				SoftwareInfo.SharedName, 0);
		software.edit().putBoolean(SoftwareInfo.guide, false).commit();
		loadGuidePage=false;
	}

	public static final int checkPageNum=5;
	
	
	public static void setGuidePageChecked(Context context,String pageTag) {
		if (pageTag==null) {
			return;
		}
		SharedPreferences software = context.getSharedPreferences(
				SoftwareInfo.SharedName, 0);
		String pagestr=software.getString("page", "");
		
		software.edit().putString("page", pagestr+pageTag+",").commit();//last add ,
		//check other page is all checked 
		String newPagestr=software.getString("page", "");
		String pageNum[]=newPagestr.split(",");
		if (pageNum!=null&&pageNum.length==checkPageNum) {
			setCheckALLGuidePage(context);
		}
		
	}
	
	Handler handler = new Handler();
	long startTime =0;
    private void go2main() {
		Intent intentEx = new Intent();
		intentEx.setClass(this, MainActivity.class);
		overridePendingTransition(R.anim.welcome_begin_anim,
				R.anim.welcome_end_anim);
		startActivity(intentEx);
		finish();
	}
    
    @Override
    public void finish() {
    	// TODO Auto-generated method stub
    	super.finish();
    	overridePendingTransition(0,0);
    }
    
	public static class MyAdapter extends FragmentStatePagerAdapter implements IconPagerAdapter{

		public MyAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public int getCount() {
			return NUM_ITEMS;
		}

		@Override
		public Fragment getItem(int position) {
			return GuidePageFragment.newInstance(position);
		}

		@Override
		public int getIconResId(int index) {
			// TODO Auto-generated method stub
			return R.drawable.dot_state;
		}
	}
	
	
}
