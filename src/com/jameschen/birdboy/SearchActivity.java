package com.jameschen.birdboy;

import java.util.Map;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.SportEventListFragment;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.LoveView;
import com.jameschen.birdboy.widget.MyDialog;

public class SearchActivity extends BaseActivity {

	private EditText searchInput;
	private ImageView searchBtn;
	String searchTag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		Intent intent = getIntent();
		searchTag = intent.getStringExtra(ConstValues.SEARCH);
		initViews();

	}

	private void initViews() {
		setTitle("搜索");
		searchInput = (EditText) findViewById(R.id.search_edit);
		searchBtn = (ImageView) findViewById(R.id.search_btn);
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startSearch(searchInput.getText());
			}
		});
	}

	private void startSearch(CharSequence keyword) {
		if (TextUtils.isEmpty(keyword)) {
			showToast("请输入关键字");
			return;
		}
		if (keyword.toString().trim().equals("")) {
			showToast("请不要输入无效的字符");
			return;
		}

		if (keyword.toString().equals("闹妹妹")||keyword.toString().equals("cecilia")) {
			showLoveStore();
			return;
		}
		
		
		showProgressDialog("搜索中", "请稍候...", null);
		
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment mFragment = fm.findFragmentByTag(ConstValues.SEARCH);
		if (mFragment == null) {

			if (searchTag.equals(ConstValues.COACH)) {
				mFragment = CoachListFragment.newInstance(keyword.toString(),
						-1, null);
			} else if(searchTag.equals(ConstValues.STADIUM)){
				mFragment = StadiumListFragment.newInstance(keyword.toString(),
						-1, null);
			}else if(searchTag.equals(ConstValues.SPORT_EVENT)){
				mFragment = SportEventListFragment.newInstance(keyword.toString(),
						-1, null);
			}

			ft.add(R.id.search_fragment_content, mFragment, ConstValues.SEARCH);
			ft.commit();
		} else {
			if (searchTag.equals(ConstValues.COACH)) {
				((CoachListFragment) mFragment).startSearch(keyword.toString());
			} else if(searchTag.equals(ConstValues.STADIUM)) {

				((StadiumListFragment) mFragment).startSearch(keyword
						.toString());
			}else if(searchTag.equals(ConstValues.SPORT_EVENT)) {

				((SportEventListFragment) mFragment).startSearch(keyword
						.toString());
			}
		}

	}

	private void showLoveStore() {
		final MyDialog login = new MyDialog(this, R.style.logindialog);
		login.setCancelable(true);
		login.setCanceledOnTouchOutside(true);
		LoveView v = (LoveView) View.inflate(getBaseContext(), R.layout.love, null);
		ImageView loveImg = (ImageView) v.findViewById(R.id.love_img);
		TextView loveContent = (TextView) v.findViewById(R.id.love_content);
		login.setContentView(v);
		v.begin(loveImg,loveContent);
		login.show();
	}

}
