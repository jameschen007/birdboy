package com.jameschen.birdboy;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;

import org.apache.http.Header;
import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.protocol.HttpContext;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import com.jameschen.birdboy.utils.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseUploadPhotoActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Register;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

public class RegisterUploadPhotoActivity extends BaseUploadPhotoActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_uploadlogo);
		
			if (getIntent()!=null&&id<=0) {
			id =getIntent().getIntExtra("id", 0);
			}
				Log.i(TAG, "id=="+id);
			initViews();	
			setTitle("上传头像 ");
			initDataLoaderFromNativeCache(null, LoadMode.NONE, uiFreshController);		
			setTopBarBtnVisiable(View.GONE);
			setTopBarLeftBtnVisiable(View.GONE);
	}
	
	
	
	private void initViews() {

		upload = (Button) findViewById(R.id.btn_upload);
		photo = (ImageView) findViewById(R.id.image_choose);
		photoText = (TextView) findViewById(R.id.photo_text);
		failedText = (TextView) findViewById(R.id.failed_text);
		//
		
		upload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				if (isStart) {
//					showToast("正在上传，请稍候...");
//					return;
//				}
			
				if (photo.getTag()==null) {
					showToast("请选择一张照片");
//					setResult(RESULT_OK);
//					finish();
					return;
				}
				File file  = new File((String)photo.getTag());
				if (!file.exists()) {//file deleted?
					showToast("图片不存在");
					return;
				}
				failedText.setText("");
				
				 RequestParams params = new RequestParams();  
				 
				 params.put("method", "logoV2");
				 params.put("uid", id+"");
				 params.put("ssize", 200);
				 params.put("size", 500);
				 try {
					params.put("logo_file", file);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			// Upload a File  
				 PersistentCookieStore myCookieStore = new PersistentCookieStore(RegisterUploadPhotoActivity.this);
				 final AsyncHttpClient client =new AsyncHttpClient();
				 client.setCookieStore(myCookieStore);
				 if (LogInController.getHttpClientInstance()!=null) {
						client.setCookieStore(LogInController.getHttpClientInstance().getCookieStore());
				}
			
				// Map<String, File> fileMap = new HashMap()<String, File>();
//				 fileMap.put("image_file", file);
//				 HttpUtil.get("http://birdboy.cn/upload", params, fileMap);
				 
				client.post("http://birdboy.cn/upload", params, new AsyncHttpResponseHandler(){
					

					




					@Override
					@Deprecated
					public void onSuccess(String content) {
						// TODO Auto-generated method stub
						super.onSuccess(content);
						Log.i(TAG, "&&&&&"+content);
						readJsonData(content, uiController, null);
						  HttpContext httpContext = client.getHttpContext();
					        cookies = (CookieStore) httpContext.getAttribute(ClientContext.COOKIE_STORE);
					}
					@Override
					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0, arg1, arg2);
						isStart=false;
						
						debugHeaders(TAG, arg1);
					}
					 @Override
					public void onProgress(int bytesWritten, int totalSize) {
						// TODO Auto-generated method stub
						super.onProgress(bytesWritten, totalSize);
						int percent = bytesWritten*100/totalSize;
						//photoText.setText("已上传"+percent+"%");
						if (percent==100) {//ok.
							percent=99;
						}
						showProgressDialog("上传图片", "已上传"+percent+"%", null);
					}
					 @Override
					public void onStart() {
						super.onStart();
						isStart=true;
						showProgressDialog("上传图片", "已上传"+0+"%", new OnCancelListener() {
							
							@Override
							public void onCancel(DialogInterface arg0) {
								// TODO Auto-generated method stub
								isStart =false;
							}
						});
						//photoText.setText("开始上传。。。");
					}
					
					@Override
					public void onFailure(int arg0, Header[] arg1, byte[] arg2,
							Throwable arg3) {
						super.onFailure(arg0, arg1, arg2, arg3);
						
						
							failedText.setText("抱歉，上传头像失败，请重试.");
							showToast("上传图片失败!");
						
						
						
					}
					
				
					 @Override
					public void onFinish() {
						super.onFinish();

						cancelProgressDialog();
						isStart=false;
						//isSucc = false;
						
					}
				 
				});  
				 
				 
				 
			}
		});

		photo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			showAlert();
			}
		});
		
	
	}
    
	
	long startTime=0;
	protected void gotoUploadEnd(String url, String imgUrl) {
		Intent intent = new Intent();
		intent.putExtra("url", url);
		intent.putExtra("img_bitmap",imgUrl);
		setResult(RESULT_OK,intent);
		finish();
		
	}
	
	
	private UIController uiFreshController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode,Page page) {
			// TODO Auto-generated method stub
		}
		
		@Override
		public <E> void refreshUI(E content,Page page) {
			// TODO Auto-generated method stub
			showToast("上传头像成功!");
			//
			setResult(Activity.RESULT_OK);
			finish();
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return  new TypeToken<WebBirdboyJsonResponseContent<Account> >(){}.getType();
		}
	};
	
	

	private CookieStore cookies;
	
	boolean isStart=false;
	private Button upload;
	TextView photoText;
	protected int id;

	private TextView failedText;

	private UIController uiController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			String logoUrl = (String) content;
			Log.i(TAG, "logoUrl="+logoUrl);
			gotoUploadEnd(logoUrl,(String)(photo.getTag()));
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "upload_logo";
		}
		
		@Override
		public Type getEntityType() {
			return new TypeToken<WebBirdboyJsonResponseContent<String>>() {
			}.getType();
		}
	};


	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}



}
