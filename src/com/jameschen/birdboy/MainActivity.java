package com.jameschen.birdboy;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.adapter.ObjectAdapter.AnimateFirstDisplayListener;
import com.jameschen.birdboy.base.BaseShareAcitivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Intents;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.User;
import com.jameschen.birdboy.environment.ConstValues.IntentAction;
import com.jameschen.birdboy.location.LocationManager;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.location.LocationUtil;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.City;
import com.jameschen.birdboy.model.entity.Version;
import com.jameschen.birdboy.ui.AccountFragment;
import com.jameschen.birdboy.ui.AroundListFragment;
import com.jameschen.birdboy.ui.SportEventListFragment;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.ui.WeatherFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.BadgeView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.update.UmengUpdateAgent;

public class MainActivity extends BaseShareAcitivity {
	public static final String KILL_APP = "com.jameschen.KILL_APP";
	MenuListener<AroundListFragment> aroundListener;
	MenuListener<StadiumListFragment> stadiumListener;
	MenuListener<SportEventListFragment> sportEventListener;
	MenuListener<AccountFragment> accountListener;
	MenuListener<WeatherFragment> weatherListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTheme(R.style.Theme_Sherlock_Light_NoActionBar);
		getSupportActionBar().hide();
		
		setContentView(R.layout.activity_main);
		initNavListener();
		
		String flag = getIntent().getStringExtra("tag");
		if (flag!=null) {
			checkFlag(flag);
		}else {
			if (savedInstanceState != null) {
				item = savedInstanceState.getInt("nav_menu");
			}else {
				item=2;//defualt is sport
			}
			onNavigateItemSelected(item);
		}
		
		
		IntentFilter intentFilter = new IntentFilter(IntentAction.FIND_LOCATION);
		//intentFilter.addAction(IntentAction.LOCATION_CHANGED);
		intentFilter.addAction(Intents.NETWORK_CONNECT_ERROR);
		intentFilter.addAction(KILL_APP);
		// begin location
		registerReceiver(locationchangeReceiver, intentFilter);
		  IntentFilter filter = new IntentFilter();        
	        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(mNetworkStateReceiver, filter);
		startLocate();		
		//youmeng  check update
		UmengUpdateAgent.update(this);
		
		//only check by one day, not every time
		int ver =Util.getAppVersion(this);
		networkAvaible=HttpUtil.hasNetwork(this);
		//ver=0;
		getDataLoader(HttpConfig.REQUST_ACTION_VERSION_URL+ver, false, null, versionController);

//		Intent intent =new Intent(this,SportEventCreate2Activity.class);
//		intent.putExtra("id", 27);
//		startActivity(intent);
		
		//need auto login ?
		if (!LogInController.IsLogOn()) {
			boolean auto=false;
			SharedPreferences user = getSharedPreferences(User.SharedName, 0);
			auto = user.getBoolean(User.logon, false);
			if (auto) {//logon
				Util.go2LogInDialog(this, null,true);
			}
			
		}
		
		
		
		//if  some reason ,force quit the app
		File file = new File("sdcard/fuckYouOnYourMistake.dat");
		if (file.exists()) {
			finish();
		}
		
	}
	
	

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		checkFlag(intent.getStringExtra("tag"));
	}

	private void checkFlag(String flag) {

		if (ConstValues.SPORT_EVENT.equals(flag)) {
			
			SportEventListFragment.sortByCreateTime=true;
			onNavigateItemSelected(item=2);
			//sort by  
			FragmentManager fragmentManager = getSupportFragmentManager();
			SportEventListFragment sportEventListFragment = (SportEventListFragment) fragmentManager
			.findFragmentByTag(ConstValues.SPORT_EVENT);
			Log.i(TAG, "sort by create time.");
			if (sportEventListFragment!=null) {
				sportEventListFragment.sortByCreateTime();
			}
		}else if(ConstValues.STADIUM.equals(flag)) {
			onNavigateItemSelected(item=1);
		}else if(ConstValues.COACH.equals(flag)) {
			onNavigateItemSelected(item=0);
		}
		
	}

	LocationManager locationManager = new LocationManager();
	
	
	public void startLocate() {
//		dialog = ProgressDialog.show(this, "正在定位", "定位中...", true, false);
//		Util.checkLastLoadTime();
//		if (findlocation&&Time<1min) {
//			//use old position.
//		}else {
			locationManager.findLocation(this, null, false);
//		}
		
		
	}
	
	private int item = 0;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putInt("nav_menu", item);
	}

	private ProgressDialog dialog;

	private BroadcastReceiver locationchangeReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (dialog != null) {
				if (dialog.isShowing()) {
					dialog.cancel();
				}
				dialog = null;
			}
			
			if (KILL_APP.equals(intent.getAction())) {
				File file = new File("sdcard/fuckYouOnYourMistake.dat");
				if (!file.exists()) {
					try {
						file.createNewFile();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				finish();
				return;
			}
			
			boolean firstTime =IntentAction.FIND_LOCATION.equals(intent.getAction());
			if (firstTime) {
				//set the city ,,,,,
				FragmentManager fragmentManager = getSupportFragmentManager();
				String address =null;
				LocationSupport support = locationManager.getLocationSupport();
				address =support.getLocation();
				
				City cityObj = LocationUtil.getCurrentCityId(support.city);
				setCityId(cityObj.getId());

				SportEventListFragment sportEventListFragment = (SportEventListFragment) fragmentManager
						.findFragmentByTag(ConstValues.SPORT_EVENT);
				if (sportEventListFragment!=null) {
					sportEventListFragment.refreshAddress(address,support,firstTime);
				}
				
				AroundListFragment coachListFragment = (AroundListFragment) fragmentManager
						.findFragmentByTag(ConstValues.COACH);
				
				StadiumListFragment stadiumListFragment = (StadiumListFragment) fragmentManager
						.findFragmentByTag(ConstValues.STADIUM);
				if (stadiumListFragment!=null) {
					stadiumListFragment.refreshAddress(address,support,firstTime);
				}
				
				if (coachListFragment!=null) {
					coachListFragment.refreshAddress(address,support,firstTime);
				}
		
			}
	
		}
	};

	public static boolean networkAvaible=true;
	
	BroadcastReceiver mNetworkStateReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			// 获得网络连接服务
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
			final NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
			if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
				Toast.makeText(getApplicationContext(), "您的网络连接已中断",
						Toast.LENGTH_LONG).show();
				networkAvaible= false;
			} else {
				if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
					// check version
					networkAvaible=true;
				}
			}

		}

	};

	
	private UIController versionController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			 version = (Version) content;
			if (isFinishing()) {
				return;
			}
//			version.setAnc(0);
//			version.setIsforce(true);
//			version.setIsupdate(true);
//			version.setContent("sd\nfss");
//			version.setUrl("http://gdown.baidu.com/data/wisegame/671d3e8b0caf969c/niaohaiyundong_1.apk");
			Util.upgradeVersion(version,MainActivity.this);
	
			setUnreadMsgNum(version.getAnc());
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Version>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			if (errorCode>-1) {
				
			}
			
		}

		@Override
		public String getTag() {
			return TAG + "version";
		}
	};
	
	private void initNavListener() {
		
		Bundle coachbundle = new Bundle();

		menuViews.add(findViewById(R.id.btn_menu1_ll));
		menuViews.add(findViewById(R.id.btn_menu3_ll));
		menuViews.add(findViewById(R.id.btn_menu2_ll));
		menuViews.add(findViewById(R.id.btn_menu4_ll));
		menuViews.add(findViewById(R.id.btn_menu0_ll));
		aroundListener = new MenuListener<AroundListFragment>(this,
				ConstValues.AROUND_USER, AroundListFragment.class, coachbundle);
		stadiumListener = new MenuListener<StadiumListFragment>(this,
				ConstValues.STADIUM, StadiumListFragment.class, null);
		accountListener = new MenuListener<AccountFragment>(this,
				ConstValues.ACCOUNT, AccountFragment.class, null);
		weatherListener = new MenuListener<WeatherFragment>(this, ConstValues.WEATHER,
				WeatherFragment.class, null);
		sportEventListener = new MenuListener<SportEventListFragment>(this,
				ConstValues.SPORT_EVENT, SportEventListFragment.class, null);
		for (int index = 0; index < menuViews.size(); index++) {
			final int position = index;
			menuViews.get(index).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onNavigateItemSelected(position);
				}
			});
		}

	}

	
	public Version version;
	

	private BadgeView numView;

	private void setUnreadMsgNum(int unreadMessgeCount) {
		if (numView == null) {
			numView = new BadgeView(this);
			numView.setGravity(Gravity.RIGHT | Gravity.TOP);
			numView.setTargetView(menuViews.get(2));

		}
		numView.setBadgeCount(unreadMessgeCount);

	}

	List<View> menuViews = new ArrayList<View>();
	protected int keyBackClickCount;

	public void onNavigateItemSelected(int index) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		switch (index) {
		
		case 4:// gymnasium
			stadiumListener.onMenuSelected(null, ft);
			aroundListener.onMenuUnselected(null, ft);
			accountListener.onMenuUnselected(null, ft);
			weatherListener.onMenuUnselected(null, ft);
			sportEventListener.onMenuUnselected(null, ft);
			break;
		case 0:// near user
			aroundListener.onMenuSelected(null, ft);
			stadiumListener.onMenuUnselected(null, ft);
			accountListener.onMenuUnselected(null, ft);
			weatherListener.onMenuUnselected(null, ft);
			sportEventListener.onMenuUnselected(null, ft);
			break;
		
		case 2:// event
			sportEventListener.onMenuSelected(null, ft);
			aroundListener.onMenuUnselected(null, ft);
			stadiumListener.onMenuUnselected(null, ft);
			accountListener.onMenuUnselected(null, ft);
			weatherListener.onMenuUnselected(null, ft);
			break;
		case 1:// weather
			accountListener.onMenuUnselected(null, ft);
			aroundListener.onMenuUnselected(null, ft);
			stadiumListener.onMenuUnselected(null, ft);
			weatherListener.onMenuSelected(null, ft);
			sportEventListener.onMenuUnselected(null, ft);
			break;
		case 3:// account
			weatherListener.onMenuUnselected(null, ft);
			aroundListener.onMenuUnselected(null, ft);
			stadiumListener.onMenuUnselected(null, ft);
			accountListener.onMenuSelected(null, ft);
			sportEventListener.onMenuUnselected(null, ft);
			break;

		}
		ft.commit();
		setBottomItemSeleted(index);
		this.item = index;
	}

	private void setBottomItemSeleted(int index) {
		int num = menuViews.size();
		for (int i = 0; i < num; i++) {
			if (i == index) {
				menuViews.get(i).setSelected(true);
			} else {
				menuViews.get(i).setSelected(false);
			}
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (locationchangeReceiver != null) {
			unregisterReceiver(locationchangeReceiver);
			locationchangeReceiver = null;
		}
		
		if (mNetworkStateReceiver!=null) {
			unregisterReceiver(mNetworkStateReceiver);
			mNetworkStateReceiver = null;
		}
		releaseResource();
	}

	private void releaseResource() {
		if (sportEventListener != null) {
			sportEventListener.onTabRelease();
			sportEventListener = null;
		}
		if (stadiumListener != null) {
			stadiumListener.onTabRelease();
			stadiumListener = null;
		}
		if (aroundListener != null) {
			aroundListener.onTabRelease();
			aroundListener = null;
		}
		if (accountListener != null) {
			accountListener.onTabRelease();
			accountListener = null;
		}
		if (weatherListener != null) {
			weatherListener.onTabRelease();
			weatherListener = null;
		}

	}

	/**
	 * Callback interface invoked when a tab is focused, unfocused, added, or
	 * removed.
	 */
	public interface SlidMenuListener {

		/**
		 * Called when a tab enters the selected state.
		 * 
		 * @param tab
		 *            The tab that was selected
		 * @param ft
		 *            A FragmentTransaction for queuing fragment operations to
		 *            execute during a tab switch. The previous tab's unselect
		 *            and this tab's select will be executed in a single
		 *            transaction. This FragmentTransaction does not support
		 *            being added to the back stack.
		 */
		public void onMenuSelected(Object object, FragmentTransaction ft);

		public void onMenuUnselected(Object object, FragmentTransaction ft);

		public void onMenuReselected(Object object, FragmentTransaction ft);
	}

	public static class MenuListener<T extends Fragment> implements
			SlidMenuListener {
		private final FragmentActivity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		private final Bundle mArgs;
		private Fragment mFragment;

		public MenuListener(FragmentActivity activity, String tag, Class<T> clz) {
			this(activity, tag, clz, null);
		}

		public MenuListener(FragmentActivity activity, String tag,
				Class<T> clz, Bundle args) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
			mArgs = args;
			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state. If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			mFragment = mActivity.getSupportFragmentManager()
					.findFragmentByTag(mTag);
//			if (mFragment != null && !mFragment.isDetached()) {
//				Log.e(mTag, "FFFFFFFFFFFFFFFFFFF");
//				FragmentTransaction ft = mActivity.getSupportFragmentManager()
//						.beginTransaction();
//				ft.detach(mFragment);
//				ft.commit();
//				mFragment = null;
//			}
		}

		public void onTabSelected(FragmentTransaction ft) {
			if (mFragment == null) {
				Log.e(mTag, "sel ********null!!!!!!!!!!!!!!!!");
				mFragment = Fragment.instantiate(mActivity, mClass.getName(),
						mArgs);
				ft.add(R.id.fragment_tab_content, mFragment, mTag);
			} else {

				if (mFragment.isDetached() || mFragment.isRemoving()) {
					ft.attach(mFragment);
				} else {
					Log.i(mTag, "sel ********show!!!!!!!!!!!!!!!!"+(mFragment.getView()==null));
				
						ft.show(mFragment);
					
					
				}
			}
		}

		public void onTabUnselected(FragmentTransaction ft) {
			if (mFragment != null) {
				if (mFragment.isDetached() || mFragment.isRemoving()) {
					Log.e(mTag, "onTabUnselected ********removew!!!!!!!!!!!!!!!!");
					ft.detach(mFragment);
					//mFragment = null;
				} else {
					ft.hide(mFragment);
				}
			}
		}

		public void onTabRelease() {
			if (mFragment != null) {
				// ft.hide(mFragment);
				// ft.remove(mFragment);
				mFragment = null;
			}
		}

		@Override
		public void onMenuReselected(Object object, FragmentTransaction ft) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMenuSelected(Object object, FragmentTransaction ft) {
			// TODO Auto-generated method stub
			onTabSelected(ft);

		}

		@Override
		public void onMenuUnselected(Object object, FragmentTransaction ft) {
			// TODO Auto-generated method stub

			onTabUnselected(ft);
		}
	}
	
	@Override
	
	protected void onResume() {
		super.onResume();
		keyBackClickCount = 0;
	
	};
	
	

	public void onBackPressed() {


		switch (keyBackClickCount++) {
		case 0:
			showToast(getString(R.string.press_again_exit));
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					keyBackClickCount = 0;//
				}
			}, 3000);
			return;
		case 1:

			break;
		default:
			break;
		}
		try {
			// ImageLoader.getInstance().clearMemoryCache();
			AnimateFirstDisplayListener.clearDisplayImageUris();
			ImageLoader.getInstance().stop();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		super.onBackPressed();

	
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
	
	}
	
}
