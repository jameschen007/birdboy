package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import android.R.integer;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.internal.Utils;
import com.jameschen.birdboy.adapter.CategoryAdapter;
import com.jameschen.birdboy.adapter.ChildAreaAdapter;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.CoachDetailAdapter;
import com.jameschen.birdboy.adapter.FeedbackExpandableAdapter;
import com.jameschen.birdboy.adapter.SportEventCreateExpandableAdapter;
import com.jameschen.birdboy.adapter.StadiumAdapter;
import com.jameschen.birdboy.adapter.SportEventCreateExpandableAdapter.CommitListener;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.SportCreateBaseActivity;
import com.jameschen.birdboy.base.BaseActivity.LoadMode;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.SportType;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.location.LocationUtil;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.QueryBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebCoachDetail;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.model.entity.ChildArea;
import com.jameschen.birdboy.model.entity.City;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.utils.AreaSelectPopupWindow;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.FliterDialog;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.PopupWindowUtil;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.utils.UtilsUI;
import com.jameschen.birdboy.widget.MyListView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;


public class SportEventCreate0Activity extends SportCreateBaseActivity {

ExpandableListView createExpandableListView;
private String place;
private String position;
private int areaId=-1,childaredId=-1;
private TextView item;
private int buildid=-1;
PopupWindowUtil<City>cityPopupWindow = new PopupWindowUtil<City>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sportevent_create);
		Intent extraIntent= getIntent();
		Stadium stadium = extraIntent.getParcelableExtra(ConstValues.STADIUM);
		//
	
		if (stadium!=null) {
			chooseStadium(stadium);
			checkCreateSportActivity();
			finish();
			return;
		}
		City city = LocationUtil.getCurrentCityById(cityId);
		sportCityId =cityId;
		setTopBarRightBtnListener(city.getName(), new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				cityPopupWindow.showActionWindow(arg0, SportEventCreate0Activity.this, LocationUtil.Citys);
				cityPopupWindow.setItemOnClickListener(new PopupWindowUtil.OnItemClickListener() {

					@Override
					public void onItemClick(int index) {
							City cityObj = LocationUtil.Citys.get(index);
							setTopButtonRightText(cityObj.getName());
							sportCityId=cityObj.getId();
							refreshArea();
//							refreshWeather(cityObj, true,weatherTv);
						showProgressDialog("切换城市", "请稍候...", null);
					}
				});				
			}
		});
		setTopButtonRightRes(R.drawable.change_play_arrow0);
		initviews();
		setTitle("选择场馆(1/2)");
		
		refreshArea();
		
	}
int sportCityId =cityId;

	public void checkCreateSportActivity() {

//		place          String      活动地点                                       必填
//		position       String      活动的坐标位置                                  必填
//		buildid        int         活动所选的场馆                                  可选
//		area           int         活动的一级城市区域                               可选
//		childarea      int         活动的二级城市区域                               可选
		
		if (sportType==-1) {
			showToast("请选择运动类型");
			return;
		}
		if (place==null) {
			showToast("请选择场地");
			return;
		}
		Intent intent=new Intent(SportEventCreate0Activity.this,SportEventCreate1Activity.class);
		intent.putExtra("place",place);
		 intent.putExtra("sportType",sportType);
		intent.putExtra("position",position);
		Bundle data=getIntent().getBundleExtra("data");
		if (data!=null) {
			intent.putExtra("data", data);
		}
		
		if (areaId>-1) {
		 	intent.putExtra("areaId",areaId);
		}
		if (buildid>-1) {
		 	intent.putExtra("buildid",buildid);
		}
		if (childaredId>-1) {
		 	intent.putExtra("childAreaId",childaredId);
		}
		startActivityForResult(intent,ConstValues.REQUST_CREATE_SPORT);
	
	}
	
	public void loadData() {
		refreshArea();
	}
	
	protected void refreshArea() {
		Map<String, String> map = getPublicParamRequstMap();
		areas.clear();
		// load area。
		map.put("type", "" + 1);
		map.put("cityId", sportCityId+"");
		if (sportType>-1) {
			map.put("typedesc", "" + sportType);	
		}
		String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_SERACH_AREA_URL, map);
		
	
		if (sportType==-1) {
			initDataLoaderFromNativeCache(url, LoadMode.SAVE_CACHE,
					areaUIController,1000*3600);
		}else{
			getDataLoader(url, false, new Page(), areaUIController);
		}
			
				
	}

	

	private int sportType=-1;
	private  void showCategorySport( List<Category> categorySports) {
		
		final List<Category> sCategories =new ArrayList<Category>(categorySports);
		sCategories.remove(0);
		CustomSelectPopupWindow customSelectPopupWindow = new CustomSelectPopupWindow();
		customSelectPopupWindow.showActionWindow((View) (item.getParent()), this, sCategories);
		int width =UtilsUI.getWidth(getApplication())-Util.getPixByDPI(getBaseContext(), 20);
		customSelectPopupWindow.getPopupWindow().update(width, LayoutParams.WRAP_CONTENT);
		customSelectPopupWindow.setItemOnClickListener(new CustomSelectPopupWindow.OnItemClickListener() {
			

			@Override
			public void onItemClick(int index) {
				sportType = sCategories.get(index).getType();
				CategoryAdapter.attchReourceToView(item, sCategories.get(index));
				sportEventCreateExpandableAdapter.setSportType(sportType);
				//refresh stadium..
				refreshArea();
				
			}
		});
		
		customSelectPopupWindow.setOnDismissListener(new CustomSelectPopupWindow.OnDismissListener() {

			@Override
			public void onDismiss() {
			}
		});
	}
	
	private void initviews() {
		
		item = (TextView)findViewById(R.id.sportevent_create_title);
		
		
		
		((View)item.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showCategorySport(FliterDialog.getLikedSportCategories());
			}
		});
		
		sportEventCreateExpandableAdapter = new SportEventCreateExpandableAdapter(this,new SportEventCreateExpandableAdapter.CommitListener() {
			
			@Override
			public void done(Stadium stadium) {
				if (checkContent(stadium.getPositionDesc())) {
					chooseStadium(stadium);
					checkCreateSportActivity();
		}

	}
			
		})		;
		createExpandableListView = (ExpandableListView) findViewById(R.id.sportevent_create_expandableListView);
		createExpandableListView.setAdapter(sportEventCreateExpandableAdapter);
		createExpandableListView.setOnGroupClickListener(new OnGroupClickListener() {
			
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
//				 if (sign== -1) {  
//	                  // 展开被选的group  
//	              	createExpandableListView.expandGroup(groupPosition);  
//	                  // 设置被选中的group置于顶端  
//	              	createExpandableListView.setSelectedGroup(groupPosition);  
//	                  sign= groupPosition;  
//	              } else if (sign== groupPosition) {  
//	              	createExpandableListView.collapseGroup(sign);  
//	                  sign= -1;  
//	              } else {  
//	              	createExpandableListView.collapseGroup(sign);  
//	                  // 展开被选的group  
//	              	createExpandableListView.expandGroup(groupPosition);  
//	                  // 设置被选中的group置于顶端  
//	              	createExpandableListView.setSelectedGroup(groupPosition);  
//	                  sign= groupPosition;  
//	              }  
				if (sportType==-1) {
	    			showToast("请选择运动类型");
	    			return false;
	    		}
				if (groupPosition==0) {//last one
					Util.go2Map(SportEventCreate0Activity.this, ConstValues.MAP_VAL);
				}else if (groupPosition==2) {
					showArea(v);
				}
				return false;
			}
		});
		createExpandableListView.setOnGroupExpandListener(new OnGroupExpandListener() {  
			  
            @Override  
            public void onGroupExpand(int groupPosition) {  
                for (int i = 0; i < sportEventCreateExpandableAdapter.getGroupCount()-1; i++) {  
                    if (groupPosition != i) {  
                    	createExpandableListView.collapseGroup(i);  
                    }  
                }  
  
            }  
      	     
  
        });  
		
		Bundle bundle =getIntent().getBundleExtra("data");
		if (bundle!=null) {
			sportType=bundle.getInt("sportType", -1);	
		}
		
		Log.i("sport_type", "sport_type=="+sportType);
		
		Category category;
			if (sportType==-1) {
				if (LogInController.IsLogOn()) {
					sportType = LogInController.currentAccount.getMylike();
					 category=SportType.getSportCatogryById(sportType);
					
				}else {
					//set default
					category = SportType.initSportList().get(1);
					sportType = category.getType();
				}
			}else {
				 category=SportType.getSportCatogryById(sportType);
			}
			
			CategoryAdapter.attchReourceToView(item, category);
			sportEventCreateExpandableAdapter.setSportType(sportType);
				
	}
	private List<Area> areas = new ArrayList<Area>();
	protected AreaSelectPopupWindow areaPopupWindow = new AreaSelectPopupWindow();

	private void showArea(View v) {
		areaPopupWindow.setWidth(UtilsUI.getWidth(getApplication())-Util.getPixByDPI(v.getContext(), 20));
		areaPopupWindow.showActionWindow(v, this, areas);
		areaPopupWindow
				.setItemOnClickListener(new AreaSelectPopupWindow.OnItemClickListener() {

					@Override
					public void onItemClick(int index) {
						Area area = areas.get(index);
						if (area.getId()==-1) {//all
							Map<String, String> map = getPublicParamRequstMap();
							//	http://birdboy.cn/solr/building/select AND positionarea:4 AND childarea:411?sort=&indent=true&wt=json&appVersion=1&appAgent=android
								String queryUrl=null;
								if (sportType > -1) {
									queryUrl = StadiumListFragment.parseParam(queryUrl, "typedesc", sportType + "");
								}
								map.put("q", queryUrl);
								map.put("wt", "json");
								map.put("indent", "true");
								String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_STADIUM_SEARCH_URL, map);
								
								// TODO Auto-generated method stub

//								area = mArea.getId();
//								childArea = childAreas.get(arg2).getId();
//								startSearch(keyword, map);
								showProgressDialog("正在查询", "请稍候...",
										null);
//								areaPopupWindow.dismiss();
								getDataLoader(url, false,
										null, uiFreshController);
								return;
							}
						List<ChildArea> childAreaLists = area.getChilds();				
						if (childAreaLists.size() > 0) {
							if (childAreaLists.get(0).getId() != -1) {// not
																		// contains
																		// all
								ChildArea allChildArea = new ChildArea();
								allChildArea.setId(-1);
								allChildArea.setName("全部");
								allChildArea.setCount(area.getCount());

								childAreaLists.add(0, allChildArea);
							}
						}else {
							ChildArea allChildArea = new ChildArea();
							allChildArea.setId(-1);
							allChildArea.setName("全部");
							allChildArea.setCount(area.getCount());
							childAreaLists.add(0, allChildArea);
						}
						
						showChildArea(childAreaLists, area);
						}
				});
		areaPopupWindow
				.setOnDismissListener(new AreaSelectPopupWindow.OnDismissListener() {

					@Override
					public void onDismiss() {
						
					}
				});
		
	}
	private void showChildArea(final List<ChildArea> childAreas,
			final Area mArea) {
		areaPopupWindow.getChildListView().setAdapter(
				new ChildAreaAdapter(this, childAreas));
		areaPopupWindow.showChildArea();
		areaPopupWindow.getChildListView().setOnItemClickListener(
				new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						Map<String, String> map = getPublicParamRequstMap();
					//	http://birdboy.cn/solr/building/select AND positionarea:4 AND childarea:411?sort=&indent=true&wt=json&appVersion=1&appAgent=android
						int areaID = mArea.getId();
						String queryUrl=null;
						if (areaID > -1) {
							queryUrl = StadiumListFragment.parseParam(queryUrl, "positionarea", areaID + "",true);

						}

						int childArea=childAreas.get(arg2).getId();
						if (childArea > -1) {
							queryUrl = StadiumListFragment.parseParam(queryUrl, "childarea", childArea + "");
						}
						if (sportType > -1) {
							queryUrl = StadiumListFragment.parseParam(queryUrl, "typedesc", sportType + "");
						}
						map.put("q", queryUrl);
						map.put("wt", "json");
						map.put("indent", "true");
						String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_STADIUM_SEARCH_URL, map);
						
						// TODO Auto-generated method stub

//						area = mArea.getId();
//						childArea = childAreas.get(arg2).getId();
//						startSearch(keyword, map);
						showProgressDialog("正在查询", "请稍候...",
								null);
//						areaPopupWindow.dismiss();
						getDataLoader(url, false,
								null, uiFreshController);

					}

				});

	}
	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
						if (!isFinishing()) {
							showStadiumDialog((List<Stadium>) content);
						}

		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<QueryBirdboyJsonResponseContent<Stadium>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			if(errorCode>-1)
			showToast(getString(R.string.network_error_time_out));
		
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "activity_stadiums";
		}
	};
	
	int lastPosition = -1;	
	Stadium selecStadium;
	protected void showStadiumDialog(final List<Stadium> content) {
		if (content.size()==0) {
			return ;
		}
		selecStadium=null;
		lastPosition=-1;
		
		LayoutInflater inflater = getLayoutInflater();
		   View layout = inflater.inflate(R.layout.sportevent_stadium_list,null);
		   ListView listView =(ListView) layout.findViewById(R.id.sportevent_stadium_list_view);
		   StadiumAdapter stadiumAdapter =new StadiumAdapter(this);
		   stadiumAdapter.setObjectList(content);
		   stadiumAdapter.setShowDistance(false);
		   listView.setAdapter(stadiumAdapter);
		   //show distance... 
		   
			final AlertDialog mStadiumDilog = new AlertDialog.Builder(this).setTitle("选择场馆").setView(layout)
				     .setPositiveButton("下一步", new AlertDialog.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (selecStadium!=null&&lastPosition>=0) {
								dialog.dismiss();
								if (checkContent(selecStadium.getPositionDesc())) {
									chooseStadium(selecStadium);
									checkCreateSportActivity();
						}

							}else {
								showToast("请选择一个场馆");
							}
						
						}
					})
				     .setNegativeButton("取消", null).show();
		   listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				selecStadium = 	content.get(position);
				if (lastPosition>=0) {
					parent.getChildAt(lastPosition).setBackgroundResource(R.drawable.list_selector);
				}
				view.setBackgroundColor(getResources().getColor(R.color.blue2));
				lastPosition=position;
			}
		});
		   
			
			
	}
	protected void chooseStadium(Stadium stadium) {
		place = stadium.getPositionDesc();
		position = stadium.getPosition();
		areaId = stadium.getPositionarea();
		childaredId = stadium.getChildarea();
		buildid =stadium.getId();		
	}

	@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub
			super.onActivityResult(requestCode, resultCode, data);
			switch (requestCode) {
			case ConstValues.MAP_VAL:
			{
				if (resultCode== RESULT_OK) {
					Stadium stadium = data.getParcelableExtra(ConstValues.STADIUM);
					CommitListener commitListener =sportEventCreateExpandableAdapter.getCommitListener();
					commitListener.done(stadium);
				}
			}
				break;

			default:
				break;
			}
	}
	
	
	private int sign= -1;//控制列表的展开  
	private SportEventCreateExpandableAdapter sportEventCreateExpandableAdapter ;
	
	
	private boolean checkContent(CharSequence content) {
		// TODO Auto-generated method stub
		if (TextUtils.isEmpty(content)) {
			showToast("无法获取详细地址！");
			return false;
		}
		return true;
	}
	

	
	private UIController areaUIController = new UIController() {

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			areas.clear();
		}

		@Override
		public <E> void refreshUI(E content, Page page) {
			areas.clear();
			List<Area> areaLists = (List<Area>) content;
		
			Area area = new Area();
			area.setId(-1);
			area.setName("全部区域");
			int count = 0;
			for (Area areaItem : areaLists) {
				count += areaItem.getCount();
			}
			area.setCount(count);
			areaLists.add(0, area);
			areas = areaLists;
		
			//sportEventCreateExpandableAdapter.setAreas(areaLists);
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return "Stadium_Area";
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<List<Area>>>() {
			}.getType();
		}
	};
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}



}
