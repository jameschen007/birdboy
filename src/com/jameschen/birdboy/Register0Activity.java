package com.jameschen.birdboy;
import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.RegisterBaseActivity;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Register;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.User;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.Code;
import com.jameschen.birdboy.utils.AES4all;
import com.jameschen.birdboy.utils.CheckValid;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.SimpleBase64Encoder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;



public class Register0Activity extends RegisterBaseActivity {

	private EditText phone;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
			initViews();	
			setTitle("验证手机 (1/4)");
			setTopBarRightBtnListener(new OnClickListener() {			
				@Override
				public void onClick(View v) {
					
					register(phone.getText());
					
				}
			});
			initDataLoaderFromNativeCache(null, LoadMode.NONE, uiFreshController);		
	}
	
	private void initViews() {
		// TODO Auto-generated method stub
		phone = (EditText) findViewById(R.id.edtMobile);
		phone.setText(getDevPhoneNum());
		findViewById(R.id.btn_Register).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			}
		});
	}
    private  String getDevPhoneNum() {
	// TODO Auto-generated method stub
		TelephonyManager tm = (TelephonyManager) this
				.getSystemService(Context.TELEPHONY_SERVICE);
		String tel = tm.getLine1Number();
		if (tel == null) {// by message send to server.10010 or 10086 or other.

		} else {
			if (tel.startsWith("+86")) {
				tel = tel.substring(3);
			}
		}
		return tel;
}
    
	private void register(CharSequence phone) {
		boolean nextCheckStep = CheckValid.checkPhoneValid(this,phone, 11, 11);
		if (nextCheckStep) {
			closeInputMethod();			
			showProgressDialog("获取验证码", "请稍候...", null);
			Map<String, String> map = getPublicParamRequstMap();
			map.put("mobile", phone.toString());
		  getDataLoader(HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_REG_GET_SMS_VERIFY_CODE_URL, map), false, null, uiFreshController);
		
		}
	}
	

	private UIController uiFreshController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode,Page page) {
			// TODO Auto-generated method stub
		}
		
		@Override
		public <E> void refreshUI(E content,Page page) {
			//showToast(msg)
			Intent intent =new Intent(Register0Activity.this,Register1Activity.class);
			intent.putExtra("mobile", phone.getText().toString());
			Code code =(Code) content;
			intent.putExtra("code", code.getCode()+"");
			startActivityForResult(intent, Register.REQUST_REGISTER);
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return  new TypeToken<WebBirdboyJsonResponseContent<Code> >(){}.getType();
		}
	};
	
	
	public static String[]  readAccountDataFromPreference(Context context) {
		// TODO Auto-generated method stub
		// 如果文件不存在，则进行创建
		SharedPreferences userPref = context.getSharedPreferences(User.SharedName, 0);
		// 取出保存的NAME，取出改字段名的值，不存在则创建默认为空
		String name = userPref.getString(User.account, null); // 取出保存的 NAME
		String password = userPref.getString(User.password, null); // 取出保存的 uid
		String[] accounts =new String[2];
		if (name==null) {
			return accounts;
		}
		try {
			accounts[0]=new String(AES4all
					.decryptAESECB(SimpleBase64Encoder.decode(name)));
			if (password!=null) {
				accounts[1]=new String(AES4all
						.decryptAESECB(SimpleBase64Encoder.decode(password)));
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return accounts;
	}
	
	public static void removeUserPasswordFromPreference(Context context) {
		// TODO Auto-generated method stub
		SharedPreferences user = context.getSharedPreferences(User.SharedName, 0);
		try {
			user.edit()
					.remove(User.password).commit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public static void saveUserToPreference(Context context,String account, String pswd, boolean logon) {
		// TODO Auto-generated method stub
		SharedPreferences user = context.getSharedPreferences(User.SharedName, 0);
		try {
			user.edit()
					.putString(
							User.account,
							new String(SimpleBase64Encoder.encode(AES4all
									.encryptAESECB(account.getBytes())))).commit();
				if (!TextUtils.isEmpty(pswd)) {
					user.edit().putString(
							User.password,new String(SimpleBase64Encoder.encode(AES4all
									.encryptAESECB(pswd.getBytes())))).
									putBoolean(User.logon, logon).commit();
				}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

}
