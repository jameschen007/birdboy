package com.jameschen.birdboy.network;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.jameschen.birdboy.utils.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;
import com.jameschen.birdboy.BirdboyApplication;

public class BirdBoyStringRequest extends StringRequest {
	private Map<String, String> map;

	public BirdBoyStringRequest(Map<String, String> requstMap, int method,
			String url, Listener<String> listener, ErrorListener errorListener) {
		super(method, url, listener, errorListener);
		// TODO Auto-generated constructor stub
		map = requstMap;
	}


	
	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		// TODO Auto-generated method stub
		if (map != null) {
			Log.i("requestPostMap", map.toString());
			return map;
		}
		return super.getParams();
	}
	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		// TODO Auto-generated method stub
		return super.parseNetworkResponse(response);
		//response.statusCode;
	
	}
	
//	@Override
//	protected Response<String> parseNetworkResponse(NetworkResponse response) {
//		 // since we don't know which of the two underlying network vehicles
//        // will Volley use, we have to handle and store session cookies manually
//        BirdboyApplication.getInstance().checkSessionCookie(response.headers);
//		return super.parseNetworkResponse(response);
//	}
	
	  /* (non-Javadoc)
     * @see com.android.volley.Request#getHeaders()
     */
//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//        Map<String, String> headers = super.getHeaders();
//
//        if (headers == null
//                || headers.equals(Collections.emptyMap())) {
//            headers = new HashMap<String, String>();
//        }
//
//        BirdboyApplication.getInstance().addSessionCookie(headers);
//
//        return headers;
//    }
	
}
