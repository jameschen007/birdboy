package com.jameschen.birdboy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.entity.ActivityInfo;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.MyActiviyInfoListFragment;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.widget.CustomViewPager;
import com.viewpagerindicator.IconPagerAdapter;
import com.viewpagerindicator.TabPageIndicator;

public class MyActivityInfo extends BaseActivity {


	private CustomViewPager pager;
	private TabPageIndicator indicator;
	private int categoryItemNum=3;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info_main);
		initViews();

	}
	MyActivityInfoListAdapter myActivityInfoListAdapter;
	private void initViews() {
		setTitle("我的活动");
		pager = (CustomViewPager) findViewById(R.id.vPager);
		indicator = (TabPageIndicator) findViewById(R.id.indicator);
		pager.setAdapter(myActivityInfoListAdapter=new MyActivityInfoListAdapter(getSupportFragmentManager()));
		 pager.setOffscreenPageLimit(3);// just cache views for 3.
		indicator.setViewPager(pager);
		 indicator.setOnPageChangeListener(new OnPageChangeListener() {
				
			 @Override
			 public void onPageSelected(int position) {
			 // TODO Auto-generated method stub
			 // add text tab color for selected state.
			 setCurrentSelectBackGround(position);
			 }
			
			 @Override
			 public void onPageScrolled(int arg0, float arg1, int arg2) {
			
			 }
			
			 @Override
			 public void onPageScrollStateChanged(int state) {
			
			 }
			 });
			indicator.setCurrentItem(0);
			setCurrentSelectBackGround(0);
	}
	private void setCurrentSelectBackGround(int index) {
		// TODO Auto-generated method stub
		for (int i = 0; i < categoryItemNum; i++) {
			if (index == i) {
				(indicator.getTabView(i)).setTextColor(getResources().getColor(R.color.text_foucs));
			} else {
				(indicator.getTabView(i)).setTextColor(getResources().getColor(R.color.gray));
			}
		}
		checkDataIsLoad(index);
	}
	
	private void checkDataIsLoad(int index) {
		if (myActivityInfoListAdapter!=null) {
			MyActiviyInfoListFragment myActiviyInfoListFragment =(MyActiviyInfoListFragment) myActivityInfoListAdapter.getCurrentFragment(index);
			if (myActiviyInfoListFragment!=null) {
				myActiviyInfoListFragment.loadData();
			}
		}
	}

	static class MyActivityInfoListAdapter extends FragmentStatePagerAdapter implements
	IconPagerAdapter {
		
		private int categoryId = -1;

// private SparseArray<Fragment> fragmentSparseArray = new
// SparseArray<Fragment>();
	private ArrayList<Fragment> myActiviyInfoListFragments = new ArrayList<Fragment>();

	public MyActivityInfoListAdapter(FragmentManager fm) {
		super(fm);
		fragmentManager = fm;
		}

	public Fragment getCurrentFragment(int index) {
		if (index<myActiviyInfoListFragments.size()) {
			return myActiviyInfoListFragments.get(index);
		}
		return null;
		}

	FragmentManager fragmentManager;
	@Override
	public Fragment getItem(int position) {
	// Fragment fragment = ProductListFragment.newInstance(categoryId,
	// categoryItems.get(position).getId(),
	// new ArrayList<ProductInfo>());
	
	Fragment fragment = MyActiviyInfoListFragment.newInstance(ConstValues.MINE_VAL,position,new ArrayList<ActivityInfo>());
	if (!myActiviyInfoListFragments.contains(fragment)) {
		myActiviyInfoListFragments.add(fragment);
	}
	
	return fragment;

}

String titles[]=new String[]{"报名的","创建的","收藏的"};
@Override
public CharSequence getPageTitle(int position) {
	return titles[position];
}

@Override
public int getIconResId(int index) {
	return 0;// no resource need load
}

@Override
public int getCount() {
	return 3;/* categoryItems.size(); */
}

private void clear() {

}

}

}
