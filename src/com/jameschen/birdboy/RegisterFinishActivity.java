package com.jameschen.birdboy;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.jameschen.birdboy.base.BaseBindActivity;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.plugin.AuthBindController;
import com.jameschen.plugin.BindModel;

public class RegisterFinishActivity extends BaseBindActivity {

	private Button login;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_finish);
		
		
			initViews();	
			setTitle("注册完成 ");
			setTopBarBtnVisiable(View.GONE);
			setTopBarLeftBtnVisiable(View.GONE);
	}
	
	private void initViews() {
		// TODO Auto-generated method stub
		login =(Button)findViewById(R.id.btn_login);
		login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				goToMyCenter();
			}
		});

		bindSinaText=(TextView) findViewById(R.id.bind_weibo);
		bindTqqText=(TextView)findViewById(R.id.bind_qq);
		bindSinaText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Util.go2LogInDialog(RegisterFinishActivity.this,new OnLogOnListener() {
					
					@Override
					public void logon() {
						doWeiboAuth(new AuthBindController.OnbindResultListener() {
							
							@Override
							public void bindSucc(BindModel bindModel) {
								//bind no need update bind status
								updateUserBind(bindModel);
							}
							
							@Override
							public void bindFailed(String reason) {
								// TODO Auto-generated method stub
								
							}
						});
						
					}
				},true);
			
			}
		});
		bindTqqText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				
				Util.go2LogInDialog(RegisterFinishActivity.this,new OnLogOnListener() {
					
					@Override
					public void logon() {
						doQQAuth(new AuthBindController.OnbindResultListener() {
						
						@Override
						public void bindSucc(BindModel bindModel) {
							
							//bind
							updateUserBind(bindModel);
						}
						
						

						@Override
						public void bindFailed(String reason) {
							// TODO Auto-generated method stub
							
						}
					});}
				},true);
			
			}
		});
		
	}

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(arg0, arg1, arg2);
    	
    }
	
    private void updateUserBind(BindModel bindModel) {
		// TODO Auto-generated method stub
    	if (bindModel.fromtype==BindModel.QQ) {
			bindTqqText.setEnabled(false);
			bindTqqText.setBackgroundResource(R.drawable.zhuce_qq);
			bindTqqText.setText("已绑定");
			bindTqqText.setTextColor(Color.WHITE);
    	}else if (bindModel.fromtype==BindModel.SINA) {
			bindSinaText.setEnabled(false);
			bindSinaText.setText("已绑定");
			bindSinaText.setTextColor(Color.WHITE);
			bindSinaText.setBackgroundResource(R.drawable.zhuce_sina);
		}
		
	}
    
	    
	protected void goToMyCenter() {
		setResult(RESULT_OK);
		finish();		
	}
	


	TextView bindSinaText,bindTqqText;
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}



}
