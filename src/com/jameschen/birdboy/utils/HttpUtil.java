package com.jameschen.birdboy.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.SocketHttpServerConnection;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.environment.Config.HttpConnect;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.jameschen.birdboy.utils.Log;
import android.widget.Toast;

/**
 * HttpUtil Class Capsule Most Functions of Http Operations
 * 
 * @author sfshine
 * 
 */
public class HttpUtil {
	private static Header[] headers = new BasicHeader[1];
	private static String TAG = "HTTPUTIL";
	private static int TIMEOUT = 15 * 1000;
	/**
	 * Your header of http op
	 * 
	 * @return
	 */
	static {

		headers[0] = new BasicHeader("User-Agent", "Android");
		// "Mozilla/4.0 (compatible; MSIE 5.0; Windows XP; DigExt)"

	}

	private static AbstractHttpClient mHttpClient;

	public static boolean isInitClientInstance() {
		return mHttpClient != null;
	}

	public static Map<String, String> createEditAccountMap(BaseActivity activity){
		Map<String, String> map = activity.getPublicParamRequstMap();
		map.put("sex", ""+LogInController.currentAccount.getSex());
		map.put("password", LogInController.currentAccount.getPassword());
		map.put("gcode", 1234+"");	
		return map;
	}
	
	public static AbstractHttpClient getHttpClientInstance() {
		synchronized (HttpUtil.class) {
			if (mHttpClient == null) {
				mHttpClient = new DefaultHttpClient();

			}
			return mHttpClient;
		}
	}

	public static boolean delete(String murl) throws Exception {
		URL url = new URL(murl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("DELETE");
		conn.setConnectTimeout(5000);
		if (conn.getResponseCode() == 204) {

			MLog.e(conn.toString());
			return true;
		}
		MLog.e(conn.getRequestMethod());
		MLog.e(conn.getResponseCode() + "");
		return false;
	}

	/**
	 * Op Http get request
	 * 
	 * @param url
	 * @param map
	 *            Values to request
	 * @return
	 */
	static public String get(String url) {
		return get(url, null);

	}

	public static String getRequestMapUrlStr(String url, Map<String, String> map) {
		if (null != map) {
			int i = 0;
			for (Map.Entry<String, String> entry : map.entrySet()) {
				String value = null;
				try {
					if (entry.getValue()!=null) {
						value = URLEncoder.encode(entry.getValue(), "UTF-8");
					}
					
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					value = entry.getValue();// use default code
					e.printStackTrace();
				}
				// value = entry.getValue();
				Log.i(TAG, entry.getKey() + "=>" + value);
				if (i == 0) {
					url = url + "?" + entry.getKey() + "=" + value;
				} else {
					url = url + "&" + entry.getKey() + "=" + value;
				}

				i++;

			}
		}
		Log.i(TAG, url);
		return url;
	}

	static public String get(String url, HashMap<String, String> map) {

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), TIMEOUT);
		HttpConnectionParams.setSoTimeout(client.getParams(), TIMEOUT);
		ConnManagerParams.setTimeout(client.getParams(), TIMEOUT);
		String result = "ERROR";
		if (null != map) {
			int i = 0;
			for (Map.Entry<String, String> entry : map.entrySet()) {
				String value = null;
				try {
					value = URLEncoder.encode(entry.getValue(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					value = entry.getValue();// use default code
					e.printStackTrace();
				}
				// value = entry.getValue();
				Log.i(TAG, entry.getKey() + "=>" + value);
				if (i == 0) {
					url = url + "?" + entry.getKey() + "=" + value;
				} else {
					url = url + "&" + entry.getKey() + "=" + value;
				}

				i++;

			}
		}

		// add 3 times to check network is OK?

		HttpGet get = new HttpGet(url);
		get.setHeaders(headers);
		Log.i(TAG, url);

		try {

			HttpResponse response = client.execute(get);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// setCookie(response);
				result = EntityUtils.toString(response.getEntity(), "UTF-8");

			} else {
				result = EntityUtils.toString(response.getEntity(), "UTF-8")
						+ response.getStatusLine().getStatusCode() + "ERROR";
			}
		} catch (ConnectTimeoutException e) {
			result = HttpConnect.HTTP_CONNECT_TIMEOUT;
		} catch (SocketTimeoutException e) {
			// TODO: handle exception
			result = HttpConnect.HTTP_CONNECT_TIMEOUT;
		} catch (HttpHostConnectException e) {
			// TODO: handle exception
			result = HttpConnect.HTTP_OTHER_ERROR;
		}
		// catch (NoRouteToHostException e) {
		// // TODO: handle exception
		// result = HttpConnect.HTTP_CONNECT_TIMEOUT;
		// }

		catch (UnknownHostException e) {
			// TODO: handle exception
			result = HttpConnect.HTTP_UNKOWN_HOST_ERROR;
		}

		catch (Exception e) {
			result = HttpConnect.HTTP_OTHER_ERROR;
			e.printStackTrace();

		} finally {
			get.abort();
			get = null;
		}
		Log.i(TAG, "result =>" + result);

		return result;
	}

	/**
	 * Op Http post request , "404error" response if failed
	 * 
	 * @param url
	 * @param map
	 *            Values to request
	 * @return
	 */

	static public String post(String url, HashMap<String, String> map) {

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), TIMEOUT);
		HttpConnectionParams.setSoTimeout(client.getParams(), TIMEOUT);
		ConnManagerParams.setTimeout(client.getParams(), TIMEOUT);
		HttpPost post = new HttpPost(url);
		MLog.i(TAG, url);
		post.setHeaders(headers);
		String result = "ERROR";
		ArrayList<BasicNameValuePair> pairList = new ArrayList<BasicNameValuePair>();
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				String value = null;
				try {
					value = URLEncoder.encode(entry.getValue(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					value = entry.getValue();// use default code
					e.printStackTrace();
				}
				// value = entry.getValue();
				Log.i(TAG, entry.getKey() + "=>" + value);
				BasicNameValuePair pair = new BasicNameValuePair(
						entry.getKey(), value);
				pairList.add(pair);
			}

		}
		//
		try {
			HttpEntity entity = new UrlEncodedFormEntity(pairList, "UTF-8");
			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

				result = EntityUtils.toString(response.getEntity(), "UTF-8");

			} else {
				result = EntityUtils.toString(response.getEntity(), "UTF-8")
						+ response.getStatusLine().getStatusCode() + "ERROR";
			}

		} catch (ConnectTimeoutException e) {
			result = HttpConnect.HTTP_CONNECT_TIMEOUT;
		} catch (SocketTimeoutException e) {
			// TODO: handle exception
			result = HttpConnect.HTTP_CONNECT_TIMEOUT;
		} catch (HttpHostConnectException e) {
			// TODO: handle exception
			result = HttpConnect.HTTP_OTHER_ERROR;
		}
		// catch (NoRouteToHostException e) {
		// // TODO: handle exception
		// result = HttpConnect.HTTP_CONNECT_TIMEOUT;
		// }

		catch (UnknownHostException e) {
			// TODO: handle exception
			result = HttpConnect.HTTP_UNKOWN_HOST_ERROR;
		}

		catch (Exception e) {
			result = HttpConnect.HTTP_OTHER_ERROR;
			e.printStackTrace();

		} finally {
			post.abort();
			post = null;
		}
		Log.i(TAG, "result =>" + result);
		return result;
	}

	/**
	 * �Զ����http�����������ΪDELETE PUT�ȶ���GET
	 * 
	 * @param url
	 * @param params
	 * @param method
	 * @throws IOException
	 */

	public static String customrequest(String url,
			HashMap<String, String> params, String method) {
		try {

			URL postUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) postUrl
					.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setConnectTimeout(5 * 1000);

			conn.setRequestMethod(method);
			conn.setUseCaches(false);
			conn.setInstanceFollowRedirects(true);
			conn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			conn.setRequestProperty("User-Agent",
					"Mozilla/4.0 (compatible; MSIE 5.0; Windows XP; DigExt)");

			conn.connect();
			OutputStream out = conn.getOutputStream();
			StringBuilder sb = new StringBuilder();
			if (null != params) {
				int i = params.size();
				for (Map.Entry<String, String> entry : params.entrySet()) {
					if (i == 1) {
						sb.append(entry.getKey() + "=" + entry.getValue());
					} else {
						sb.append(entry.getKey() + "=" + entry.getValue() + "&");
					}

					i--;
				}
			}
			String content = sb.toString();
			out.write(content.getBytes("UTF-8"));
			out.flush();
			out.close();
			InputStream inStream = conn.getInputStream();
			String result = inputStream2String(inStream);
			Log.i(TAG, "result>" + result);
			conn.disconnect();
			return result;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * �����ϸ�����get������������������� �������Ҳ�����Զ�������
	 * 
	 * @param url
	 * @param method
	 * @throws Exception
	 */

	public static String customrequestget(String url,
			HashMap<String, String> map, String method) {

		if (null != map) {
			int i = 0;
			for (Map.Entry<String, String> entry : map.entrySet()) {

				if (i == 0) {
					url = url + "?" + entry.getKey() + "=" + entry.getValue();
				} else {
					url = url + "&" + entry.getKey() + "=" + entry.getValue();
				}

				i++;
			}
		}
		try {

			URL murl = new URL(url);
			System.out.print(url);
			HttpURLConnection conn = (HttpURLConnection) murl.openConnection();
			conn.setConnectTimeout(5 * 1000);
			conn.setRequestMethod(method);

			conn.setRequestProperty("User-Agent",
					"Mozilla/4.0 (compatible; MSIE 5.0; Windows XP; DigExt)");

			InputStream inStream = conn.getInputStream();
			String result = inputStream2String(inStream);
			Log.i(TAG, "result>" + result);
			conn.disconnect();
			return result;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * �ϴ�����ͼƬ
	 * @return 
	 */
	public static String get(String actionUrl, Map<String, String> params,
			Map<String, File> files) throws IOException {

		String BOUNDARY = java.util.UUID.randomUUID().toString();
		String PREFIX = "--", LINEND = "\r\n";
		String MULTIPART_FROM_DATA = "multipart/form-data";
		String CHARSET = "UTF-8";

		URL uri = new URL(actionUrl);
		HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
		conn.setReadTimeout(5 * 1000); // ������ʱ��
		conn.setDoInput(true);// ��������
		conn.setDoOutput(true);// �������
		conn.setUseCaches(false); // ������ʹ�û���
		conn.setRequestMethod("GET");
		conn.setRequestProperty("connection", "keep-alive");
		conn.setRequestProperty("Charsert", "UTF-8");
		conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA
				+ ";boundary=" + BOUNDARY);

		// ������ƴ�ı����͵Ĳ���
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			sb.append(PREFIX);
			sb.append(BOUNDARY);
			sb.append(LINEND);
			
			sb.append("Content-Disposition: form-data; name=\""
					+ entry.getKey() + "\"" + LINEND);
			sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
			sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
			sb.append(LINEND);
			String value = null;
			try {
				value = URLEncoder.encode(entry.getValue(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				value = entry.getValue();// use default code
				e.printStackTrace();
			}
			sb.append(value);
			sb.append(LINEND);
		}

		DataOutputStream outStream = new DataOutputStream(
				conn.getOutputStream());
		outStream.write(sb.toString().getBytes());
		// �����ļ����
		if (files != null) {
			for (Map.Entry<String, File> file : files.entrySet()) {

				StringBuilder sb1 = new StringBuilder();
				sb1.append(PREFIX);
				sb1.append(BOUNDARY);
				sb1.append(LINEND);
				sb1.append("Content-Disposition: form-data; name=\"source\"; filename=\""
						+ file.getValue().getName() + "\"" + LINEND);
				sb1.append("Content-Type: image/pjpeg; " + LINEND);
				sb1.append(LINEND);
				outStream.write(sb1.toString().getBytes());

				InputStream is = new FileInputStream(file.getValue());
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
					outStream.write(buffer, 0, len);
				}

				is.close();
				outStream.write(LINEND.getBytes());
			}

			// ��������־
			byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
			outStream.write(end_data);
			outStream.flush();
			// �õ���Ӧ��
			 int res = conn.getResponseCode();
			InputStream inStream = conn.getInputStream();
			String result = inputStream2String(inStream);
			
			outStream.close();
			conn.disconnect();
			 if (res == 200) {
				 return result;
			 }else {
				return null;
			}
		}
		// return in.toString();
		return null;
	}

	/**
	 * isתString
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static String inputStream2String(InputStream in) throws IOException {
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			out.append(new String(b, 0, n));
		}
		return out.toString();
	}

	/**
	 * check net work
	 * 
	 * @param context
	 * @return
	 */
	public static boolean hasNetwork(Context context) {
		ConnectivityManager con = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo workinfo = con.getActiveNetworkInfo();
		if (workinfo == null || !workinfo.isAvailable()) {
		
			return false;
		}
		return true;
	}

	/***
	 * @category check if the string is null
	 * @return true if is null
	 * */
	public static boolean isNull(String string) {
		boolean t1 = "".equals(string);
		boolean t2 = string == null;
		boolean t3 = string.equals("null");
		if (t1 || t2 || t3) {
			return true;
		} else {
			return false;
		}
	}

	static public byte[] getBytes(File file) throws IOException {
		InputStream ios = null;
		ByteArrayOutputStream ous = null;
		try {
			byte[] buffer = new byte[4096];
			ous = new ByteArrayOutputStream();
			ios = new FileInputStream(file);
			int read = 0;
			while ((read = ios.read(buffer)) != -1) {
				ous.write(buffer, 0, read);
			}
		} finally {
			try {
				if (ous != null)
					ous.close();
			} catch (IOException e) {
			}

			try {
				if (ios != null)
					ios.close();
			} catch (IOException e) {
			}
		}

		return ous.toByteArray();
	}

	public static class MLog {
		static public void e(String msg) {
			android.util.Log.e("=======ERROR======", msg);
		}

		static public void e(String tag, String msg) {
			android.util.Log.e(tag, msg);
		}

		static public void i(String msg) {
			android.util.Log.i("=======INFO======", msg);
		}

		static public void i(String tag, String msg) {
			android.util.Log.i(tag, msg);
		}

	}
}