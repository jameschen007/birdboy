package com.jameschen.birdboy.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.client.ClientProtocolException;

import com.jameschen.plugin.qq.AppConstants;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class UpLoadTqqPic {

	
	public static void doUpLoadReq(String token, String openid, File imgeFile,AsyncHttpResponseHandler asyncHttpResponseHandler, Object content) {
		try {String uriSendQQWeibo = "https://graph.qq.com/t/add_pic_t";
        
        URL url = new URL(uriSendQQWeibo);
        
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        
   conn.setRequestMethod("POST");   
   conn.setDoOutput(true);   
   conn.setDoInput(true);
   conn.setUseCaches(false);
   
   String BOUNDARY = "c9152e99a2d6487fb0bfd02adec3aa16"; // 分隔符   
      
   StringBuffer sb = new StringBuffer();   
   sb.append("--");   
   sb.append(BOUNDARY);   
   sb.append("\r\n");   
   sb.append("Content-Disposition: form-data; name=\"access_token\"\r\n\r\n");
   sb.append(token+"\r\n");
   
   
   sb.append("--");   
   sb.append(BOUNDARY);   
   sb.append("\r\n");
   sb.append("Content-Disposition: form-data; name=\"oauth_consumer_key\"\r\n\r\n");   
   sb.append(AppConstants.APP_ID+"\r\n");
   
   sb.append("--");   
   sb.append(BOUNDARY);   
   sb.append("\r\n");   
   sb.append("Content-Disposition: form-data; name=\"openid\"\r\n\r\n");   
   sb.append(openid+"\r\n");
   
   sb.append("--");   
   sb.append(BOUNDARY);   
   sb.append("\r\n");   
   sb.append("Content-Disposition: form-data; name=\"format\"\r\n\r\n");   
   sb.append("json\r\n");
   
   sb.append("--");   
   sb.append(BOUNDARY);   
   sb.append("\r\n");   
   sb.append("Content-Disposition: form-data; name=\"content\"\r\n\r\n");   
   sb.append(content);
   
   sb.append("--");   
   sb.append(BOUNDARY);
   sb.append("\r\n");   
   sb.append("Content-Disposition: form-data; name=\"pic\"; filename=\""+imgeFile.getName()+"\"\r\n");   
   sb.append("Content-Type:image/jpeg\r\n\r\n");
   
   byte[] data = sb.toString().getBytes("utf-8");
   StringBuffer sb2 = new StringBuffer();
   sb2.append("--");   
   sb2.append(BOUNDARY);   
   sb2.append("\r\n");   
   sb2.append("Content-Disposition: form-data; name=\"syncflag\"\r\n\r\n");   
   sb2.append("0\r\n");
   
   sb2.append("--");   
   sb2.append(BOUNDARY);   
   sb2.append("\r\n");   
   sb2.append("Content-Disposition: form-data; name=\"compatibleflag\"\r\n\r\n");   
   sb2.append("0\r\n");
   
   sb2.append("\r\n--" + BOUNDARY + "--\r\n");
   byte[] data2 = sb2.toString().getBytes("utf-8");   
      
   byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes("utf-8");
   
   long sizepic = imgeFile.length();
   
   conn.setRequestProperty("Content-Type", "multipart/form-data; boundary="+BOUNDARY); //设置表单类型和分隔符   
    conn.setRequestProperty("Content-Length", ""+(data.length + sizepic + data2.length +end_data.length)); //不是必须的
    conn.setRequestProperty("Host", "graph.qq.com"); //不是必须的
    conn.setRequestProperty("Cache-Control", "no-cache"); //不是必须的
    conn.setRequestProperty("Connection", "keep-alive");
    //
   OutputStream os = conn.getOutputStream();   
   os.write(data);
      
      
   FileInputStream fis = new FileInputStream(imgeFile); //要上传的文件   
      
   int rn2;   
   byte[] buf2 = new byte[1024];   
   while((rn2=fis.read(buf2, 0, 1024))>0)   
   {      
       os.write(buf2,0,rn2);   
   }
   
   os.write(data2);
   
   os.write(end_data);   
   os.flush();   
   os.close();   
   fis.close();   
      
   int code = conn.getResponseCode();
   System.out.println(code);
   InputStream is = null;
   boolean succ=false;
   if(200==code){
   //得到返回的信息   
     is = conn.getInputStream();   
     succ=true;

     asyncHttpResponseHandler.onSuccess(null);
   }else{
     is = conn.getErrorStream();
     asyncHttpResponseHandler.onFailure(code, null, null);
   }
      
      
  BufferedReader br = new BufferedReader(new InputStreamReader(is,"utf-8"));
  String str = null;
   while((str=br.readLine())!=null){
     Log.e("分享图片的返回信息","打印:"+str);
   }
        
        
         } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
        } catch (ClientProtocolException e) {
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        }
	}

}
