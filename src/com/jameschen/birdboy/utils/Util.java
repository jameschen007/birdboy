package com.jameschen.birdboy.utils;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.utils.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jameschen.birdboy.BindPlugActivity;
import com.jameschen.birdboy.CommentListActivity;
import com.jameschen.birdboy.CommentSendActivity;
import com.jameschen.birdboy.DetailContentActivity;
import com.jameschen.birdboy.FeedbackActivity;
import com.jameschen.birdboy.ImageDetailActivity;
import com.jameschen.birdboy.MapActivity;
import com.jameschen.birdboy.MapAddressActivity;
import com.jameschen.birdboy.MessageSendActivity;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.Register0Activity;
import com.jameschen.birdboy.SeeUserHomePageActivity;
import com.jameschen.birdboy.SeeUserPlugInPageActivity;
import com.jameschen.birdboy.SettingActivity;
import com.jameschen.birdboy.SportEventCreate0Activity;
import com.jameschen.birdboy.SportEventCreate1Activity;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.base.BaseShareAcitivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Register;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.SoftwareInfo;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.User;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.InvitedUser;
import com.jameschen.birdboy.model.entity.Location;
import com.jameschen.birdboy.model.entity.QQ;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.model.entity.Version;
import com.jameschen.birdboy.model.entity.Weibo;
import com.jameschen.birdboy.widget.MyDialog;
import com.jameschen.plugin.ShareModel;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.umeng.analytics.MobclickAgent;

public class Util {

	public static boolean hasFroyo() {
		// Can use static final constants like FROYO, declared in later versions
		// of the OS since they are inlined at compile time. This is guaranteed
		// behavior.
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	public static boolean hasIcs() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	/**
	 * make true current connect service is wifi
	 * 
	 * @param mContext
	 * @return
	 */
	public static boolean isWifi(Context mContext) {
		ConnectivityManager connectivityManager = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
		if (activeNetInfo != null
				&& activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
			return true;
		}
		return false;
	}

	/**
	 * �ж��Ƿ�����������
	 */
	/**
	 * network available
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivity == null) {
			Log.w("network", "couldn't get connectivity manager");
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].isAvailable()) {
						Log.d("newwork", "network is available");
						return true;
					}
				}
			}
		}
		Log.d("network", "network is not available");
		return false;
	}

	public static int getPixByDPI(Context context, int value) {
		int mPix = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				value, context.getResources().getDisplayMetrics());
		return mPix;
	}

	public static int version(Context context) {
		int code = getAppVersion(context);

		if (SoftwareInfo.SOFT_VERSION_CODE == -1) {
			SoftwareInfo.SOFT_VERSION_CODE = code;
		}
		return code;
	}

	public static int getAppVersion(Context context) {
		int code = -1;
		PackageInfo manager = null;
		try {
			manager = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			code = manager.versionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return code;
	}

	private final static double EARTH_RADIUS = 6378137.0;

	public static double gps2m(double lat_a, double lng_a, double lat_b,
			double lng_b) {
		double radLat1 = (lat_a * Math.PI / 180.0);
		double radLat2 = (lat_b * Math.PI / 180.0);
		double a = radLat1 - radLat2;
		double b = (lng_a - lng_b) * Math.PI / 180.0;
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * EARTH_RADIUS;
		s = Math.round(s * 10000) / 10000;
		return s;
	}

	public static void go2SendMessage(BaseActivity activity, String name,
			String id, int fromType) {
		boolean isLogOn = true;
		if (isLogOn) {
			Intent mIntent = new Intent(activity, MessageSendActivity.class);
			mIntent.putExtra("id", id);
			mIntent.putExtra("type", fromType);
			mIntent.putExtra("name", name);
			activity.startActivity(mIntent);
		} else {
			// Util.showLoginDialog(activity,requestCode);
		}
	}

	

	public static void go2Map(BaseActivity context, int mapVal) {
		Intent mIntent = new Intent(context, MapActivity.class);
		mIntent.putExtra("type", mapVal);
		context.startActivityForResult(mIntent, mapVal);
	}
	
	public static void go2CommentList(BaseActivity activity, int fromType,
			long id) {
		Intent mIntent = new Intent(activity, CommentListActivity.class);
		mIntent.putExtra("id", id);
		mIntent.putExtra("type", fromType);
		activity.startActivityForResult(mIntent, fromType);

	}

	public static void go2Map(Context context,Location location) {
			if (location==null) {
				return;
			}
			Intent intent = new Intent(context,MapAddressActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra(MapAddressActivity.ADDRESS, location);
			context.startActivity(intent);
	}
	
	public static void go2SendComment(final BaseActivity activity, final long commentId,
			final int fromType) {
		go2SendComment(activity, commentId,-1, null,fromType);
	}
	
	public static void go2SendComment(final BaseActivity activity, final long commentId,
			final int userId,final String userNick,final int fromType) {
		if (LogInController.IsLogOn()) {
			Intent mIntent = new Intent(activity, CommentSendActivity.class);
			mIntent.putExtra("id", commentId);
			mIntent.putExtra("type", fromType);
			if (userNick!=null) {
				mIntent.putExtra("uid", userId);
				mIntent.putExtra("nick", userNick);
			}
			activity.startActivityForResult(mIntent, fromType);
		} else {
			Util.go2LogInDialog(activity,new OnLogOnListener() {
				
				@Override
				public void logon() {
					Intent mIntent = new Intent(activity, CommentSendActivity.class);
					mIntent.putExtra("id", commentId);
					mIntent.putExtra("type", fromType);
					if (userNick!=null) {
						mIntent.putExtra("uid", userId);
						mIntent.putExtra("nick", userNick);
					}
					activity.startActivityForResult(mIntent, fromType);					
				}
			});
		}
	}

	public static void go2LogInDialog(final BaseActivity activity,final OnLogOnListener logOnListener) {
		boolean auto=false;
		SharedPreferences user = activity.getSharedPreferences(User.SharedName, 0);
		auto = user.getBoolean(User.logon, false);
		go2LogInDialog(activity, logOnListener, auto);
	}
	
	static MyDialog login;
	
	public static void go2LogInDialog(final BaseActivity activity,final OnLogOnListener logOnListener,boolean auto) {
	
	
		String[] accounts = Register0Activity
				.readAccountDataFromPreference(activity);
		
		if (auto) {
			if (!TextUtils.isEmpty(accounts[0])&&!TextUtils.isEmpty(accounts[1])) {
				LogInController.login(activity, accounts[0],accounts[1], new OnLogOnListener() {

					@Override
					public void logon() {
						if (logOnListener!=null) {
							logOnListener.logon();
						}
					}
				});
	
				return;
			}
		}
		
		if (login!=null) {
			try {
				login.cancel();
				login=null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		login = new MyDialog(activity, R.style.logindialog);
		login.setCancelable(true);
		login.setCanceledOnTouchOutside(false);
		View v = View.inflate(activity, R.layout.dialog_login, null);
		login.setContentView(v);
		final EditText accountInput = (EditText) v.findViewById(R.id.login_nickname);
		final EditText passwordInput = (EditText) v.findViewById(R.id.login_password);
		Button bt_login_login = (Button) v.findViewById(R.id._bt_login);
		Button bt_login_register = (Button) v.findViewById(R.id._bt_register);
		if (accounts[0] != null) {
			accountInput.setText(accounts[0]);
			passwordInput.setText(accounts[1]);
		}
		
		
		bt_login_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				LogInController.login(activity, accountInput.getText().toString(),passwordInput.getText().toString(), new OnLogOnListener() {

							@Override
							public void logon() {
								//resetInitViews();
								try {
									if (login!=null) {
										login.cancel();
										login=null;
									}
									
								} catch (Exception e) {
									e.printStackTrace();
								}
								if (logOnListener!=null) {
									logOnListener.logon();
								}
							}
						});
			}
		});
		bt_login_register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				activity.startActivityForResult(new Intent(activity,
						Register0Activity.class), Register.REQUST_REGISTER);
			}
		});

		login.show();

		// activity.showToast("请先登录");
		// Intent mIntent =new Intent(activity,LoginActivity.class);
		// mIntent.putExtra("id", commentId);
		// mIntent.putExtra("type", fromType);
		// activity.startActivityForResult(mIntent,fromType);
	}

	public static void go2ForgetPassword(final BaseActivity activity) {

		final MyDialog login = new MyDialog(activity, R.style.logindialog);
		login.setCancelable(true);
		login.setCanceledOnTouchOutside(false);
		View v = View.inflate(activity, R.layout.forget_password, null);
		login.setContentView(v);
		final TextView dial = (TextView) v.findViewById(R.id._bt_dial);
		dial.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent("android.intent.action.CALL",Uri.parse("tel:"+"18280041890"));
				activity.startActivity(intent);				
			}
		});
		
		login.show();
	
		
	}
	
	public static boolean saveJsonCache(Context context,String sharedName,String cacheName, String jsonContent) {
		SharedPreferences prefs = context.getSharedPreferences(sharedName, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(cacheName, jsonContent);
		return editor.commit();
	}

	public static String loadJsonCacheData(Context context,String sharedName,String cacheName) {
		SharedPreferences prefs = context.getSharedPreferences(sharedName, 0);
		String jsonContent = prefs.getString(cacheName, null);
		return jsonContent;
	}
	public static void showContactDialog(final BaseActivity activity,String name,final String phone,String qq) {
		showContactDialog(0,0, activity, name, phone, qq);
	}
	public static void showContactDialog(final int sportType,int type,final BaseActivity activity,final String name,final String phone,String qq) {
		final MyDialog login = new MyDialog(activity, R.style.logindialog);
		login.setCancelable(true);
		login.setCanceledOnTouchOutside(true);
		View v = View.inflate(activity, R.layout.dialog_contact, null);
		login.setContentView(v);
		final TextView name_tv = (TextView) v.findViewById(R.id._name);
		final TextView phone_tv = (TextView) v.findViewById(R.id._contact_phone);
		name_tv.setText(name);

		phone_tv.setText(phone);
		Button commit = (Button) v.findViewById(R.id._bt_commit);
		boolean goTofeedback=false;
		if (TextUtils.isEmpty(phone.trim())) {
			commit.setText("无此电话,来帮帮我们");
			goTofeedback = true;
		}
		if (type==1) {//stadium
			MobclickAgent.onEvent(activity, "contact_stadium");
		}else if(type==0){//coach by default
			MobclickAgent.onEvent(activity, "contact_coach");
		}
//		if (type>0) {
//			Button share = (Button) v.findViewById(R.id._bt_share);
//			share.setVisibility(View.VISIBLE);
//			share.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View arg0) {
//					Intent intent = new Intent(Intent.ACTION_SEND); // 启动分享发送的属性
//					intent.setType("text/plain"); // 分享发送的数据类型
//					String msg = "你的朋友分享了"+CoachAdapter.setSportTypeStr(sportType)+" "+"场馆 "+name+"的介绍给你，快来使用鸟孩运动APP吧。在这里，你能找到周边的场馆。"+
//							"立刻体验:\nhttp://apps.wandoujia.com/apps/com.jameschen.birdboy/download";
//					intent.putExtra(Intent.EXTRA_TEXT, msg); // 分享的内容
//					activity.startActivity(Intent.createChooser(intent, "选择分享"));
//				}
//			});
//		}
		
		final boolean noPhone=goTofeedback;
		commit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			//	login.cancel();
				//call phone
				if (noPhone) {
					activity.startActivity(new Intent(activity,FeedbackActivity.class));
					return;
				}
				Intent intent=new Intent("android.intent.action.CALL",Uri.parse("tel:"+phone));
				activity.startActivity(intent);	
			}
		});
		login.show();
	}

	public static void CommitDialog(Context context, String title,String message,AlertDialog.OnClickListener commitListener) {
		CommitDialog("确定", context, title, message, commitListener,null);
	}

	public static void CommitDialog(String positiveStr,Context context, String title,String message,AlertDialog.OnClickListener commitListener,
			android.content.DialogInterface.OnClickListener cancelClickListener) {
		final AlertDialog commitDilog = new AlertDialog.Builder(context).setTitle(title).setMessage(message)
			     .setPositiveButton(positiveStr, commitListener)
			     .setNegativeButton("取消", cancelClickListener).show();
		if (cancelClickListener!=null) {
			commitDilog.setCancelable(false);
			commitDilog.setCanceledOnTouchOutside(false);
		}
		
		
	}
	
	
	// 接受下载完成后的intent
    class DownloadCompleteReceiver extends BroadcastReceiver {
    	
    	public long  downID=0;
    	
              @Override
              public void onReceive(Context context, Intent intent) {
                       if (intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                            long downId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                            if (downID==downId) {
                            	//  installApk(file);
							}
                              
                       }
              }
    }
	 // 安装apk
    protected static void installApk(File file, BaseActivity mContext) {
              if (file.toString().endsWith(".apk")) {
                       Intent intent = new Intent();
                       intent.setAction(Intent.ACTION_VIEW);
                       intent.setDataAndType(Uri.fromFile(file),
                                          "application/vnd.android.package-archive");
                       mContext.startActivity(intent);
              } 
    }
	
	public static void upgradeVersion(final Version version,final Context context) {
		if (!version.isIsupdate()) {
			return;
		}
		SharedPreferences software = context.getSharedPreferences(
				SoftwareInfo.SharedName, 0);
		// 判断上次提示的时间间隔是否满足3天。
		long lastTimeNotiyTime = software.getLong("last_noti_update_time",
							0);
		int day = (int) ((System.currentTimeMillis() - lastTimeNotiyTime) / (1000 * 3600 * 24));
			
		if (day<3&&!version.isIsforce()) {
			if (context instanceof SettingActivity) {//always need show dialog
				
			}else {
				return ;
			}
			
		}
		
		android.content.DialogInterface.OnClickListener cancelClickListener=null;
		if (version.isIsforce()) {
			cancelClickListener=new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					((BaseActivity)context).finish();
				}
			};
		}
		software.edit().putLong("last_noti_update_time", System.currentTimeMillis()).commit();
		//判断当前是2g还是wifi
		Util.CommitDialog("升级",context, "版本更新", "" +
				version.getContent()+
				"", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						// context.registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));    
						//download....
						 //开始下载 
				        Uri resource = Uri.parse(version.getUrl()); 
				        DownloadManager.Request request = new DownloadManager.Request(resource); 
				        request.setAllowedNetworkTypes(Request.NETWORK_MOBILE | Request.NETWORK_WIFI); 
				        request.setAllowedOverRoaming(false); 
				        //设置文件类型
				        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
				        String mimeString = mimeTypeMap.getMimeTypeFromExtension("apk");
				        request.setMimeType(mimeString);
				        //在通知栏中显示 
				        request.setShowRunningNotification(true);
				        request.setVisibleInDownloadsUi(true);
				        //sdcard的目录下的download文件夹
				        request.setDestinationInExternalPublicDir("/download/", "birdboy.apk");
				        request.setTitle("鸟孩版本更新"); 
				        DownloadManager downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
				        long id = downloadManager.enqueue(request); 
				        //保存id 
				        //prefs.edit().putLong(DL_ID, id).commit(); 

					}
				},cancelClickListener);		
	}

	public static void createSportEvent(FragmentActivity activity, Stadium stadium) {
		createSportEvent(activity, stadium, null);
	}

	public static void createSportEvent(FragmentActivity activity, Stadium stadium,Bundle bundle) {
		MobclickAgent.onEvent(activity, "activity_create");
		if (stadium==null) {
			 Intent intent =new Intent(activity,
						SportEventCreate0Activity.class);
			 if (bundle!=null) {
				 intent.putExtra("data",bundle);
				 MobclickAgent.onEvent(activity, "invite_member");
			}
				activity.startActivityForResult(intent,ConstValues.REQUST_CREATE_SPORT);								
		}else {
			Intent intent = new Intent(activity,SportEventCreate1Activity.class);
			intent.putExtra(ConstValues.STADIUM, (Parcelable)stadium);
			 if (bundle!=null) {
				 intent.putExtra("data",bundle);
				 MobclickAgent.onEvent(activity, "invite_member");
			}
			activity.startActivityForResult(intent,0x11);
		}
	}

	public static <T>void go2Bind(T fromWhichUI) {
		if (fromWhichUI instanceof BaseFragment) {
			((BaseFragment)fromWhichUI).startActivityForResult(new Intent(((BaseFragment)fromWhichUI).getActivity(), BindPlugActivity.class),BindPlugActivity.REQ_BIND);
			
		}else {//must activity
			((BaseActivity)fromWhichUI).startActivityForResult(new Intent((BaseActivity)fromWhichUI, BindPlugActivity.class),BindPlugActivity.REQ_BIND);
			
		}
	
	}

	
	public static void go2SeeSNS(BaseActivity baseActivity, SNS sns,int type) {
			MobclickAgent.onEvent(baseActivity, "see_sns");
			Intent intent  = new Intent(baseActivity,SeeUserPlugInPageActivity.class);
			intent.putExtra("sns", sns);
			intent.putExtra("type", type);
			baseActivity.startActivity(intent);
	}

	public static void go2DetailContent(BaseActivity activity, String content) {
		// TODO Auto-generated method stub
		Intent intent  = new Intent(activity,DetailContentActivity.class);
		intent.putExtra("content", content);
		activity.startActivity(intent);
	}

	
	public static void go2SeeUserHomePage(Activity activity,int uid){
		MobclickAgent.onEvent(activity, "see_home_page");
		Intent intent  = new Intent(activity,SeeUserHomePageActivity.class);
		intent.putExtra("id", uid);
		activity.startActivity(intent);
		
	}

	public static void go2SeeBigPics(Activity context,ArrayList<String> picsList){
		Intent intent=new Intent(context,ImageDetailActivity.class);
		intent.putStringArrayListExtra("photos", picsList);
		//phoneList
		context.startActivity(intent);		
	}
	public static void go2SeeBigPic(Activity context,String blogoUrl) {
		ArrayList<String> phoneList = new ArrayList<String>();
		phoneList.add(blogoUrl);
		go2SeeBigPics(context, phoneList);	
	}

	public static void addMyFav(final BaseActivity activity,final InvitedUser invitedUser) {
		MobclickAgent.onEvent(activity, "add_fav_friend");
			UIController addMyFavController = new UIController() {
				@Override
				public void responseNetWorkError(int errorCode, Page page) {
					// TODO Auto-generated method stub
					activity.showToast("关注失败");
				}
				
				@Override
				public <E> void refreshUI(E content, Page page) {
					activity.showToast("关注"+invitedUser.getNick()+"成功!");
				}
				
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return "add"+"fav"+invitedUser.getId();
				}
				
				@Override
				public Type getEntityType() {
					return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
					}.getType();
				}
			};
		
		Map<String, String> map=activity.getPublicParamRequstMap();
		map.put("userid", invitedUser.getId()+"");
		map.put("username", invitedUser.getNick());
		map.put("logo", invitedUser.getLogo());
		map.put("sex", invitedUser.getSex()+"");
		
		activity.postDataLoader(map, HttpConfig.REQUST_ADD_FAV_URL, addMyFavController,false, null,new HttpClientStack(LogInController.getHttpClientInstance()));	
	}

	
	
}
