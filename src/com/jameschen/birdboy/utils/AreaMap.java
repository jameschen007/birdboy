package com.jameschen.birdboy.utils;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import android.content.Context;
import android.util.Xml;

import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.jameschen.birdboy.model.entity.Area;

public class AreaMap extends DefaultHandler{
	// We don't use namespaces
	private static List<Area> areaList = null;

	// private static String locale_String = null;

	private static List xml_parse(InputStream in )
			throws XmlPullParserException, IOException {

		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			return xml_readPlist(parser);
		} finally {
			in.close();
		}
	}

	private static void xml_skip(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

	private static String xml_readText(XmlPullParser parser)
			throws IOException, XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}


	private static List xml_readPlist(XmlPullParser parser )
			throws XmlPullParserException, IOException {
		while (true) {
			parser.nextTag();
			if ((parser.getEventType() == XmlPullParser.START_TAG)
					&& parser.getName().equals("dict")) {
				parser.nextTag();
				break;
			}
			;
		}
		String plist_key = "";
		String plist_string = "";
		while (true) {
			if ((parser.getEventType() == XmlPullParser.END_TAG)
					&& (parser.getName().equals("dict"))) {
				break;
			}
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				parser.nextTag();
				
				continue;
			}
			String name = parser.getName();
			if (name.equals("key")) {
				plist_key = xml_readText(parser);
			} else if (name.equals("string")) {	
				plist_string = xml_readText(parser);
					Area area = new Area();
					area.setId(Integer.valueOf(plist_key));
					area.setName(plist_string);
					areaList.add(area );
			} else {
				xml_skip(parser);
			}
		}
		return null;
	}

	public static List<Area> initAreaList(Context context) {
		if (areaList != null)
			return areaList;
		areaList = new ArrayList<Area>();
		Area area=new Area();
		area.setId(-1);
		area.setName("全部区域");
		areaList.add(area);
		String fileName = "area.plist";
		try {
			InputStream in = context.getResources().getAssets().open(fileName);// EN
			xml_parse(in);// first we get standrander,
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
		return areaList;

	}


}
