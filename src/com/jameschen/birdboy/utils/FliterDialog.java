package com.jameschen.birdboy.utils;

import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.utils.Util;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.environment.Config;
import com.jameschen.birdboy.model.entity.Category;

public class FliterDialog {
	
	
	public interface onFilerSelectedListener{
		void onItemSelect(Category sex, Category sport);
	}
	private Category sex,sport;
	private boolean all =true ,showSex=true;
	
	public FliterDialog() {
	}
	
	public FliterDialog(boolean all, boolean showSex) {
		this.all = all;
		this.showSex =showSex;
	}

	public  Dialog showFilterDialog(final Activity context,final onFilerSelectedListener commitListener,int lastSport) {
					return showFilterDialog(context, commitListener, -1, lastSport);
	}
	public  Dialog showFilterDialog(final Activity context,final onFilerSelectedListener commitListener,int lastSex,int lastSport) {
		final Dialog dlg = new Dialog(context, R.style.logindialog);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.choose_around_filter_dialog_layout, null);
		final int cFullFillWidth = 10000;
		layout.setMinimumWidth(cFullFillWidth);
		Button commitButton=(Button) layout.findViewById(R.id._bt_commit);
		Button	cancelButton=(Button) layout.findViewById(R.id._bt_cancel);
		
		GridView sexGird =(GridView) layout.findViewById(R.id.gv_sex);
		GridView sportGird = (GridView) layout.findViewById(R.id.gv_sport);
		if (showSex) {
			createSexGirdView(sexGird,lastSex);
		}else {
			layout.findViewById(R.id.sex_tv).setVisibility(View.GONE);
			sexGird.setVisibility(View.GONE);
		}
		
		TextView textView =(TextView) layout.findViewById(R.id.sport_tv);
		if (!all) {
			LinearLayout.LayoutParams params=(android.widget.LinearLayout.LayoutParams) textView.getLayoutParams();
			params.topMargin = Util.getPixByDPI(context, 10);
			textView.setLayoutParams(params);
		}
			createSportGirdView(sportGird,lastSport);
		
		
		
		commitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dlg.dismiss();
				if (sport!=null) {
					Log.i("select---", sport.getName()+";"+sport.getType());
				}
				
				commitListener.onItemSelect(sex, sport);
			}
		});
		
		cancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				dlg.dismiss();
			}
		});
		
		dlg.setContentView(layout);
		dlg.show();
		int width =(int) (UtilsUI.getWidth(context.getApplication())*0.8);
		dlg.getWindow().setLayout(width, LayoutParams.WRAP_CONTENT);
		return dlg;
	}
	
	
	public static List<Category> getLikedSportCategories() {
		List<Category> categories = new ArrayList<Category>();
		categories.addAll(Config.SportType.initSportList());
		//追加 跑步65 ,自行车 66   瑜伽 67
				Category item0= new Category();
				Category item2= new Category();
				item2.setName("跑步");
				item2.setType(65);
				item2.setIconResource(R.drawable.paobu);
				categories.add(item2);
				item0.setName("自行车");
				item0.setType(66);
				item0.setIconResource(R.drawable.zixingche);
				categories.add(item0);
				Category item1= new Category();
				item1.setName("瑜伽");
				item1.setType(67);
				item1.setIconResource(R.drawable.yujia);

				categories.add(item1);
				Category item3= new Category();
				item3.setName("爬山");
				item3.setType(68);
				item3.setIconResource(R.drawable.pashan);

				categories.add(item3);
		return categories;
	}
	
	void createSportGirdView(GridView sportGird, int lastSport){
		final  List<Category> sportList = getLikedSportCategories();
		final CategoryAdapter categoryAdapter = new CategoryAdapter(sportGird.getContext(), sportList);
		sportGird.setAdapter(categoryAdapter);
		if (lastSport>-1) {
			categoryAdapter.setPreSelect(lastSport);
		}else {
			if (showSex) {
				categoryAdapter.setPreSelect(lastSport);
			}
		}
		
		sportGird.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (!all&&arg2==0) {//choose sport not seleted all
					
					return;
				}
				sport =sportList.get(arg2);
				categoryAdapter.setLastSelect(arg2);
			}
		});
	}
	
	void createSexGirdView(GridView sexGird, int lastSex){
		final List<Category> sexCategories = new ArrayList<Category>();
		Category item0= new Category();
		Category item2= new Category();
		item2.setName("全部");
		item2.setType(-1);
		sexCategories.add(item2);
		item0.setName("女");
		item0.setType(2);
		sexCategories.add(item0);
		Category item1= new Category();
		item1.setName("男");
		item1.setType(1);
		sexCategories.add(item1);
		final CategoryAdapter categoryAdapter = new CategoryAdapter(sexGird.getContext(), sexCategories);
		sexGird.setAdapter(categoryAdapter);
		
		if (lastSex>-1) {
			
			categoryAdapter.setPreSelect(lastSex);
		}else {
			if (showSex) {
			categoryAdapter.setPreSelect(lastSex);
			}
		}
		
		
		sexGird.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				sex =sexCategories.get(arg2);
				categoryAdapter.setLastSelect(arg2);
				sexlastIndex = arg2;
			}
		});
	}
	int lastIndex=-1, sexlastIndex=-1;

	public class CategoryAdapter extends ArrayAdapter<Category> {
		private Context context;

		public CategoryAdapter(Context context, final List<Category> objects) {
			super(context, -1, objects);
			this.context = context;
		}
		public void setPreSelect(int lastId) {
			for (int i = 0; i <getCount(); i++) {
				if (getItem(i).getType()==lastId) {
					setLastSelect(i);
					break;
				}
			}
		}
		private int lastIndex=-2;
		public void setLastSelect(int mlastIndex) {
		lastIndex = mlastIndex;
		notifyDataSetChanged();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView != null) {
				holder = (ViewHolder) convertView.getTag();
				attchReourceToView(holder.item, getItem(position));
				if (lastIndex==position) {
					convertView.setBackgroundColor(context.getResources().getColor(R.color.white));	
				}else {
					convertView.setBackgroundColor(context.getResources().getColor(R.color.color_e8));
				}
				return convertView;
			}
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = (ViewGroup) inflater.inflate(R.layout.filter_category_item,
					parent, false);
			holder = new ViewHolder();
			holder.item = (TextView) convertView.findViewById(R.id.item_text1);
			convertView.setTag(holder);
			if (lastIndex==position) {
				convertView.setBackgroundColor(context.getResources().getColor(R.color.white));	
			}else {
				convertView.setBackgroundColor(context.getResources().getColor(R.color.color_e8));
			}
			attchReourceToView(holder.item, getItem(position));
			return convertView;
		}

		
		private void attchReourceToView(TextView item, Category category) {
			item.setText(category.getName());
			
		}

		final class ViewHolder {
			TextView item;
		}

	}
}
