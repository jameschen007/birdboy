package com.jameschen.birdboy.utils;

import java.util.HashMap;
import java.util.List;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.AreaAdapter;
import com.jameschen.birdboy.adapter.OptionsAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.FliterOptionItem;


import android.R.integer;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

public class OptionsPopupWindow   {
	PopupWindow popupWindow;
	TextView mView;
	Object selectItem;
	List<FliterOptionItem> mList;

	public OptionsPopupWindow() {
	}

	public OptionsPopupWindow(TextView view) {
		mView = view;
	}

	public OptionsPopupWindow(TextView view, Object selectItem) {
		mView = view;
		context = view.getContext();
		this.selectItem = selectItem;
	}

	public void setItemOnClickListener(OnItemClickListener itemClickListener) {
		// TODO Auto-generated method stub
		onItemClickListener = itemClickListener;
	}

	public void setOnDismissListener(OnDismissListener dismissListener) {
		// TODO Auto-generated method stub
		this.onDismissListener = dismissListener;
		popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
			
			@Override
			public void onDismiss() {
				int options=0;
				if (adapter!=null) {
					 options=adapter.getAllCheckOptionsValue();
				}
				onDismissListener.onDismiss(options);
			}
		});
	}
	
	OnItemClickListener onItemClickListener;
	OnDismissListener  onDismissListener;
	public interface OnItemClickListener {
		void onItemClick(int index);
	}
	public interface OnDismissListener {
		void onDismiss(int optionValue);
	}

	int width = 0;
	Context context;
	
	public void showActionWindow(View parent, BaseActivity context, List<FliterOptionItem> list,
			int  width) {
		this.context = context;
		// final RingtoneclipModel currentData = model;
		// final int res_id = currentData.getId();
		mList = list;
		int[] location = new int[2];
		int popWidth = width;
		parent.getLocationOnScreen(location);
		View view = getView(context, list);
		popupWindow = new PopupWindow(view, popWidth, LayoutParams.WRAP_CONTENT);// new
																					// PopupWindow(view,
																					// popWidth,
																					// LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.showAsDropDown(parent, 0, 0);
	}
	
	public void showActionWindow(View parent, BaseActivity context, List<FliterOptionItem> list) {
		mList = list;
		int[] location = new int[2];
		parent.getLocationOnScreen(location);
		View view = getView(context, list);
		popupWindow = new PopupWindow(view, LayoutParams.WRAP_CONTENT, Util.getPixByDPI(context, 220));// new
																					// PopupWindow(view,
																					// popWidth,
																					// LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.showAsDropDown(parent, 0, 0);
	}
	
	 OptionsAdapter adapter ;
	private View getView(BaseActivity context, List<FliterOptionItem> list) {
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		ViewGroup layout  = (ViewGroup) inflater.inflate(R.layout.options_select_ui, null);
			GridView optionsGridView = (GridView) layout.findViewById(R.id.options_gridView);
			 adapter = new OptionsAdapter(context,list);
			optionsGridView.setAdapter(adapter);
			optionsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					if (onItemClickListener != null) {	
						adapter.setItemChecked(position,view.getTag());
						onItemClickListener.onItemClick(position);
					}
					//popupWindow.dismiss();
				}
			});
		return layout;
	}



}
