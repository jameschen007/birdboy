package com.jameschen.birdboy.utils;

import java.util.HashMap;
import java.util.List;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.AreaAdapter;
import com.jameschen.birdboy.adapter.CategoryAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.model.entity.Area;
import com.jameschen.birdboy.model.entity.Category;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

public class CustomSelectPopupWindow   {
	PopupWindow popupWindow;
	TextView mView;
	Object selectItem;
	List<Category> mList;

	public CustomSelectPopupWindow() {
	}

	public CustomSelectPopupWindow(TextView view) {
		mView = view;
	}

	public CustomSelectPopupWindow(TextView view, Object selectItem) {
		mView = view;
		context = view.getContext();
		this.selectItem = selectItem;
	}

	public void setItemOnClickListener(OnItemClickListener itemClickListener) {
		// TODO Auto-generated method stub
		onItemClickListener = itemClickListener;
	}

	public void setOnDismissListener(OnDismissListener dismissListener) {
		// TODO Auto-generated method stub
		this.onDismissListener = dismissListener;
		popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
			
			@Override
			public void onDismiss() {
				onDismissListener.onDismiss();
			}
		});
	}
	
	OnItemClickListener onItemClickListener;
	OnDismissListener  onDismissListener;
	public interface OnItemClickListener {
		void onItemClick(int index);
	}
	public interface OnDismissListener {
		void onDismiss();
	}

	int width = 0;
	Context context;
	
	public void showActionWindow(View parent, BaseActivity context, List<Category> list,
			int  width) {
		this.context = context;
		// final RingtoneclipModel currentData = model;
		// final int res_id = currentData.getId();
		mList = list;
		int[] location = new int[2];
		int popWidth = width;
		parent.getLocationOnScreen(location);
		View view = getView(context, list);
		popupWindow = new PopupWindow(view, popWidth, LayoutParams.WRAP_CONTENT);// new
																					// PopupWindow(view,
																					// popWidth,
																					// LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.showAsDropDown(parent, 0, 0);
	}
	
	public void showActionWindow(View parent, BaseActivity context, List<Category> list) {
		// final RingtoneclipModel currentData = model;
		// final int res_id = currentData.getId();
		mList = list;
		int[] location = new int[2];
		parent.getLocationOnScreen(location);
		View view = getView(context, list);
		popupWindow = new PopupWindow(view, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);// new
																					// PopupWindow(view,
																					// popWidth,
																					// LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.showAsDropDown(parent, 0, 0);
	}
	
	public PopupWindow getPopupWindow(){
		return popupWindow;
	}
	
	private ListView childListView;
	public ListView getChildListView() {
		// TODO Auto-generated method stub
		return childListView;
	}
	public void dismiss() {
		popupWindow.dismiss();
	}
	private ProgressBar loadingbar;
	private TextView loadingNoData;
	private View getView(BaseActivity context, List<Category> list) {
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		ViewGroup layout  = (ViewGroup) inflater.inflate(R.layout.category_select_ui, null);
			ListView categoryListView = (ListView) layout.findViewById(R.id.category_label_listView);
			
		final	CategoryAdapter adapter = new CategoryAdapter(context,list);
		categoryListView.setAdapter(adapter);
		categoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					selectPosition=position;
					//beginLoadingData();
					if (onItemClickListener != null) {
						onItemClickListener.onItemClick(position);
					}
					dismiss();
				}
			});
			
			//reselected
		//categoryListView.setSelection(selectPosition);
			//beginLoadingData();
			
		return layout;
	}
	
	

	private int selectPosition=0,childSelectPosition=0;
	
	private void beginLoadingData() {
		loadingbar.setVisibility(View.VISIBLE);
		loadingNoData.setVisibility(View.GONE);
		childListView.setVisibility(View.GONE);
	}
	
	private void showLoadingFinished() {
		childListView.setVisibility(View.VISIBLE);
		loadingbar.setVisibility(View.GONE);
		loadingNoData.setVisibility(View.GONE);
	}
	private void showNodata() {
		loadingNoData.setVisibility(View.VISIBLE);
		loadingbar.setVisibility(View.GONE);
	}



}
