package com.jameschen.birdboy.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity;

public class CheckValid {

	public static final int REGULAR_LETTER = 0, REGULAR_LETTER_SIGN = 2;

	public static final int INPUT_NULL = -3;
	public static final int INPUT_LEN_SHORT = -2;
	public static final int INPUT_LEN_LONG = -4;
	public static final int CHECK_OK = 0;

	public static int LengthIsInvalid(CharSequence content, int minSpecLen,
			int maxSpecLen) {
		if (content == null || content.length() == 0) {

			return INPUT_NULL;
		}

		if (content.length() < minSpecLen) {

			return INPUT_LEN_SHORT;
		}

		if (content.length() > maxSpecLen) {

			return INPUT_LEN_LONG;
		}
		return CHECK_OK;
	}
	
	
	public static boolean passwordCheckValid(BaseActivity activity,CharSequence password, int minLen,
			int maxLen) {
		int registerCode = CheckValid.LengthIsInvalid(password, minLen, maxLen);
		switch (registerCode) {
		case CheckValid.INPUT_NULL:
			activity.showToast(activity.getString(R.string.password_hint));

			return false;
		case CheckValid.INPUT_LEN_SHORT:

			activity.showToast(activity.getString(R.string.password_too_short));
			return false;
		case CheckValid.INPUT_LEN_LONG:
			activity.showToast(activity.getString(R.string.password_too_long));
			return false;
		default:
			break;
		}

		int regularValid = CheckValid.CheckInputValid(password,
				CheckValid.REGULAR_LETTER);

		switch (regularValid) {
		case CheckValid.INPUT_CONTENT_INVALID:
			activity.showToast("输入包含非法字符，请重新输入!");

			return false;
		case CheckValid.INPUT_CONTENT_SIMPLE_ERROR:
			activity.showToast("输入的密码太简单!");

			return false;
		default:
			break;
		}

		return true;
	}
	
	public static boolean verifyCodeCheckValid(BaseActivity activity,CharSequence verifyCode, int minLen,
			int maxLen) {
		int registerCode = CheckValid.LengthIsInvalid(verifyCode, minLen,
				maxLen);
		switch (registerCode) {
		case CheckValid.INPUT_NULL:
			activity.showToast("输入的验证码为空");

			return false;
		case CheckValid.INPUT_LEN_SHORT:
		case CheckValid.INPUT_LEN_LONG:
			activity.showToast("输入的验证码不正确");
			return false;

		default:
			break;
		}

		int regularValid = CheckValid.CheckInputValid(verifyCode,
				CheckValid.REGULAR_LETTER);

		switch (regularValid) {
		case CheckValid.INPUT_CONTENT_INVALID:
			activity.showToast("输入包含非法字符，请重新输入!");

			return false;

		default:
			break;
		}

		return true;
	}
	
	
	public static boolean checkNickNameValid(BaseActivity activity,CharSequence account, int minLen,
			int maxLen) {

		int registerCode = CheckValid.LengthIsInvalid(account, minLen, maxLen);
		switch (registerCode) {
		case CheckValid.INPUT_NULL:
			activity.showToast("请输入昵称!");
			return false;
		case CheckValid.INPUT_LEN_SHORT:
			activity.showToast("昵称太短！");
			return false;
		case CheckValid.INPUT_LEN_LONG:
			activity.showToast("昵称太长！");
			return false;
		default:
			break;
		}
	
		return true;

	}
	
	public static boolean checkPhoneValid(BaseActivity activity,CharSequence account, int minLen,
			int maxLen) {

		int registerCode = CheckValid.LengthIsInvalid(account, minLen, maxLen);
		switch (registerCode) {
		case CheckValid.INPUT_NULL:
			activity.showToast(activity.getString(R.string.register_phone));
			return false;
		case CheckValid.INPUT_LEN_SHORT:
			activity.showToast(activity.getString(R.string.account_too_short));
			return false;
		case CheckValid.INPUT_LEN_LONG:
			activity.showToast(activity.getString(R.string.account_too_long));
			return false;
		default:
			break;
		}
		if (!CheckValid.isMobileNO(account.toString())) {
			activity.showToast(activity.getString(R.string.error_phone_num));
			return false;
		}
		return true;

	}
	
	
	/**
	 * @param mobiles
	 *          
	 * @return
	 */
	public static boolean isMobileNO(String mobiles) {

		Pattern p = Pattern
				.compile("^((13[0-9])|(15[0-9])|(18[0-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	public static final int INPUT_CONTENT_SIMPLE_ERROR = -3;
	public static final int INPUT_CONTENT_INVALID = -2;

	public static int CheckInputValid(CharSequence input, int type) {
		String str = null;

		if (type == REGULAR_LETTER) {
			str = "^[A-Za-z\\d\\u4E00-\\u9FA5]+$";
			Pattern p = Pattern.compile(str);
			Matcher m = p.matcher(input);
			if (m.matches()) {
				return CHECK_OK;
			} else {
				return -1;
			}

		} else {
			str = "(.*)\\1{5}";
			Pattern p = Pattern.compile(str);
			Matcher m = p.matcher(input);
			String password = input.toString();
			if (password.contains(" ")) {
				return INPUT_CONTENT_INVALID;
			} else if (m.matches()) {
				return INPUT_CONTENT_SIMPLE_ERROR;
			}
		}

		return CHECK_OK;
	}
}
