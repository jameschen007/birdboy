package com.jameschen.birdboy.utils;

import java.util.HashMap;
import java.util.List;

import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.AreaAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.model.entity.Area;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

public class AreaSelectPopupWindow   {
	PopupWindow popupWindow;
	TextView mView;
	Object selectItem;
	List<Area> mList;

	public AreaSelectPopupWindow() {
	}

	public AreaSelectPopupWindow(TextView view) {
		mView = view;
	}

	public AreaSelectPopupWindow(TextView view, Object selectItem) {
		mView = view;
		context = view.getContext();
		this.selectItem = selectItem;
	}

	public void setItemOnClickListener(OnItemClickListener itemClickListener) {
		// TODO Auto-generated method stub
		onItemClickListener = itemClickListener;
	}

	public void setOnDismissListener(OnDismissListener dismissListener) {
		// TODO Auto-generated method stub
		this.onDismissListener = dismissListener;
		popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
			
			@Override
			public void onDismiss() {
				onDismissListener.onDismiss();
			}
		});
	}
	
	OnItemClickListener onItemClickListener;
	OnDismissListener  onDismissListener;
	public interface OnItemClickListener {
		void onItemClick(int index);
	}
	public interface OnDismissListener {
		void onDismiss();
	}

	int width = 0;
	Context context;
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public void showActionWindow(View parent, BaseActivity context, List<Area> list,
			int  width) {
		this.context = context;
		// final RingtoneclipModel currentData = model;
		// final int res_id = currentData.getId();
		mList = list;
		int[] location = new int[2];
		int popWidth = width;
		parent.getLocationOnScreen(location);
		View view = getView(context, list);
		popupWindow = new PopupWindow(view, popWidth, LayoutParams.WRAP_CONTENT);// new
																					// PopupWindow(view,
																					// popWidth,
																					// LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.showAsDropDown(parent, 0, 0);
	}
	
	public PopupWindow getPopupWindow(){
		return popupWindow;
	}
	
	
	public void showActionWindow(View parent, BaseActivity context, List<Area> list) {
		// final RingtoneclipModel currentData = model;
		// final int res_id = currentData.getId();
		mList = list;
		int[] location = new int[2];
		parent.getLocationOnScreen(location);
		View view = getView(context, list);
		if (width>0) {
			popupWindow = new PopupWindow(view, width, Util.getPixByDPI(context, 320));
		
		}else {
			popupWindow = new PopupWindow(view, LayoutParams.WRAP_CONTENT, Util.getPixByDPI(context, 320));// new
			// PopupWindow(view,
			// popWidth,
			// LayoutParams.WRAP_CONTENT);
		}
	
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow.showAsDropDown(parent, 0, 0);
	}
	
	private ListView childListView;
	public ListView getChildListView() {
		// TODO Auto-generated method stub
		return childListView;
	}
	public void dismiss() {
		popupWindow.dismiss();
	}
	private ProgressBar loadingbar;
	private TextView loadingNoData;
	private ListView areaGridView;
	private AreaAdapter adapter;
	
	
	
	private View getView(BaseActivity context, List<Area> list) {
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		ViewGroup layout  = (ViewGroup) inflater.inflate(R.layout.area_select_ui, null);
			 areaGridView = (ListView) layout.findViewById(R.id.area_label_listView);
			childListView  = (ListView) layout.findViewById(R.id.child_area_label_listView);
			loadingbar =  (ProgressBar) layout.findViewById(R.id.child_area_label_loading);
			loadingNoData =  (TextView) layout.findViewById(R.id.child_area_label_tv);
			
		 adapter = new AreaAdapter(context,list);
			areaGridView.setAdapter(adapter);
			areaGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					selectPosition=position;
					adapter.setCurrentSelection(position);
					beginLoadingData();
					if (onItemClickListener != null) {
						onItemClickListener.onItemClick(position);
					}
				}
			});
			
			//reselected
//			adapter.setCurrentSelection(selectPosition);
//			areaGridView.setSelection(selectPosition);
//			beginLoadingData();
//			if (onItemClickListener != null&&selectPosition>0) {//not select all
//				onItemClickListener.onItemClick(selectPosition);
//			}else {
//				showLoadingFinished();
//			}
		return layout;
	}
	
	

	private int selectPosition=0,childSelectPosition=0;
	
	private void beginLoadingData() {
		loadingbar.setVisibility(View.VISIBLE);
		loadingNoData.setVisibility(View.GONE);
		childListView.setVisibility(View.GONE);
	}
	
	private void showLoadingFinished() {
		childListView.setVisibility(View.VISIBLE);
		loadingbar.setVisibility(View.GONE);
		loadingNoData.setVisibility(View.GONE);
	}
	private void showNodata() {
		loadingNoData.setVisibility(View.VISIBLE);
		loadingbar.setVisibility(View.GONE);
	}
	public void showChildArea() {
		if (childListView.getAdapter()==null) {
			beginLoadingData();
			return;
		}
		if (childListView.getAdapter().getCount()>0) {
			showLoadingFinished();
		}else {
			showNodata();
		}
		
		
	}
	boolean showChildArea=true;
	public void setChildAreaShow(boolean childAreaShow) {
		this.showChildArea = childAreaShow;
	}



}
