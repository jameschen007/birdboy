package com.jameschen.birdboy.utils;

import java.io.IOException;

/**
 * Control log status by myself
 * @author jameschen
 *
 */
public class Log   {
	private static boolean  isShowLog=false;
 public static void i(String tag, String msg){
	 if (isShowLog) {
		 android.util.Log.i(tag, msg);
	}
	 
 }
 
 public static void e(String tag, String msg, IOException ex){

	 android.util.Log.e(tag, msg,ex);
 
 }
 public static void e(String tag, String msg){
	
	 android.util.Log.e(tag, msg);
 
 }
 
 public static void v(String tag, String msg){
	 if (isShowLog) {
		 android.util.Log.v(tag, msg);
	 }
 }
 
 public static void d(String tag, String msg){
	 if (isShowLog) {
		 android.util.Log.d(tag, msg);
	 }
 }
 
 public static void w(String tag, String msg, Exception e){
	 if (isShowLog) {
		 android.util.Log.w(tag, msg,e);
	 }
 }
 public static void w(String tag, String msg){
	 if (isShowLog) {
		 android.util.Log.w(tag, msg);
	 }
 }

public static void print(String msg) {
	// TODO Auto-generated method stub
	 if (isShowLog) {
		 android.util.Log.i("print", msg);
	 }
}
}
