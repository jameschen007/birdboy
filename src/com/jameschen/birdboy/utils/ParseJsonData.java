package com.jameschen.birdboy.utils;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.model.BaiduLocation;
import com.jameschen.birdboy.model.QueryBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.plugin.qq.TQq;

public class ParseJsonData {

	
	private static final String TAG = "json";
	 static Gson gson =new Gson();  
	public static   <T> QueryBirdboyJsonResponseContent<T>  getQueryData(String json,Type type, QueryBirdboyJsonResponseContent<T> jsonResponseContent) throws Exception {
		
          jsonResponseContent = gson.fromJson(json,type);
          return jsonResponseContent;
	}
	
	
	public static  TQq getQQData(String json, Type type) throws Exception{
		Log.i(TAG, "json===="+json);
		 TQq webBirdboyJsonResponseContent = gson.fromJson(json,type);
		 //
		 return webBirdboyJsonResponseContent;
	}
	
	public static  SNS getSNSData(String json) throws Exception{
		Log.i(TAG, "SNS json===="+json);
		 SNS webBirdboyJsonResponseContent = gson.fromJson(json,new TypeToken<SNS>() {
			}.getType());
		 //
		 return webBirdboyJsonResponseContent;
	}
	
	public static   BaiduLocation getBaiduLocationWebData(String json, Type type) throws Exception{
		
		 BaiduLocation webBirdboyJsonResponseContent = gson.fromJson(json,type);
		 //
		 return webBirdboyJsonResponseContent;
	}
	
	
	public static   <T> WebBirdboyJsonResponseContent<T>  getWebData(String json, Type type, WebBirdboyJsonResponseContent<T> webBirdboyJsonResponseContent) throws Exception{
		
		 webBirdboyJsonResponseContent = gson.fromJson(json,type);
		 return webBirdboyJsonResponseContent;
	}
}
