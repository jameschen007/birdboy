package com.jameschen.birdboy.weather;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.protocol.HTTP;

import com.jameschen.birdboy.utils.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.utils.HttpUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class ParsePM25 {
	Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	public List<PM25> getPM25List(String content) {
		Type type = new TypeToken<List<PM25>>() {
			}.getType();
		List<PM25> pm25Lists = gson.fromJson(content, type);
		// save by one day.
		return pm25Lists;
	}

	public  void getCityPM25(String city, OnPM25ResultCallbackListener resultCallbackListener) {
		if (city.contains("市")) {
			city=city.replace("市", "");
		}
		try {
			city = URLEncoder.encode(city, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url ="http://www.pm25.in/api/querys/aqi_details.json?token=MZ1npysKnWhqpz6sih35&city="+city+"&stations=yes";
		this.pm25ResultCallbackListener = resultCallbackListener;
		Log.i("city", "fromPM2.5url~~~~~~~"+city);
		AsyncHttpClient client = new AsyncHttpClient();
		client.setUserAgent("Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1");
		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			@Deprecated
			public void onFailure(int statusCode, Header[] headers,
					Throwable error, String content) {
				// TODO Auto-generated method stub
				super.onFailure(statusCode, headers, error, content);
				Log.i("reason", "code=="+statusCode);
			}
			@Override
			@Deprecated
			public void onSuccess(String content) {
				super.onSuccess(content);
				Log.i("PMInfo", content);
				if (content.contains("error")) {
					if (pm25ResultCallbackListener != null) {
						pm25ResultCallbackListener.PM25Result(null, null);
					}	
					return;
				}
				
				 List<PM25> pm25List = getPM25List(content);
				// save by one day.
				if (pm25ResultCallbackListener != null&&pm25List.size()>0) {
					pm25ResultCallbackListener.PM25Result(pm25List.get(pm25List.size()-1), null);
				}
			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				super.onFailure(arg0, arg1, arg2, arg3);
				if (pm25ResultCallbackListener != null) {
					pm25ResultCallbackListener.PM25Result(null, null);
				}
			}
		});
	}
	private OnPM25ResultCallbackListener pm25ResultCallbackListener;

	public interface OnPM25ResultCallbackListener {
		void PM25Result(PM25 pm25, String jsonContent);

	}
	
}
