package com.jameschen.birdboy.weather;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.jameschen.birdboy.model.entity.City;

public class ParseWeather {

	// 解析天气预报字符串成一个天气信息对象
	public static Weather parse(String weatherContent,City city) {

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

		Weather weather = null;

		try {
			 weather = new Weather();
			XMLReader xmlReader = saxParserFactory.newSAXParser().getXMLReader();
			WeatherXMLHandler handler = new WeatherXMLHandler(weather,city.getName());
			xmlReader.setContentHandler(handler);

			xmlReader.parse(new InputSource(new StringReader(weatherContent)));

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return weather;

	}

}