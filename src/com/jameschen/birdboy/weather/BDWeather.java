package com.jameschen.birdboy.weather;

import java.util.List;

public class BDWeather {
 private int error;
 private String status;
private String date;
private  List<BDResult> results;
public  List<BDResult> getResults() {
	return results;
}
public void setResults( List<BDResult> results) {
	this.results = results;
}
 
private String msg;
public String getMsg() {
	return msg;
}
public void setMsg(String msg) {
	this.msg = msg;
}
public int getError() {
	return error;
}
public void setError(int error) {
	this.error = error;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}

 
}
