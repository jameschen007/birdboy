package com.jameschen.birdboy.weather;

import java.util.List;

public class BDResult {
private String currentCity;
private List< BDWeatherData> weather_data;
public String getCurrentCity() {
	return currentCity;
}
public void setCurrentCity(String currentCity) {
	this.currentCity = currentCity;
}
public List<BDWeatherData> getWeather_data() {
	return weather_data;
}
public void setWeather_data(List<BDWeatherData> weather_data) {
	this.weather_data = weather_data;
}
}
