package com.jameschen.birdboy.weather;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.jameschen.birdboy.utils.Log;

public class WeatherXMLHandler extends DefaultHandler {

	// 天气预报weather
	Weather weather;

	// 记录出现次数
	int findCount = 0;

	// 默认构造方法
	public WeatherXMLHandler() {
		super();
	}
	private String cityName;

	private boolean find;
	// 构造方法
	public WeatherXMLHandler(Weather weather,String cityName) {
		this.weather = weather;
		this.cityName = cityName;
	}

	/*
	 * 文档结束时触发
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();
	}

	/*
	 * 文档开始时触发
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
	}

	/*
	 * 元素开始时触发
	 */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	

			if (qName.equals("city")) {
				if (attributes.getValue("cityname").contains(cityName)) {
					ParseWeather(attributes);
				}else	 if (attributes.getValue("cityname").contains("海淀")) {
					ParseWeather(attributes);
				}else	 if (attributes.getValue("cityname").contains("浦东")) {
					ParseWeather(attributes);
				}
			}
		
		super.startElement(uri, localName, qName, attributes);
	}

	private void ParseWeather(Attributes attributes) {
		weather.setCentername(attributes.getValue("centername"));
		weather.setHumidity(attributes.getValue("humidity"));
		Log.i("temp", "sss="+attributes.getValue("temNow"));
		weather.setTemNow(attributes.getValue("temNow"));
		weather.setTem1(attributes.getValue("tem1"));
		weather.setTem2(attributes.getValue("tem2"));
		Log.i("temp1", "sss="+attributes.getValue("tem1"));
		weather.setStateDetailed(attributes.getValue("stateDetailed"));
		weather.setWindDir(attributes.getValue("windDir"));
		weather.setWindState(attributes.getValue("windState"));		
		weather.setState1(attributes.getValue("state1"));		
		weather.setState2(attributes.getValue("state2"));		
	}

	/*
	 * 元素结束时触发
	 */
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
	}

	/*
	 * 读取元素内容
	 */
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		super.characters(ch, start, length);
	}

}