package com.jameschen.birdboy.weather;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.http.Header;

import com.jameschen.birdboy.utils.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.model.entity.SportWeatherInfo;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * 
 * 
 * @author Jameschen
 * 
 */
public class BDParseWeather {
	
	static HashMap<String, SportWeatherInfo> hashMap = new HashMap<String, SportWeatherInfo>();//name  drawableid
	
	public static SportWeatherInfo getWeatherImgRes(String weather){
		if (weather==null) {
			return null;
		}else {
			weather = weather.trim();
		}
		if (hashMap.containsKey(weather)) {
			return hashMap.get(weather);
		}else {
			if (weather.contains("转")) {
				String weatherStr[] = weather.split("转");
				Calendar c = Calendar.getInstance();
				int curHours = c.get(Calendar.HOUR_OF_DAY);
				if (curHours>12) {//time >12 use log2  <12 use log 1
					if (hashMap.containsKey(weatherStr[1])) {
						return hashMap.get(weatherStr[1]);
					}
				}else {
					if (hashMap.containsKey(weatherStr[0])) {
						return hashMap.get(weatherStr[0]);
					}
				}
			}
			return null;
		}
		
	}
	
	static{
		
		hashMap.put("晴", new SportWeatherInfo("阳光下的感觉真好",R.drawable.qing));
		hashMap.put("多云",new SportWeatherInfo("^_^广告词招租中...",R.drawable.yin));
		hashMap.put("阴",new SportWeatherInfo("^_^广告词招租中...",R.drawable.yin));
		hashMap.put("阵雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.xiaoyu));
		hashMap.put("雷阵雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.leizhenyu));
		hashMap.put("雷阵雨伴有冰雹",new SportWeatherInfo("乖乖待家里吧",R.drawable.leizhenyu));
		hashMap.put("雨夹雪",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.xiaoxue));
		hashMap.put("小雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.xiaoyu));
		hashMap.put("中雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.dayu));
		hashMap.put("大雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.dayu));
		
		hashMap.put("暴雨",new SportWeatherInfo("乖乖待家里吧",R.drawable.dayu));
		hashMap.put("大暴雨",new SportWeatherInfo("乖乖待家里吧",R.drawable.dayu));
		hashMap.put("特大暴雨",new SportWeatherInfo("乖乖待家里吧",R.drawable.dayu));
		hashMap.put("阵雪",new SportWeatherInfo("运动的都是(女)汉子",R.drawable.xiaoxue));
		hashMap.put("小雪",new SportWeatherInfo("运动的都是(女)汉子",R.drawable.xiaoxue));
		hashMap.put("中雪",new SportWeatherInfo("运动的都是(女)汉子",R.drawable.daxue));
		hashMap.put("大雪",new SportWeatherInfo("运动的都是(女)汉子",R.drawable.daxue));
		hashMap.put("暴雪",new SportWeatherInfo("乖乖待家里吧",R.drawable.daxue));
		hashMap.put("雾",new SportWeatherInfo("不要出来当吸尘器了",R.drawable.wumai));
		hashMap.put("冻雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.xiaoyu));
		
		hashMap.put("沙尘暴",new SportWeatherInfo("不要出来当吸尘器了",R.drawable.wumai));
		hashMap.put("小雨转中雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.xiaoyu));
		hashMap.put("中雨转大雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.dayu));
		hashMap.put("大雨转暴雨",new SportWeatherInfo("风雨无阻，运动记得带伞",R.drawable.dayu));
		hashMap.put("暴雨转大暴雨",new SportWeatherInfo("乖乖待家里吧",R.drawable.dayu));
		hashMap.put("大暴雨转特大暴雨",new SportWeatherInfo("乖乖待家里吧",R.drawable.dayu));
		hashMap.put("小雪转中雪",new SportWeatherInfo("运动的都是(女)汉子",R.drawable.xiaoxue));
		hashMap.put("中雪转大雪",new SportWeatherInfo("运动的都是(女)汉子",R.drawable.daxue));
		hashMap.put("大雪转暴雪",new SportWeatherInfo("运动的都是(女)汉子",R.drawable.daxue));
		hashMap.put("浮尘",new SportWeatherInfo("不要出来当吸尘器了",R.drawable.wumai));
		
		hashMap.put("扬沙",new SportWeatherInfo("不要出来当吸尘器了",R.drawable.wumai));
		hashMap.put("强沙尘暴",new SportWeatherInfo("不要出来当吸尘器了",R.drawable.wumai));
		hashMap.put("霾",new SportWeatherInfo("不要出来当吸尘器了",R.drawable.wumai));
	}
	

	Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	public BDWeather getBdWeather(String content) {
		BDWeather bdWeather = gson.fromJson(content, BDWeather.class);
		// save by one day.
		return bdWeather;
	}

	public void getCityWeater(String cityName,
			OnWeatherResultCallbackListener resultCallbackListener) {
		try {
			cityName = URLEncoder.encode(cityName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String url = "http://api.map.baidu.com/telematics/v3/weather?location="
				+ cityName + "&output=json&ak=3712a21fd73c350d7c322e6d834f20a3";
		this.weatherResultCallbackListener = resultCallbackListener;
		Log.i(cityName, "fromurl~~~~~~~");
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			@Deprecated
			public void onSuccess(String content) {
				super.onSuccess(content);
				BDWeather bdWeather =getBdWeather(content);
				// save by one day.
				if (weatherResultCallbackListener != null) {
					weatherResultCallbackListener.weatherResult(bdWeather,
							content);
				}
			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				super.onFailure(arg0, arg1, arg2, arg3);
				if (weatherResultCallbackListener != null) {
					weatherResultCallbackListener.weatherResult(null, null);
				}
			}
		});
	}

	private OnWeatherResultCallbackListener weatherResultCallbackListener;

	public interface OnWeatherResultCallbackListener {
		void weatherResult(BDWeather weather, String jsonContent);

	}
}
