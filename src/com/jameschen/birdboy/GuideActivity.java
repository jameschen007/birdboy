package com.jameschen.birdboy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.ui.GuidePageFragment;
import com.jameschen.birdboy.utils.UtilsUI;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class GuideActivity extends BaseActivity{

	static class Guide{
		public int resId;
		public Point point;
	}

	
	private String pageTag;
	static HashMap<String, Guide> hashMap = new HashMap<String, Guide>();
	static{
		
		Guide guide = new Guide();
		guide.resId=R.drawable.fujinyindao;
		hashMap.put(ConstValues.AROUND_USER, guide);
		guide = new Guide();
		guide.resId=R.drawable.huodongliebiao_yindao;
	    hashMap.put(ConstValues.SPORT_EVENT, guide);//
	    guide = new Guide();
		guide.resId=R.drawable.huodongxiangqingyindao;
		hashMap.put(ConstValues.SPORT_EVENT_DETAIL,guide);
		guide = new Guide();
		guide.resId=R.drawable.wode_wodehuod_xinshouyindao;
		hashMap.put(ConstValues.MY_SPORT_LIST, guide);
		guide = new Guide();
		guide.resId=R.drawable.wode_xinshouyindao;
		hashMap.put(ConstValues.ACCOUNT, guide);

	}
	
	ImageView guideBg;
	ImageView startButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTheme(R.style.Theme_Sherlock_Light_NoActionBar);
		setContentView(R.layout.guide_enter_ui);
		getSupportActionBar().hide();
		pageTag =getIntent().getStringExtra(ConstValues.Tag);
		guideBg = (ImageView) findViewById(R.id.guide_img);
		Guide guide = hashMap.get(pageTag);
		if (guide==null) {
			finish();
			return;
		}
		guideBg.setImageBitmap(GuidePageFragment.readBitMap(getBaseContext(), guide.resId));
		findViewById(R.id.start_btn0).setVisibility(View.GONE);
		findViewById(R.id.start_btn1).setVisibility(View.GONE);
		findViewById(R.id.start_btn2).setVisibility(View.GONE);
		findViewById(R.id.start_btn3).setVisibility(View.GONE);
		

		if (pageTag.equals(ConstValues.SPORT_EVENT_DETAIL)
				) {

			startButton = (ImageView) findViewById(R.id.start_btn1);
		}else if (pageTag.equals(ConstValues.ACCOUNT)
				) {

			startButton = (ImageView) findViewById(R.id.start_btn0);
		}else if (pageTag.equals(ConstValues.AROUND_USER)
				) {

			startButton = (ImageView) findViewById(R.id.start_btn3);
		}else if (pageTag.equals(ConstValues.MY_SPORT_LIST)
				) {

			startButton = (ImageView) findViewById(R.id.start_btn2);
		}else {
			startButton = (ImageView) findViewById(R.id.start_btn3);
		}
		
		startButton.setVisibility(View.VISIBLE);
		startButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}
	
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		overridePendingTransition(0,0);

	}
	
	@Override
	public void onBackPressed() {
		SlapshActivity.setGuidePageChecked(getBaseContext(), pageTag);
		super.onBackPressed();
		
	}
//check use what ui...bg
}
