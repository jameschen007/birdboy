package com.jameschen.birdboy;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jameschen.birdboy.adapter.ObjectAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.entity.QQUser;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.plugin.BindModel;
import com.jameschen.plugin.qq.AppConstants;
import com.jameschen.plugin.weibo.SNSUser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;

import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.AsyncWeiboRunner;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.openapi.UsersAPI;
import com.sina.weibo.sdk.openapi.legacy.StatusesAPI;
import com.sina.weibo.sdk.openapi.models.ErrorInfo;
import com.sina.weibo.sdk.openapi.models.Status;
import com.sina.weibo.sdk.openapi.models.StatusList;
import com.sina.weibo.sdk.openapi.models.User;
import com.sina.weibo.sdk.utils.LogUtil;

//查看个人主页 qq weibo
public class SeeUserPlugInPageActivity extends BaseActivity {

	private SNS sns;
	int which=-1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		sns = getIntent().getParcelableExtra("sns");
		which = getIntent().getIntExtra("type", -1);
		
		switch (which) {
		case BindModel.QQ:
			setTitle("腾讯微博");
			break;
		case BindModel.SINA:
			setTitle("新浪微博");
			break;
		default:
			break;
		}
	
		setContentView(R.layout.sns_layout);
		initViews();

		// test
		initStatusAPI(sns);
	}

	private ImageView logo, sexImg;
	private TextView name, care, fans, tweetNum;
	private View sexBackground;
	private TextView age;
	private TextView address;
	private View userInfoView;
	private ListView snsListView;
	private SNSAdapter snsAdapter;
	
	private TextView seeMoreInfo;
	
	private void initViews() {
		// sinaUser.avatar_large
		snsListView = (ListView) findViewById(R.id.sns_list_view);
		userInfoView = View.inflate(SeeUserPlugInPageActivity.this,
				R.layout.sns_user_bref, null);
		logo = (ImageView) userInfoView.findViewById(R.id.sns_logo);
		name = (TextView) userInfoView.findViewById(R.id.sns_name);
		care = (TextView) userInfoView.findViewById(R.id.foucs_num);
		fans = (TextView) userInfoView.findViewById(R.id.fans_num);
		tweetNum = (TextView) userInfoView.findViewById(R.id.tweet_num);
		sexBackground = userInfoView.findViewById(R.id.sex_age_container);
		sexImg = (ImageView) userInfoView.findViewById(R.id.sex_flag);
		age = (TextView) userInfoView.findViewById(R.id.age);
		address = (TextView) userInfoView.findViewById(R.id.sns_address);
		seeMoreInfo = (TextView) userInfoView.findViewById(R.id.sns_see_more);
		seeMoreInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String url =null;
				if (which==BindModel.SINA) {
					 url = "http://weibo.com/"+sns.getWeibo().getNick();
				}else if (which==BindModel.QQ) {
					 url = "http://t.qq.com/"+sns.getQq().getNick();

				}else {
					return;
				}
				
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent .setData(Uri.parse(url));
				startActivity(intent);
			}
		});
	}

	public void setUserInfo(final SNSUser user,int ageNum) {
		name.setText(user.name);
		care.setText(user.friends_count+"\n关注");
		fans.setText(user.followers_count+"\n粉丝");
		tweetNum.setText(user.statuses_count+"\n微博");
		address.setText(user.location);
		//age.setText(user.);
		int sexRes=R.drawable.boy;
		int defualtIcon = R.drawable.m;
		
		if (which==BindModel.QQ) {
			age.setText(""+ageNum);
		}

		int sexId=1;//default
		if ("f".equals(user.gender)) {
			sexId=2;
		}else {
			
		}
		sexRes=sexId==1?R.drawable.fujin_boy:R.drawable.fujin_girl;
		defualtIcon =sexId==1?R.drawable.m:R.drawable.wm;
		sexBackground.setBackgroundResource(sexId==1?R.drawable.round_bg_blue:R.drawable.round_bg_pink);
		sexImg.setImageResource(sexRes);
		DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(defualtIcon)
		.showImageForEmptyUri(defualtIcon)
		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(10))
		.cacheOnDisc(true).build();
		if (!TextUtils.isEmpty(user.avatar_large)) {
			loadImage(user.avatar_large, logo,mOptions);
			logo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent=new Intent(SeeUserPlugInPageActivity.this,ImageDetailActivity.class);
					ArrayList<String> phoneList = new ArrayList<String>();
					String blogoUrl = user.avatar_hd;
					phoneList.add(blogoUrl);
					intent.putStringArrayListExtra("photos", phoneList);
					//phoneList
					startActivity(intent);
				}
			});
		}else {
			logo.setImageResource(defualtIcon);
		}
		Log.i(TAG, "get data");
		snsListView.addHeaderView(userInfoView);
		snsAdapter = new SNSAdapter(SeeUserPlugInPageActivity.this);
		
		
		 snsListView.setAdapter(snsAdapter);
	
		 if (user.status!=null) {
				snsAdapter.addObject(user.status);
				snsAdapter.notifyDataSetChanged();
			} else {
				Log.e("status is null", "null....");
			}
		 
	}

	class SNSAdapter extends ObjectAdapter<Status> {
			
	SNSAdapter(BaseActivity activity){
		super(activity);
		initImageDisplayOptions(activity,defaultImageResId=0);
	}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			HoldView holdView;
			if (convertView==null) {
				LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = (ViewGroup) inflater.inflate(R.layout.sns_item,
						parent, false);
				holdView = new HoldView();
				bindItemId(holdView,convertView);
				convertView.setTag(holdView);
			}else {
				holdView=(HoldView) convertView.getTag();
			}
			
			setItemInfo(holdView,getItem(position));
			return convertView;
		}
		
		public void bindItemId(HoldView holdView, View convertView) {
			holdView.pic=(ImageView)convertView.findViewById(R.id.sns_pic);
			holdView.text=(TextView)convertView.findViewById(R.id.sns_text);
			holdView.time=(TextView)convertView.findViewById(R.id.sns_time);
		}
int defualtIcon = R.drawable.icon_default;
		
	private void setItemInfo(HoldView holdView, Status item) {
			//item.
		
		holdView.text.setText(Html.fromHtml(item.text));
		holdView.time.setText(item.created_at);
		DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(defualtIcon)
		.showImageForEmptyUri(defualtIcon)
		.showImageOnFail(defualtIcon).considerExifParams(true).cacheInMemory(true)
		.cacheOnDisc(true).build();
		Log.e(TAG, "pic----"+item.thumbnail_pic);
		if (!TextUtils.isEmpty(item.thumbnail_pic)) {
			holdView.pic.setVisibility(View.VISIBLE);
			loadImage(item.thumbnail_pic, holdView.pic,mOptions);
//			holdView.pic.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					Intent intent=new Intent(SeeUserPlugInPageActivity.this,ImageDetailActivity.class);
//					ArrayList<String> phoneList = new ArrayList<String>();
//					String blogoUrl = user.avatar_hd;
//					phoneList.add(blogoUrl);
//					intent.putStringArrayListExtra("photos", phoneList);
//					//phoneList
//					startActivity(intent);
//				}
//			});
		}else {
			holdView.pic.setImageResource(defualtIcon);
			holdView.pic.setVisibility(View.GONE);

		}
		}

	class	HoldView {
		private ImageView pic;
		private TextView text,time;
		
		
		
		
	}
		
		
	}

	/**
	 * 从 server 返回 读取 Token 信息。
	 * 
	 * @param context
	 *            应用程序上下文环境
	 * 
	 * @return 返回 Token 对象
	 */
	public static Oauth2AccessToken readAccessToken(String tokenStr, String uid) {

		Oauth2AccessToken token = new Oauth2AccessToken();
		token.setUid(uid);
		token.setToken(tokenStr);
		// token.setExpiresTime(pref.getLong(KEY_EXPIRES_IN, 0));
		return token;
	}

	private StatusesAPI mStatusesAPI;
	private Oauth2AccessToken mAccessToken;

	void initStatusAPI(SNS sns) {
		showProgressDialog("获取详情", "请稍候...",null);
		switch (which) {
		case BindModel.QQ:
		{
			Log.i("id==", "+===sns.getQq().getOpenid()"+sns.getQq().getOpenid());
			AsyncHttpClient client = new AsyncHttpClient();
			client.get("https://graph.qq.com/user/get_info?access_token="
					+ sns.getQq().getToken()
					+ "&oauth_consumer_key="+AppConstants.APP_ID+"&openid="
					+ sns.getQq().getOpenid() + "&format=json",
					new AsyncHttpResponseHandler() {
						@Override
						public void onFailure(int arg0, Header[] arg1,
								byte[] arg2, Throwable arg3) {
							Log.i(TAG, "errcode = " + arg0);
							super.onFailure(arg0, arg1, arg2, arg3);
							cancelProgressDialog();
							showToast("访问异常,请稍后再试");
						}

						@Override
						@Deprecated
						public void onSuccess(String content) {
							// TODO Auto-generated method stub
							super.onSuccess(content);
							Log.i(TAG, "qq user info=" + content);
							if (content.startsWith("{\"data\"")) {
								QQUser qqUser = QQUser.parse(content);
								setUserInfo(qqUser.getSNSUser(),qqUser.age);
							}else {
								showToast("访问异常");
							}

							cancelProgressDialog();
						}
					});
		
		}
			break;
		case BindModel.SINA:
			mAccessToken = readAccessToken(sns.getWeibo().getToken(), sns
					.getWeibo().getNick());
			// 对statusAPI实例化
			mStatusesAPI = new StatusesAPI(mAccessToken);
			// getTimeLine();
			getUserInfo();
		
			break;
		default:
			break;
		}
	
	}

	private void getUserInfo() {
		if (mAccessToken != null) {
	/*		UsersAPI usersAPI = new UsersAPI(mAccessToken);
			usersAPI.show(Long.parseLong(mAccessToken.getUid()), new RequestListener() {

				@Override
				public void onWeiboException(WeiboException arg0) {
					// TODO Auto-generated method stub
					Log.i(TAG, "wei bo ---onWeiboException==" + arg0);
					cancelProgressDialog();
					showToast("访问异常,请稍后再试");
				}

				@Override
				public void onComplete(String arg0) {
					Log.i(TAG, "wei bo ---response==" + arg0);
					if (arg0.startsWith("{\"id\"")) {
						User user = User.parse(arg0);
						setUserInfo(user,18);
					}else {
						showToast("访问异常");
					}

					cancelProgressDialog();
				}
			});*/

			Log.i("id==", "+===sns.gweibo");
			AsyncHttpClient client = new AsyncHttpClient();
			client.get("https://api.weibo.com/2/users/show.json?access_token=" +
					sns.getWeibo().getToken()+"&uid="
					+ sns.getWeibo().getNick(),
					new AsyncHttpResponseHandler() {
						@Override
						public void onFailure(int arg0, Header[] arg1,
								byte[] arg2, Throwable arg3) {
							Log.i(TAG, "errcode = " + arg0);
							super.onFailure(arg0, arg1, arg2, arg3);
							cancelProgressDialog();
							showToast("访问异常,请稍后再试");
						}

						@Override
						@Deprecated
						public void onSuccess(String content) {
							// TODO Auto-generated method stub
							super.onSuccess(content);
							Log.i(TAG, "weibo user info=" + content);
							if (content.startsWith("{\"id\"")) {
								SNSUser user = SNSUser.parse(content);
								setUserInfo(user,18);
							}else {
								showToast("访问异常");
							}

							cancelProgressDialog();
						}
					});
		
		
		}
	}

	private void getTimeLine() {
		if (mAccessToken != null /* && mAccessToken.isSessionValid() */) {
			mStatusesAPI.userTimeline(0, 0, 10, 1, false, 0, false,
					new RequestListener() {
						@Override
						public void onComplete(String response) {
							Log.i(TAG, "wei bo ---response==" + response);
							if (!TextUtils.isEmpty(response)) {
								LogUtil.i(TAG, response);
								if (response.startsWith("{\"statuses\"")) {
									// 调用 StatusList#parse 解析字符串成微博列表对象
									StatusList statuses = StatusList
											.parse(response);
									if (statuses != null
											&& statuses.total_number > 0) {
										Toast.makeText(
												SeeUserPlugInPageActivity.this,
												"获取微博信息流成功, 条数: "
														+ statuses.statusList
																.size(),
												Toast.LENGTH_LONG).show();
									}
								} else if (response
										.startsWith("{\"created_at\"")) {
									// 调用 Status#parse 解析字符串成微博对象
									Status status = Status.parse(response);
									// Toast.makeText(WBStatusAPIActivity.this,
									// "发送一送微博成功, id = " + status.id,
									// Toast.LENGTH_LONG).show();
								} else {

								}
							}
						}

						@Override
						public void onWeiboException(WeiboException e) {
							LogUtil.e(TAG, e.getMessage());
							ErrorInfo info = ErrorInfo.parse(e.getMessage());
							// Toast.makeText(WBStatusAPIActivity.this,
							// info.toString(), Toast.LENGTH_LONG).show();
						}
					});
		} else {
			Log.i(TAG, "??????????????");
		}
	}
}
