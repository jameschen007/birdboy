package com.jameschen.birdboy.location;

import java.util.ArrayList;
import java.util.List;

import android.text.TextUtils;

import com.jameschen.birdboy.model.entity.City;

public class LocationUtil {
	public static int[] cityIds = { 8, 2, 1, 4, 3,9};
	private static String[] cityNames = { "成都", "北京", "上海", "广州", "杭州","重庆"};
	private static String[] cityPinyinNames = { "chengdu", "beijing", "shanghai", "guangzhou", "hangzhou","chongqing"};
	// 成都 8 北京2 上海1 广州 4 杭州 3
	public static List<City> Citys = new ArrayList<City>();
	static {
		if (Citys.size() == 0) {
			for (int i = 0; i < cityIds.length; i++) {
				City city = new City();
				city.setId(cityIds[i]);
				city.setName(cityNames[i]);
				city.setPinyinName(cityPinyinNames[i]);
				Citys.add(city);
			}
		}
	}

	private  static City createDefaultCity() {
//		City city = new City();
//		city.setId(-1);
//		city.setName("全部");
//		city.setPinyinName("all");
//		return city;
		return Citys.get(0);
	}
	
	public static City getCurrentCityById(int id){
		if (id ==-1) {
			
			return createDefaultCity();//just default
		}
		for (int i = 0; i < cityIds.length; i++) {
			if (id == cityIds[i]) {
				return Citys.get(i);
			}
		}
		return createDefaultCity();//just default
	}
	
	public static City getCurrentCityId(String cityName){
		if (TextUtils.isEmpty(cityName)) {
			
			return createDefaultCity();//just default
		}
		for (int i = 0; i < cityNames.length; i++) {
			if (cityName.contains(cityNames[i])) {
				return Citys.get(i);
			}
		}
		return createDefaultCity();//just default
	}
	
}
