package com.jameschen.birdboy.location;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.widget.TextView;

import com.jameschen.birdboy.environment.ConstValues.IntentAction;
import com.jameschen.birdboy.location.LocationSupport.LocationType;
import com.jameschen.birdboy.location.LocationSupport.OnReceiveLocationListener;
import com.jameschen.birdboy.utils.Log;

public class LocationManager     {
	Context context;

	private LocationSupport locationSupport;
	private TextView locationTextView;

	private  float plat,plont;
	
	
	public LocationSupport getLocationSupport() {
		return locationSupport;
	}

	private boolean findlocation=false,locByHandle=false;
	
	public interface GetLastLocationListener{
		void onLocResult(float lat,float lont, String where);
	}
	
	GetLastLocationListener getLastLocationListener;
	
	public void findLocation(Context context,GetLastLocationListener onGetLastLocationListener,boolean isLocByHandle) {
		this.context = context;
		this.getLastLocationListener = onGetLastLocationListener;
		findlocation=false;
		this.locByHandle =isLocByHandle;
		locationSupport = LocationSupport
				.getLocationSupportInstance(context.getApplicationContext());
		locationSupport.startLocate();
		locationSupport.setOnReciveLocationListener(
				new OnReceiveLocationListener() {

					@Override
					public void recevieLocation(float lat, float lont,String where,boolean fresh) {
						if (!findlocation) {
							findlocation=true;
							if (getLastLocationListener!=null) {
								getLastLocationListener.onLocResult(lat, lont, where);	
							}
							if (!locByHandle) {
								Intent intent=new Intent(IntentAction.FIND_LOCATION);
								intent.putExtra("address", locationSupport.getLocation());
								LocationManager.this.context.sendBroadcast(intent);
							}
							
						}
					if (plat!=lat||plont!=lont||fresh) {

							Log.i(LOCATION, "New position changed--->;at==="+lat+";lont=="+lont);
							if (!TextUtils.isEmpty( locationSupport.getLocation())) {
								Intent intent=new Intent(IntentAction.LOCATION_CHANGED);
								 intent.putExtra("lat", lat);
								 intent.putExtra("lng", lont);
								LocationManager.this.context.sendBroadcast(intent);
							}
					
					}
					plat =lat;
					plont =lont;
					}

					@Override
					public void recevieAddress(String address) {
						if (locationTextView==null) {
							return;
						}
						locationTextView.setText(address);

					}
				}, LocationType.ADDRESS);
	}

	



	private final String LOCATION = "location";
	public static final int HASCHANGE = 0x10;

	private void saveLocation() {
		String curLocation = locationTextView.getText().toString();
		SharedPreferences perPreferences = context.getApplicationContext()
				.getSharedPreferences(LOCATION, 0);
		Editor editor = perPreferences.edit();
		editor.putString(LOCATION, curLocation);
		editor.commit();
	}


	private TextView addrRoadText;


	public void cancel() {
		if (locationSupport!=null) {
			locationSupport.cancelLocate();
		}
	
	}




}
