package com.jameschen.birdboy.location;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.model.BaiduLocation;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.content.Context;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import com.jameschen.birdboy.utils.Log;
public class LocationSupport {
	public enum LocationType {
		STREET, ADDRESS
	}

	public static final int STARTED = -2;
	public static final int RUN = 0;
	LocationManager locationManager;
	private String wherePlace = null;
	// ��λ���
	LocationClient mLocClient;
	public MyLocationListenner myListener ;

	private static LocationSupport locationSupport;

	public static LocationSupport getLocationSupportInstance(Context context) {
		synchronized (LocationSupport.class) {
			if (locationSupport == null) {
				locationSupport = new LocationSupport(context);
			}
		}
		return locationSupport;
	}

	Context context;

	private LocationSupport(Context context) {
		this.context = context;
		init();
	}
	private void init() {
		mLocClient = new LocationClient(context);
		 myListener = new MyLocationListenner();
		mLocClient.registerLocationListener(myListener);
		
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// ��gps
		option.setCoorType("bd09ll"); // �����������
		option.setAddrType("all");
		option.setPriority(LocationClientOption.GpsFirst);
		option.setScanSpan(5000);
		mLocClient.setLocOption(option);

	}
	public int startLocate() {
		cancelLocate();
		init();
//		if (mLocClient.isStarted()) {
//			return RUN;
//		}
		mLocClient.start();
		return RUN;
	}

	public void cancelLocate() {
		if (mLocClient != null)
			mLocClient.stop();
		unregisterListener();

	}

	private void unregisterListener() {
		if (mLocClient != null && myListener != null) {
			mLocClient.unRegisterLocationListener(myListener);
		}
		myListener = null;
		mLocClient = null;
		receiveLocationListener = null;
		handler.removeCallbacksAndMessages(null);
		//locationSupport = null;
	}

	public String getLocation() {

		return wherePlace;
	}

	private static final int MSG_GET_GPS_LOCATION = 0x01;
	private static final int MSG_GET_UNKONW_GPS_LOCATION = 0x02;
	Handler handler = new Handler() {
		public void dispatchMessage(android.os.Message msg) {
			switch (msg.what) {
			case MSG_GET_GPS_LOCATION:
			case MSG_GET_UNKONW_GPS_LOCATION:
				if (receiveLocationListener != null) {
					receiveLocationListener.recevieAddress(msg.obj.toString());
				}
				break;

			default:
				break;
			}
		};
	};

	private float latitude = -1, longtitude = -1;

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(float longtitude) {
		this.longtitude = longtitude;
	}

	private OnReceiveLocationListener receiveLocationListener;
	private LocationType mode = LocationType.ADDRESS;
	public String province;
	public String city;
	public String district;

	public void setOnReciveLocationListener(
			OnReceiveLocationListener mReceiveLocationListener,
			LocationType type) {
		receiveLocationListener = mReceiveLocationListener;
		mode = type;
	}

	public interface OnReceiveLocationListener {
		void recevieLocation(float lat, float lont,String address,boolean fresh);

		void recevieAddress(String address);
	}
	AsyncHttpClient client = new AsyncHttpClient();
	private boolean localByServer=false;
	public boolean isLocByServer() {
		// TODO Auto-generated method stub
		return localByServer;
	}
	/**
	 * 
	 */
	public class MyLocationListenner implements BDLocationListener {
		@Override
		public void onReceiveLocation(BDLocation location) {
			Message msg = new Message();
			if (location == null) {
					msg.obj = context.getResources().getString(
							R.string.location_unkonw);
					msg.what = MSG_GET_UNKONW_GPS_LOCATION;
				
				handler.sendMessage(msg);
				return;
			}
			msg.what = MSG_GET_GPS_LOCATION;
			// String address = location.getAddrStr();

			setLatitude((float) (location.getLatitude()));
			setLongtitude((float) (location.getLongitude()));
			
			
			province = location.getProvince();
			city = location.getCity();
			district = location.getDistrict();
			
			String address = null;
			switch (mode) {
			case STREET:
				address = location.getStreet();
				break;
			case ADDRESS:
				address = location.getAddrStr();
				break;

			default:
				break;
			}
			if (address!=null) {
				localByServer=false;
			}else {
//				if (latitude>0) {
//					localByServer=true;
//					String url = "http://api.map.baidu.com/geocoder?output=json&location="+latitude+",%20"+longtitude+"&key=3712a21fd73c350d7c322e6d834f20a3";
//				
//					client.get(url, new AsyncHttpResponseHandler(){
//						@Override
//						public void onFinish() {
//							// TODO Auto-generated method stub
//							super.onFinish();
//							localByServer = false;
//						}
//						@Override
//						@Deprecated
//						public void onSuccess(String content) {
//							// TODO Auto-generated method stub
//							super.onSuccess(content);
//							
//							BaiduLocation baiduLocation =ParseJsonData.getBaiduLocationWebData(content, new TypeToken<BaiduLocation>() {
//							}.getType());
//							
//							if (baiduLocation!=null&&baiduLocation.getStatus()!=null&&baiduLocation.getStatus().equals("0")) {
//								if (baiduLocation.getResult()!=null) {
//									if (baiduLocation.getResult().getFormatted_address()!=null) {
//										wherePlace =baiduLocation.getResult().getFormatted_address();
//									}
//									if (baiduLocation.getResult().getAddressComponent()!=null) {
//										city = baiduLocation.getResult().getAddressComponent().getCity();
//									}
//									
//								}
//							}
//							
//							if (receiveLocationListener!=null) {
//								receiveLocationListener.recevieLocation(getLatitude(), getLongtitude(),wherePlace,true);
//							}
//						}
//						@Override
//						public void onFailure(int arg0, Header[] arg1, byte[] arg2,
//								Throwable arg3) {
//							// TODO Auto-generated method stub
//							super.onFailure(arg0, arg1, arg2, arg3);
//							
//						}
//					});
//				}
			}
			wherePlace =address;
			if (receiveLocationListener!=null) {
				receiveLocationListener.recevieLocation(getLatitude(), getLongtitude(),wherePlace,false);
			}
		
			
			msg.obj = address;
			if (address == null) {
				msg.obj = context.getResources().getString(
						R.string.location_unkonw);
				msg.what = MSG_GET_UNKONW_GPS_LOCATION;
			}
			handler.sendMessage(msg);
		}

		public void onReceivePoi(BDLocation poiLocation) {
			if (poiLocation == null) {
				return;
			}
		}
	}
	public boolean isLocByHandle() {
		// TODO Auto-generated method stub
		return false;
	}


	
//	public class MyLocation {
//	    String key = "3712a21fd73c350d7c322e6d834f20a3";
//	                                                                                                                                                                                                                  
//	    public String getAddress(String latValue, String longValue){
//	        String location = getJsonLocation(latValue, longValue);
//	        location = getLocation(makeResults(location));
//	        return location;
//	    }
//	                                                                                                                                                                                                                  
//	    private String getJsonLocation(String latValue, String longValue){
//	        String urlStr = "http://api.map.baidu.com/geocoder?location=" + latValue + "," + longValue + "&output=json&key=" + key;
//	        HttpClient httpClient = new DefaultHttpClient();
//	           String responseData = "";
//	           try{
//	               //向指定的URL发送Http请求
//	               HttpResponse response = httpClient.execute(new HttpGet(urlStr));
//	               //取得服务器返回的响应
//	               HttpEntity entity = response.getEntity();
//	               BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()));
//	               String line = "";
//	               while((line = bufferedReader.readLine()) != null){
//	                   responseData = responseData + line;
//	               }
//	           }
//	           catch (Exception e) {
//	               e.printStackTrace();
//	           }
//	             return responseData;
//	    }
//	                                                                                                                                                                                                                  
//	    private String makeResults(String result){
//	        String dealResult = result.substring(0, result.indexOf("result") +8) + "[" + result.substring(result.indexOf("result") +8, result.length()-1) + "]}";
//	        return dealResult;
//	    }
//	                                                                                                                                                                                                                  
//	    private String getLocation(String str){
//	          JSONArray jsonObjs;
//	          String location = "";
//	        try {
//	             jsonObjs = new JSONObject(str).getJSONArray("result");
//	                //取出数组中第一个json对象(本示例数组中实际只包含一个元素)
//	                JSONObject jsonObj = jsonObjs.getJSONObject(0);
//	              //解析得formatted_address值
//	              String address = jsonObj.getString("formatted_address");
//	              String bussiness = jsonObj.getString("business");
//	              location = address + ":" + bussiness;
//	        } catch (JSONException e) {
//	            // TODO Auto-generated catch block
//	            e.printStackTrace();
//	        }
//	          //取出数组中第一个json对象(本示例数组中实际只包含一个元素)
//	          return location;
//	    }
//	}


}
