package com.jameschen.birdboy.common;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpStatus;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.network.BirdBoyStringRequest;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.ParseJsonData;

import android.content.Context;
import android.os.Handler;

public class UploadLocationFreshController {

	private static Timer locTimer;
	private static long period=5*60*1000;//5 min
	public static void uploadLocation(final Context applicationContext,final Map<String, String> headMap) {
		//retry
		timerCancel();
		locTimer = new Timer();
		locTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				final	LocationSupport locationSupport = LocationSupport.getLocationSupportInstance(applicationContext);
				if (locationSupport.getLatitude()>0) {
					if (LogInController.IsLogOn()) {//make sure is log on..
						upload(applicationContext,headMap,locationSupport.getLongtitude(),locationSupport.getLatitude());
					}else {
						timerCancel();
					}
				}else {
					//wait next time try
				}
			}
		}, 0, period);	
		
	}

	private static void timerCancel(){
		if (locTimer!=null) {
			locTimer.cancel();
			locTimer=null;
		}
	}
	private static void upload(Context applicationContext, Map<String, String> headMap, float longtitude,
			float latitude) {
//			IF (RETURN INVIAL) {//SESSION ERROR  JUST CANCEL...
//				
//			}
//		birdboy.cn/positionAPI  POST请求
//
//		字段               类型                描述                           备注            值条件
//
//
//		position    String        104.06195 30.584685       必填
//
//		返回字段描述
//
//		 3、返回字段
//
//		{
//		    "result": {      
//		    }, 
//		    "success": true, 
//		    "code": 0
//		}
		
		RequestQueue queue  = Volley.newRequestQueue(applicationContext, new HttpClientStack(LogInController.getHttpClientInstance()));
	Map<String, String> paraMap=headMap;
	paraMap.put("position", longtitude+" "+latitude);
	
		BirdBoyStringRequest stringRequest = new BirdBoyStringRequest(paraMap, Request.Method.POST, 
				HttpConfig.REQUST_POSITION_URL, new Response.Listener<String>() {
					

					@Override
					public void onResponse(String response) {
						final String responseData = response;
						new Thread(new Runnable() {
							@Override
							public void run() {
								int status = -1;
								String saveResponseData = responseData;
								// for encode bug
								try {
									saveResponseData = new String(
											responseData
													.getBytes("ISO-8859-1"),
											"utf-8");
									Log.i("uploadPosition", "onResponse:*****"
											+ saveResponseData);
									WebBirdboyJsonResponseContent<Object> webBirdboyJsonResponseContent = new WebBirdboyJsonResponseContent<Object>();
									try {
										webBirdboyJsonResponseContent = ParseJsonData.getWebData(saveResponseData,  new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
										}.getType(),
												webBirdboyJsonResponseContent);
										if (webBirdboyJsonResponseContent.getCode()==-15) {//session loss
											timerCancel();
										}else {
																	
										
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								} catch (UnsupportedEncodingException e) {
									e.printStackTrace();
								}
							
								
							

							}
						}).start();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						error.printStackTrace();
						
					}
				});
		stringRequest.setTag("uploadPosition");
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));                
		queue.add(stringRequest);
	}

}
