package com.jameschen.birdboy.common;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import cn.jpush.android.api.JPushInterface;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.Register0Activity;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.handler.FreshHandler;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.QQ;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.model.entity.Weibo;
import com.jameschen.birdboy.service.JpushReceiver;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.CheckValid;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.MD5Encode;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.jameschen.plugin.AuthBindController;
import com.jameschen.plugin.BindModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

public class LogInController  {

	
	
	public static Type getEntityType() {
		return new TypeToken<WebBirdboyJsonResponseContent<Account>>() {
		}.getType();
	}

	
	private static void cancelLogin() {

		if (loginDialog!=null) {
			loginDialog.cancel();
			loginDialog=null;
		}
		
	}
	
	private static void getDetailInfo(Account content, final BaseActivity activity, String account, String password, OnLogOnListener logOnListener) {

		cancelLogin();
		Register0Activity.saveUserToPreference(activity, account, password, true);
		activity.showToast("登录成功!");
		activity.sendBroadcast(new Intent(LoginFragment.ACTION_LOGON));
	//	Cookie cookie =	getCookie(mHttpClient.getCookieStore(), "JSESSIONID");
	//	saveJsessionIdToPref(cookie.getValue(),activity);	
		currentAccount = content;
		JPushInterface.setAliasAndTags(activity, "zcz"+currentAccount.getId(), null);
		JpushReceiver.showNotionficationFromNative(activity);
		if (logOnListener!=null) {
			logOnListener.logon();
		}
		
		if (!TextUtils.isEmpty(currentAccount.getSns())) {
			try {
				currentAccount.setSns(ParseJsonData.getSNSData(currentAccount.getSns()));
				//parse SNS....
				BindModel bindModel;
				//if (bindModel!=null) {//every >3 montch  need get a  new token...
				//刷新token
					//判断token是否过期
					//upload token... .by sns .
					//AuthBindController.bindAuth(activity, bindModel, resultListener);
				//}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				Log.i("fresh location", "fresh location~~~");
				//fresh location every 5 min.
				UploadLocationFreshController.uploadLocation(activity.getApplicationContext(),activity.getPublicParamRequstMap());
									
			}
		}, 3000);

		
	
	}
	
	
	
	public interface OnLogOnListener{
		void logon();
	}
	
	
	public static void login(final BaseActivity activity,final CharSequence account, final CharSequence password,final OnLogOnListener  logListener ) {
		activity.closeInputMethod();
		boolean nextCheckStep = CheckValid.checkNickNameValid(activity, account, 1, 14);

		if (nextCheckStep) {
			nextCheckStep = CheckValid.passwordCheckValid(activity,
					password, 6, 14);
		}

		if (nextCheckStep) {
			activity.closeInputMethod();
			// MD5Encode.encode(password) 
			activity.showProgressDialog("登录", "正在登录,请稍候...", null);
			Map<String, String> map =activity. getPublicParamRequstMap();
			map.put("nickname",  account.toString());
			map.put("password",MD5Encode.encode( password.toString()));
			map.put("gcode", "1234");
			final AsyncHttpClient logHttpClient = new AsyncHttpClient();
			 PersistentCookieStore myCookieStore = new PersistentCookieStore(activity);
			 myCookieStore.clear();
			 if (cookieStore!=null) {
				cookieStore.clear();
				cookieStore=null;
			}
			 logHttpClient.setCookieStore(myCookieStore);
			
			logHttpClient.post(HttpConfig.REQUST_LOGIN_URL, new RequestParams(map), new AsyncHttpResponseHandler(){
				 @Override
				@Deprecated
				public void onSuccess(int statusCode, String content) {
					super.onSuccess(statusCode, content);
					if (activity.isFinishing()) {
						return;
					}
					//check ??
					Log.i(content, "succ~~"+content);
					Type type =getEntityType();
					WebBirdboyJsonResponseContent webBirdboyJsonResponseContent = new WebBirdboyJsonResponseContent();
					try {
						webBirdboyJsonResponseContent = ParseJsonData.getWebData(content, type,
								webBirdboyJsonResponseContent);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						activity.cancelProgressDialog();
						return ;
					}
					activity.cancelProgressDialog();
					
					if (webBirdboyJsonResponseContent.isSuccess()) {
						activity.cancelProgressDialog();
						 HttpContext httpContext = logHttpClient.getHttpContext();
					     cookieStore = (CookieStore) httpContext.getAttribute(ClientContext.COOKIE_STORE);
					
						checkLogon(activity,account.toString(),password.toString(),logListener);
						
					} else {
						activity.showToast(webBirdboyJsonResponseContent.getInfo());
					}
				
				 }
				 @Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					 if (activity.isFinishing()) {
						return;
					}
					 activity.showToast("访问异常,请稍候重试 错误码:"+arg0);
					 activity.cancelProgressDialog();
				 }
			 });
//			activity.postDataLoader(map,
//					HttpConfig.REQUST_LOGIN_URL, uiFreshController,
//					new HttpClientStack(mHttpClient));
		}

	}
	

	public static Account currentAccount;
	public static boolean IsLogOn(){
		return cookieStore==null||currentAccount==null?false:true;
	}
	
	private static CookieStore cookieStore;
	public static AbstractHttpClient getHttpClientInstance(){
		if (cookieStore!=null) {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			httpClient.setCookieStore(cookieStore);
			return httpClient;
		}
		return null;
	}
	public static void setHttpClientNull(boolean sessionInvild) {
	
			if (cookieStore!=null) {
				cookieStore.clear();
			}
		
		cookieStore= null;
		if (!sessionInvild) {
			currentAccount=null;	
			}
	}
	public static void setHttpClientNull(){
				setHttpClientNull(false);
		
	}
//	
	

	
	private static void checkLogon(final BaseActivity activity ,final String mAccount, final String mPassword, final OnLogOnListener logOnListener) {

		
		Map<String, String> map = activity.getPublicParamRequstMap();
		
		
		//loginController. loginDialog = activity.createProgressDialog("登录", "正在登录,请稍候...", null);
		createProgressDialog(activity, "登录", "正在登录,请稍候...", null);
		final AsyncHttpClient logHttpClient = new AsyncHttpClient();
		
		if (cookieStore!=null) {
			Log.i("cookie", "fssfsfsf");
			logHttpClient.setCookieStore(cookieStore);
		}
		
			logHttpClient.post(HttpConfig.REQUST_USER_DETIAL_INFO_URL, new RequestParams(map), new AsyncHttpResponseHandler(){
			 @Override
			@Deprecated
			public void onSuccess(int statusCode, String content) {
				super.onSuccess(statusCode, content);
				if (activity.isFinishing()) {
					return;
				}
				Log.i("logOn", "content~~"+content);
				Type type =getEntityType();
				WebBirdboyJsonResponseContent webBirdboyJsonResponseContent = new WebBirdboyJsonResponseContent();
				try {
					webBirdboyJsonResponseContent = ParseJsonData.getWebData(content, type,
							webBirdboyJsonResponseContent);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					activity.cancelProgressDialog();
					return ;
				}
				activity.cancelProgressDialog();
				cancelLogin();
				if (webBirdboyJsonResponseContent.isSuccess()) {
					
					Account accountInfo =(Account) webBirdboyJsonResponseContent.getResult();
					getDetailInfo(accountInfo, activity, mAccount, mPassword, logOnListener);   
				
				} else {
					activity.showToast(webBirdboyJsonResponseContent.getInfo());
				}
			
			 }
			 @Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1, arg2, arg3);
				 if (activity.isFinishing()) {
					return;
				}

					activity.cancelProgressDialog();
				 activity.showToast("访问异常,请稍候重试 错误码:"+arg0);
				 cancelLogin();
			 }
		 });
//		activity.postDataLoader(map,
//				HttpConfig.REQUST_USER_DETIAL_INFO_URL, loginController,
//				new HttpClientStack(mHttpClient));
	}
	
	public static ProgressDialog createProgressDialog(Activity activity, String pTitle,
			String pMessage,
			DialogInterface.OnCancelListener pCancelClickListener) {
		if (loginDialog!=null) {
			loginDialog.cancel();
			loginDialog=null;
		}
		loginDialog = ProgressDialog.show(activity, pTitle, pMessage, true, true);
		loginDialog.setCancelable(true);
		loginDialog.setOnCancelListener(pCancelClickListener);

		return (ProgressDialog) loginDialog;
	}
	private static ProgressDialog loginDialog;
	public static void setBindModel(BindModel bindModel) {

		if (LogInController.currentAccount ==null) {
			return;
		}
			if (bindModel.fromtype==BindModel.QQ) {

				SNS sns = LogInController.currentAccount.getSnsObj();
				if (sns==null) {
					sns = new SNS();
				}
				QQ qq = new QQ();
				qq.setNick(bindModel.nick);
				qq.setOpenid(bindModel.openid);
				qq.setToken(bindModel.token);
				sns.setQq(qq);
				LogInController.currentAccount.setSns(sns);
			
			}else if (bindModel.fromtype==BindModel.SINA) {

				SNS sns = LogInController.currentAccount.getSnsObj();
				if (sns==null) {
					 sns = new SNS();
				}
				Weibo weibo = new Weibo();
				weibo.setNick(bindModel.nick);
				weibo.setToken(bindModel.token);
				sns.setWeibo(weibo);
				LogInController.currentAccount.setSns(sns);
			
			}
	}
	
//	public static String readJsessionIdFromPref(BaseActivity activity) {
//
//		SharedPreferences _preferences = PreferenceManager
//				.getDefaultSharedPreferences(activity);
//		return _preferences.getString(SESSIONID, "");
//	}
//
//	public static void saveJsessionIdToPref(String val,BaseActivity activity) {
//
//		SharedPreferences _preferences = PreferenceManager
//				.getDefaultSharedPreferences(activity);
//		_preferences.edit().putString(SESSIONID, val).commit();
//	}
}