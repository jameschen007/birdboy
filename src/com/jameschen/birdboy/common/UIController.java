package com.jameschen.birdboy.common;

import java.lang.reflect.Type;

import com.jameschen.birdboy.model.Page;

public interface UIController {
	
public Type getEntityType();

public   <E> void refreshUI(E content,Page page);

public void  responseNetWorkError(int errorCode,Page page);



/**
 * 
 * */
public String getTag();


}
