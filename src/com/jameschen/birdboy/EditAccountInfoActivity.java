package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.adapter.AroundUserAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.Account;
import com.jameschen.birdboy.model.entity.Category;
import com.jameschen.birdboy.ui.AccountFragment;
import com.jameschen.birdboy.utils.FliterDialog;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.FliterDialog.onFilerSelectedListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class EditAccountInfoActivity extends BaseActivity implements
		OnClickListener {
	private static final int EDIT = 0x12;
	private View headPhotoView,nickNameView,sexView,signView;
	private ImageView headPhoto;
	private TextView nickname, sex,sign,age,sportLike;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.edit_accout);
		setTitle("个人信息");
		Category item0= new Category();
		item0.setName("女");
		item0.setType(2);
		categories.add(item0);
		Category item1= new Category();
		item1.setName("男");
		item1.setType(1);
		categories.add(item1);
		Account account =LogInController.currentAccount;
		initView(account);
//		
	
	}

	List<Category> categories = new ArrayList<Category>();
	private DisplayImageOptions roundOptions;
	private boolean isChanged;
	
	private void initView(Account account) {
		if (account==null) {
			return;
		}
		nickname = (TextView) findViewById(R.id.nickname);
		sign = (TextView) findViewById(R.id.sign);
		sex = (TextView) findViewById(R.id.sex);
		age = (TextView) findViewById(R.id.age);
		sportLike = (TextView) findViewById(R.id.sport_like);
		headPhoto = (ImageView) findViewById(R.id.head_logo);
		//set infos
		nickname.setText(account.getNick());
		sportLike.setText(AroundUserAdapter.getSportTypeStr(account.getMylike()));
		age.setText(account.getAge()+"");
		if (TextUtils.isEmpty(account.getSignstr())) {
			sign.setText(getString(R.string.no_sign));
		}else {
			sign.setText(account.getSignstr());
		}
		if (account.getSex()==2) {
			sex.setTag(categories.get(0));
		}else {
			sex.setTag(categories.get(1));
		}
		
		int defaultResImg=R.drawable.icon_default;
		if (account.getSex()==1) {//girl
			
		}else {
			
		}
		
		
		roundOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(defaultResImg)
		.showImageForEmptyUri(defaultResImg)
		.showImageOnFail(defaultResImg).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(20))
		.cacheOnDisc(true).build();
		loadImage(HttpConfig.HTTP_BASE_URL+ account.getLogo(), headPhoto, roundOptions);
		
		sex.setText(account.getSex()==2?"女":"男");
		//set listeners
		findViewById(R.id.headphoto_layout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivityForResult(new Intent(EditAccountInfoActivity.this,EditHeadPhotoActivity.class), EDIT);
			}
		});
		findViewById(R.id.nickname_layout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//startActivityForResult(new Intent(EditAccountInfoActivity.this,EditNickNameActivity.class), EDIT);
			}
		});
		
		findViewById(R.id.sign_layout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(EditAccountInfoActivity.this,EditSignActivity.class), EDIT);
			}
		});
		
		findViewById(R.id.sex_layout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(EditAccountInfoActivity.this,EditSexActivity.class), EDIT);
			}
		});
		
		
		findViewById(R.id.age_layout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(EditAccountInfoActivity.this,EditAgeActivity.class), EDIT);
			}
		});
		
		mylike = LogInController.currentAccount.getMylike();
		
		findViewById(R.id.sport_like_layout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FliterDialog fliterDialog = new FliterDialog(false,false);
				fliterDialog.showFilterDialog(EditAccountInfoActivity.this, new onFilerSelectedListener() {
					
					
					@Override
					public void onItemSelect(Category sex, Category sport) {
						
						if (sport==null||LogInController.currentAccount.getMylike()==sport.getType()) {
							return;
						}
						//
						 mylike =sport.getType();
						
						 UIController updateAccountController = new UIController() {
							
							@Override
							public void responseNetWorkError(int errorCode, Page page) {
								showToast("修改失败");
							}
							
							@Override
							public <E> void refreshUI(E content, Page page) {
								showToast("更新资料成功!");
								sportLike.setText(AroundUserAdapter.getSportTypeStr(mylike));
								LogInController.currentAccount.setMylike(mylike);
							}
							
							@Override
							public String getTag() {
								// TODO Auto-generated method stub
								return TAG;
							}
							
							@Override
							public Type getEntityType() {
								// TODO Auto-generated method stub
								return new TypeToken<WebBirdboyJsonResponseContent<Object> >(){}.getType();
							}
						};
						
						showProgressDialog("修改资料", "正在提交，请稍候...", null);
						Map<String, String> map = HttpUtil.createEditAccountMap(EditAccountInfoActivity.this);
						map.put("mylike", ""+mylike);
						postDataLoader(map,"http://birdboy.cn/updateMeAPI", updateAccountController,new HttpClientStack(LogInController.getHttpClientInstance()));
						
						
					}
				},mylike);
			}
		});
	}
	private int mylike;

	@Override
	public void onBackPressed() {
		if (isChanged) {
			setResult(RESULT_OK);
		}
		super.onBackPressed();
	
	}
	
    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(arg0, arg1, arg2);
    	switch (arg0) {
		case EDIT:
			if (arg1==RESULT_OK) {
				isChanged=true;
				Account account =LogInController.currentAccount;
				initView(account);
			}
			
			break;

		default:
			break;
		}
    }
	

}
