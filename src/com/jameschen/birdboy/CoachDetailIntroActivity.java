package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.internal.Utils;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.CoachDetailAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebCoachDetail;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;


public class CoachDetailIntroActivity extends BaseActivity {


	private TextView coachName,level,courseIntro,ability,info;
	private String frontuserid;
	
	


	@Override
			protected void onCreate(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				super.onCreate(savedInstanceState);
				setContentView(R.layout.coach_detail_intro);
				 
				Intent intent =getIntent();
				CoachDetailInfo coach =intent.getParcelableExtra(CoachListFragment.COACH);
				frontuserid = intent.getStringExtra("frontuid");
				setTitle("详细资料");
				initViews();
				setCoachInfoToUI(coach);
				
	}
	
	
	private Button contactButton;
	private void initViews() {
		coachName = (TextView) findViewById(R.id.coach_name);
		level = (TextView) findViewById(R.id.coach_level);
		courseIntro = (TextView) findViewById(R.id.coach_course);
		ability = (TextView) findViewById(R.id.coach_ability_intro);
		contactButton= (Button) findViewById(R.id.contact_btn);
		info = (TextView) findViewById(R.id.coach_detail_intro);
	

	findViewById(R.id.send_message_btn).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (frontuserid!=null) {
					Util.go2SendMessage(CoachDetailIntroActivity.this,coachName.getText().toString(),frontuserid,0);
				}
			}
		});

	contactButton.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (LogInController.IsLogOn()) {
				if (mCoachDetailInfo!=null) {
					Util.showContactDialog(CoachDetailIntroActivity.this, mCoachDetailInfo.getName(), mCoachDetailInfo.getMobile(), null);

				}
			}else {
				Util.go2LogInDialog(CoachDetailIntroActivity.this,new OnLogOnListener() {
					
					@Override
					public void logon() {
						if (mCoachDetailInfo!=null) {
							Util.showContactDialog(CoachDetailIntroActivity.this, mCoachDetailInfo.getName(), mCoachDetailInfo.getMobile(), null);

						}
					}
				});
			}
		}
	});
	}

	
	
	private <T> void setCoachInfoToUI(T coach) {

			 mCoachDetailInfo = (CoachDetailInfo) coach;
			coachName.setText(mCoachDetailInfo.getName());
		
			level.setText(CoachAdapter.getLevelStr(mCoachDetailInfo.getLevel()));
			ability.setText(Html.fromHtml(mCoachDetailInfo.getClasses()));
			courseIntro.setText(Html.fromHtml(mCoachDetailInfo.getSkill()));
			info.setText(Html.fromHtml(mCoachDetailInfo.getInfo()));
		
	

		}
	
	
	CoachDetailInfo mCoachDetailInfo;

}
