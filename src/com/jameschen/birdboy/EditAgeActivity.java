package com.jameschen.birdboy;

import java.util.Map;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.toolbox.HttpClientStack;
import com.jameschen.birdboy.base.BaseEditAccountActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.utils.HttpUtil;

public class EditAgeActivity extends BaseEditAccountActivity {

	
	private EditText ageEdit;
	private int age;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_age);
		setTitle("年龄");
		initView();
		
		setTopBarRightBtnListener("保存", new OnClickListener() {
			
			@Override
			public void onClick(View arg) {
				if (TextUtils.isEmpty(ageEdit.getText())) {
					showToast("请输入年龄");
					return;
				}
				
				int inputAge =Integer.parseInt(ageEdit.getText().toString());
				if (inputAge<0||inputAge>120) {
					showToast("输入年龄非法");
					return;
				}
				//no change. do nothing.
				if (age==inputAge) {
					finish();
					return;
				}

				commitDone();
			}
		});
	}

private void initView() {
	age = LogInController.currentAccount.getAge();
	ageEdit = (EditText) findViewById(R.id.age_edit);
	

	}

@Override
protected void commitDone() {
	showProgressDialog("修改资料", "正在提交，请稍候...", null);
	Map<String, String> map = HttpUtil.createEditAccountMap(EditAgeActivity.this);
	age = Integer.parseInt(ageEdit.getText().toString());
	map.put("age", ""+age);
		
	postDataLoader(map,"http://birdboy.cn/updateMeAPI", updateAccountController,new HttpClientStack(LogInController.getHttpClientInstance()));
	
}

@Override
protected void editOk() {
LogInController.currentAccount.setAge(age);
}
}
