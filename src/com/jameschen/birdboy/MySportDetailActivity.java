package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import android.R.integer;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.internal.Utils;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.CoachDetailAdapter;
import com.jameschen.birdboy.adapter.SportEventAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.base.BaseDetailActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebCoachDetail;
import com.jameschen.birdboy.model.WebSportEventDetail;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Location;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.model.entity.SportEvent;
import com.jameschen.birdboy.model.entity.SportEventDetail;
import com.jameschen.birdboy.model.entity.SportEventPhoto;
import com.jameschen.birdboy.model.entity.SportUser;
import com.jameschen.birdboy.model.entity.Stadium;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.umeng.analytics.MobclickAgent;


public class MySportDetailActivity extends BaseDetailActivity {

	private boolean showPhone = false;
	private boolean showMoreDetail= false;
	private SportEventDetail sportEventDetail;
	private SportEvent sportEvent;
	
	
	/*1、功能说明
	活动创建人在“我创建活动”审核报名人

	2、请求字段
	birdboy.cn/joinCheckAPI  POST请求

	字段               类型                描述                           备注            值条件


	actionid   String        活动的id                必填


	userid      String       用户的id                 必填

	返回字段描述

	 3、返回字段

	{
	    "result": {      
	    }, 
	    "success": true, 
	    "code": 0
	}PS:参加活动的人会收到push 消息，依据 myaction 里面 addint 字段标示状态，addint=1 表示报名通过审核，其他状态都是未审核状态。
*/
	
	@Override
			protected void onCreate(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				super.onCreate(savedInstanceState);
				setContentView(R.layout.my_activity_detail);
				 
				Intent intent =getIntent();
				SportEvent sportEvent =intent.getParcelableExtra(ConstValues.SPORT_EVENT);
				if (sportEvent==null) {
					id=intent.getIntExtra("activity_id", -1);
					showMoreDetail = intent.getBooleanExtra("showOtherDetail", false);
					showPhone = intent.getBooleanExtra("showPhone", false);
				}else{
					id=sportEvent.getId();
				
					dist = (float) sportEvent.getDist();
				}
				Log.i(TAG, "id==="+id);
				setTopBarRightBtnListener(R.drawable.share, new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (sportEventDetail!=null) {
							String content ="你的朋友使用了“鸟孩运动” 邀请你参加活动“"+sportEventDetail.getAction().getName()+"”，快来下载吧。你可以约周边的朋友一起运动。找到周边的场馆，教练。";
							v.setTag(content);
							share(v,ConstValues.TYPE_EVENT_VAL);
						}
						
					}
				});
				
				
				if (showMoreDetail) {
					setTitle("我创建的");
				}else {
					setTitle("我报名的");
				}
				
				initViews();
				showProgressDialog("刷新", "请稍候...", null);
				setDetailInfoToUI(sportEvent);
				getDataLoader("http://birdboy.cn/uactionAPI?aid="+id, false,new Page(), uiFreshController,new HttpClientStack(LogInController.getHttpClientInstance()));
				
	}
	
	long id;
	private ImageView photo,sex;
	private TextView fav,join,comment,admin,jioners,commentNum;
	private ViewGroup memberLayout;
	private TextView name;
	private TextView address;
	private TextView content;
	private TextView distance;
	private TextView time,extraInfo,week;
	private TextView phone,stillCheck;
	private ImageView createrPhoto;
	
	private void initViews() {
		name = (TextView)  findViewById(R.id.event_item_name);
		address = (TextView)findViewById(R.id.event_item_address);
		distance = (TextView) findViewById(R.id.event_item_location_distance);
		time = (TextView) findViewById(R.id.event_item_date_time);
		week = (TextView) findViewById(R.id.event_item_week);
		fav = (TextView) findViewById(R.id.event_fav_num);
		content =(TextView) findViewById(R.id.event_intro);
		comment = (TextView) findViewById(R.id.event_add_comment);
		commentNum = (TextView) findViewById(R.id.event_comment);
		createrPhoto =(ImageView) findViewById(R.id.event_creater_photo);
		fav = (TextView) findViewById(R.id.event_add_fav);
		join = (TextView) findViewById(R.id.event_add_join);
		jioners = (TextView) findViewById(R.id.event_joiner);
		memberLayout = (ViewGroup)findViewById(R.id.event_joiners_info);
		extraInfo= (TextView) findViewById(R.id.event_item_category_info);
		admin =(TextView) findViewById(R.id.event_creater_name_text);
		sex =(ImageView) findViewById(R.id.event_creater_sex);
		phone =(TextView) findViewById(R.id.event_phone);
		stillCheck =(TextView) findViewById(R.id.event_still_check);
		
		photo =(ImageView) findViewById(R.id.event_item_portrait);
	address.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (sportEventDetail!=null&&sportEventDetail.getAction()!=null) {
					Util.go2Map(getBaseContext(), Location.CreateLocation(sportEventDetail.getAction().getPlace(),sportEventDetail.getAction().getPosition()));
				}
			}
		});
		photo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				if (sportPhotos==null||sportPhotos.size() == 0) {
					return;
				}
				ArrayList<String> phoneList = new ArrayList<String>();
				for (int i = 0; i < sportPhotos.size(); i++) {
					phoneList.add(HttpConfig.HTTP_BASE_URL+sportPhotos.get(i).getUrl());
				}
				Util.go2SeeBigPics(MySportDetailActivity.this,phoneList);
			
				
			}
		});
		
		roundOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.icon_default)
		.showImageForEmptyUri(R.drawable.icon_default).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.showImageOnFail(R.drawable.icon_default).considerExifParams(true).cacheInMemory(true) 
		.cacheOnDisc(true).build();
		
		((View)comment.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				Util.go2SendComment(MySportDetailActivity.this,id,ConstValues.TYPE_EVENT_VAL);
			}
		});
		
		((View)fav.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				if (LogInController.IsLogOn()) {
					showProgressDialog("关注活动", "提交，请稍候...", null);
					Log.i(TAG, "id==="+id);
					getDataLoader("http://birdboy.cn/keepUactionAPI?aid="+id, false, null, favController, new HttpClientStack(LogInController.getHttpClientInstance()));
				}else {
					Util.go2LogInDialog(MySportDetailActivity.this,new OnLogOnListener() {
						
						@Override
						public void logon() {
							showProgressDialog("关注活动", "提交，请稍候...", null);
							Log.i(TAG, "id==="+id);
							getDataLoader("http://birdboy.cn/keepUactionAPI?aid="+id, false, null, favController, new HttpClientStack(LogInController.getHttpClientInstance()));
					
						}
					});
				}
		
			}
		});
		((View)join.getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (sportEventDetail==null) {
					return ;
				}
				Util.CommitDialog(MySportDetailActivity.this,"参与报名","请确认活动时间?\n"+
				""+sportEventDetail.getAction().getTimeStr() +" "+ 
				SportEventAdapter.getWeekDayStr(sportEventDetail.getAction().getSweek())		,new Dialog.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (LogInController.IsLogOn()) {
							showProgressDialog("参加活动", "提交，请稍候...", null);
							getDataLoader(HttpConfig.REQUST_JOIN_EVENT_URL+id, false, null, joinController, new HttpClientStack(LogInController.getHttpClientInstance()));
						}else {
							Util.go2LogInDialog(MySportDetailActivity.this,new OnLogOnListener() {
								
								@Override
								public void logon() {
									showProgressDialog("参加活动", "提交，请稍候...", null);
									getDataLoader(HttpConfig.REQUST_JOIN_EVENT_URL+id, false, null, joinController, new HttpClientStack(LogInController.getHttpClientInstance()));
						
								}
							});
						}
				
					}
					
				});
				
			}
		});
		
		commentNum.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Util.go2CommentList(MySportDetailActivity.this,ConstValues.TYPE_EVENT_VAL,id);
			}
		});
	}

	private DisplayImageOptions roundOptions;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case ConstValues.TYPE_EVENT_VAL:
		case ConstValues.TYPE_USER_VAL:
		{
			if (resultCode == RESULT_OK) {
				showProgressDialog("刷新", "请稍候...", null);
				doRefresh();

			}
		}
			break;

		default:
			break;
		}
	}
	
	
	private String getCostInfo(float rmb) {
		return rmb==0?"免费":"￥"+rmb;
	}
	public String getDistanceStr(double  distance) {
		if (distance >= 1000) {// show in km
			DecimalFormat df = new DecimalFormat(".0");
			return df.format(distance / 1000) + "km";
		} else {
			return (int) distance + "m";
		}
	}
	
	private boolean fromDetail =false;
	private float dist=-1;
	
	private <T> void setDetailInfoToUI(T detail) {
		if (detail==null) {
			return;
		}
		if (detail instanceof SportEvent) {
			SportEvent sportEvent =(SportEvent) detail;
			List<SportUser> sportUsers =SportEventAdapter.getUsers(sportEvent.getUsers());
			int sexRes=R.drawable.boy;
			if (sportUsers!=null) {
				sexRes=sportUsers.get(0).getSex()==1?R.drawable.boy:R.drawable.girl;
				if (showPhone) {
					phone.setVisibility(View.VISIBLE);
					stillCheck.setVisibility(View.GONE);
					final String phoneNum =sportUsers.get(0).getPhone();
					SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(phoneNum);
					spannableStringBuilder.setSpan(new UnderlineSpan(), 0, spannableStringBuilder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					
					phone.setText(spannableStringBuilder);
					phone.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							Intent intent=new Intent("android.intent.action.CALL",Uri.parse("tel:"+phoneNum));
							startActivity(intent);	
							//Util.showContactDialog(activity, name, phone, qq)
						}
					});
				}else {
					phone.setVisibility(View.GONE);
					stillCheck.setVisibility(View.VISIBLE);
				}
			}
			
			String businessTag =sportEvent.getAddint()>=2?"商家":"个人";
			if (sportEvent.getAddint()<2) {//personal 
				sex.setVisibility(View.VISIBLE);
				sex.setImageResource(sexRes);
			}else {
				sex.setVisibility(View.GONE);
			}
			
			extraInfo.setText(CoachAdapter.setSportTypeStr(sportEvent.getType()) +"  "+ getCostInfo(sportEvent.getRmb())+" "+businessTag+" "+ 
"邀请"+sportEvent.getNum()+"人");
			name.setText(sportEvent.getName());
			address.setText(sportEvent.getPlace());
			admin.setText(sportEvent.getContact());
			jioners.setText("报名 "+"("+sportEvent.getWantnum()+"人)"
					+ "    通过  ("+sportEvent.getNownum()+")");
		//	commentNum.setText("留言"+"("+sportEvent.getComments()+")");
			time.setText(sportEvent.getTimeStr());
			week.setText(SportEventAdapter.getWeekDayStr(sportEvent.getSweek()));
			
			if (dist>=0) {
				findViewById(R.id.event_item_location_icon).setVisibility(View.VISIBLE);
				distance.setVisibility(View.VISIBLE);
				distance.setText(getDistanceStr(dist));
			}else {
				findViewById(R.id.event_item_location_icon).setVisibility(View.GONE);
				distance.setVisibility(View.GONE);
			}
			
			if (sportUsers!=null) {
				setSportMemberInfoToUI(sportUsers);
			}
			
			if (sportEvent.getAddtext()==null) {
				return;
			}
			
			loadImage(HttpConfig.HTTP_BASE_URL+ sportEvent.getAddtext(), photo, roundOptions);
		} else {
			SportEventDetail sportEventDetail =(SportEventDetail) detail;
			this.sportEventDetail =sportEventDetail;
			SportEvent sportEvent =sportEventDetail.getAction();
			setDetailInfoToUI(sportEvent);
			//jioners.setText(text);
			time.setText(sportEvent.getTimeStr());
			week.setText(SportEventAdapter.getWeekDayStr(sportEvent.getSweek()));
			content.setText(Html.fromHtml(sportEventDetail.getContent()));
		}
	}
	
	private void loadImageByUrl(final SportUser sportUser, ImageView photo,int roundVal) {
		String logo =sportUser.getLogo();
		 DisplayImageOptions options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.icon_default).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.showImageForEmptyUri(R.drawable.icon_default).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		.showImageOnFail(R.drawable.icon_default).considerExifParams(true).cacheInMemory(true) .displayer(new RoundedBitmapDisplayer(roundVal))
		.cacheOnDisc(true).build();
		 if (!TextUtils.isEmpty(logo)) {
			 loadImage(HttpConfig.HTTP_BASE_URL+ logo, photo, options);
		}
		
		final String slogoUrl =logo;
		photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Util.go2SeeUserHomePage(MySportDetailActivity.this, sportUser.getId());
									
			}
		});
			
	}
	
	private void setSportMemberInfoToUI(List<SportUser> sportUsers) {

		ViewGroup memberLayout = (ViewGroup)findViewById(R.id.event_joiners_info);
		int size =sportUsers.size();
		int len=0;
		if (size == 0) {
			return;//no more
		}
		
		
		loadImageByUrl(sportUsers.get(0), createrPhoto, 20);
		
		
		//showMoreDetail
		memberLayout.removeViews(1, memberLayout.getChildCount()-1);
		
		//reCalcu size
		sportUsers=SportEventDetailActivity.getReverseList(sportUsers);
		size = sportUsers.size();
		
		
		for (int i = 0; i < size; i++) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup vGroup =(ViewGroup) inflater.inflate(R.layout.sport_join_member_list_item, memberLayout, false);
			TextView name = (TextView) vGroup.findViewById(R.id.name0);
			ImageView photo = (ImageView) vGroup.findViewById(R.id.photo0);
			TextView  phone =(TextView) vGroup.findViewById(R.id.phone0);
			name.setText(sportUsers.get(i).getName());
			if (showMoreDetail) {
				phone.setText(sportUsers.get(i).getPhone());
				phone.setVisibility(View.VISIBLE);
				vGroup.findViewById(R.id.allow_layout).setVisibility(View.VISIBLE);
				//setoncli
				final int usrId =sportUsers.get(i).getId();
				Button checkJion = (Button) vGroup.findViewById(R.id.allow_btn);
				boolean isCheck = CheckUserID(usrId);
				changeJionBttonStatus(checkJion, isCheck);
				checkJion.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						setJoinCheckReq((Button)v,usrId);
	
					}
				});
			}else {
				phone.setVisibility(View.GONE);
				final int usrId =sportUsers.get(i).getId();
				boolean isCheck = CheckUserID(usrId);
				checkJionPassStatus(phone, isCheck);
				
			}
		
			loadImageByUrl(sportUsers.get(i), photo, 10);
			
			
			memberLayout.addView(vGroup);
			 if (i<size-1) {
				 View line =(View) inflater.inflate(R.layout.item_dot_line, memberLayout, false);
				 memberLayout.addView(line);
			 }
		}
	
	}
	
	private boolean CheckUserID(int usrId) {
		if (userCheckIDs==null) {
			Log.e(TAG, "user  is null"+usrId);
			return false;
		}
		int len =userCheckIDs.length;
		Log.i(TAG, usrId+"len=="+len);
		for (int i = 0; i < len; i++) {
			if (usrId==userCheckIDs[i]) {
				Log.i(TAG, "find");
				return true;
			}
		}
		
		return false;
	}

	private List<SportEventPhoto> sportPhotos;
	private void setPhotosToUI(List<SportEventPhoto> sportPhotos) {
		if (sportPhotos.size()==0) {
			return ;
		}
		if (this.sportPhotos ==null) {
			this.sportPhotos = new ArrayList<SportEventPhoto>();
		}
		this.sportPhotos.clear();
		this.sportPhotos.addAll(sportPhotos);
	}
	
	private void setJoinCheckReq(final Button checkView,final int userId){
		//post
	
		
		UIController checkJionController = new UIController() {
			
			@Override
			public void responseNetWorkError(int errorCode, Page page) {
				// TODO Auto-generated method stub
				showToast("审核失败，请稍候重试");
			}
			
			@Override
			public <E> void refreshUI(E content, Page page) {
				// TODO Auto-generated method stub
				showToast("审核通过");
				changeJionBttonStatus(checkView,true);
				
			}
			
			@Override
			public String getTag() {
				// TODO Auto-generated method stub
				return TAG+userId;
			}
			
			@Override
			public Type getEntityType() {
				// TODO Auto-generated method stub
				return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
				}.getType();
			}
		};	//sportUsers.get(i).get
		Map<String, String> map = getPublicParamRequstMap();
		map.put("actionid", ""+id);
		map.put("userid", ""+userId);
		
		postDataLoader(map, "http://birdboy.cn/joinCheckAPI", checkJionController, false, null, new HttpClientStack(LogInController.getHttpClientInstance()));
	
		
	}
	
	protected void checkJionPassStatus(TextView checkView, boolean checked) {
		if (checked) {
			checkView.setText("活动人同意TA参加");
			checkView.setTextColor(getResources().getColor(R.color.darkgreen));
			checkView.setVisibility(View.VISIBLE);
		}else {
			checkView.setVisibility(View.GONE);
		}
	}

	
	protected void changeJionBttonStatus(Button checkView, boolean checked) {
		if (checked) {
			checkView.setText("已通过");
			checkView.setEnabled(false);
			checkView.setBackgroundResource(R.drawable.wodebangding_03);
		}else {
			checkView.setText("通过");
			checkView.setEnabled(true);
			checkView.setBackgroundResource(R.drawable.invite_btn_state);
		}
	}


	private void setCommentsToUI(List<Comment> comments) {

		ViewGroup viewGroup = (ViewGroup) findViewById(R.id.event_comment_info);
		int size =comments.size();
		int len=0;
		if (size == 0) {
	
			return;//no more
		}
		viewGroup.removeViews(1, viewGroup.getChildCount()-1);
		if (size>5) {//show more page.
			len=5;
		}else {
			len =size ;
		}
		
		for (int i = 0; i < len; i++) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup vGroup =(ViewGroup) inflater.inflate(R.layout.comment_item, viewGroup, false);
			TextView name = (TextView) vGroup.findViewById(R.id.comment_item_name);
			TextView info = (TextView) vGroup.findViewById(R.id.comment_item_content);
			TextView  date = (TextView) vGroup.findViewById(R.id.comment_item_date);
			name.setText(comments.get(i).getUsernick());
			info.setText(Html.fromHtml(comments.get(i).getContent()));
			date.setText(comments.get(i).getCtstr());
			final Comment comment =comments.get(i);
			View replay=(View) vGroup.findViewById(R.id.comment_reply);
			replay.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
				Util.go2SendComment(MySportDetailActivity.this,id,comment.getUserid(),comment.getUsernick(),ConstValues.TYPE_EVENT_VAL);
				}
			});
			viewGroup.addView(vGroup);
			 if (i<len-1) {
				 
				 View line =(View) inflater.inflate(R.layout.item_dot_line, viewGroup, false);
					viewGroup.addView(line);
			 }
		}
	
	}

private int[] userCheckIDs;
	
	private void onDataChangeNotify(WebSportEventDetail webSportEventDetail) {
		if (webSportEventDetail.getCheckUserids()!=null) {
			userCheckIDs =webSportEventDetail.getCheckUserids();
		}
		
		SportEventDetail sportEventDetail =webSportEventDetail.getActionDetail();
		sportEventDetail.setAction(webSportEventDetail.getAction());
		
		setDetailInfoToUI(sportEventDetail);
		
		setPhotosToUI(webSportEventDetail.getPhotos());
		setCommentsToUI(webSportEventDetail.getComments());

		commentNum.setText("留言"+"("+webSportEventDetail.getCommentCount()+")");
	}
	
	void doRefresh(){
		
		getDataLoader("http://birdboy.cn/uactionAPI?aid="+id, false,new Page(), uiFreshController);

	}
	protected UIController joinController=new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			MobclickAgent.onEvent(MySportDetailActivity.this, "activity_jion");
			showToast("报名成功，去【我的活动】查看发起人是否审核");
			doRefresh();
			currentSportChangeStatus=true;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			if (errorCode>-1) {
				
			showToast(getString(R.string.jion_failed_by_network));
			}
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG+"join";
		}
	};
	
	private boolean currentSportChangeStatus=false;
	
	private UIController favController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
	
			showToast("收藏活动成功！进入“个人面板-我的活动”查看我收藏的活动");
			currentSportChangeStatus=true;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
	if (errorCode>-1) {
				showToast(getString(R.string.get_detail_info_error));
			}
			
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG+"fav";
		}
	};
	
	private UIController uiFreshController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			WebSportEventDetail webSportEventDetail = (WebSportEventDetail) content;
			onDataChangeNotify(webSportEventDetail);
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<WebSportEventDetail>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			// TODO Auto-generated method stub
			if(errorCode>-1){
			showToast(getString(R.string.get_detail_info_error));
		}
		}

		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
	};

}
