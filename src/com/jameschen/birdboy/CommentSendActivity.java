package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.Map;

import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RatingBar;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Util;

//http://birdboy.cn/addcommentAPI    get请求
//
//字段               类型                描述                           备注            值条件
//
//score               int                 提交的分数
//
//replycontent    string            点评的内容
//
//userid              int                点评用户的id
//
//nick                 string            点评用户的昵称
//
//id                     int                 场馆或者教练的id
//
//fromtype          int               评论的类型 1：场馆   2  教练
//
//返回数据
//
//{success:true;}


public class CommentSendActivity extends BaseActivity{
	EditText inputEditText;
	RatingBar score;
	long id;
	int uid;
	int type;
	boolean isFeedback=false;
	String nick;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment_send_ui);
		setTitle("点评");
		Intent intent = getIntent();
		 id = intent.getLongExtra("id", -1);
		 type =intent.getIntExtra("type", -1);
		 uid = intent.getIntExtra("uid", -1);
		 nick = intent.getStringExtra("nick");
		initViews();
		 if (type == ConstValues.TYPE_EVENT_VAL) {//sport
				((View)score.getParent()).setVisibility(View.GONE);
			}
		
		 if (uid>-1) {
			 ((View)score.getParent()).setVisibility(View.GONE);
			 setTitle("回复"+nick);
			 isFeedback=true;
		 }
		 
		setTopBarRightBtnListener("发送", new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				closeInputMethod();
				// TODO Auto-generated method stub
				if (TextUtils.isEmpty(inputEditText.getText())||inputEditText.getText().toString().trim().equals("")) {
					showToast("请输入点评内容");
					return;
				}else {
					if (!LogInController.IsLogOn()) {
						Util.go2LogInDialog(CommentSendActivity.this,new OnLogOnListener() {
							
							@Override
							public void logon() {
								commitSend();
							}
						});
						return;
					}

				 commitSend();
				}
			
			}
		});
	}

	protected void commitSend() {
		Map<String, String> map = getPublicParamRequstMap();
		showProgressDialog("点评", "正在提交,请稍候...", null);
		map.put("userid", LogInController.currentAccount.getId()+"");
		map.put("nick", LogInController.currentAccount.getNick());
		
		map.put("id", ""+id);
		map.put("score", ""+(int)(score.getRating()*2));
		map.put("replycontent", inputEditText.getText().toString());
		if (uid>-1) {
			map.put("renick", ""+nick);
			map.put("renickId", ""+uid);
		}
		map.put("fromtype", ""+type);
		String url = HttpUtil.getRequestMapUrlStr(HttpConfig.REQUST_COMMENT_URL, map);
		getDataLoader(url, false,null, uiFreshPhotoController,new HttpClientStack(LogInController.getHttpClientInstance()));		
	}

	private void initViews() {
		 inputEditText = (EditText) findViewById(R.id.comment_Edit);
		 if (isFeedback) {
			inputEditText.setHint("写下你想对ta说的话.");
		}
		 score =(RatingBar)findViewById(R.id.total_comment_num);
		 inputEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				checkInputLen(s);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				checkInputLen(s);
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		 
	}

	protected void checkInputLen(CharSequence s) {
		if (!TextUtils.isEmpty(s)&&s.length()>=240) {
			showToast("小伙伴，你的内容太多了，简单点嘛？(字数限制240以内)");
		}
	}

	protected UIController uiFreshPhotoController=new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {

	if (errorCode>-1) {
				
			}
			
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			if (isFeedback) {
				showToast("回复成功");
			}else {
				showToast("谢谢你的点评!");
			}
			
			setResult(RESULT_OK);
			finish();
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}

		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object>>() {
			}.getType();
		}
	};
	
}
