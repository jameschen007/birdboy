package com.jameschen.birdboy.handler;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.model.Page;

public class FreshHandler extends Handler {

	public static final int MSG_OBTAION_CONTENT = 0x01;
	public static final int MSG_OBTAION_CONTENT_FAILED = 0x02;
	public static final String TAG = "FreshHandler";
	private BaseActivity activity;

	public interface DoRetryListener{
		 String  getRetryTag();
		 void retry();
	 }
	private DoRetryListener retryListener;
	public FreshHandler(BaseActivity activity) {
		this.activity = activity;
	}

	@Override
	public void dispatchMessage(Message msg) {
		// TODO Auto-generated method stub
		super.dispatchMessage(msg);

		switch (msg.what) {
		case MSG_OBTAION_CONTENT: {
			Bundle bundle =msg.getData();
			String tagString=null;
			Page page =null;
			if (bundle!=null) {
				tagString = bundle.getString(TAG);
				 page = bundle.getParcelable("page");
			}
			activity.refreshUI(msg.obj,page,tagString,retryListener);
		}
			break;
		case MSG_OBTAION_CONTENT_FAILED: {
			Bundle bundle =msg.getData();
			String tagString=null;
			Page page =null;
			if (bundle!=null) {
				tagString = bundle.getString(TAG);
				 page = bundle.getParcelable("page");
			}
			activity.refreshUI(msg.obj,page,tagString,retryListener);
		
		}
			break;
		default:
			break;
		}
	}

	public DoRetryListener getRetryListener() {
		return retryListener;
	}

	public void setRetryListener(DoRetryListener retryListener) {
		this.retryListener = retryListener;
	}

}
