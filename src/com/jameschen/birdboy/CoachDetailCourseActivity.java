package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.internal.Utils;
import com.jameschen.birdboy.adapter.CoachAdapter;
import com.jameschen.birdboy.adapter.CoachDetailAdapter;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebCoachDetail;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.CoachDetailInfo;
import com.jameschen.birdboy.model.entity.CoachPhoto;
import com.jameschen.birdboy.model.entity.Comment;
import com.jameschen.birdboy.model.entity.Notice;
import com.jameschen.birdboy.model.entity.Product;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.widget.MyListView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
public class CoachDetailCourseActivity extends BaseActivity {


	
	


	@Override
			protected void onCreate(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				super.onCreate(savedInstanceState);
				setContentView(R.layout.coach_detail_course_intro);
				 
				Intent intent =getIntent();
				Product product =(Product) intent.getParcelableExtra("product");				
				setTitle("培训项目");
				initViews();
				setCourseIntro(product);
	}
	
	public void setCourseIntro(Product courseIntro) {
		courseName.setText(Html.fromHtml(courseIntro.getName()));
		intro.setText(Html.fromHtml(courseIntro.getInfo()));
		price.setText("￥"+courseIntro.getRmb());
		member.setText(courseIntro.getNum()+"/"+courseIntro.getTotal()+"人报名");
	}

	private TextView courseName,intro,price,member;
	private void initViews() {
		courseName = (TextView) findViewById(R.id.couse_name);
		intro = (TextView) findViewById(R.id.course_intro_content);
		price = (TextView) findViewById(R.id.price);
		member = (TextView) findViewById(R.id.course_member_num);

	

	
	}

	
	CoachDetailInfo mCoachDetailInfo;

}
