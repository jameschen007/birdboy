package com.jameschen.birdboy;

import java.util.Map;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.android.volley.toolbox.HttpClientStack;
import com.jameschen.birdboy.base.BaseEditAccountActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.utils.HttpUtil;

public class EditSignActivity extends BaseEditAccountActivity {
	EditText signEdit;
	String sign;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_sign);
		setTitle("个性签名");
		signEdit = (EditText) findViewById(R.id.signEdit);
		if (LogInController.currentAccount!=null) {
			signEdit.setText(LogInController.currentAccount.getSignstr());
		}
		
		setTopBarRightBtnListener("保存", new OnClickListener() {
			
			@Override
			public void onClick(View arg) {
				//no change. do nothing.
				if (TextUtils.isEmpty(signEdit.getText())) {
					finish();
					return;
				}
				if (LogInController.currentAccount!=null&&signEdit.getText().toString().equals(LogInController.currentAccount.getSignstr())) {
					finish();
					return;
				}

				sign =signEdit.getText().toString();
				commitDone();
			}
		});
	}

@Override
protected void commitDone() {
	showProgressDialog("修改资料", "正在提交，请稍候...", null);
	Map<String, String> map = HttpUtil.createEditAccountMap(EditSignActivity.this);
	map.put("sign", sign);			
	postDataLoader(map,"http://birdboy.cn/updateMeAPI", updateAccountController,new HttpClientStack(LogInController.getHttpClientInstance()));
	
}

@Override
protected void editOk() {
LogInController.currentAccount.setSignstr(sign);
}
}
