package com.jameschen.birdboy;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.base.BaseActivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebFeedBack;
import com.jameschen.birdboy.model.WebInbox;
import com.jameschen.birdboy.model.entity.Coach;
import com.jameschen.birdboy.model.entity.Message;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.HttpUtil;
//
//http://birdboy.cn/addChat  Post请求
//
//字段               类型                描述                           备注            值条件
//

//
// 3、返回字段
//
//{
//    "success": true, 
//    "result": {
//    },
//
//    "info":""
//
//} 



public class MessageReceiveListActivity extends BaseActivity{
	public static final int Read_Msg = 0x12;
	private ListView messageListView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_receive_list);
		setTitle("收信箱");
		initViews();
	}

	private void initViews() {
		 messageListView = (ListView) findViewById(R.id.message_list_view);
		 getDataLoader(HttpConfig.REQUST_INBOX_MESSAGE_URL,false,null, checkMessageController,
					new HttpClientStack(LogInController.getHttpClientInstance()));
				messageListView.setAdapter(messageAdapter);	
		empty =(TextView) findViewById(R.id.empty_info);
		messageListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent messageIntent = new Intent(MessageReceiveListActivity.this,MessageReceiveActivity.class);
				messageIntent.putExtra("message", messageAdapter.getItem(arg2));
				startActivityForResult(messageIntent,Read_Msg);
			}
		});
	}
TextView empty;
	MessageAdapter messageAdapter=new MessageAdapter();
	public void setMessageList(List<Message> messageList) {
		if (messageList.size()==0) {
			showToast("没有邮件");
			empty.setText("没有邮件");
			empty.setVisibility(View.VISIBLE);
			messageListView.setVisibility(View.GONE);
			return;
		}
		messageListView.setVisibility(View.VISIBLE);
		empty.setVisibility(View.GONE);
		messageAdapter.setMessages(messageList);
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
	
		switch (arg0) {
		case Read_Msg:
		{
			 getDataLoader(HttpConfig.REQUST_INBOX_MESSAGE_URL,false,null, checkMessageController,
						new HttpClientStack(LogInController.getHttpClientInstance()));
			 setResult(Read_Msg);
			
		}
			break;

		default:
			break;
		}
	}
	
	
	private UIController checkMessageController = new UIController() {

		@Override
		public <E> void refreshUI(E content, Page page) {
			setMessageList(((WebInbox)content).getList());
//
//			List<Message> messageList=new ArrayList<Message>();
//			for (int i = 0; i < 10; i++) {
//				messageList.add(new Message());
//			}
//			//test
//			setMessageList(messageList);
		}

		@Override
		public Type getEntityType() {
			return new TypeToken<WebBirdboyJsonResponseContent<WebInbox>>() {
			}.getType();
		}

		@Override
		public void responseNetWorkError(int errorCode, Page page) {
	if (errorCode>-1) {
				showToast(getString(R.string.network_error_time_out));
			}
			
		}

		@Override
		public String getTag() {
			return TAG+"inbox";
		}
	};
	
	class MessageAdapter extends BaseAdapter{

		List<Message> messages = new ArrayList<Message>();
		
		public void setMessages(List<Message> messages) {
			notifyDataSetInvalidated();
			this.messages = messages;
			notifyDataSetChanged();
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return messages.size();
		}

		@Override
		public Message getItem(int arg0) {
			// TODO Auto-generated method stub
			return messages.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			ViewHolder holder;
			if (arg1==null) {
				holder = new ViewHolder();
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				arg1 = (ViewGroup) inflater.inflate(R.layout.message_item,
						arg2, false);
				
				
				holder.mTitle =(TextView) arg1.findViewById(R.id.message_item_title_tv);
				holder.messageReadFlag =(ImageView) arg1.findViewById(R.id.message_item_mail_img);
				arg1.setTag(holder);
			}else {
				holder=(ViewHolder) arg1.getTag();
			}
			
			bindMessageInfo(holder,getItem(arg0));
			
			return arg1;
		}
		
		private void bindMessageInfo(ViewHolder holder, Message item) {
			if (item.getStatus()==0) {
				holder.messageReadFlag.setImageResource(R.drawable.sixin_unread);
			}else {
				holder.messageReadFlag.setImageResource(R.drawable.sixin_read);
			}
			holder.mTitle.setText("来自"+item.getSname()+"发给你的消息");
			
		}
		
		
		
		
		final class ViewHolder {
			TextView  mTitle;
			ImageView messageReadFlag;
		}
	}
	
}
