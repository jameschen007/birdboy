package com.jameschen.birdboy.base;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.jameschen.birdboy.utils.Log;

import android.view.MotionEvent;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.SeeUserPlugInPageActivity;
import com.jameschen.birdboy.adapter.BindAdapter;
import com.jameschen.birdboy.adapter.BindAdapter.BindItemClickListener;
import com.jameschen.birdboy.base.BaseShareAcitivity;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.entity.SNS;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.plugin.AuthBindController;
import com.jameschen.plugin.AuthBindController.OnbindResultListener;
import com.jameschen.plugin.AuthUtil;
import com.jameschen.plugin.BindModel;
import com.jameschen.plugin.ShareModel;
import com.jameschen.plugin.ShareUtil;
import com.jameschen.plugin.qq.TQq;
import com.jameschen.plugin.qq.TencentModel;
import com.jameschen.plugin.weibo.Constants;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuth.AuthInfo;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.openapi.legacy.StatusesAPI;
import com.sina.weibo.sdk.openapi.models.ErrorInfo;
import com.sina.weibo.sdk.openapi.models.Status;
import com.sina.weibo.sdk.openapi.models.StatusList;
import com.sina.weibo.sdk.utils.LogUtil;
import com.sina.weibo.sdk.widget.LoginButton;
import com.tencent.mm.sdk.ConstantsUI.Contact;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import com.umeng.analytics.MobclickAgent;

public class BaseBindActivity extends BaseShareAcitivity {
	public static final int REQ_BIND = 0x13;
	private LoginButton mWeiBoLoginBtn;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initbind();

	}

	private void initbind() {
		//create bind 
		  mWeiBoLoginBtn = new LoginButton(BaseBindActivity.this);
		   AuthInfo authInfo = new AuthInfo(BaseBindActivity.this, Constants.APP_KEY, Constants.REDIRECT_URL, Constants.SCOPE);
	        mWeiBoLoginBtn.setWeiboAuthInfo(authInfo, mLoginListener);
		
	}
	
	private OnbindResultListener biListener;
	
	protected void doWeiboAuth(OnbindResultListener bindResultListener) {
		MobclickAgent.onEvent(BaseBindActivity.this, "bind_weibo");
		mWeiBoLoginBtn.performClick();
		biListener = bindResultListener;
	}
	
	protected void doQQAuth(OnbindResultListener bindResultListener) {
		MobclickAgent.onEvent(BaseBindActivity.this, "bind_qq");
			
		biListener = bindResultListener;

		if (tencentModel==null) {
			tencentModel =TencentModel.getTencentInstance(getApplicationContext());
		}
		//AuthUtil.showProgressDialog(getBaseActivity(), "正在登录QQ", "请稍候...");
		if (tencentModel.isSessonValid()) {
			getBindQQInfo();
			return;
		}
		
		tencentModel.Login(BaseBindActivity.this, new IUiListener() {
			
			@Override
			public void onError(UiError arg0) {
				// TODO Auto-generated method stub
				AuthUtil.dismissDialog();
			}
			
			@Override
			public void onComplete(Object arg0) {
				if (isFinishing()) {
					return;
				}
				Log.i(TAG, "qqlogon=="+arg0.toString());

					getBindQQInfo();
			}
			
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				AuthUtil.dismissDialog();
			}
		});
			
	}
	
	protected void getBindQQInfo() {
		tencentModel.getTQQInfo(BaseBindActivity.this, new IUiListener() {
			
			@Override
			public void onError(UiError arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onComplete(Object arg0) {
				// TODO Auto-generated method stub
				 BindModel bindModel=new BindModel();
	              bindModel.fromtype=BindModel.QQ;
	              bindModel.token=tencentModel.getToken();
	              bindModel.openid = tencentModel.getOpenId();
	             try {
					TQq tQq= ParseJsonData.getQQData(arg0.toString(), new TypeToken<TQq>(){}.getType());
					 bindModel.nick = tQq.getData().getName();
		              checkUserBindReady(bindModel);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					showToast("绑定失败!");
				}
	             
				
			}
			
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	private void checkUserBindReady(final BindModel bindModel) {
		//check is logon
				if (LogInController.IsLogOn()) {
					bindAuth(bindModel);
				}else {
					Util.go2LogInDialog(BaseBindActivity.this, new OnLogOnListener() {
						
						@Override
						public void logon() {
							bindAuth(bindModel);
						}
					},true);
				}
	}
	
	private void bindAuth(final BindModel bindModel) {
		MobclickAgent.onEvent(BaseBindActivity.this, "share_sns");
		//do bind things
		AuthBindController.bindAuth(BaseBindActivity.this,bindModel,new OnbindResultListener() {
			
			@Override
			public void bindSucc(BindModel bindModel) {
				
				LogInController.setBindModel(bindModel);
				if (biListener!=null) {
					biListener.bindSucc(bindModel);
				}
				setResult(Activity.RESULT_OK);
				
				
				ShareModel shareModel = new ShareModel();
				shareModel.title =ShareUtil.shareTitle;
				shareModel.summary=ShareUtil.shareBindContent;
				shareModel.url=ShareUtil.ShareLink;
				shareModel.imgUrl =ShareUtil.imgShareLink;
				
				//...
				if (bindModel.fromtype==BindModel.QQ) {
					AuthUtil.dismissDialog();
					sendbindQQSuccReq(shareModel);//share...
					
				}else {
					//share sina. by open API...
					//sendMessage(ShareUtil.getTextObj("约运动测试分享到新浪微博"), null, null);
				sendWeiboShareMsg(BaseBindActivity.this, shareModel, accessToken, accessToken.getToken());
			
				}
				
			}
			
			@Override
			public void bindFailed(String reason) {
				// TODO Auto-generated method stub
				 //try again.
				if (biListener!=null) {
					biListener.bindFailed(reason);
				}
			}
		});
	
	}
	protected void sendbindQQSuccReq(ShareModel shareModel) {
		
		tencentModel.commitQZoneShare(null,BaseBindActivity.this, new IUiListener() {
			
			@Override
			public void onError(UiError arg0) {
				// TODO Auto-generated method stub
				AuthUtil.showResultDialog(BaseBindActivity.this, arg0.errorMessage+"", "");
			}
			
			@Override
			public void onComplete(Object arg0) {
				Log.i(TAG, "qzone=="+arg0.toString());
				
			}
			
			@Override
			public void onCancel() {
				
			}
		}, shareModel);
		tencentModel.commitTQQShare(BaseBindActivity.this, shareModel, new IUiListener() {
			
			@Override
			public void onError(UiError arg0) {
				AuthUtil.showResultDialog(BaseBindActivity.this, arg0.errorMessage+"", "");
				
			}
			
			@Override
			public void onComplete(Object arg0) {
				Log.i(TAG, "tqq=="+arg0.toString());
			}
			
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}
		});
	}

	

	  /** 登陆认证对应的listener */
  private AuthListener mLoginListener = new AuthListener();
  /**
   * 登入按钮的监听器，接收授权结果。
   */
  Oauth2AccessToken accessToken;
  
  private class AuthListener implements WeiboAuthListener {
      @Override
      public void onComplete(Bundle values) {
           accessToken = Oauth2AccessToken.parseAccessToken(values);
          if (accessToken != null && accessToken.isSessionValid()) {
          	
              String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(
                      new java.util.Date(accessToken.getExpiresTime()));
              
                Log.i(TAG, "sina token date==="+date);
                Log.i(TAG, "sina token ==="+accessToken.getToken()+"  uid=="+accessToken.getUid());
                Log.i(TAG, "sina token ==="+accessToken.getRefreshToken());
                
	              BindModel bindModel=new BindModel();
	              bindModel.fromtype=BindModel.SINA;
	              bindModel.token=accessToken.getToken();
	              bindModel.nick = accessToken.getUid();
	              checkUserBindReady(bindModel);
	      	//	((MainActivity)getBaseActivity()).sendMessage(ShareUtil.getTextObj("约运动测试分享到新浪微博"), null, null);
				
//              String format = getString(R.string.weibosdk_demo_token_to_string_format_1);
//              AccessTokenKeeper.writeAccessToken(getApplicationContext(), accessToken);
          }
      }

      @Override
      public void onWeiboException(WeiboException e) {
        //  Toast.makeText(WBLoginLogoutActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
      }

      @Override
      public void onCancel() {
//          Toast.makeText(WBLoginLogoutActivity.this, 
//                  R.string.weibosdk_demo_toast_auth_canceled, Toast.LENGTH_SHORT).show();
      }
  }
  
  private TencentModel tencentModel;
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		if (mWeiBoLoginBtn!=null) {
			mWeiBoLoginBtn.onActivityResult(arg0, arg1, arg2);
		}
	}
}
