package com.jameschen.birdboy.base;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.BackgroundColorSpan;
import com.jameschen.birdboy.utils.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.XListView;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.ObjectAdapter;
import com.jameschen.birdboy.base.BaseFragment;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.ConstValues.Setting;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.location.LocationUtil;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.entity.City;
import com.jameschen.birdboy.ui.LoginFragment;
import com.jameschen.birdboy.utils.CustomSelectPopupWindow;
import com.jameschen.birdboy.utils.PopupWindowUtil;
import com.jameschen.birdboy.utils.Util;
import com.jameschen.birdboy.weather.BDParseWeather;
import com.jameschen.birdboy.weather.BDParseWeather.OnWeatherResultCallbackListener;
import com.jameschen.birdboy.weather.BDWeather;
import com.jameschen.birdboy.weather.ParseWeather;
import com.jameschen.birdboy.weather.Weather;
import com.jameschen.birdboy.widget.MListView;
import com.jameschen.birdboy.widget.MyListView;
import com.jameschen.birdboy.widget.MyListView.OnLoadTaskListener;
import com.jameschen.birdboy.widget.MListView.MyRefreshListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public abstract class BaseListFragment<T> extends BaseFragment {

	public static final String LIST = "list";
	public List<T> mInfos;
	public final int numPerPage = 10;
	public String sort;
	private int currentPositon = 0;
	private int totalNum = 0;
	protected MyListView mListView;
	private ObjectAdapter<T> mAdapter;
	private View emptyView;
	protected TextView locationView;
	
	
	
	protected TextView city,weatherTv;
	
	public void refreshAddress(String address,LocationSupport support) {
		refreshAddress(address, support, false);
	}
	protected abstract void refreshArea();
	public void refreshAddress(String address,LocationSupport support,boolean firstLoc) {
		if (locationView==null) {
			return;
		}
		if (TextUtils.isEmpty(address)) {
			locationView.setText(getString(R.string.distance_unkonw));
		}else {
			
			SimpleDateFormat  formatter  =   new  SimpleDateFormat ("HH:mm");       
			Date    curDate   =   new    Date(System.currentTimeMillis());//获取当前时间       
			String    str     =" "+formatter.format(curDate);     
			//new BackgroundColorSpan(Color.WHITE)
			locationView.setText("位置:"+address+str);
		}
		if (support==null) {
			return;
		}
//		City cityObj = LocationUtil.getCurrentCityId(support.city);
//		if (city!=null&&firstLoc) {
//			//do noting.
//		}
		
		//setCityId(cityObj.getId());
		//reselect the city item...
		//fresh weather
	//	refreshWeather(cityObj,false,weatherTv);
		
	}
	long lastFreshTime =0;
	
	
	private void ansaysisWeather(BDWeather weather,String cityName,String content,boolean saveCache) {
		if (weather==null) {
			return;
		}
		if (getActivity()==null||getActivity().isFinishing()) {
			return;
		}
		if ("success".equals(weather.getStatus())) {
			//save wheather,
			if (weather.getResults().size()>0) {
				if (saveCache) {
					Util.saveJsonCache(getActivity(),Setting.SharedName,"weather"+cityName, content);
					Util.saveJsonCache(getActivity(),Setting.SharedName,"weatherTime", System.currentTimeMillis()+"");
				}
				Log.i(TAG, "weather....="+weather.getResults().get(0).getWeather_data().get(0).getWeather());
			}
		
		}
	
	
	}
	
	protected  void refreshWeather(City city,boolean force,TextView weatherInfo) {
		if (true) {
			return;
		}
		if (true) {
			LocationSupport locationSupport = LocationSupport.getLocationSupportInstance(getActivity());
			if (locationSupport.city!=null) {
				Log.i(TAG, "locationSupport.city=="+locationSupport.city);
				BDParseWeather parseWeather = new BDParseWeather();
				//check if has weather today.
				String content	=Util.loadJsonCacheData(getActivity(),Setting.SharedName,"weather"+locationSupport.city);
				long when=0;
				try {
					when = Long.parseLong(Util.loadJsonCacheData(getActivity(),Setting.SharedName,"weatherTime"));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
	
				if (content!=null&&DateUtils.isToday(when)) {
					BDWeather weather = parseWeather.getBdWeather(content);
					ansaysisWeather(weather, locationSupport.city, content, false);
				}else {
					final String cityName =locationSupport.city;
					parseWeather.getCityWeater(locationSupport.city, new OnWeatherResultCallbackListener() {
						
						@Override
						public void weatherResult(BDWeather weather,String content) {
							ansaysisWeather(weather, cityName, content, true);
						}
					});
				}
			
			}
			return;
		}
		if (force||System.currentTimeMillis()-lastFreshTime>7200*1000) {//2 h.
				readWeatherByCity(city,weatherInfo);
		}else {
			
			if (weather==null) {
				readWeatherByCity(city,weatherInfo);
			}
		}
		lastFreshTime = System.currentTimeMillis();
	}
	Weather weather;
	private void readWeatherByCity(final City city, final TextView weatherInfo) {
		if (weatherInfo!=null) {
			weatherInfo.setCompoundDrawables(null, null, null, null);
			weatherInfo.setText("");
		}
			AsyncHttpClient asyncHttpClient= new AsyncHttpClient();
			
			asyncHttpClient.get("http://flash.weather.com.cn/wmaps/xml/"+city.getPinyinName()+".xml", 
					
					new AsyncHttpResponseHandler(){
				@Override
				@Deprecated
				public void onSuccess(String content) {
					// TODO Auto-generated method stub
					super.onSuccess(content);
					if (getActivity()==null||getActivity().isFinishing()) {
						return;
					}
					final String weatherString =content;
					new Thread(new Runnable() {
						
						@Override
						public void run() {
//							//save image...
//							for (int i = 0; i < 32; i++) {
//								String path ="http://m.weather.com.cn/img/c"+i+".gif";
//								
//								try {
//									try {
//										URL url;
//										url = new URL(path);
//										 HttpURLConnection conn = (HttpURLConnection) url.openConnection();      
//										 Bitmap bitmap = BitmapFactory.decodeStream(conn.getInputStream() );
//										 File dirFile = new File("sdcard/james/");   
//									        if(!dirFile.exists()){   
//									            dirFile.mkdirs();   
//									        }   
//									        File myCaptureFile = new File(dirFile.getAbsolutePath()+"/" +"weather"+ i+".png");   
//									        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));   
//									        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);   
//									        bos.flush();   
//									        bos.close();   
//									} catch (MalformedURLException e) {
//										// TODO Auto-generated catch block
//										e.printStackTrace();
//									} catch (IOException e) {
//										// TODO Auto-generated catch block
//										e.printStackTrace();
//									}
//								} catch (Exception e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}      
//						       
//							}
							
							weather=ParseWeather.parse(weatherString, city);
							if (getActivity()==null||getActivity().isFinishing()) {
								return;
							}
							if (weatherInfo!=null&&weather!=null) {
								 boolean isNum =false;
								if (weather.getTemNow()!=null) {
									 isNum = TextUtils.isDigitsOnly(weather.getTemNow());
								}
								final boolean isDigitsOnly=isNum;
								weatherInfo.post(new Runnable() {
									
									@Override
									public void run() {
										if (isDigitsOnly) {
											//0.晴 1.多云 2.阴 6.雨夹雪 7.小雨 8.中雨 13.阵雪 14.小雪
											// http://m.weather.com.cn/img/c0.gif  tupian baocundaobendi,..
											weatherInfo.setText(""+weather.getTemNow()+"℃");
											Context context =weatherInfo.getContext();
											Log.i("weacher..", "state==="+weather.getState1());
											Calendar c = Calendar.getInstance();
											int curHours = c.get(Calendar.HOUR_OF_DAY);
											String state =weather.getState1();
											if (curHours>12) {//time >12 use log2  <12 use log 1
												 state =weather.getState2();
											}
											int resId = context.getResources().getIdentifier("weather"+state, "drawable", context.getPackageName());
											Log.i("weacher..", "resId==="+resId);
											if (resId>0) {
												weatherInfo.setCompoundDrawablePadding(Util.getPixByDPI(context, 1));
												Drawable drawable = context.getResources().getDrawable(resId);
												drawable.setBounds(0, 0, drawable.getMinimumWidth(),
														drawable.getMinimumHeight());// 必须设置图片大小，否则不显示
													weatherInfo.setCompoundDrawables(null, null, drawable, null);
											}
										
										}else {
											weatherInfo.setText("");
										}
										
									}
								});
							}
						}
					}).start();
					
				}
			});
	}

	public void initListAdapter(final ObjectAdapter<T> mAdapter,
			final UIController uiFreshController) {
		this.mAdapter = mAdapter;
		this.mListView.setAdapter(mAdapter);
		//this.mListView.getLoadingLayoutProxy().setLastUpdatedLabel(label)
		this.mListView.setLoadTaskListener(new OnLoadTaskListener() {

			@Override
			public void reloadTask() {// if empty view occur and click fresh

				if (emptyView != null) {
					emptyView.performClick();
				}
			}
		});
		this.mListView.setRefreshListener(new MyRefreshListener() {

			@Override
			public void onRefresh(PullToRefreshBase<XListView> refreshView) {// fresh
																				// top
				// add delay 100 ms later.
				// if (running) {
				// return;
				// }
				// running =true;
				if (emptyView != null) {
					emptyView.setVisibility(View.GONE);
				}
				// if distance url.... relocatin first...

				Page page = createInitPage();

				String url = setRequestLoadMoreUrl(page);
				if (url == null) {
					cancelLoadingProgress(false);
					return;
				}
				if (url.equals(ConstValues.DISTANCE)) {
					return;
				}
				//save cache  by flag..!!!!!!!!!!!!!!!!
				loadData(url, true, page, uiFreshController,needHttpStack?new HttpClientStack(LogInController.getHttpClientInstance()):null );

			}

			@Override
			public void OnRefreshFooterListener() {// load more
				boolean nomore = mListView.getRefreshableView().getChildCount() == mListView
						.getRefreshableView().getCount() ? true : false;
				// TODO Auto-generated method stub
				if (nomore) {
					cancelLoadingProgress(false);
					return;
				}
				// running =true;
				Page page = new Page();
				Log.i(TAG, "totalNum=" + totalNum);
				currentPositon = totalNum <= 0 ? numPerPage : totalNum;
				page.setCurrentPositon(currentPositon);
				totalNum = currentPositon + numPerPage;
				page.setTotalNum(totalNum);
				Log.i(TAG, "currentPositon=" + currentPositon + ";totalNum="
						+ totalNum);

				page.setLoadMore(Page.LOAD_MORE);
				String url = setRequestLoadMoreUrl(page);
				if (url == null) {
					cancelLoadingProgress(false);
					return;
				}
				if (url.equals(ConstValues.DISTANCE)) {
					return;
				}

				getDataLoader(url, false, page, uiFreshController,needHttpStack?new HttpClientStack(LogInController.getHttpClientInstance()):null);
				// (dataTask = new GetDataTask(false)).execute(++currentPage);
			}

			@Override
			public void OnPauseImageLoadWork(boolean pauseWork) {
				// TODO Auto-generated method stub
				mAdapter.setImageFetcherPauseWork(pauseWork);
			}

		});

		mListView.setRefreshing(true);
	}

	protected boolean needHttpStack = false;

	protected abstract void  initViews(View view);
	
	protected Page createInitPage() {
		Page page = new Page();
		page.setLoadMore(Page.CLEAR);
		return page;
	}

	@Override
	public void showToast(String string) {
		cancelLoadingProgress(false);
		super.showToast(string);

	}

	protected void setEmptyView(View emptyView) {
		this.emptyView = emptyView;
	}

	public View getEmptyView() {
		return emptyView;
	}

	protected abstract String setRequestLoadMoreUrl(Page page);

	public void loadData(String requstUrl, boolean cacheSave, Page page,
			UIController uiFreshController, HttpStack httpStack) {
			cancelRequstByTag(TAG);
			cancelRequstByTag(uiFreshController.getTag());
		initData();
		getDataLoader(requstUrl, cacheSave, page, uiFreshController, httpStack);
	}

	public void loadData(String requstUrl, boolean cacheSave, Page page,
			UIController uiFreshController) {
		loadData(requstUrl, cacheSave, page, uiFreshController, null);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	public void clearResource() {
		if (mAdapter != null) {
			mAdapter.recycle();
			mAdapter = null;
		}
	}

	public void releaseResource() {
		// TODO Auto-generated method stub
		clearResource();

	}

	public static String parseParam(String url, String key, String value,
			boolean firstParam) {
		if (TextUtils.isEmpty(url))
			return key + ":" + value;
		if (firstParam) {
			return url = url + key + ":" + value;
		} else {
			return url = url + " AND " + key + ":" + value;
		}

	}

	public static String parseParam(String url, String key, String value) {
		return parseParam(url, key, value, false);
	}

	public void initData() {
		currentPositon = 0;
		totalNum = 0;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mAdapter != null && isVisible()) {
			mAdapter.onResume();
		}

	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		if (!hidden) {
			if (mAdapter != null) {
				mAdapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		if (mAdapter != null) {
			mAdapter.onPause();
		}
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		if (clear) {
			mListView = null;
			
		}

		// clearResource();
	}

	@Override
	public void onDestroy() {

		// if (banner != null) {
		// banner.onDestroy();
		// }
		// banner = null;
		if (clear) {
			releaseResource();
		}

		super.onDestroy();
	}

	protected boolean clear = true;

	public void setErrorPage(int errorCode,Page page) {

		setPrevPage(page);
	}
	
	public void setPrevPage(Page page) {
		synchronized (this) {
			if (page.getTag()!=null&&page.getTag().equals("login")) {
				cancelLoadingProgress(true);
				return;
			}
			currentPositon = page.getCurrentPositon();
			if (currentPositon > 0 && currentPositon < numPerPage) {
				initData();
				cancelLoadingProgress(true);
			} else if (currentPositon > numPerPage) {// peer back last page
				currentPositon = currentPositon - numPerPage;
				totalNum = totalNum - numPerPage;
			}

		}
	}

	protected void clearAdapter() {
		// TODO Auto-generated method stub
		if (mAdapter != null) {
			mAdapter.clear(true);
		}
	}

	public <E> void refreshList(E content, Page page) {

		// page ,
		List<T> mList = (List<T>) content;
		if (mListView==null) {
//			//some error...
			getActivity().finish();
//			FragmentTransaction ft = getFragmentManager()
//					.beginTransaction();
//			ft.detach(BaseListFragment.this);
//			Fragment.instantiate(getBaseActivity(), BaseListFragment.class.getName(),
//					null);
//			ft.replace(R.id.fragment_tab_content, this, TAG);
//			//ft.attach(BaseListFragment.this);
//			ft.commit();
			return;
		}else {
		}
		if (mListView==null) {
			Log.e(TAG, "listSize =="+(mList!=null?mList.size():0)+"mAdapter=="+(mAdapter!=null)+"mListView=="+(mListView!=null));
		}
	
		if (mListView != null) {

			if (page != null) {
				if (page.isLoadMore() != Page.LOAD_MORE
						&& page.isLoadMore() != Page.NO_MORE) {
					mAdapter.clear();
				}
			} else {
				mAdapter.clear();
			}

			mAdapter.addObjectList(mList);
			if (isVisible()) {
				mAdapter.notifydataChanged(page);
			} else {
				mAdapter.setCurrentPage(page);
			}
			if (mAdapter.getCount() == 0) {
				if (emptyView != null) {
					emptyView.setVisibility(View.VISIBLE);
				}
			} else {
				if (emptyView != null) {
					emptyView.setVisibility(View.GONE);
				}
			}

			if ((mList.size()<numPerPage)
					|| (page != null && page.isLoadMore() == Page.NO_MORE)) {
				mListView.onRefreshComplete(false);
			} else {
				mListView.onRefreshComplete(true);
			}
		}
		
		if (mList.size() == 0) {
			cancelLoadingProgress(false);

			if (TAG != null && TAG.equals(ConstValues.SEARCH)) {
				if (mAdapter.getCount() == 0) {
					showToast("未搜索到信息！");
				} else {
					showToast("没有更多！");
				}

			} else {
				if (mAdapter.getCount() == 0) {
					showToast("没有数据！");
				} else {
					showToast("没有更多！");
				}
			}
		}
	}

	protected void cancelLoadingProgress(boolean loadMore) {
		// TODO Auto-generated method stub
		if (mListView != null) {
			mListView.onRefreshComplete(loadMore);
		}
	}

}
