package com.jameschen.birdboy.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;

import org.apache.http.Header;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.entity.QQ;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.plugin.AuthUtil;
import com.jameschen.plugin.ShareModel;
import com.jameschen.plugin.ShareUtil;
import com.jameschen.plugin.qq.AppConstants;
import com.jameschen.plugin.qq.TencentModel;
import com.jameschen.plugin.weibo.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;
import com.sina.weibo.sdk.api.BaseMediaObject;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboDownloadListener;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.exception.WeiboShareException;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.openapi.legacy.StatusesAPI;
import com.sina.weibo.sdk.openapi.models.ErrorInfo;
import com.sina.weibo.sdk.openapi.models.Status;
import com.sina.weibo.sdk.openapi.models.StatusList;
import com.sina.weibo.sdk.utils.LogUtil;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;

public class BaseShareAcitivity  extends BaseActivity implements IWeiboHandler.Response{


    
    /** 微博微博分享接口实例 */
    private IWeiboShareAPI  mWeiboShareAPI = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onCreate(savedInstanceState);


        // 创建微博分享接口实例
        mWeiboShareAPI = WeiboShareSDK.createWeiboAPI(this, Constants.APP_KEY);
        
        // 注册第三方应用到微博客户端中，注册成功后该应用将显示在微博的应用列表中。
        // 但该附件栏集成分享权限需要合作申请，详情请查看 Demo 提示
        // NOTE：请务必提前注册，即界面初始化的时候或是应用程序初始化时，进行注册
        mWeiboShareAPI.registerApp();
        
        // 如果未安装微博客户端，设置下载微博对应的回调
        if (!mWeiboShareAPI.isWeiboAppInstalled()) {
            mWeiboShareAPI.registerWeiboDownloadListener(new IWeiboDownloadListener() {
                @Override
                public void onCancel() {
                    Toast.makeText(BaseShareAcitivity.this, "取消下载", 
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
        
		// 当 Activity 被重新初始化时（该 Activity 处于后台时，可能会由于内存不足被杀掉了），
        // 需要调用 {@link IWeiboShareAPI#handleWeiboResponse} 来接收微博客户端返回的数据。
        // 执行成功，返回 true，并调用 {@link IWeiboHandler.Response#onResponse}；
        // 失败返回 false，不调用上述回调
        if (savedInstanceState != null) {
            mWeiboShareAPI.handleWeiboResponse(getIntent(), this);
        }
    
    }
    
    /**
     * @see {@link Activity#onNewIntent}
     */	
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        
        // 从当前应用唤起微博并进行分享后，返回到当前应用时，需要在此处调用该函数
        // 来接收微博客户端返回的数据；执行成功，返回 true，并调用
        // {@link IWeiboHandler.Response#onResponse}；失败返回 false，不调用上述回调
        mWeiboShareAPI.handleWeiboResponse(intent, this);
    }

    /**
     * 接收微客户端博请求的数据。
     * 当微博客户端唤起当前应用并进行分享时，该方法被调用。
     * 
     * @param baseRequest 微博请求数据对象
     * @see {@link IWeiboShareAPI#handleWeiboRequest}
     */
    @Override
    public void onResponse(BaseResponse baseResp) {
        switch (baseResp.errCode) {
        case WBConstants.ErrorCode.ERR_OK:
            Toast.makeText(this, "分享成功", Toast.LENGTH_LONG).show();
            break;
        case WBConstants.ErrorCode.ERR_CANCEL:
            Toast.makeText(this, "取消分享", Toast.LENGTH_LONG).show();
            break;
        case WBConstants.ErrorCode.ERR_FAIL:
            Toast.makeText(this, "分享失败" + "Error Message: " + baseResp.errMsg, 
                    Toast.LENGTH_LONG).show();
            break;
        }
    }

  

    /**
     * 第三方应用发送请求消息到微博，唤起微博分享界面。
     * @param textObject 
     * @param imageObject 
     * @param mediaObject 
     * @see {@link #sendMultiMessage} 或者 {@link #sendSingleMessage}
     */
    public void sendMessage(TextObject textObject, ImageObject imageObject, BaseMediaObject mediaObject) {
  
        try {
            // 检查微博客户端环境是否正常，如果未安装微博，弹出对话框询问用户下载微博客户端
            if (mWeiboShareAPI.checkEnvironment(true)) {                    
            	 if (mWeiboShareAPI.isWeiboAppSupportAPI()) {
                       sendMultiMessage(textObject, imageObject, mediaObject);
                 } else {

                     Toast.makeText(this,"不支持分享", Toast.LENGTH_SHORT).show(); 
            }

            }
        } catch (WeiboShareException e) {
            e.printStackTrace();
            Toast.makeText(BaseShareAcitivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    	
    }

    /**
     * 第三方应用发送请求消息到微博，唤起微博分享界面。
     * 注意：当 {@link IWeiboShareAPI#getWeiboAppSupportAPI()} >= 10351 时，支持同时分享多条消息，
     * 同时可以分享文本、图片以及其它媒体资源（网页、音乐、视频、声音中的一种）。
     * 
     * @param hasText    分享的内容是否有文本
     * @param hasImage   分享的内容是否有图片
     * @param hasWebpage 分享的内容是否有网页
     * @param hasMusic   分享的内容是否有音乐
     * @param hasVideo   分享的内容是否有视频
     * @param hasVoice   分享的内容是否有声音
     */
    private void sendMultiMessage(TextObject textObject,ImageObject imageObject,BaseMediaObject mediaObject) {
        
        // 1. 初始化微博的分享消息
        WeiboMultiMessage weiboMessage = new WeiboMultiMessage();
        if (textObject!=null) {
            weiboMessage.textObject = textObject;
        }
        
        if (imageObject!=null) {
            weiboMessage.imageObject = imageObject;
        }
        
        // 用户可以分享其它媒体资源（网页、音乐、视频、声音中的一种）
        if (mediaObject!=null) {
            weiboMessage.mediaObject =mediaObject;
        }
        
        // 2. 初始化从第三方到微博的消息请求
        SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
        // 用transaction唯一标识一个请求
        request.transaction = String.valueOf(System.currentTimeMillis());
        request.multiMessage = weiboMessage;
        
        // 3. 发送请求消息到微博，唤起微博分享界面
        mWeiboShareAPI.sendRequest(request);
    }

   
    
    public void sendQzoneShareReq(ShareModel shareModel, QQ qq, Context context) {
    	TencentModel tencentModel = TencentModel.getTencentInstance(getApplicationContext());
    	
    	tencentModel.commitQZoneShare(qq,BaseShareAcitivity.this, new IUiListener() {
			
			@Override
			public void onError(UiError arg0) {
				final UiError uiError =arg0;
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						AuthUtil.showResultDialog(BaseShareAcitivity.this, uiError.errorMessage+"", "");						
					}
				});
			}
			
			@Override
			public void onComplete(Object arg0) {
				Log.i(TAG, "qzone=="+arg0.toString());
				
			}
			
			@Override
			public void onCancel() {
				
			}
		}, shareModel);
    }
    
    
    public static AsyncHttpClient getNewHttpClient() {
    	   try {
    	       KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
    	       trustStore.load(null, null);
    	 
    	       MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
    	       sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    	 
    	       HttpParams params = new BasicHttpParams();
    	       HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
    	       HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
    	 
    	       SchemeRegistry registry = new SchemeRegistry();
    	       registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    	       registry.register(new Scheme("https", sf, 443));
    	 
    	       ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
    	       AsyncHttpClient asyncHttpClient = new AsyncHttpClient(registry);
    	       return asyncHttpClient;
    	   } catch (Exception e) {
    	       return new AsyncHttpClient();
    	   }
    }
    
    public void sendQQTShareReq(final ShareModel shareModel, final QQ qq, final Context context) {
    	TencentModel tencentModel = TencentModel.getTencentInstance(getApplicationContext());
		boolean isOk =false;
				//tencentModel.commitTQQShare(BaseShareAcitivity.this, shareModel, new IUiListener() {
//			
//			@Override
//			public void onError(UiError arg0) {
//				shareByAPI(shareModel,qq,context);
//			}
//			
//			@Override
//			public void onComplete(Object arg0) {
//				Log.i(TAG, "tqq=="+arg0.toString());
//			}
//			
//			@Override
//			public void onCancel() {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//    	
    	if (!isOk) {
			shareByAPI(shareModel,qq,context);
		}
	
	}
    	void shareByAPI(final ShareModel shareModel, final QQ qq, final Context context){

			//by other way....
	    	//oauth_consumer_key
	    	//access_token
	    	//openid
	    	//https://graph.qq.com/t/add_t 
	    	//https://graph.qq.com/t/add_pic_t content	必须 pic	必须

	    	//try other way...
			AsyncHttpClient asyncHttpClient = getNewHttpClient();
			
			RequestParams requestParams = new RequestParams();
			requestParams.put("oauth_consumer_key", AppConstants.APP_ID);
			requestParams.put("format", "json");
			requestParams.put("openid", qq.getOpenid());
			requestParams.put("access_token", qq.getToken());
			requestParams.put("content", shareModel.summary);
			String reqUrl="https://graph.qq.com/t/add_t";
			if (ShareUtil.getShareImgFilePath(context)!=null) {
				try {
					requestParams.put("pic", new FileInputStream(ShareUtil.getShareImgFilePath(context)),"multipart/form-data");
					reqUrl="https://graph.qq.com/t/add_pic_t";
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			Log.i(TAG, "qq reqUrl="+reqUrl);
			
			asyncHttpClient.post(reqUrl, requestParams, new AsyncHttpResponseHandler(){
				@Override
				@Deprecated
				public void onSuccess(int statusCode, String response) {
					// TODO Auto-generated method stub
					super.onSuccess(statusCode, response);
					Log.i(TAG, "qq succ=="+response);
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					Log.i(TAG, "share qq failed=="+arg0);
				}
			});
		
    	}

	public void sendWeiboShareMsg(Context context,ShareModel shareModel, Oauth2AccessToken accessToken
			,String accessTokenStr) {
		
        if (accessToken != null && accessToken.isSessionValid()) {
      	  StatusesAPI mStatusesAPI = new StatusesAPI(accessToken);
      	  LocationSupport locationSupport = LocationSupport.getLocationSupportInstance(context);
      	  String lat="0",lont="0";
      	  if (locationSupport.getLatitude()>0&&locationSupport.getLongtitude()>0) {
				lat=locationSupport.getLatitude()+"";
				lont=locationSupport.getLongtitude()+"";
			}
      	  Bitmap bitmap =ShareUtil.getShareImgFileBitmap(context);
      	  if (bitmap==null) {
      		mStatusesAPI.update(shareModel.summary,lat,lont,new RequestListener() {
		        @Override
		        public void onComplete(String response) {
		        	Log.i(TAG, "wei bo ---response=="+response);
		            if (!TextUtils.isEmpty(response)) {
		                LogUtil.i(TAG, response);
		                if (response.startsWith("{\"statuses\"")) {
		                    // 调用 StatusList#parse 解析字符串成微博列表对象
		                    StatusList statuses = StatusList.parse(response);
		                    if (statuses != null && statuses.total_number > 0) {
//		                        Toast.makeText(BaseBindActivity.this, 
//		                                "获取微博信息流成功, 条数: " + statuses.statusList.size(), 
//		                                Toast.LENGTH_LONG).show();
		                    }
		                } else if (response.startsWith("{\"created_at\"")) {
		                    // 调用 Status#parse 解析字符串成微博对象
		                    Status status = Status.parse(response);
//		                    Toast.makeText(WBStatusAPIActivity.this, 
//		                            "发送一送微博成功, id = " + status.id, 
//		                            Toast.LENGTH_LONG).show();
		                } else {
		                    
		                }
		            }
		        }

		        @Override
		        public void onWeiboException(WeiboException e) {
		            LogUtil.e(TAG, e.getMessage());
		            ErrorInfo info = ErrorInfo.parse(e.getMessage());
		          //  Toast.makeText(WBStatusAPIActivity.this, info.toString(), Toast.LENGTH_LONG).show();
		        }
		    });
		}else {
			mStatusesAPI.upload(shareModel.summary,bitmap,lat,lont,new RequestListener() {
		        @Override
		        public void onComplete(String response) {
		        	Log.i(TAG, "wei bo ---response=="+response);
		            if (!TextUtils.isEmpty(response)) {
		                LogUtil.i(TAG, response);
		                if (response.startsWith("{\"statuses\"")) {
		                    // 调用 StatusList#parse 解析字符串成微博列表对象
		                    StatusList statuses = StatusList.parse(response);
		                    if (statuses != null && statuses.total_number > 0) {
//		                        Toast.makeText(BaseBindActivity.this, 
//		                                "获取微博信息流成功, 条数: " + statuses.statusList.size(), 
//		                                Toast.LENGTH_LONG).show();
		                    }
		                } else if (response.startsWith("{\"created_at\"")) {
		                    // 调用 Status#parse 解析字符串成微博对象
		                    Status status = Status.parse(response);
//		                    Toast.makeText(WBStatusAPIActivity.this, 
//		                            "发送一送微博成功, id = " + status.id, 
//		                            Toast.LENGTH_LONG).show();
		                } else {
		                    
		                }
		            }
		        }

		        @Override
		        public void onWeiboException(WeiboException e) {
		            LogUtil.e(TAG, e.getMessage());
		            ErrorInfo info = ErrorInfo.parse(e.getMessage());
		          //  Toast.makeText(WBStatusAPIActivity.this, info.toString(), Toast.LENGTH_LONG).show();
		        }
		    });
		}
      	  
		
		
        }else {
        	//try other way...
			AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
			RequestParams requestParams = new RequestParams();
			requestParams.put("source", Constants.APP_KEY);
			requestParams.put("access_token", accessTokenStr);
			requestParams.put("status", shareModel.summary);
			String reqUrl="https://upload.api.weibo.com/2/statuses/upload.json";
			if (ShareUtil.getShareImgFilePath(context)!=null) {
				try {
					requestParams.put("pic", new File(ShareUtil.getShareImgFilePath(context)));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					reqUrl="https://api.weibo.com/2/statuses/update.json";
				}
			}else {
				reqUrl="https://api.weibo.com/2/statuses/update.json";
			}
			asyncHttpClient.post(reqUrl, requestParams, new AsyncHttpResponseHandler(){
				@Override
				@Deprecated
				public void onSuccess(int statusCode, String response) {
					// TODO Auto-generated method stub
					super.onSuccess(statusCode, response);
					Log.i(TAG, "succ==");
				    if (!TextUtils.isEmpty(response)) {
		                LogUtil.i(TAG, response);
		                if (response.startsWith("{\"statuses\"")) {
		                    // 调用 StatusList#parse 解析字符串成微博列表对象
		                    StatusList statuses = StatusList.parse(response);
		                    if (statuses != null && statuses.total_number > 0) {
//		                        Toast.makeText(BaseBindActivity.this, 
//		                                "获取微博信息流成功, 条数: " + statuses.statusList.size(), 
//		                                Toast.LENGTH_LONG).show();
		                    }
		                } else if (response.startsWith("{\"created_at\"")) {
		                    // 调用 Status#parse 解析字符串成微博对象
		                    Status status = Status.parse(response);
//		                    Toast.makeText(WBStatusAPIActivity.this, 
//		                            "发送一送微博成功, id = " + status.id, 
//		                            Toast.LENGTH_LONG).show();
		                } else {
		                    
		                }
		            }
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2,
						Throwable arg3) {
					// TODO Auto-generated method stub
					super.onFailure(arg0, arg1, arg2, arg3);
					Log.i(TAG, "share weibbo failed=="+arg0);
				}
			});
		}
        
	}

}
