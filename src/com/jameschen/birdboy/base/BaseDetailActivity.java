package com.jameschen.birdboy.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.jameschen.birdboy.environment.ConstValues;
import com.umeng.analytics.MobclickAgent;

public abstract class BaseDetailActivity extends BaseShareAcitivity {
	private String APP_ID = "100587829";

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

	}
	//save datacache to database later
	public void share(View view,int type) {
		if (view.getTag()==null) {
			return;
		}
		Intent intent = new Intent(Intent.ACTION_SEND); // 启动分享发送的属性
		intent.setType("text/plain"); // 分享发送的数据类型
		String msg = view.getTag() +
				"立刻体验:\nhttp://apps.wandoujia.com/apps/com.jameschen.birdboy/download";
		intent.putExtra(Intent.EXTRA_TEXT, msg); // 分享的内容
		startActivity(Intent.createChooser(intent, "选择分享"));// 目标应用选择对话框的标题 
		switch (type) {
		case ConstValues.TYPE_COACH_VAL:
			MobclickAgent.onEvent(getBaseContext(), "share_coach");
			break;
case ConstValues.TYPE_EVENT_VAL:
	MobclickAgent.onEvent(getBaseContext(), "share_activity");
		
			break;
case ConstValues.TYPE_STADIUM_VAL:
	MobclickAgent.onEvent(getBaseContext(), "share_stadium");
	
	break;
		default:
			break;
		}
//		final Bundle bundle = new Bundle();
//		// 这条分享消息被好友点击后的跳转URL。
//		bundle.putString(Constants.PARAM_TARGET_URL, "http://birdboy.cn/birdboy.apk");
//		// 分享的标题。注：PARAM_TITLE、PARAM_IMAGE_URL、PARAM_SUMMARY不能全为空，最少必须有一个是有值的。
//		bundle.putString(Constants.PARAM_TITLE, "鸟孩应用app下载");
//		// 分享的图片URL
////		bundle.putString(Constants.PARAM_IMAGE_URL,
////				"http://birdboy.cn/birdboy.apk");
//		// 分享的消息摘要，最长50个字
//		bundle.putString(Constants.PARAM_SUMMARY, "分享");
//		// 手Q客户端顶部，替换“返回”按钮文字，如果为空，用返回代替
//		bundle.putString(Constants.PARAM_APPNAME, null);
//		// 标识该消息的来源应用，值为应用名称+AppId。
//		bundle.putString(Constants.PARAM_APP_SOURCE, "鸟孩" + APP_ID);
//
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				mTencent.shareToQQ(BaseDetailActivity.this, bundle,
//						new IUiListener() {
//
//							@Override
//							public void onError(UiError arg0) {
//								// TODO Auto-generated method stub
//
//							}
//
//							@Override
//							public void onComplete(JSONObject arg0) {
//								// TODO Auto-generated method stub
//
//							}
//
//							@Override
//							public void onCancel() {
//								// TODO Auto-generated method stub
//
//							}
//						});
//			}
//		}).start();

	}
}
