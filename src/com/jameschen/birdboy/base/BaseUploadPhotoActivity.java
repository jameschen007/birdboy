package com.jameschen.birdboy.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.Header;
import org.json.JSONObject;

import android.R.integer;
import android.accounts.Account;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.jameschen.birdboy.utils.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.ScanImageActivity;
import com.jameschen.birdboy.utils.MediaFile;

import eu.janmuller.android.simplecropimage.CropImage;


public class BaseUploadPhotoActivity extends BaseShareAcitivity {

	private static final int REQ_CODE_TAKE_PICTURE = 0x11;
	protected static final int REQUEST_CODE_FOR_SELECT_IMAGE = 0x12;
	private static final int REQUEST_CODE_FOR_RESIZE_IMAGE = 0x13;
	private static final int FLAG_IMAGE_CAPTURE_CROP = 0x14;
	protected boolean crop=true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
	}
	
	
	boolean isStart=false;
	
	protected final void debugHeaders(String TAG, Header[] headers) {
        if (headers != null) {
            Log.d(TAG, "Return Headers:");
            StringBuilder builder = new StringBuilder();
            for (Header h : headers) {
                String _h = String.format(Locale.US, "%s : %s", h.getName(), h.getValue());
                Log.d(TAG, _h);
                builder.append(_h);
                builder.append("\n");
            }
           Log.i(TAG, "http=="+builder.toString());
           // addView(getColoredView(YELLOW, builder.toString()));
        }
    }

	


	public Dialog showAlert() {
		final Dialog dlg = new Dialog(this, R.style.MMTheme_DataSheet);
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout layout = (LinearLayout) inflater.inflate(
				R.layout.choose_image_dialog_layout, null);
		final int cFullFillWidth = 10000;
		layout.setMinimumWidth(cFullFillWidth);

		// only 3 button .
		TextView btn0 = (TextView) layout
				.findViewById(R.id.menu_dialog_take_by_camera);
		TextView btn1 = (TextView) layout
				.findViewById(R.id.menu_dialog_choose_from_exist);
		TextView btn2 = (TextView) layout
				.findViewById(R.id.menu_dialog_cancel);
		btn0.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Select content"),
						REQUEST_CODE_FOR_SELECT_IMAGE);
				dlg.dismiss();
			}
		});

		btn1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				takePhoto(imageCachePath);
				dlg.dismiss();
			}
		});
		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dlg.dismiss();
			}
		});
		// set a large value put it in bottom
		Window w = dlg.getWindow();
		WindowManager.LayoutParams lp = w.getAttributes();
		lp.x = 0;
		final int cMakeBottom = -1000;
		lp.y = cMakeBottom;
		lp.gravity = Gravity.BOTTOM;
		lp.height = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 160, this.getResources()
						.getDisplayMetrics());
		dlg.onWindowAttributesChanged(lp);
		dlg.setCanceledOnTouchOutside(true);

		dlg.setContentView(layout);
		dlg.show();

		return dlg;
	}
	
	
	
	String imageCachePath = ScanImageActivity.imageCachePath;

	public String takePhoto(String path) {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			String directoryPath = path;// Constants.FILE_SAVE_PATH娑撳搫鐖堕柌蹇撶摟缁楋缚瑕�

			File fileDirectory = new File(directoryPath);
			if (!fileDirectory.exists()) {
				fileDirectory.mkdirs();
			}
			File mPhotoFile = null;
			try {
				// define sending file name
				sendName = getPhotoFileName();
				Log.i(TAG, "sendName==="+sendName);
				String mPhotoPath = directoryPath + "/" + sendName;
				mPhotoFile = new File(mPhotoPath);
				if (!mPhotoFile.exists()) {
					mPhotoFile.createNewFile();
				}
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
			Log.i(TAG, "sendName=after=="+sendName);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mPhotoFile));
			startActivityForResult(intent, REQ_CODE_TAKE_PICTURE);
			return sendName;
		} else {

		}
		return null;
	}

	protected ImageView photo;
	protected String picPath;
	private void cropImageUri(String path) {
		if (!crop) {
			photo.setTag(path);
			photo.setImageBitmap(BitmapFactory.decodeFile(path));
			return;
		}
//       Uri uri = Uri.parse("file://"+path); 
//        Intent intent = new Intent("com.android.camera.action.CROP");
//        intent.setDataAndType(uri, "image/*");
//        intent.putExtra("crop", "true");
//        intent.putExtra("aspectX", 1); //设置按长和宽的比例裁剪
//        intent.putExtra("aspectY", 1);
//        int sizeImageHead = 500;
//        intent.putExtra("outputX", sizeImageHead); //设置输出的大小
//        intent.putExtra("outputY", sizeImageHead);
//        intent.putExtra("scale", true); //设置是否允许拉伸
//        // 如果要在给定的uri中获取图片，则必须设置为false，如果设置为true，那么便不会在给定的uri中获取到裁剪的图片
//        intent.putExtra("return-data", false);
//        intent.putExtra("output", Uri.fromFile(new File(picPathString)));
//        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());//设置输出格式
//        intent.putExtra("noFaceDetection", true); // 无需人脸识别  默认不需要设置
//        startActivityForResult(intent, FLAG_IMAGE_CAPTURE_CROP);
        
        Intent intent = new Intent(this, CropImage.class);
		intent.putExtra(CropImage.IMAGE_PATH,path);
		intent.putExtra(CropImage.SCALE, true);
		 int sizeImageHead = 500;
		intent.putExtra(CropImage.OUTPUT_X, sizeImageHead);
		intent.putExtra(CropImage.OUTPUT_Y, sizeImageHead);
		intent.putExtra(CropImage.ASPECT_X, 1);
		intent.putExtra(CropImage.ASPECT_Y, 1);
		startActivityForResult(intent, FLAG_IMAGE_CAPTURE_CROP);

      }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case FLAG_IMAGE_CAPTURE_CROP:
				String picPathString = intent.getStringExtra(CropImage.IMAGE_PATH);
				Bitmap bitmap =BitmapFactory.decodeFile(picPathString);
				if (bitmap==null) {
					return;
				}
				
				photo.setTag(picPathString);
				photo.setImageBitmap(bitmap);
				break;
			
			case REQUEST_CODE_FOR_RESIZE_IMAGE:
				picPathString = intent.getStringExtra("imagePath");
				cropImageUri(picPathString);
//				photo.setTag(pathString);
//				photo.setImageBitmap(BitmapFactory.decodeFile(pathString));
				break;

			case REQ_CODE_TAKE_PICTURE:
				  picPathString = imageCachePath + sendName;
				Log.i(TAG, "picPathString ==" + picPathString);
				ScanImageActivity.compressImage(this, picPathString);
				Log.i(TAG, "take photo ==" + picPathString);
				if (!new File(picPathString).exists()) {
					return;
				}
				cropImageUri(picPathString);
//				photo.setTag(picPathString);
//				photo.setImageBitmap(BitmapFactory.decodeFile(picPathString));
				break;

			case REQUEST_CODE_FOR_SELECT_IMAGE:
				Uri selectedImangeUri = intent.getData();
				if (selectedImangeUri == null) {
					Toast.makeText(BaseUploadPhotoActivity.this, "获取照片失败",
							Toast.LENGTH_SHORT).show();
					return;
				}

				String sendingFileName = null;
				sendingFileName = Uri2Filename(getApplication(), intent);

				if (sendingFileName == null) {
					Toast.makeText(BaseUploadPhotoActivity.this, "文件不存在!",
							Toast.LENGTH_SHORT).show();
					return;
				}
				// judge file type.
				File sendingFile = new File(sendingFileName);
				Log.i(TAG, "file path&&&&&&" + sendingFile);
				if (!sendingFile.exists()) {// maybe need to decode ..
					try {
						sendingFileName = URLDecoder.decode(sendingFileName,
								"utf-8");
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					sendingFile = new File(sendingFileName);
					// double check file is exist or not
					if (!sendingFile.exists()) {
						Toast.makeText(this, "文件不存在!", Toast.LENGTH_SHORT)
								.show();
						return;
					}
				}
				// is image file??
				if (!MediaFile.isImageFileType(sendingFile.getAbsolutePath())) {
					Toast.makeText(this, "请选择图片文件!", Toast.LENGTH_LONG).show();
					return;
				}
				intent = new Intent(this, ScanImageActivity.class);
				intent.setAction(ScanImageActivity.IMAGE_SCAN_RESIZE);
				intent.putExtra("filePath", sendingFile.getAbsolutePath());
				intent.putExtra("fileName", sendingFile.getName());
				startActivityForResult(intent, REQUEST_CODE_FOR_RESIZE_IMAGE);
				break;
			default:
				break;
			}

		}

	}

	String sendName = null;

	public static String getPhotoFileName() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"'IMG'_yyyy-MM-dd-HHmmss");
		return dateFormat.format(date) + ".jpg";
	}

	// get file absolute path.
	public static String Uri2Filename(Application application, Intent intent) {
		String file = null;

		Uri uriName = intent.getData();

		if (null != uriName) {
			if (0 == uriName.getScheme().toString().compareTo("content")) {
				Cursor c = application.getContentResolver().query(uriName,
						null, null, null, null);
				if (null != c && c.moveToFirst()) {
					try {
						int column_index = c
								.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
						file = c.getString(column_index);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
						try {
							int column_index = c
									.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA);
							file = c.getString(column_index);
						} catch (Exception e2) {
							e2.printStackTrace();
							Toast.makeText(application, "选择图片出错",
									Toast.LENGTH_LONG).show();
						}
					}

				}
			} else if (0 == uriName.getScheme().toString().compareTo("file")) {
				file = uriName.toString().replace("file://", "");
			}
		}
		return file;
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}

}
