package com.jameschen.birdboy.base;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.XListView;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.adapter.ObjectAdapter;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.location.LocationSupport;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.UtilsTime;
import com.jameschen.birdboy.widget.MList2View.MyRefresh2Listener;
import com.jameschen.birdboy.widget.MyList2View;
import com.jameschen.birdboy.widget.MyList2View.OnLoadTask2Listener;

public abstract class BaseList2Fragment<T> extends BaseFragment {

	public static final String LIST = "list";
	public List<T> mInfos;
	public final int numPerPage = 10;
	public String sort;
	private int currentPositon = 0;
	private int totalNum = 0;
	protected MyList2View mListView;
	private ObjectAdapter<T> mAdapter;
	private View emptyView;
	protected TextView locationView;
	
	
	
	protected TextView city,weatherTv;
	
	public void refreshAddress(String address,LocationSupport support) {
		refreshAddress(address, support, false);
	}
	protected abstract void refreshArea();
	public void refreshAddress(String address,LocationSupport support,boolean firstLoc) {
		if (locationView==null) {
			return;
		}
		if (TextUtils.isEmpty(address)) {
			locationView.setText(getString(R.string.distance_unkonw));
		}else {
			
			SimpleDateFormat  formatter  =   new  SimpleDateFormat ("HH:mm");       
			Date    curDate   =   new    Date(System.currentTimeMillis());//获取当前时间       
			String    str     =" "+formatter.format(curDate);     
			//new BackgroundColorSpan(Color.WHITE)
			locationView.setText("位置:"+address+str);
		}
		if (support==null) {
			return;
		}
		
	}
	long lastFreshTime =0;
	

	public void initListAdapter(final ObjectAdapter<T> mAdapter,
			final UIController uiFreshController) {
		this.mAdapter = mAdapter;
		this.mListView.setAdapter(mAdapter);
		
		
		this.mListView.setLoadTaskListener(new OnLoadTask2Listener() {

			@Override
			public void reloadTask() {// if empty view occur and click fresh

				if (emptyView != null) {
					emptyView.performClick();
				}
			}
		});
		this.mListView.setRefreshListener(new MyRefresh2Listener() {

			@Override
			public void onRefresh(PullToRefreshBase<XListView> refreshView) {// fresh
																				// top
				
				ILoadingLayout loadingView = BaseList2Fragment.this.mListView.getLoadingLayoutProxy();
				long lastRefreshTime =UtilsTime.getLastRefreshTime(getActivity(),"around");
				String lastRefreshTimeStr	=UtilsTime.convertTimeFormartByMillsec(lastRefreshTime==0?lastRefreshTime:(System.currentTimeMillis()-lastRefreshTime));
				loadingView.setLastUpdatedLabel("上次刷新:"+lastRefreshTimeStr);
				// loadingView.setRefreshingLabel(refreshingLabel)
				// add delay 100 ms later.
				// if (running) {
				// return;
				// }
				// running =true;
				 doFreshFromTop();
				 //saveTime
				 UtilsTime.saveRefreshTime(getActivity(),System.currentTimeMillis(),"around");
			}

			@Override
			public void OnRefreshFooterListener() {
				
			}

			@Override
			public void OnPauseImageLoadWork(boolean pauseWork) {
				// TODO Auto-generated method stub
				mAdapter.setImageFetcherPauseWork(pauseWork);
			}

			@Override
			public void onClickNextPageListener() {
				doNextPage(uiFreshController);
			}

		});

		mListView.setRefreshing(true);
	}

	protected abstract void doFreshFromTop() ;
	
	private void doNextPage(UIController uiFreshController) {
		// load more
		boolean nomore = mListView.getRefreshableView().getChildCount() == mListView
				.getRefreshableView().getCount() ? true : false;
		// TODO Auto-generated method stub
		if (nomore) {
			cancelLoadingProgress(false);
			return;
		}
		// running =true;
		Page page = new Page();
		Log.i(TAG, "totalNum=" + totalNum);
		currentPositon = totalNum <= 0 ? numPerPage : totalNum;
		page.setCurrentPositon(currentPositon);
		totalNum = currentPositon + numPerPage;
		page.setTotalNum(totalNum);
		Log.i(TAG, "currentPositon=" + currentPositon + ";totalNum="
				+ totalNum);

		page.setLoadMore(Page.LOAD_MORE);
		String url = setRequestLoadMoreUrl(page);
		if (url == null) {
			cancelLoadingProgress(false);
			return;
		}
	

		getDataLoader(url, false, page, uiFreshController,needHttpStack?new HttpClientStack(LogInController.getHttpClientInstance()):null);
	
	}
	
	
	protected boolean needHttpStack = false;

	protected abstract void  initViews(View view);
	
	protected Page createInitPage() {
		Page page = new Page();
		page.setLoadMore(Page.CLEAR);
		return page;
	}

	@Override
	public void showToast(String string) {
		cancelLoadingProgress(true);//errors
		super.showToast(string);

	}

	protected void setEmptyView(View emptyView) {
		this.emptyView = emptyView;
	}

	public View getEmptyView() {
		return emptyView;
	}

	protected abstract String setRequestLoadMoreUrl(Page page);

	public void loadData(String requstUrl, boolean cacheSave, Page page,
			UIController uiFreshController, HttpStack httpStack) {
			cancelRequstByTag(TAG);
			cancelRequstByTag(uiFreshController.getTag());
		initData();
		getDataLoader(requstUrl, cacheSave, page, uiFreshController, httpStack);
	}

	public void loadData(String requstUrl, boolean cacheSave, Page page,
			UIController uiFreshController) {
		loadData(requstUrl, cacheSave, page, uiFreshController, null);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	public void clearResource() {
		if (mAdapter != null) {
			mAdapter.recycle();
			mAdapter = null;
		}
	}

	public void releaseResource() {
		// TODO Auto-generated method stub
		clearResource();

	}

	public static String parseParam(String url, String key, String value,
			boolean firstParam) {
		if (TextUtils.isEmpty(url))
			return key + ":" + value;
		if (firstParam) {
			return url = url + key + ":" + value;
		} else {
			return url = url + " AND " + key + ":" + value;
		}

	}

	public static String parseParam(String url, String key, String value) {
		return parseParam(url, key, value, false);
	}

	public void initData() {
		currentPositon = 0;
		totalNum = 0;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (mAdapter != null && isVisible()) {
			mAdapter.onResume();
		}

	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		if (!hidden) {
			if (mAdapter != null) {
				mAdapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		if (mAdapter != null) {
			mAdapter.onPause();
		}
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		if (clear) {
			mListView = null;
			
		}

		// clearResource();
	}

	@Override
	public void onDestroy() {

		// if (banner != null) {
		// banner.onDestroy();
		// }
		// banner = null;
		if (clear) {
			releaseResource();
		}

		super.onDestroy();
	}

	protected boolean clear = true;

	public void setErrorPage(int errorCode,Page page) {

		setPrevPage(page);
	}
	
	public void setPrevPage(Page page) {
		synchronized (this) {
			if (page.getTag()!=null&&page.getTag().equals("login")) {
				cancelLoadingProgress(true);
				return;
			}
			currentPositon = page.getCurrentPositon();
			if (currentPositon > 0 && currentPositon < numPerPage) {
				initData();
				cancelLoadingProgress(true);
			} else if (currentPositon > numPerPage) {// peer back last page
				currentPositon = currentPositon - numPerPage;
				totalNum = totalNum - numPerPage;
			}

		}
	}

	protected void clearAdapter() {
		// TODO Auto-generated method stub
		if (mAdapter != null) {
			mAdapter.clear(true);
		}
	}

	public <E> void refreshList(E content, Page page) {

		// page ,
		List<T> mList = (List<T>) content;
		if (mListView==null) {
//			//some error...
			getActivity().finish();
//			FragmentTransaction ft = getFragmentManager()
//					.beginTransaction();
//			ft.detach(BaseListFragment.this);
//			Fragment.instantiate(getBaseActivity(), BaseListFragment.class.getName(),
//					null);
//			ft.replace(R.id.fragment_tab_content, this, TAG);
//			//ft.attach(BaseListFragment.this);
//			ft.commit();
			return;
		}else {
		}
		if (mListView==null) {
			Log.e(TAG, "listSize =="+(mList!=null?mList.size():0)+"mAdapter=="+(mAdapter!=null)+"mListView=="+(mListView!=null));
		}
	
		if (mListView != null) {

			if (page != null) {
				if (page.isLoadMore() != Page.LOAD_MORE
						&& page.isLoadMore() != Page.NO_MORE) {
					mAdapter.clear();
				}
			} else {
				mAdapter.clear();
			}

			mAdapter.addObjectList(mList);
			if (isVisible()) {
				mAdapter.notifydataChanged(page);
			} else {
				mAdapter.setCurrentPage(page);
			}
			if (mAdapter.getCount() == 0) {
				if (emptyView != null) {
					emptyView.setVisibility(View.VISIBLE);
				}
			} else {
				if (emptyView != null) {
					emptyView.setVisibility(View.GONE);
				}
			}

			if ((mList.size()<numPerPage)
					|| (page != null && page.isLoadMore() == Page.NO_MORE)) {
				mListView.onRefreshComplete(false);
			} else {
				mListView.onRefreshComplete(true);
			}
		}
		
		if (mList.size() == 0) {
			cancelLoadingProgress(false);

			if (TAG != null && TAG.equals(ConstValues.SEARCH)) {
				if (mAdapter.getCount() == 0) {
					showToast("未搜索到信息！");
				} else {
					showToast("没有更多！");
				}

			} else {
				if (mAdapter.getCount() == 0) {
					showToast("没有数据！");
				} else {
					showToast("没有更多！");
				}
			}
		}
	}

	protected void cancelLoadingProgress(boolean loadMore) {
		
		if (mListView != null) {
			mListView.onRefreshComplete(loadMore);
		}
	}

}
