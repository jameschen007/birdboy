package com.jameschen.birdboy.base;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpConnection;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.protocol.HTTP;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.Volley;
import com.jameschen.birdboy.common.LogInController;
import com.jameschen.birdboy.common.LogInController.OnLogOnListener;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.environment.Config.HttpConfig;
import com.jameschen.birdboy.environment.Config.HttpConnect;
import com.jameschen.birdboy.environment.ConstValues;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.Cache;
import com.jameschen.birdboy.environment.ConstValues.CategoryInfo.SoftwareInfo;
import com.jameschen.birdboy.environment.ConstValues.Setting;
import com.jameschen.birdboy.handler.FreshHandler;
import com.jameschen.birdboy.handler.FreshHandler.DoRetryListener;
import com.jameschen.birdboy.location.LocationUtil;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.QueryBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;
import com.jameschen.birdboy.model.entity.ReqParam;
import com.jameschen.birdboy.network.BirdBoyStringRequest;
import com.jameschen.birdboy.ui.AccountFragment;
import com.jameschen.birdboy.ui.CoachListFragment;
import com.jameschen.birdboy.ui.SportEventListFragment;
import com.jameschen.birdboy.ui.StadiumListFragment;
import com.jameschen.birdboy.utils.HttpUtil;
import com.jameschen.birdboy.utils.Log;
import com.jameschen.birdboy.utils.ParseJsonData;
import com.jameschen.birdboy.utils.Util;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import android.R.bool;
import android.R.integer;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jameschen.birdboy.MainActivity;
import com.jameschen.birdboy.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.umeng.analytics.MobclickAgent;

public abstract class BaseActivity extends SherlockFragmentActivity implements
		OnClickListener {
	private static final int MY_SOCKET_TIMEOUT_MS = 10*1000;
	public static int THEME = com.actionbarsherlock.R.style.Theme_Sherlock_Light;
	public String TAG = "BirdBoy";
	private List<UIController> uiControllerList;
	private FreshHandler freshHandler;
	
	
	//注册广播 监听网络中断，然后在get post 请求 判断状态 时候返回,如果是listview 通知
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setTheme(THEME);
		super.onCreate(savedInstanceState);
//		setBehindContentView(R.layout.fragment_main);
//		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		TAG = getLocalClassName();
		TopBarInit(getSupportActionBar());
	}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	MobclickAgent.onResume(this);

}
@Override
protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	MobclickAgent.onPause(this);
}
	@Override
	public void startActivity(Intent intent) {
		// TODO Auto-generated method stub
		super.startActivity(intent);

		overridePendingTransition(R.anim.enter_right_anim,
				R.anim.exit_right_anim);

	}

	
	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		// TODO Auto-generated method stub
		super.startActivityForResult(intent, requestCode);
		overridePendingTransition(R.anim.enter_right_anim,
				R.anim.exit_right_anim);
	}

	public void TopBarInit(ActionBar actionBar) {
		actionBar.setBackgroundDrawable(new BitmapDrawable(getResources(),
				BitmapFactory.decodeResource(getResources(),
						R.drawable.top_title_bg)));
		actionBar.setCustomView(R.layout.top_view);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		ViewGroup customView = (ViewGroup) actionBar.getCustomView();

		titleTv = (TextView) customView.findViewById(R.id.top_title);
		(backBtn=(ImageView) customView.findViewById(R.id.btn_back)).setOnClickListener(this);
		(nextBtn = (Button) customView.findViewById(R.id.btn_next)).setOnClickListener(this);
		(nextImgBtn = (ImageView) customView.findViewById(R.id.img_btn_next)).setOnClickListener(this);
		//default set right btn not visiable
		setTopBarRightBtnVisiable(View.GONE);
	}

	private TextView titleTv;
	private ImageView backBtn,nextImgBtn;
	private Button nextBtn;
	
	protected void  setTopBarRightBtnListener(OnClickListener nextBtnOnClickListener) {
		setTopBarRightBtnListener("下一步", nextBtnOnClickListener);
	}

	
	protected void  setTopBarRightBtnListener(int resourceId,OnClickListener nextBtnOnClickListener) {
		setTopBarRightBtnVisiable(View.GONE);
		nextImgBtn.setImageResource(resourceId);
		nextImgBtn.setVisibility(View.VISIBLE);
		nextImgBtn.setOnClickListener(nextBtnOnClickListener);
	}
	
	
	protected void setTopButtonRightText(String text) {
		nextBtn.setText(text);
	}
	
	
	protected void setTopButtonRightRes(int drawableId) {
		if (drawableId==0) {
			nextBtn.setCompoundDrawables(null, null, null, null);
		}else {
			Drawable drawable = getResources().getDrawable(drawableId);
			drawable.setBounds(0, 0, drawable.getMinimumWidth(),
					drawable.getMinimumHeight());// 必须设置图片大小，否则不显示
			nextBtn.setCompoundDrawablePadding(0);
			nextBtn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
			
			nextBtn.setCompoundDrawables(null, null, drawable, null);
		}
		
	}
	
	protected void  setTopBarRightBtnListener(String text,OnClickListener nextBtnOnClickListener) {
		if (!TextUtils.isEmpty(text)) {
			nextBtn.setText(text);
			if (text.equals("下一步")) {
				setTopBarRightBtnListener(R.drawable.next_step, nextBtnOnClickListener);
				return;	
			}
		}	
	
		
		setTopBarRightBtnVisiable(View.VISIBLE);
		nextBtn.setOnClickListener(nextBtnOnClickListener);
	}
	protected void  setTopBarLeftBtnVisiable(int visibility) {
		backBtn.setVisibility(visibility);
	}
	protected void  setTopBarRightBtnVisiable(int visibility) {
		nextBtn.setVisibility(visibility);
		if (visibility==View.GONE) {
			nextImgBtn.setVisibility(visibility);	
		}
	}
	protected void  setTopBarBtnVisiable(int visibility) {
		nextBtn.setVisibility(visibility);
		setTopBarRightBtnVisiable(visibility);
	}
	
	
	protected void setTitle(String title) {
		titleTv.setBackgroundDrawable(null);
		titleTv.setText(title);
	}

	
	protected boolean resultOk=false;
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back: {
			if (resultOk) {
				setResult(RESULT_OK);
			}
			//finish();
			onBackPressed();
		}
			break;

		default:
			break;
		}
	}

	public boolean saveJsonCache(String cacheName, String jsonContent) {
		SharedPreferences prefs = getApplicationContext().getSharedPreferences(
				Cache.SharedName, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(cacheName, jsonContent);
		return editor.commit();
	}

	public String loadJsonCacheData(String cacheName) {
		SharedPreferences prefs = getApplicationContext().getSharedPreferences(
				Cache.SharedName, 0);
		String jsonContent = prefs.getString(cacheName, null);
		return jsonContent;
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
		overridePendingTransition(R.anim.exit_left_anim, R.anim.enter_left_anim);

	}

	Object loker = new Object();

	
  
	
	final public <E> void refreshUI(E content, Page page, String Tag ,DoRetryListener retry) {
		synchronized (loker) {
			if (uiControllerList!=null) {
				List<UIController> refreshControllers = new ArrayList<UIController>(uiControllerList);
				for (UIController uiController : refreshControllers) {
					if (uiController != null && uiController.getTag().equals(Tag)) {

						/**
						 * return failed show reason
						 * */
						if (content instanceof WebBirdboyJsonResponseContent) {
							WebBirdboyJsonResponseContent<E> mBirdboyJsonResponseContent = (WebBirdboyJsonResponseContent<E>) content;
							String error=mBirdboyJsonResponseContent.getInfo();
							showToast(error);
							if (mBirdboyJsonResponseContent.getCode()==-1||error!=null&&("请先登录".equals(error))) {//need logon>>>>	
									
									Page loginPage = new Page();
									loginPage.setTag("login");
									if (isFinishing()) {
										return;
									}
									uiController.responseNetWorkError(-1, loginPage);
									LogInController.setHttpClientNull(true);
									Util.go2LogInDialog(this,new OnLogOnListener() {
										
										@Override
										public void logon() {
											//..
											
										}
									});
							}
						
						} else {
							Log.i(TAG, "uiController :" + Tag);
							if (!isFinishing()) {
								uiController.refreshUI(content, page);
							}else {
								//cancel.
							}
						}
						cancelProgressDialog();
						
						break;
					}
				}
			}

		}

	}

	protected ProgressDialog mAlertDialog;

	protected ProgressDialog showProgressDialog(int pTitelResID, String pMessage) {
		String title = getResources().getString(pTitelResID);
		return showProgressDialog(title, pMessage, null);
	}

	public synchronized ProgressDialog createProgressDialog(String pTitle,
			String pMessage,
			DialogInterface.OnCancelListener pCancelClickListener) {
		
		ProgressDialog mDialog = ProgressDialog.show(this, pTitle, pMessage, true, true);
		mDialog.setCancelable(true);
		mDialog.setOnCancelListener(pCancelClickListener);

		return (ProgressDialog) mDialog;
	}
	
	public synchronized ProgressDialog showProgressDialog(String pTitle,
			String pMessage,
			DialogInterface.OnCancelListener pCancelClickListener) {
		if (mAlertDialog != null) {
			mAlertDialog.setTitle(pTitle);
			mAlertDialog.setMessage(pMessage);
			return mAlertDialog;
		}
		mAlertDialog = ProgressDialog.show(this, pTitle, pMessage, true, true);
		mAlertDialog.setCancelable(true);
		mAlertDialog.setOnCancelListener(pCancelClickListener);

		return (ProgressDialog) mAlertDialog;
	}

	public synchronized void cancelProgressDialog() {
		// TODO Auto-generated method stub
		if (mAlertDialog != null) {
			if (mAlertDialog.isShowing()) {
				mAlertDialog.cancel();
			}
			mAlertDialog = null;
		}
	}

	private Toast mWarningToast, mInfoToast;

	public void showToast(String msg) {
		if (msg == null) {
			return;
		}
		if (Util.hasIcs()) {
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT)
					.show();
		} else {
			if (mInfoToast == null) {
				mInfoToast = Toast.makeText(getApplicationContext(), "",
						Toast.LENGTH_SHORT);
			}
			mInfoToast.cancel();
			mInfoToast.setText(msg);
			mInfoToast.show();
		}
	}

	public void setCityId(int id) {
		cityId = id;
		SharedPreferences prefs = getApplicationContext().getSharedPreferences(
				Setting.SharedName, 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(Setting.cityId, cityId);
		editor.commit();
	}
	
	public static int cityId=LocationUtil.cityIds[0];//default show chengdu
	
	public Map<String, String> getPublicParamRequstMap() {
		Map<String, String> headers = new HashMap<String, String>();
		// headers.put("Charset", "UTF-8");
		// headers.put("Content-Type", "application/x-javascript");
		// headers.put("Accept-Encoding", "gzip,deflate");
		// add screen w & h later..
		headers.put("appAgent", "android");
		headers.put("cityId", cityId+"");
		
		if (SoftwareInfo.SOFT_VERSION_CODE == -1) {
			SoftwareInfo.SOFT_VERSION_CODE = Util
					.version(getApplicationContext());
		}
		headers.put("appVersion", SoftwareInfo.SOFT_VERSION_CODE + "");

		return headers;
	}

	// close input method
	public void closeInputMethod() {
		// hide input method and emotion mothod
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		// switch input status
		if (inputMethodManager.isActive()) {
			try {
				inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
						.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			} catch (Exception e) {
				inputMethodManager.toggleSoftInput(
						InputMethodManager.SHOW_IMPLICIT,
						InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}
	}

	public void getDataLoader(String requstUrl, boolean cacheSave, Page page,
			UIController uiController) {
		getDataLoader(requstUrl, cacheSave, page, uiController, null);
	}

	private long lastFrshTime =0;
	public void getDataLoader(String requstUrl, boolean cacheSave, Page page,
			UIController uiController, HttpStack httpStack) {
		if (TextUtils.isEmpty(requstUrl)) {
			throw new IllegalArgumentException(
					"requstUrl is null ,pls load initDataLoaderFromNativeCache first, and argument for url. ");
		}
	
		if (page==null) {
			page=new Page();
		}
		
		ReqParam reqParam = new ReqParam();
		reqParam.setCacheSave(cacheSave);
		reqParam.setHasHttpStack(httpStack!=null);
		reqParam.setMethod(Request.Method.GET);
		reqParam.setUrl(requstUrl);
		page.setReqParam(reqParam );
		getJsonVolley(null, Request.Method.GET, requstUrl, cacheSave, page,
				uiController, httpStack);
		
	}

	private boolean readCache(String url,String name, UIController uiFreshController ,Page page,long time) {
		String content =loadJsonCacheData(name);
		if (content==null) {
			return false;
		}
		if (url.startsWith(HttpConfig.HTTP_BASE_URL + "/solr/")) {
			readJsonDataAndFreshSearchList(content,
					uiFreshController, page);
		} else {
			readJsonDataAndFresh(content, uiFreshController,
					page);
		}
		return true;
	}

	public void postDataLoader(Map<String, String> map, String requstUrl,
			UIController uiController, HttpStack httpStack) {
		
		postDataLoader(map, requstUrl, uiController, false, null, httpStack);
	}
	public void postDataLoader(Map<String, String> map, String requstUrl,
			UIController uiController, boolean saveCache, Page page, HttpStack httpStack) {
		if (TextUtils.isEmpty(requstUrl)) {
			throw new IllegalArgumentException(
					"requstUrl is null ,pls load initDataLoaderFromNativeCache first, and argument for url. ");
		}
		
		if (page==null) {
			page=new Page();
		}
		
		ReqParam reqParam = new ReqParam();
		reqParam.setCacheSave(saveCache);
		reqParam.setHasHttpStack(httpStack!=null);
		reqParam.setMethod(Request.Method.POST);
		reqParam.setUrl(requstUrl);
		reqParam.setReqMap(map);
		page.setReqParam(reqParam );
		getJsonVolley(map, Request.Method.POST, requstUrl, saveCache, page,
				uiController, httpStack);
	}
	public enum LoadMode {
		SAVE_CACHE, NO_SAVE_CACHE, NONE
	}

	protected ImageLoader imageLoader;

	public ImageLoader getImageLoader() {
		if (imageLoader == null) {
			imageLoader = ImageLoader.getInstance();
		}
		return imageLoader;
	}

	public void loadImage(String url,ImageView imageView,DisplayImageOptions options ) {
		loadImage(url, imageView, options, null);
	}
	
	protected void loadImage(String url,ImageView imageView,DisplayImageOptions options,ImageLoadingListener loadImgLisnter) {
		if (getImageLoader()!=null) {
			String urls[]=url.split(HttpConfig.HTTP_BASE_URL);
			if (urls.length>=2&&urls[1].startsWith("http")) {
				url = urls[1];
			}
			imageLoader.displayImage(url, imageView, options,loadImgLisnter);
		}
	}
	
	public void initDataLoaderFromNativeCache(String url, LoadMode loadMode,
			UIController uiFreshController) {
		initDataLoaderFromNativeCache(url, loadMode, uiFreshController, 0);
	}
	
	
	public void initDataLoaderFromNativeCache(String url, LoadMode loadMode,
			UIController uiFreshController,long cacheTime) {
		registerUIController(uiFreshController);
		if (url == null || loadMode == LoadMode.NONE) {
			return;
		}
		if (imageLoader == null) {
			imageLoader = ImageLoader.getInstance();
		}

		String tag = uiFreshController.getTag();
		if (url.contains("SearchArea")) {
			tag = url;
		}
		boolean hasData = readCache(url, tag, uiFreshController,new Page(),cacheTime);

		long lastReadTime=0;
		long daltaTime=0;
		if (hasData&&cacheTime>0) {	
			if ((tag)!=null) {
				//coach 
				//stadium
				//sport.
				if (tag.equals("sport_Area")) {
					lastReadTime=SportEventListFragment.areaGetTime;
					daltaTime=System.currentTimeMillis()-lastReadTime;
					SportEventListFragment.areaGetTime=System.currentTimeMillis();
				}else if (tag.equals("staduim_Area")) {
					lastReadTime=StadiumListFragment.areaGetTime;
					daltaTime=System.currentTimeMillis()-lastReadTime;
					StadiumListFragment.areaGetTime=System.currentTimeMillis();
				}else if (tag.equals("coach_Area")) {
					lastReadTime=CoachListFragment.areaGetTime;
					daltaTime=System.currentTimeMillis()-lastReadTime;
					CoachListFragment.areaGetTime=System.currentTimeMillis();
				}
				
			}
			
			
			if (daltaTime<cacheTime) {
				return;
			}
		}
		// firstTime always load
		switch (loadMode) {
		case SAVE_CACHE:
			getDataLoader(url, true, null, uiFreshController);
			break;
		case NO_SAVE_CACHE:
			getDataLoader(url, false, null, uiFreshController);
			break;
		case NONE:
			break;
		}

	}

	
	void unregisterUIController(UIController uiController){
		if (uiControllerList==null) {
			return;
		}
		if (uiControllerList.contains(uiController)) {
			uiControllerList.remove(uiController);
		}
	}
	
	private void registerUIController(UIController uiFreshController) {
		// TODO Auto-generated method stub
		if (freshHandler == null) {
			freshHandler = new FreshHandler(this);
		}

		if (uiControllerList == null) {
			uiControllerList = new ArrayList<UIController>();
		}
		if (!uiControllerList.contains(uiFreshController)) {

			uiControllerList.add(uiFreshController);
		}else {//may do different things so best way is remove and add again?
			//dead code
		}
		curUIRegTag =uiFreshController.getTag();
	}

	private  String  curUIRegTag;
	
	public String getCurrentRegisterUITag() {
		// TODO Auto-generated method stub
		return curUIRegTag;
	}
	
	
	private void unregisterUIController() {
		// TODO Auto-generated method stub
		if (freshHandler != null) {
			freshHandler.removeCallbacksAndMessages(null);
			freshHandler = null;
		}

		if (uiControllerList != null) {
			uiControllerList.clear();
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		RequestQueue queue = Volley.newRequestQueue(this);
		if (queue != null) {
			if (uiControllerList != null) {
				for (UIController uiController : uiControllerList) {
					if (uiController.getTag()!=null) {
						queue.cancelAll(uiController.getTag());
					}
				}
			}
			queue.cancelAll(TAG);
		}
		unregisterUIController();

	}
	
	protected int readJsonData(String json,UIController uiController,Page page){
		registerUIController(uiController);
		return readJsonDataAndFresh(json, uiController, page);
		
	}
	
	private <T> int readJsonDataAndFresh(String json,
			UIController uiController, Page page) {
		if (TextUtils.isEmpty(json)) {
			return -1;
		}
		Type type = uiController.getEntityType();
		if (type == null) {
			throw new IllegalArgumentException("fuck you ,can't get the class type ");
		}
		WebBirdboyJsonResponseContent<T> webBirdboyJsonResponseContent = new WebBirdboyJsonResponseContent<T>();

		try {
			webBirdboyJsonResponseContent = ParseJsonData.getWebData(json, type,
					webBirdboyJsonResponseContent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			cancelProgressDialog();
			return -1;
		}
		int status =-1;
		if (freshHandler != null) {
			Message msg = new Message();
			if (webBirdboyJsonResponseContent.isSuccess()) {

				postHandlerSendMsg(FreshHandler.MSG_OBTAION_CONTENT,
						webBirdboyJsonResponseContent.getResult(),
						uiController.getTag(), page);
				status=0;
			} else {
				status = -2;
				postHandlerSendMsg(FreshHandler.MSG_OBTAION_CONTENT_FAILED,
						webBirdboyJsonResponseContent, uiController.getTag(),
						page);
			}

			freshHandler.sendMessage(msg);
		}

		return status;
	}

	// for search List
	private <T> int readJsonDataAndFreshSearchList(String json,
			UIController uiController, Page page) {
		if (TextUtils.isEmpty(json)) {
			return -1;
		}
		Type type = uiController.getEntityType();
		if (type == null) {
			throw new IllegalArgumentException("can't get the class type ");
		}
		List<T> mList = new ArrayList<T>();
		QueryBirdboyJsonResponseContent<T> jsonResponseContent = new QueryBirdboyJsonResponseContent<T>();

		try {
			jsonResponseContent = ParseJsonData.getQueryData(json, type,
					jsonResponseContent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
			cancelProgressDialog();
			return -3;
		}
		
		if (jsonResponseContent.getResponse()==null) {
			cancelProgressDialog();
			return -3;
		}
		
		mList.addAll(jsonResponseContent.getResponse().getDocs());

		Log.i(TAG, "get  list size===" + mList.size());
		if (page != null) {
			page.setCurrentPositon(jsonResponseContent.getResponse().getStart());
			page.setTotalNum(jsonResponseContent.getResponse().getNumFound());
			Log.i(TAG, "position=" + page.getCurrentPositon() + ";totalnum="
					+ page.getTotalNum());
		}
		Log.i(uiController.getTag(), mList.size() + "");
		postHandlerSendMsg(FreshHandler.MSG_OBTAION_CONTENT, mList,
				uiController.getTag(), page);

		if (mList.size() > 0) {
			return 0;
		}
		return -3;
	}

	private void postHandlerSendMsg(int what, Object content, String tagStr,
			Page page) {
		if (freshHandler != null) {
			Message msg = new Message();
			msg.what = what;
			msg.obj = content;
			Bundle tagArg = new Bundle();
			tagArg.putString(FreshHandler.TAG, tagStr);
			if (page != null) {
				tagArg.putParcelable("page", page);
			}
			msg.setData(tagArg);
			freshHandler.sendMessage(msg);
		}
	}

	private void getJsonVolley(Map<String, String> requestMap, int method,
			final String requstUrl, final boolean saveData, final Page page,
			final UIController uiController, final HttpStack httpStack) {

		registerUIController(uiController);
		
//		//use cache data
//		if (method == Request.Method.GET) {
//			//hasp map save data  for url  json  default 1 hour, not contains activity,
//			if (saveCache) {
//				if (page!=null&&ConstValues.DISTANCE.equals(page.getTag())) {//not read
//					
//				}else {
//					//time>1hour
//					if (System.currentTimeMillis()-lastFrshTime<3600*1000) {//1 hour
//						boolean succ = readCache(requstUrl,requstUrl,uiController,page);
//						if (succ) {
//							lastFrshTime = System.currentTimeMillis();
//							return;
//						}
//					}
//				}
//			}
//		}
//		
//		if (saveCache) {
//			if (page!=null&&ConstValues.DISTANCE.equals(page.getTag())) {//not save
//				
//			}else {
//				if (System.currentTimeMillis()-lastFrshTime<3600*1000) {
//				saveJsonCache(requstUrl,saveResponseData);}
//			}
//		}
		
		if (!MainActivity.networkAvaible) {
			//double check...
			MainActivity.networkAvaible=HttpUtil.hasNetwork(this);
			showToast("当前网络不可用!");
			uiController.responseNetWorkError(-1, page);
			cancelProgressDialog();
			return;
		}
		
		lastFrshTime = System.currentTimeMillis();
		
		RequestQueue queue ;
		if (httpStack!=null) {
			 queue = Volley.newRequestQueue(this, httpStack);
		}else {
			 queue = Volley.newRequestQueue(this);
		}
	
		BirdBoyStringRequest stringRequest = new BirdBoyStringRequest(
				requestMap, method, requstUrl, new Response.Listener<String>() {
					

					@Override
					public void onResponse(String response) {
						final String responseData = response;
						new Thread(new Runnable() {
							@Override
							public void run() {
								int status = -1;
								String saveResponseData = responseData;
								if (requstUrl
										.startsWith(HttpConfig.HTTP_BASE_URL
												+ "/solr/")) {

									Log.i(TAG, "onResponse:*****"
											+ responseData);
									status = readJsonDataAndFreshSearchList(
											responseData, uiController, page);

								} else {// for encode bug
									try {
										saveResponseData = new String(
												responseData
														.getBytes("ISO-8859-1"),
												"utf-8");
										Log.i(TAG, "onResponse:*****"
												+ saveResponseData);
										status = readJsonDataAndFresh(
												saveResponseData, uiController,
												page);
									} catch (UnsupportedEncodingException e) {
										e.printStackTrace();
									}
								}
							
								if (saveData && status == 0) {//save by url is keep different from other.
									if (requstUrl.contains("SearchArea")) {
										saveJsonCache(requstUrl,
												saveResponseData);
									}else {
										saveJsonCache(uiController.getTag(),
												saveResponseData);
									}
									
								}
								
							

							}
						}).start();
						// if(dialog.isShowing()&&dialog!=null){
						// dialog.dismiss();
						// }
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						error.printStackTrace();
//						if (error instanceof NoConnectionError) {
//							uiController.responseNetWorkError(
//									HttpStatus.SC_NO_CONTENT, page);
//						} else if (error instanceof TimeoutError) {
//							uiController.responseNetWorkError(
//									HttpStatus.SC_REQUEST_TIMEOUT, page);
//						} else {// server error
//
//						}
						if (isFinishing()) {
							return;
						}
						uiController.responseNetWorkError(
								HttpStatus.SC_REQUEST_TIMEOUT, page);
						cancelProgressDialog();
					}
				});
		stringRequest.setTag(uiController.getTag());
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS, 3, 
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));                
		queue.add(stringRequest);

	}
}
