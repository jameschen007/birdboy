package com.jameschen.birdboy.base;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.Volley;
import com.jameschen.birdboy.R;
import com.jameschen.birdboy.base.BaseActivity.LoadMode;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.utils.Log;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**This is the baseFragment is for root frament ,which childFragment must extends it
 * 
 * @author jameschen
 *
 */
public abstract class BaseFragment extends Fragment{
	public  String TAG;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		TAG =getTag();
	}
	
	protected BaseActivity getBaseActivity() {
		return (BaseActivity) getActivity();
	}
 	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	Log.i(TAG, "onCreateView");
		return super.onCreateView(inflater, container, savedInstanceState);

	}
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(TAG, "onActivityCreatedFagment");
		super.onActivityCreated(savedInstanceState);
	}
	 
	@Override
	public void onResume() {
		Log.i(TAG, "onResumeFagment");
		super.onResume();
	}
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		Log.i(TAG, "onDestroyView");
		super.onDestroyView();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		cancelRequstByTag(TAG);
		Log.i(TAG, "onDestroyFagment");
	}
	public void  cancelRequstByTag(String tag){
		if (tag==null) {
			return;
		}
		RequestQueue queue = Volley.newRequestQueue(getBaseActivity());
		if (queue!=null) {
				queue.cancelAll(tag);
		}
	}
	
	public void showToast(String string) {
		if (getActivity() == null) {
			return ;
		}
			((BaseActivity) getActivity()).showToast(string);
	}

	
	public void initDataLoaderFromNativeCache(String url,LoadMode loadMode,UIController uiFreshController){
				initDataLoaderFromNativeCache(url, loadMode, uiFreshController, 0);
	}
	
	public void initDataLoaderFromNativeCache(String url,LoadMode loadMode,UIController uiFreshController,long cacheTime){
		if (getActivity() == null) {
			return ;
		}
		 ((BaseActivity) getActivity()).initDataLoaderFromNativeCache(url, loadMode, uiFreshController,cacheTime);
	}
	
	
	public void postDataLoader(Map<String, String> map,String requstUrl,boolean cacheSave,Page page,UIController uiFreshController,HttpStack httpStack) {
		if (getActivity() == null) {
			return ;
		}
		 ((BaseActivity) getActivity()).postDataLoader(map, requstUrl, uiFreshController, httpStack);
	} 
	
	public void postDataLoader(Map<String, String> map,String requstUrl,boolean cacheSave,Page page,UIController uiFreshController) {
				postDataLoader(map,requstUrl, cacheSave, page, uiFreshController, null);
	} 
	
	
	public void getDataLoader(String requstUrl,boolean cacheSave,Page page,UIController uiFreshController,HttpStack httpStack) {
		if (getActivity() == null) {
			return ;
		}
		 ((BaseActivity) getActivity()).getDataLoader(requstUrl, cacheSave, page,uiFreshController,httpStack);
	} 
	
	public void getDataLoader(String requstUrl,boolean cacheSave,Page page,UIController uiFreshController) {
				getDataLoader(requstUrl, cacheSave, page, uiFreshController, null);
	} 
	
	
	public String getCurrentRegisterUITag() {
		if (getActivity() == null) {
			return null;
		}
		return ((BaseActivity) getActivity()).getCurrentRegisterUITag();
	} 
	
	public synchronized boolean saveJsonDataStr(String cacheName, String jsonContent) {
		if (getActivity() == null) {
			return false;
		}
		return ((BaseActivity) getActivity()).saveJsonCache(cacheName, jsonContent);
		
	}
	public synchronized String loadJsonCacheData(String cacheName) {
		if (getActivity() == null) {
			return null;
		}
		return ((BaseActivity) getActivity()).loadJsonCacheData(cacheName);
	}
	
	
	public void setCityId(int cityId) {
		if (getActivity() == null) {
			return ;
		}
		 ((BaseActivity) getActivity()).setCityId(cityId);
	
	}

	
	public Map<String, String> getPublicParamRequstMap() {
		if (getActivity() == null) {
			return null;
		}
		return ((BaseActivity) getActivity()).getPublicParamRequstMap();
	
	}

	public void closeInputMethod() {
		if (getActivity() == null) {
			return ;
		}
		 ((BaseActivity) getActivity()).closeInputMethod();
	}
	@Override
	public void startActivity(Intent intent) {
		// TODO Auto-generated method stub
		super.startActivity(intent);
		getActivity().overridePendingTransition(R.anim.enter_right_anim,
				R.anim.exit_right_anim);
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		// TODO Auto-generated method stub
		super.startActivityForResult(intent, requestCode);
		getActivity().overridePendingTransition(R.anim.enter_right_anim,
				R.anim.exit_right_anim);
	}

	
	
}
