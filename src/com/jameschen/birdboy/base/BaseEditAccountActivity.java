package com.jameschen.birdboy.base;

import java.lang.reflect.Type;

import android.os.Bundle;

import com.google.gson.reflect.TypeToken;
import com.jameschen.birdboy.common.UIController;
import com.jameschen.birdboy.model.Page;
import com.jameschen.birdboy.model.WebBirdboyJsonResponseContent;

public abstract class  BaseEditAccountActivity extends BaseActivity{

	
	//commit  every edit, if ok instead of the account.  setResult ok  then , refresh,
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	
	}
	
	
	protected abstract void commitDone();
	protected abstract void editOk();
	
	protected UIController updateAccountController = new UIController() {
		
		@Override
		public void responseNetWorkError(int errorCode, Page page) {
			showToast("修改失败");
		}
		
		@Override
		public <E> void refreshUI(E content, Page page) {
			// TODO Auto-generated method stub
			editOk();
			setResult(RESULT_OK);
			finish();
		}
		
		@Override
		public String getTag() {
			// TODO Auto-generated method stub
			return TAG;
		}
		
		@Override
		public Type getEntityType() {
			// TODO Auto-generated method stub
			return new TypeToken<WebBirdboyJsonResponseContent<Object> >(){}.getType();
		}
	};
	
}
